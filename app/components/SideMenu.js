import React, { Component } from 'react';
import styles from './SideMenu.css';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import Menu from 'material-ui/Menu';
import ContentCopy from 'material-ui/svg-icons/content/content-copy';
import Note from 'material-ui/svg-icons/av/note';
import LibraryAdd from 'material-ui/svg-icons/av/library-add';
import Folder from 'material-ui/svg-icons/file/folder';
import Dexie from 'dexie';
import Divider from 'material-ui/Divider';
//import CosmicIcon from './CosmicIcon';

import { EditorState, ContentState, convertToRaw } from 'draft-js';

export default class SideMenu extends Component
{

    constructor(props){
        super(props);

        this.state = {
            menu_open: true,
            pages: [],
            notebooks: []
        };

        this.addPage = this.addPage.bind(this);
        this.showPages = this.showPages.bind(this);
        this.showPage = this.showPage.bind(this);
        this.db = props.db.bind(this);
    }

    /**
     * Update Component Pages and Notebook States
     */
    componentWillReceiveProps(nextProps){
        var notebooks = [...this.state.notebooks, ...[nextProps.notebooks]];
        var pages = [...this.state.pages, ...[nextProps.pages]];
        this.setState({
            notebooks: nextProps.notebooks,
            pages: nextProps.pages
        });
        console.log(nextProps.notebooks);
        console.log(nextProps.pages);
    }

    /**
     * Add Page To a Notebook by ID
     */
    addPage( notebook_id ){
        console.log(notebook_id);
        var editorState = EditorState.createEmpty();
        var raw = convertToRaw( editorState.getCurrentContent() );
        this.db('ADD_PAGE', notebook_id, 'Test Pages', raw);
    }

    /**
     * Show a specified page by ID
     */
    showPage( page_id ){
        console.log('page id: ' + page_id);
        this.db('GET_PAGE', page_id);
    }

    /**
     * OnClick of Notebook display the pages
     * that belong to the specified notebook
     */
    showPages( notebook_id ){
        let event = new Event('save');
        window.dispatchEvent( event );
        this.db('GET_PAGES', notebook_id);
    }

    render(){
        return(
            <Drawer open={ this.state.menu_open } zDepth={ 1 } >

                <Menu desktop={ true } width={ 256 } className={ styles.brand }>
                    <MenuItem className={ styles.brand } primaryText="">
                        <img src="./utils/CosmicNotesLogo.svg" className={ styles.logo } /> 
                    </MenuItem>
                </Menu>

                <Divider />
                <Menu desktop={ true } width={ 256 }>

                    <MenuItem leftIcon={ <Folder /> } rightIcon={ <LibraryAdd /> } primaryText="" />

                    { this.state.notebooks.map(( value, key ) =>
                            <MenuItem leftIcon={ <Folder /> } rightIcon={ <LibraryAdd onClick={ this.addPage.bind( this, value.id ) } /> } onClick={ this.showPages.bind( this, value.id ) } key={ key } primaryText={ value.name } /> ) }
                </Menu>
                            <Divider />
                <Menu desktop={ true } width={ 256 }>
                    { this.state.pages.map(( value, key) =>
                            <MenuItem onClick={ this.showPage.bind( this, value.id ) } rightIcon={ <ContentCopy /> } key={ key } primaryText={ value.title } /> ) }
                </Menu>
            </Drawer>
        );
    }
}
