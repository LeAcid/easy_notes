import React, { Component } from 'react';
import styles from './Note.css';
import { ContentState, EditorState, RichUtils, convertToRaw, convertFromRaw } from 'draft-js';
import Editor from 'draft-js-plugins-editor';
import DraftPasteProcessor from 'draft-js/lib/DraftPasteProcessor';
import TextField from 'material-ui/TextField';

/**
 * Import Icons
 */
import FormatListBulleted from 'material-ui/svg-icons/editor/format-list-bulleted';
import FormatListNumbered from 'material-ui/svg-icons/editor/format-list-numbered';
import FormatBold from 'material-ui/svg-icons/editor/format-bold';
import FormatUnderlined from 'material-ui/svg-icons/editor/format-underlined';
import FormatItalic from 'material-ui/svg-icons/editor/format-italic';
import StrikethroughS from 'material-ui/svg-icons/editor/format-strikethrough';

const txtarea = {
    fontSize: '1.6em',
    margin: '1rem'
}

export default class Note extends Component
{

    /**
     * Notes Editor
     * Built with Draft.js
     * Using components from draft-js-plugins
     */

    constructor(props){
        super(props);

        var editorState = props.content;

        this.state = {
            title: 'Welcome To Easy Notes',
            editorState: editorState,
            page: props.page
        };

        this.onChange = this.onChange.bind(this);
        this.handleKeyCommand = this.handleKeyCommand.bind(this);
        this.setBlockType = this.setBlockType.bind(this);
        this.inlineStyle = this.inlineStyle.bind(this);
        this.titleChange = this.titleChange.bind(this);
        this.save = this.save.bind(this);
        this.stop = this.stop.bind(this);
        this.db = props.db.bind(this);
        this.resetEditor = props.resetEditor.bind(this);
        this.deletePage = this.deletePage.bind(this);
    }

    /**
     * Listen for and stop all mousedown
     * to not lose focus on the editor
     * when selecting options
     */
    componentDidMount(){
        let elements = document.getElementsByTagName('button');
        for(var count = 0; count < elements.length; count++){
            elements[count].addEventListener("mousedown", this.stop, false);
        }

        /**
         * Add Save Event Listener
         * To Allow Other Components to Save
         * The Editor State
         */
        let self = this;
        window.addEventListener('save', function(e){
        let save = convertToRaw( self.state.editorState.getCurrentContent() );
            if(self.state.page.length !== 0){
                let page_id = self.state.page[0].id;
                self.db('SAVE', page_id, self.state.title, save);
                self.db('GET_PAGE', page_id);
            }
        }, true);
    }

    /**
     * Stop Mousedown Event From 
     * Taking Focus off of the Editor
     */
    stop(e){
        e.preventDefault();
    }

    /**
     * Save Note
     */
    save(){
        let self = this;
        let save = convertToRaw( self.state.editorState.getCurrentContent() );
            if( self.state.page != [] ){
                let page_id = self.state.page[0].id;
                let notebook_id = self.state.page[0].notebook_id;
                self.db('SAVE', page_id, self.state.title, save);
            }
    }

    /**
     * Component Will Unmount
     */
    componentDidUpdate(){
        console.log(this.state.page);
        this.state.page.length !== 0 ? this.save() : null;
    }

    /**
     * Allow the Editor to Load
     * a Different Document in
     * Place and Store open Page on
     * State...
     */
    componentWillReceiveProps(nextProps){
        const content = convertFromRaw( nextProps.content );
        let newEditorState = EditorState.createWithContent( content );
        let editorState = EditorState.moveFocusToEnd( newEditorState );
        /**
         * Check to See if the Editor is Being Reset
         * To Some Default Content
         */
        if(nextProps.page.length != 0){
            this.setState({
                editorState: editorState,
                page: nextProps.page,
                title: nextProps.page[0].title
            });
        } else {
            this.setState({
                editorState: editorState,
                page: nextProps.page,
                title: nextProps.title
            });
        }
    }

    /**
     * Maintain Editor State
     */
    onChange(editorState){
        this.setState({ editorState: editorState });
    }

    /**
     * Handle Key Events
     * In the Editor
     */
    handleKeyCommand(command){
        const newState = RichUtils.handleKeyCommand(this.state.editorState, command);
        if (newState) {
            this.onChange(newState);
            return 'handled';
        }
        return 'not-handled';
    }

    /**
     * Set the Block type e.g. H1 H2 etc...
     */
    setBlockType(value){
        this.onChange(RichUtils.toggleBlockType(
            this.state.editorState, value
        ));
    }

    /**
     * Handle Changing Inline Styles
     * e.g. Bold, Italic, Underline
     */
    inlineStyle(value){
        this.onChange(RichUtils.toggleInlineStyle(
            this.state.editorState,
            value
        ));
    }

    /**
     * Keep title in Sync with State
     * Title also controlls the page name
     */
    titleChange(event){
        this.setState({ title: event.target.value });
    }

    /**
     * Delete Page
     */
    deletePage(){
        if(this.state.page.length != 0){
            this.db('DELETE_PAGE', this.state.page[0].id, this.state.page[0].notebook_id);
        }
    }

    render(){
        return(
            <div className={ styles.textarea } >
                <div className={ styles.menu }>
                    <button onClick={() => { this.inlineStyle('BOLD'); }}><FormatBold /></button>
                    <button onClick={() => { this.inlineStyle('UNDERLINE'); }}><FormatUnderlined /></button>
                    <button onClick={() => { this.inlineStyle('ITALIC'); }}><FormatItalic /></button>
                    <button onClick={() => { this.inlineStyle('STRIKETHROUGH'); }}><StrikethroughS /></button>
                    <button onClick={() => { this.setBlockType('unordered-list-item'); }}><FormatListBulleted /></button>
                    <button onClick={() => { this.setBlockType('ordered-list-item'); }}><FormatListNumbered /></button>
                    <div className={ styles.font_size }><span className={ styles.font_size_title }>Font-Size</span>
                        <button onClick={() => { this.setBlockType('header-two'); }}>Big</button>
                        <button onClick={() => { this.setBlockType('unstyled'); }}>Normal</button>
                        <button onClick={() => { this.setBlockType('header-six'); }}>Small</button>
                        <button onClick={ this.deletePage }>Delete Page</button>
                    </div>
                </div>
                <TextField
                    onChange={ this.titleChange }
                    style={ txtarea }
                    multiLine={ false }
                    fullWidth={ true }
                    placeholder="Title"
                    id="title"
                    value={ this.state.title } />
                <Editor
                    onChange={(editorState) => {this.onChange( editorState ) }}
                    editorState = { this.state.editorState }
                    handleKeyCommand={ this.handleKeyCommand }
                    toggleBlockType={ this.toggleBlockType } />
            </div>
        );
    }
}
/*
 * Show Raw
 * var raw = convertToRaw( this.state.editorState.getCurrentContent() );
 * { JSON.stringify( raw ) }
 */
