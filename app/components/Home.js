import React, { Component } from 'react';
import { Link } from 'react-router';
import styles from './Home.css';
import Dexie from 'dexie';

export default class Home extends Component {

  constructor(props){
    super(props);

    this.state = {
        msg: props.value
    };

    console.log(props.value);
  }

  componentWillReceiveProps(props){
    this.setState({
        msg: props.value
    });
  }

  shouldComponentUpdate(nextProps, nextState){
    return nextProps.value !== this.props.value;
  }

  render() {
    return (
      <div>
        { this.state.msg }
      </div>
    );
  }
}
