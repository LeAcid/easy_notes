import React, { Component } from 'react';
import LibraryAdd from 'material-ui/svg-icons/av/library-add';
import IconButton from 'material-ui/IconButton';

export default class AddPage extends Component
{
    constructor(props){
        super(props);

        this.state = {
            id: props.id
        };

        this.page = props.page.bind(this);
        this.state.id = props.id.bind(this);
    }

    render(){
        return(
            <IconButton onClick={ this.page( this.id ) }>
                <LibraryAdd />
            </IconButton>
        );
    }
}
