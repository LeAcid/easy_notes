import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import styles from './AppBar.css';
import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar';
import ContentAdd from 'material-ui/svg-icons/content/add';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import Menu from 'material-ui/Menu';
import Popover from 'material-ui/Popover';
import MenuItem from 'material-ui/MenuItem';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';

const toolbar = {
    backgroundColor: '#000'
}

const button = {
    backgroundColor: '#333399'
}

export default class AppBar extends Component
{
    constructor(props){
        super(props);

        this.state = {
            menu_open: false,
            open: false,
            notebook_title: '',
            open_delete: false,
            delete_name: ''
        };

        this.handleTouchTap = this.handleTouchTap.bind(this);
        this.handleOpen = this.handleOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleRequestClose = this.handleRequestClose.bind(this);
        this.addNotebook = this.addNotebook.bind(this);
        this.openNotebookDialog = this.openNotebookDialog.bind(this);
        this.onTitleChange = this.onTitleChange.bind(this);
        this.db = props.db.bind(this);
    }

    /**
     * Keep Notebook Title In Sync
     */
    onTitleChange(event){
        this.setState({ notebook_title: event.target.value });
        console.log(this.state.notebook_title);
    }

    /**
     * Get Name of the Notebook
     */
    openNotebookDialog(){
        this.handleRequestClose();
        this.setState({ open: true });
    }

    /**
     * Add and Save the Notebook
     */
    addNotebook(){
        this.db('ADD_NOTEBOOK', null, this.state.notebook_title);
        this.handleClose();
    }

    /**
     * Handle opening popup menu
     */
    handleTouchTap = (event) => {
        event.preventDefault();

        this.setState({ menu_open: true, anchorEl: event.currentTarget });
    }

    /**
     * Handle Closing the menu
     */
    handleRequestClose = () => {
        this.setState({ menu_open: false });
    };

    /**
     * Handle closing Dialog
     */
    handleClose = () => {
        this.setState({ open: false });
    }

    /**
     * Handle opening the Dialog
     */
    handleOpen = () => {
        this.handleRequestClose();
        this.setState({ open: true });
    }

    /**
     * Handle Open Delete Menu
     */
    handleOpenDelete = () => {
        this.handleRequestClose();
        this.setState({ open_delete: true });
    }

    /**
     * Handle Closing Delete Dialog
     */
    handleCloseDelete = () => {
        this.setState({ open_delete: false });
    }

    /**
     * Sync notebook delete name
     */
    onNotebookDeleteName = (event) => {
        this.setState({ delete_name: event.target.value });
    }

    /**
     * Delete the notebook by entering the name
     */
    deleteNotebook = () => {
        this.handleCloseDelete();
        console.log('hit delete for name : ' +  this.state.delete_name);
        this.db('DELETE_NOTEBOOK', null, this.state.delete_name);
        this.setState({ delete_name: '' });
    }

    render(){
        const actions = [
            <FlatButton label="Cancel" secondary={ true } onTouchTap={ this.handleClose } />,
            <FlatButton label="Add Notebook" primary={ true } onTouchTap={ this.addNotebook } />
        ];

        const deleteActions = [
            <FlatButton label="Cancel" primary={ true } onTouchTap={ this.handleCloseDelete } />,
            <FlatButton label="Delete Notebook" secondary={ true } onTouchTap={ this.deleteNotebook } />
        ];
        return(
            <Toolbar style={ toolbar } className={ styles.toolbar }>
                <ToolbarGroup firstChild={ true }>

                </ToolbarGroup>
                <ToolbarGroup lastChild={ true }>
                        <Dialog title="Add Notebook" actions={ actions } modal={ false } open={ this.state.open } onRequestClose={ this.handleClose }>
                            <TextField ref="notebook_name" fullWidth={ true } placeholder="Enter Notebook Name"
                                       id="notebook_name" name="notebook_name" onChange={ this.onTitleChange } multiLine={ false }
                            />
                        </Dialog>
                        <Dialog title="Delete Notebook - Please note this action cannot be undone"
                                       actions={ deleteActions } modal={ false } open={ this.state.open_delete } onRequestClose={ this.handleCloseDelete }>
                            <TextField ref="deleteName" fullWidth={ true } placeholder="Enter the name of the notebook that you would like to delete"
                                       id="notebook_delete_name" value={ this.state.delete_name } onChange={ this.onNotebookDeleteName } multiLine={ false }
                            />
                        </Dialog>
                        <Popover
                            open={ this.state.menu_open }
                            anchorEl={ this.state.anchorEl }
                            anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
                            onRequestClose={ this.handleRequestClose }
                            targetOrigin={{horizontal: 'left', vertical: 'top'}}>
                        <Menu>
                            <MenuItem onClick={ this.handleOpen } primaryText="Add NoteBook" />
                            <MenuItem className={ styles.delete } onClick={ this.handleOpenDelete } primaryText="Delete NoteBook" />
                        </Menu>
                        </Popover>
                    <FloatingActionButton backgroundColor="#333399" onClick={ this.handleTouchTap } className={ styles.fab }>
                        <ContentAdd />
                    </FloatingActionButton>
                </ToolbarGroup>
            </Toolbar>
        );
    }
}
