import React from 'react'
import SvgIcon from 'material-ui/SvgIcon'

const CosmicIcon = (props) => (
            <SvgIcon>
<?xml version="1.0" encoding="utf-8"?>
<!-- Generator: Adobe Illustrator 20.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 316.5 271.5" style="enable-background:new 0 0 316.5 271.5;" xml:space="preserve">
<style type="text/css">
	.st0{enable-background:new    ;}
	.st1{fill:#B333CC;}
	.st2{fill:#991AB3;}
	.st3{fill:#7959CC;}
	.st4{fill:#4C2D9F;}
	.st5{clip-path:url(#SVGID_2_);enable-background:new    ;}
	.st6{fill:#AF35CC;}
	.st7{fill:#AC37CC;}
	.st8{fill:#A939CC;}
	.st9{fill:#A63BCC;}
	.st10{clip-path:url(#SVGID_4_);enable-background:new    ;}
	.st11{fill:#951BB1;}
	.st12{fill:#911CB0;}
	.st13{fill:#8D1DAF;}
	.st14{fill:#891EAE;}
	.st15{clip-path:url(#SVGID_6_);enable-background:new    ;}
	.st16{clip-path:url(#SVGID_8_);enable-background:new    ;}
	.st17{clip-path:url(#SVGID_10_);enable-background:new    ;}
	.st18{clip-path:url(#SVGID_12_);enable-background:new    ;}
	.st19{clip-path:url(#SVGID_14_);enable-background:new    ;}
	.st20{clip-path:url(#SVGID_16_);enable-background:new    ;}
	.st21{clip-path:url(#SVGID_18_);enable-background:new    ;}
	.st22{clip-path:url(#SVGID_20_);enable-background:new    ;}
	.st23{clip-path:url(#SVGID_22_);enable-background:new    ;}
	.st24{clip-path:url(#SVGID_24_);enable-background:new    ;}
	.st25{clip-path:url(#SVGID_26_);enable-background:new    ;}
	.st26{clip-path:url(#SVGID_28_);enable-background:new    ;}
	.st27{clip-path:url(#SVGID_30_);enable-background:new    ;}
	.st28{clip-path:url(#SVGID_32_);enable-background:new    ;}
	.st29{clip-path:url(#SVGID_34_);enable-background:new    ;}
	.st30{clip-path:url(#SVGID_36_);enable-background:new    ;}
	.st31{clip-path:url(#SVGID_38_);enable-background:new    ;}
	.st32{filter:url(#Adobe_OpacityMaskFilter);}
	.st33{mask:url(#SVGID_39_);}
	.st34{fill:#FFFFFF;}
	.st35{clip-path:url(#SVGID_41_);enable-background:new    ;}
	.st36{clip-path:url(#SVGID_43_);enable-background:new    ;}
	.st37{clip-path:url(#SVGID_45_);enable-background:new    ;}
	.st38{clip-path:url(#SVGID_47_);enable-background:new    ;}
	.st39{clip-path:url(#SVGID_49_);enable-background:new    ;}
	.st40{clip-path:url(#SVGID_51_);enable-background:new    ;}
	.st41{clip-path:url(#SVGID_53_);enable-background:new    ;}
	.st42{clip-path:url(#SVGID_55_);enable-background:new    ;}
	.st43{clip-path:url(#SVGID_57_);enable-background:new    ;}
	.st44{clip-path:url(#SVGID_59_);enable-background:new    ;}
	.st45{clip-path:url(#SVGID_61_);enable-background:new    ;}
	.st46{clip-path:url(#SVGID_63_);enable-background:new    ;}
	.st47{clip-path:url(#SVGID_65_);enable-background:new    ;}
	.st48{clip-path:url(#SVGID_67_);enable-background:new    ;}
	.st49{clip-path:url(#SVGID_69_);enable-background:new    ;}
	.st50{clip-path:url(#SVGID_71_);enable-background:new    ;}
	.st51{clip-path:url(#SVGID_73_);enable-background:new    ;}
	.st52{clip-path:url(#SVGID_75_);enable-background:new    ;}
	.st53{clip-path:url(#SVGID_77_);enable-background:new    ;}
	.st54{filter:url(#Adobe_OpacityMaskFilter_1_);}
	.st55{mask:url(#SVGID_78_);}
	.st56{clip-path:url(#SVGID_80_);enable-background:new    ;}
	.st57{clip-path:url(#SVGID_82_);enable-background:new    ;}
	.st58{clip-path:url(#SVGID_84_);enable-background:new    ;}
	.st59{clip-path:url(#SVGID_86_);enable-background:new    ;}
	.st60{clip-path:url(#SVGID_88_);enable-background:new    ;}
	.st61{clip-path:url(#SVGID_90_);enable-background:new    ;}
	.st62{clip-path:url(#SVGID_92_);enable-background:new    ;}
	.st63{clip-path:url(#SVGID_94_);enable-background:new    ;}
	.st64{clip-path:url(#SVGID_96_);enable-background:new    ;}
	.st65{clip-path:url(#SVGID_98_);enable-background:new    ;}
	.st66{clip-path:url(#SVGID_100_);enable-background:new    ;}
	.st67{clip-path:url(#SVGID_102_);enable-background:new    ;}
	.st68{clip-path:url(#SVGID_104_);enable-background:new    ;}
	.st69{clip-path:url(#SVGID_106_);enable-background:new    ;}
	.st70{clip-path:url(#SVGID_108_);enable-background:new    ;}
	.st71{clip-path:url(#SVGID_110_);enable-background:new    ;}
	.st72{clip-path:url(#SVGID_112_);enable-background:new    ;}
	.st73{clip-path:url(#SVGID_114_);enable-background:new    ;}
	.st74{clip-path:url(#SVGID_116_);enable-background:new    ;}
	.st75{filter:url(#Adobe_OpacityMaskFilter_2_);}
	.st76{clip-path:url(#SVGID_118_);enable-background:new    ;}
	.st77{clip-path:url(#SVGID_120_);enable-background:new    ;}
	.st78{clip-path:url(#SVGID_122_);enable-background:new    ;}
	.st79{clip-path:url(#SVGID_124_);enable-background:new    ;}
	.st80{clip-path:url(#SVGID_126_);enable-background:new    ;}
	.st81{clip-path:url(#SVGID_128_);enable-background:new    ;}
	.st82{clip-path:url(#SVGID_130_);enable-background:new    ;}
	.st83{clip-path:url(#SVGID_132_);enable-background:new    ;}
	.st84{clip-path:url(#SVGID_134_);enable-background:new    ;}
	.st85{clip-path:url(#SVGID_136_);enable-background:new    ;}
	.st86{clip-path:url(#SVGID_138_);enable-background:new    ;}
	.st87{clip-path:url(#SVGID_140_);enable-background:new    ;}
	.st88{clip-path:url(#SVGID_142_);enable-background:new    ;}
	.st89{clip-path:url(#SVGID_144_);enable-background:new    ;}
	.st90{clip-path:url(#SVGID_146_);enable-background:new    ;}
	.st91{clip-path:url(#SVGID_148_);enable-background:new    ;}
	.st92{clip-path:url(#SVGID_150_);enable-background:new    ;}
	.st93{clip-path:url(#SVGID_152_);enable-background:new    ;}
	.st94{clip-path:url(#SVGID_154_);enable-background:new    ;}
	.st95{filter:url(#Adobe_OpacityMaskFilter_3_);}
	.st96{mask:url(#SVGID_155_);}
	.st97{clip-path:url(#SVGID_157_);enable-background:new    ;}
	.st98{clip-path:url(#SVGID_159_);enable-background:new    ;}
	.st99{clip-path:url(#SVGID_161_);enable-background:new    ;}
	.st100{clip-path:url(#SVGID_163_);enable-background:new    ;}
	.st101{clip-path:url(#SVGID_165_);enable-background:new    ;}
	.st102{clip-path:url(#SVGID_167_);enable-background:new    ;}
	.st103{clip-path:url(#SVGID_169_);enable-background:new    ;}
	.st104{clip-path:url(#SVGID_171_);enable-background:new    ;}
	.st105{clip-path:url(#SVGID_173_);enable-background:new    ;}
	.st106{clip-path:url(#SVGID_175_);enable-background:new    ;}
	.st107{clip-path:url(#SVGID_177_);enable-background:new    ;}
	.st108{clip-path:url(#SVGID_179_);enable-background:new    ;}
	.st109{clip-path:url(#SVGID_181_);enable-background:new    ;}
	.st110{clip-path:url(#SVGID_183_);enable-background:new    ;}
	.st111{clip-path:url(#SVGID_185_);enable-background:new    ;}
	.st112{clip-path:url(#SVGID_187_);enable-background:new    ;}
	.st113{clip-path:url(#SVGID_189_);enable-background:new    ;}
	.st114{clip-path:url(#SVGID_191_);enable-background:new    ;}
	.st115{clip-path:url(#SVGID_193_);enable-background:new    ;}
	.st116{filter:url(#Adobe_OpacityMaskFilter_4_);}
	.st117{clip-path:url(#SVGID_195_);enable-background:new    ;}
	.st118{clip-path:url(#SVGID_197_);enable-background:new    ;}
	.st119{clip-path:url(#SVGID_199_);enable-background:new    ;}
	.st120{clip-path:url(#SVGID_201_);enable-background:new    ;}
	.st121{clip-path:url(#SVGID_203_);enable-background:new    ;}
	.st122{clip-path:url(#SVGID_205_);enable-background:new    ;}
	.st123{clip-path:url(#SVGID_207_);enable-background:new    ;}
	.st124{clip-path:url(#SVGID_209_);enable-background:new    ;}
	.st125{clip-path:url(#SVGID_211_);enable-background:new    ;}
	.st126{clip-path:url(#SVGID_213_);enable-background:new    ;}
	.st127{clip-path:url(#SVGID_215_);enable-background:new    ;}
	.st128{clip-path:url(#SVGID_217_);enable-background:new    ;}
	.st129{clip-path:url(#SVGID_219_);enable-background:new    ;}
	.st130{clip-path:url(#SVGID_221_);enable-background:new    ;}
	.st131{clip-path:url(#SVGID_223_);enable-background:new    ;}
	.st132{clip-path:url(#SVGID_225_);enable-background:new    ;}
	.st133{clip-path:url(#SVGID_227_);enable-background:new    ;}
	.st134{clip-path:url(#SVGID_229_);enable-background:new    ;}
	.st135{clip-path:url(#SVGID_231_);enable-background:new    ;}
	.st136{filter:url(#Adobe_OpacityMaskFilter_5_);}
	.st137{mask:url(#SVGID_232_);}
	.st138{clip-path:url(#SVGID_234_);enable-background:new    ;}
	.st139{clip-path:url(#SVGID_236_);enable-background:new    ;}
	.st140{clip-path:url(#SVGID_238_);enable-background:new    ;}
	.st141{clip-path:url(#SVGID_240_);enable-background:new    ;}
	.st142{clip-path:url(#SVGID_242_);enable-background:new    ;}
	.st143{clip-path:url(#SVGID_244_);enable-background:new    ;}
	.st144{clip-path:url(#SVGID_246_);enable-background:new    ;}
	.st145{clip-path:url(#SVGID_248_);enable-background:new    ;}
	.st146{clip-path:url(#SVGID_250_);enable-background:new    ;}
	.st147{clip-path:url(#SVGID_252_);enable-background:new    ;}
	.st148{clip-path:url(#SVGID_254_);enable-background:new    ;}
	.st149{clip-path:url(#SVGID_256_);enable-background:new    ;}
	.st150{clip-path:url(#SVGID_258_);enable-background:new    ;}
	.st151{clip-path:url(#SVGID_260_);enable-background:new    ;}
	.st152{clip-path:url(#SVGID_262_);enable-background:new    ;}
	.st153{clip-path:url(#SVGID_264_);enable-background:new    ;}
	.st154{clip-path:url(#SVGID_266_);enable-background:new    ;}
	.st155{clip-path:url(#SVGID_268_);enable-background:new    ;}
	.st156{clip-path:url(#SVGID_270_);enable-background:new    ;}
	.st157{filter:url(#Adobe_OpacityMaskFilter_6_);}
	.st158{clip-path:url(#SVGID_272_);enable-background:new    ;}
	.st159{clip-path:url(#SVGID_274_);enable-background:new    ;}
	.st160{clip-path:url(#SVGID_276_);enable-background:new    ;}
	.st161{clip-path:url(#SVGID_278_);enable-background:new    ;}
	.st162{clip-path:url(#SVGID_280_);enable-background:new    ;}
	.st163{clip-path:url(#SVGID_282_);enable-background:new    ;}
	.st164{clip-path:url(#SVGID_284_);enable-background:new    ;}
	.st165{clip-path:url(#SVGID_286_);enable-background:new    ;}
	.st166{clip-path:url(#SVGID_288_);enable-background:new    ;}
	.st167{clip-path:url(#SVGID_290_);enable-background:new    ;}
	.st168{clip-path:url(#SVGID_292_);enable-background:new    ;}
	.st169{clip-path:url(#SVGID_294_);enable-background:new    ;}
	.st170{clip-path:url(#SVGID_296_);enable-background:new    ;}
	.st171{clip-path:url(#SVGID_298_);enable-background:new    ;}
	.st172{clip-path:url(#SVGID_300_);enable-background:new    ;}
	.st173{clip-path:url(#SVGID_302_);enable-background:new    ;}
	.st174{clip-path:url(#SVGID_304_);enable-background:new    ;}
	.st175{clip-path:url(#SVGID_306_);enable-background:new    ;}
	.st176{clip-path:url(#SVGID_308_);enable-background:new    ;}
	.st177{filter:url(#Adobe_OpacityMaskFilter_7_);}
	.st178{mask:url(#SVGID_309_);}
	.st179{clip-path:url(#SVGID_311_);enable-background:new    ;}
	.st180{clip-path:url(#SVGID_313_);enable-background:new    ;}
	.st181{clip-path:url(#SVGID_315_);enable-background:new    ;}
	.st182{clip-path:url(#SVGID_317_);enable-background:new    ;}
	.st183{clip-path:url(#SVGID_319_);enable-background:new    ;}
	.st184{clip-path:url(#SVGID_321_);enable-background:new    ;}
	.st185{clip-path:url(#SVGID_323_);enable-background:new    ;}
	.st186{clip-path:url(#SVGID_325_);enable-background:new    ;}
	.st187{clip-path:url(#SVGID_327_);enable-background:new    ;}
	.st188{clip-path:url(#SVGID_329_);enable-background:new    ;}
	.st189{clip-path:url(#SVGID_331_);enable-background:new    ;}
	.st190{clip-path:url(#SVGID_333_);enable-background:new    ;}
	.st191{clip-path:url(#SVGID_335_);enable-background:new    ;}
	.st192{clip-path:url(#SVGID_337_);enable-background:new    ;}
	.st193{clip-path:url(#SVGID_339_);enable-background:new    ;}
	.st194{clip-path:url(#SVGID_341_);enable-background:new    ;}
	.st195{clip-path:url(#SVGID_343_);enable-background:new    ;}
	.st196{clip-path:url(#SVGID_345_);enable-background:new    ;}
	.st197{clip-path:url(#SVGID_347_);enable-background:new    ;}
	.st198{filter:url(#Adobe_OpacityMaskFilter_8_);}
	.st199{clip-path:url(#SVGID_349_);enable-background:new    ;}
	.st200{clip-path:url(#SVGID_351_);enable-background:new    ;}
	.st201{clip-path:url(#SVGID_353_);enable-background:new    ;}
	.st202{clip-path:url(#SVGID_355_);enable-background:new    ;}
	.st203{clip-path:url(#SVGID_357_);enable-background:new    ;}
	.st204{clip-path:url(#SVGID_359_);enable-background:new    ;}
	.st205{clip-path:url(#SVGID_361_);enable-background:new    ;}
	.st206{clip-path:url(#SVGID_363_);enable-background:new    ;}
	.st207{clip-path:url(#SVGID_365_);enable-background:new    ;}
	.st208{clip-path:url(#SVGID_367_);enable-background:new    ;}
	.st209{clip-path:url(#SVGID_369_);enable-background:new    ;}
	.st210{clip-path:url(#SVGID_371_);enable-background:new    ;}
	.st211{clip-path:url(#SVGID_373_);enable-background:new    ;}
	.st212{clip-path:url(#SVGID_375_);enable-background:new    ;}
	.st213{clip-path:url(#SVGID_377_);enable-background:new    ;}
	.st214{clip-path:url(#SVGID_379_);enable-background:new    ;}
	.st215{clip-path:url(#SVGID_381_);enable-background:new    ;}
	.st216{clip-path:url(#SVGID_383_);enable-background:new    ;}
	.st217{clip-path:url(#SVGID_385_);enable-background:new    ;}
	.st218{filter:url(#Adobe_OpacityMaskFilter_9_);}
	.st219{mask:url(#SVGID_386_);}
	.st220{clip-path:url(#SVGID_388_);enable-background:new    ;}
	.st221{clip-path:url(#SVGID_390_);enable-background:new    ;}
	.st222{clip-path:url(#SVGID_392_);enable-background:new    ;}
	.st223{clip-path:url(#SVGID_394_);enable-background:new    ;}
	.st224{clip-path:url(#SVGID_396_);enable-background:new    ;}
	.st225{clip-path:url(#SVGID_398_);enable-background:new    ;}
	.st226{clip-path:url(#SVGID_400_);enable-background:new    ;}
	.st227{clip-path:url(#SVGID_402_);enable-background:new    ;}
	.st228{clip-path:url(#SVGID_404_);enable-background:new    ;}
	.st229{clip-path:url(#SVGID_406_);enable-background:new    ;}
	.st230{clip-path:url(#SVGID_408_);enable-background:new    ;}
	.st231{clip-path:url(#SVGID_410_);enable-background:new    ;}
	.st232{clip-path:url(#SVGID_412_);enable-background:new    ;}
	.st233{clip-path:url(#SVGID_414_);enable-background:new    ;}
	.st234{clip-path:url(#SVGID_416_);enable-background:new    ;}
	.st235{clip-path:url(#SVGID_418_);enable-background:new    ;}
	.st236{clip-path:url(#SVGID_420_);enable-background:new    ;}
	.st237{clip-path:url(#SVGID_422_);enable-background:new    ;}
	.st238{clip-path:url(#SVGID_424_);enable-background:new    ;}
	.st239{filter:url(#Adobe_OpacityMaskFilter_10_);}
	.st240{clip-path:url(#SVGID_426_);enable-background:new    ;}
	.st241{clip-path:url(#SVGID_428_);enable-background:new    ;}
	.st242{clip-path:url(#SVGID_430_);enable-background:new    ;}
	.st243{clip-path:url(#SVGID_432_);enable-background:new    ;}
	.st244{clip-path:url(#SVGID_434_);enable-background:new    ;}
	.st245{clip-path:url(#SVGID_436_);enable-background:new    ;}
	.st246{clip-path:url(#SVGID_438_);enable-background:new    ;}
	.st247{clip-path:url(#SVGID_440_);enable-background:new    ;}
	.st248{clip-path:url(#SVGID_442_);enable-background:new    ;}
	.st249{clip-path:url(#SVGID_444_);enable-background:new    ;}
	.st250{clip-path:url(#SVGID_446_);enable-background:new    ;}
	.st251{clip-path:url(#SVGID_448_);enable-background:new    ;}
	.st252{clip-path:url(#SVGID_450_);enable-background:new    ;}
	.st253{clip-path:url(#SVGID_452_);enable-background:new    ;}
	.st254{clip-path:url(#SVGID_454_);enable-background:new    ;}
	.st255{clip-path:url(#SVGID_456_);enable-background:new    ;}
	.st256{clip-path:url(#SVGID_458_);enable-background:new    ;}
	.st257{clip-path:url(#SVGID_460_);enable-background:new    ;}
	.st258{clip-path:url(#SVGID_462_);enable-background:new    ;}
	.st259{filter:url(#Adobe_OpacityMaskFilter_11_);}
	.st260{mask:url(#SVGID_463_);}
	.st261{clip-path:url(#SVGID_465_);enable-background:new    ;}
	.st262{clip-path:url(#SVGID_467_);enable-background:new    ;}
	.st263{clip-path:url(#SVGID_469_);enable-background:new    ;}
	.st264{clip-path:url(#SVGID_471_);enable-background:new    ;}
	.st265{clip-path:url(#SVGID_473_);enable-background:new    ;}
	.st266{clip-path:url(#SVGID_475_);enable-background:new    ;}
	.st267{clip-path:url(#SVGID_477_);enable-background:new    ;}
	.st268{clip-path:url(#SVGID_479_);enable-background:new    ;}
	.st269{clip-path:url(#SVGID_481_);enable-background:new    ;}
	.st270{clip-path:url(#SVGID_483_);enable-background:new    ;}
	.st271{clip-path:url(#SVGID_485_);enable-background:new    ;}
	.st272{clip-path:url(#SVGID_487_);enable-background:new    ;}
	.st273{clip-path:url(#SVGID_489_);enable-background:new    ;}
	.st274{clip-path:url(#SVGID_491_);enable-background:new    ;}
	.st275{clip-path:url(#SVGID_493_);enable-background:new    ;}
	.st276{clip-path:url(#SVGID_495_);enable-background:new    ;}
	.st277{clip-path:url(#SVGID_497_);enable-background:new    ;}
	.st278{clip-path:url(#SVGID_499_);enable-background:new    ;}
	.st279{clip-path:url(#SVGID_501_);enable-background:new    ;}
	.st280{filter:url(#Adobe_OpacityMaskFilter_12_);}
	.st281{clip-path:url(#SVGID_503_);enable-background:new    ;}
	.st282{clip-path:url(#SVGID_505_);enable-background:new    ;}
	.st283{clip-path:url(#SVGID_507_);enable-background:new    ;}
	.st284{clip-path:url(#SVGID_509_);enable-background:new    ;}
	.st285{clip-path:url(#SVGID_511_);enable-background:new    ;}
	.st286{clip-path:url(#SVGID_513_);enable-background:new    ;}
	.st287{clip-path:url(#SVGID_515_);enable-background:new    ;}
	.st288{clip-path:url(#SVGID_517_);enable-background:new    ;}
	.st289{clip-path:url(#SVGID_519_);enable-background:new    ;}
	.st290{clip-path:url(#SVGID_521_);enable-background:new    ;}
	.st291{clip-path:url(#SVGID_523_);enable-background:new    ;}
	.st292{clip-path:url(#SVGID_525_);enable-background:new    ;}
	.st293{clip-path:url(#SVGID_527_);enable-background:new    ;}
	.st294{clip-path:url(#SVGID_529_);enable-background:new    ;}
	.st295{clip-path:url(#SVGID_531_);enable-background:new    ;}
	.st296{clip-path:url(#SVGID_533_);enable-background:new    ;}
	.st297{clip-path:url(#SVGID_535_);enable-background:new    ;}
	.st298{clip-path:url(#SVGID_537_);enable-background:new    ;}
	.st299{clip-path:url(#SVGID_539_);enable-background:new    ;}
	.st300{filter:url(#Adobe_OpacityMaskFilter_13_);}
	.st301{mask:url(#SVGID_540_);}
	.st302{clip-path:url(#SVGID_542_);enable-background:new    ;}
	.st303{clip-path:url(#SVGID_544_);enable-background:new    ;}
	.st304{clip-path:url(#SVGID_546_);enable-background:new    ;}
	.st305{clip-path:url(#SVGID_548_);enable-background:new    ;}
	.st306{clip-path:url(#SVGID_550_);enable-background:new    ;}
	.st307{clip-path:url(#SVGID_552_);enable-background:new    ;}
	.st308{clip-path:url(#SVGID_554_);enable-background:new    ;}
	.st309{clip-path:url(#SVGID_556_);enable-background:new    ;}
	.st310{clip-path:url(#SVGID_558_);enable-background:new    ;}
	.st311{clip-path:url(#SVGID_560_);enable-background:new    ;}
	.st312{clip-path:url(#SVGID_562_);enable-background:new    ;}
	.st313{clip-path:url(#SVGID_564_);enable-background:new    ;}
	.st314{clip-path:url(#SVGID_566_);enable-background:new    ;}
	.st315{clip-path:url(#SVGID_568_);enable-background:new    ;}
	.st316{clip-path:url(#SVGID_570_);enable-background:new    ;}
	.st317{clip-path:url(#SVGID_572_);enable-background:new    ;}
	.st318{clip-path:url(#SVGID_574_);enable-background:new    ;}
	.st319{clip-path:url(#SVGID_576_);enable-background:new    ;}
	.st320{clip-path:url(#SVGID_578_);enable-background:new    ;}
	.st321{filter:url(#Adobe_OpacityMaskFilter_14_);}
	.st322{clip-path:url(#SVGID_580_);enable-background:new    ;}
	.st323{clip-path:url(#SVGID_582_);enable-background:new    ;}
	.st324{clip-path:url(#SVGID_584_);enable-background:new    ;}
	.st325{clip-path:url(#SVGID_586_);enable-background:new    ;}
	.st326{clip-path:url(#SVGID_588_);enable-background:new    ;}
	.st327{clip-path:url(#SVGID_590_);enable-background:new    ;}
	.st328{clip-path:url(#SVGID_592_);enable-background:new    ;}
	.st329{clip-path:url(#SVGID_594_);enable-background:new    ;}
	.st330{clip-path:url(#SVGID_596_);enable-background:new    ;}
	.st331{clip-path:url(#SVGID_598_);enable-background:new    ;}
	.st332{clip-path:url(#SVGID_600_);enable-background:new    ;}
	.st333{clip-path:url(#SVGID_602_);enable-background:new    ;}
	.st334{clip-path:url(#SVGID_604_);enable-background:new    ;}
	.st335{clip-path:url(#SVGID_606_);enable-background:new    ;}
	.st336{clip-path:url(#SVGID_608_);enable-background:new    ;}
	.st337{clip-path:url(#SVGID_610_);enable-background:new    ;}
	.st338{clip-path:url(#SVGID_612_);enable-background:new    ;}
	.st339{clip-path:url(#SVGID_614_);enable-background:new    ;}
	.st340{clip-path:url(#SVGID_616_);enable-background:new    ;}
	.st341{display:none;}
	.st342{display:inline;fill:url(#SVGID_617_);stroke:url(#SVGID_618_);stroke-miterlimit:10;}
	.st343{display:inline;fill:none;stroke:url(#SVGID_619_);stroke-miterlimit:10;}
	.st344{display:inline;fill:none;stroke:url(#SVGID_620_);stroke-miterlimit:10;}
	.st345{fill:url(#SVGID_621_);}
	.st346{fill:#6666CC;}
	.st347{filter:url(#Adobe_OpacityMaskFilter_15_);}
	.st348{mask:url(#SVGID_622_);}
	.st349{clip-path:url(#SVGID_624_);enable-background:new    ;}
	.st350{clip-path:url(#SVGID_626_);enable-background:new    ;}
	.st351{clip-path:url(#SVGID_628_);enable-background:new    ;}
	.st352{clip-path:url(#SVGID_630_);enable-background:new    ;}
	.st353{clip-path:url(#SVGID_632_);enable-background:new    ;}
	.st354{clip-path:url(#SVGID_634_);enable-background:new    ;}
	.st355{filter:url(#Adobe_OpacityMaskFilter_16_);}
	.st356{mask:url(#SVGID_635_);}
	.st357{clip-path:url(#SVGID_637_);enable-background:new    ;}
	.st358{clip-path:url(#SVGID_639_);enable-background:new    ;}
	.st359{clip-path:url(#SVGID_641_);enable-background:new    ;}
	.st360{clip-path:url(#SVGID_643_);enable-background:new    ;}
	.st361{clip-path:url(#SVGID_645_);enable-background:new    ;}
	.st362{clip-path:url(#SVGID_647_);enable-background:new    ;}
	.st363{clip-path:url(#SVGID_649_);enable-background:new    ;}
	.st364{clip-path:url(#SVGID_651_);enable-background:new    ;}
	.st365{clip-path:url(#SVGID_653_);enable-background:new    ;}
	.st366{clip-path:url(#SVGID_655_);enable-background:new    ;}
	.st367{clip-path:url(#SVGID_657_);enable-background:new    ;}
	.st368{clip-path:url(#SVGID_659_);enable-background:new    ;}
	.st369{clip-path:url(#SVGID_661_);enable-background:new    ;}
	.st370{clip-path:url(#SVGID_663_);enable-background:new    ;}
	.st371{clip-path:url(#SVGID_665_);enable-background:new    ;}
	.st372{clip-path:url(#SVGID_667_);enable-background:new    ;}
	.st373{clip-path:url(#SVGID_669_);enable-background:new    ;}
	.st374{clip-path:url(#SVGID_671_);enable-background:new    ;}
	.st375{clip-path:url(#SVGID_673_);enable-background:new    ;}
	.st376{clip-path:url(#SVGID_675_);enable-background:new    ;}
	.st377{filter:url(#Adobe_OpacityMaskFilter_17_);}
	.st378{mask:url(#SVGID_676_);}
	.st379{clip-path:url(#SVGID_678_);enable-background:new    ;}
	.st380{clip-path:url(#SVGID_680_);enable-background:new    ;}
	.st381{clip-path:url(#SVGID_682_);enable-background:new    ;}
	.st382{clip-path:url(#SVGID_684_);enable-background:new    ;}
	.st383{clip-path:url(#SVGID_686_);enable-background:new    ;}
	.st384{clip-path:url(#SVGID_688_);enable-background:new    ;}
	.st385{clip-path:url(#SVGID_690_);enable-background:new    ;}
	.st386{clip-path:url(#SVGID_692_);enable-background:new    ;}
	.st387{clip-path:url(#SVGID_694_);enable-background:new    ;}
	.st388{clip-path:url(#SVGID_696_);enable-background:new    ;}
	.st389{clip-path:url(#SVGID_698_);enable-background:new    ;}
	.st390{clip-path:url(#SVGID_700_);enable-background:new    ;}
	.st391{clip-path:url(#SVGID_702_);enable-background:new    ;}
	.st392{clip-path:url(#SVGID_704_);enable-background:new    ;}
	.st393{clip-path:url(#SVGID_706_);enable-background:new    ;}
	.st394{clip-path:url(#SVGID_708_);enable-background:new    ;}
	.st395{fill:#A73BCC;}
	.st396{clip-path:url(#SVGID_710_);enable-background:new    ;}
	.st397{clip-path:url(#SVGID_712_);enable-background:new    ;}
	.st398{fill:#B134CC;}
	.st399{clip-path:url(#SVGID_714_);enable-background:new    ;}
	.st400{clip-path:url(#SVGID_716_);enable-background:new    ;}
	.st401{fill:#961AB2;}
	.st402{clip-path:url(#SVGID_718_);enable-background:new    ;}
	.st403{clip-path:url(#SVGID_720_);enable-background:new    ;}
	.st404{clip-path:url(#SVGID_722_);enable-background:new    ;}
	.st405{clip-path:url(#SVGID_724_);enable-background:new    ;}
	.st406{fill:#B034CC;}
	.st407{clip-path:url(#SVGID_726_);enable-background:new    ;}
	.st408{clip-path:url(#SVGID_728_);enable-background:new    ;}
	.st409{clip-path:url(#SVGID_730_);enable-background:new    ;}
	.st410{clip-path:url(#SVGID_732_);enable-background:new    ;}
	.st411{clip-path:url(#SVGID_734_);enable-background:new    ;}
	.st412{clip-path:url(#SVGID_736_);enable-background:new    ;}
	.st413{clip-path:url(#SVGID_738_);enable-background:new    ;}
	.st414{clip-path:url(#SVGID_740_);enable-background:new    ;}
	.st415{clip-path:url(#SVGID_742_);enable-background:new    ;}
	.st416{filter:url(#Adobe_OpacityMaskFilter_18_);}
	.st417{mask:url(#SVGID_743_);}
	.st418{clip-path:url(#SVGID_745_);enable-background:new    ;}
	.st419{clip-path:url(#SVGID_747_);enable-background:new    ;}
	.st420{clip-path:url(#SVGID_749_);enable-background:new    ;}
	.st421{clip-path:url(#SVGID_751_);enable-background:new    ;}
	.st422{clip-path:url(#SVGID_753_);enable-background:new    ;}
	.st423{clip-path:url(#SVGID_755_);enable-background:new    ;}
	.st424{clip-path:url(#SVGID_757_);enable-background:new    ;}
	.st425{clip-path:url(#SVGID_759_);enable-background:new    ;}
	.st426{clip-path:url(#SVGID_761_);enable-background:new    ;}
	.st427{clip-path:url(#SVGID_763_);enable-background:new    ;}
	.st428{clip-path:url(#SVGID_765_);enable-background:new    ;}
	.st429{clip-path:url(#SVGID_767_);enable-background:new    ;}
	.st430{clip-path:url(#SVGID_769_);enable-background:new    ;}
	.st431{clip-path:url(#SVGID_771_);enable-background:new    ;}
	.st432{clip-path:url(#SVGID_773_);enable-background:new    ;}
	.st433{clip-path:url(#SVGID_775_);enable-background:new    ;}
	.st434{clip-path:url(#SVGID_777_);enable-background:new    ;}
	.st435{clip-path:url(#SVGID_779_);enable-background:new    ;}
	.st436{clip-path:url(#SVGID_781_);enable-background:new    ;}
	.st437{filter:url(#Adobe_OpacityMaskFilter_19_);}
	.st438{mask:url(#SVGID_782_);}
	.st439{clip-path:url(#SVGID_784_);enable-background:new    ;}
	.st440{clip-path:url(#SVGID_786_);enable-background:new    ;}
	.st441{clip-path:url(#SVGID_788_);enable-background:new    ;}
	.st442{clip-path:url(#SVGID_790_);enable-background:new    ;}
	.st443{clip-path:url(#SVGID_792_);enable-background:new    ;}
	.st444{clip-path:url(#SVGID_794_);enable-background:new    ;}
	.st445{clip-path:url(#SVGID_796_);enable-background:new    ;}
	.st446{filter:url(#Adobe_OpacityMaskFilter_20_);}
	.st447{mask:url(#SVGID_797_);}
	.st448{clip-path:url(#SVGID_799_);enable-background:new    ;}
	.st449{clip-path:url(#SVGID_801_);enable-background:new    ;}
	.st450{clip-path:url(#SVGID_803_);enable-background:new    ;}
	.st451{clip-path:url(#SVGID_805_);enable-background:new    ;}
	.st452{clip-path:url(#SVGID_807_);enable-background:new    ;}
	.st453{clip-path:url(#SVGID_809_);enable-background:new    ;}
	.st454{clip-path:url(#SVGID_811_);enable-background:new    ;}
	.st455{fill:#AB38CC;}
	.st456{fill:#8E1CB0;}
	.st457{fill:#B233CC;}
	.st458{filter:url(#Adobe_OpacityMaskFilter_21_);}
	.st459{mask:url(#SVGID_812_);}
</style>
<g id="NOTES">
	<g>
		<g>
			<g>
				<g>
					<g>
						<g class="st0">
							<g>
								<rect x="95.1" y="156.6" class="st1" width="5.7" height="2.6"/>
							</g>
							<g>
								<rect x="106.8" y="156.6" class="st1" width="5.4" height="2.6"/>
							</g>
							<g>
								<polygon class="st1" points="105.8,163.8 105.8,166.4 100.8,159.2 100.8,156.6 								"/>
							</g>
							<g>
								<polygon class="st1" points="106.8,163.8 106.8,166.4 106.1,166.7 106.1,164.1 								"/>
							</g>
							<g>
								<polygon class="st1" points="106.1,164.1 106.1,166.7 105.8,166.4 105.8,163.8 								"/>
							</g>
							<g>
								<polygon class="st2" points="101.2,165.4 101.2,168 100.5,168.4 100.5,165.8 								"/>
							</g>
							<g>
								<polygon class="st2" points="101.5,165.8 101.5,168.4 101.2,168 101.2,165.4 								"/>
							</g>
							<g>
								<polygon class="st2" points="106.5,173 106.5,175.6 101.5,168.4 101.5,165.8 								"/>
							</g>
							<g>
								<rect x="106.5" y="173" class="st2" width="5.7" height="2.6"/>
							</g>
							<g>
								<rect x="95.1" y="173" class="st2" width="5.4" height="2.6"/>
							</g>
							<g>
								<path class="st3" d="M105.8,155.6h7.4V174h-7.3l-4.4-6.4v6.4h-7.4v-18.4h7.3l4.4,6.4V155.6z M112.2,173v-16.4h-5.4v7.2
									l-0.7,0.3l-0.3-0.3l-5-7.2h-5.7V173h5.4v-7.2l0.7-0.3l0.3,0.3l5,7.2H112.2"/>
							</g>
							<g>
								<polygon class="st4" points="112.2,156.6 112.2,173 106.5,173 101.5,165.8 101.2,165.4 100.5,165.8 100.5,173 95.1,173 
									95.1,156.6 100.8,156.6 105.8,163.8 106.1,164.1 106.8,163.8 106.8,156.6 								"/>
							</g>
							<g>
								<polygon class="st1" points="105.9,174 105.9,176.6 101.5,170.2 101.5,167.6 								"/>
							</g>
							<g>
								<rect x="105.9" y="174" class="st1" width="7.3" height="2.6"/>
							</g>
							<g>
								<rect x="94.1" y="174" class="st1" width="7.4" height="2.6"/>
							</g>
						</g>
						<g class="st0">
							<g class="st0">
								<defs>
									<path id="SVGID_1_" class="st0" d="M133.3,164.8l0,2.6c0-4.7-4-8.4-9.2-8.4c-5.2,0-9.2,3.7-9.2,8.4l0-2.6
										c0-4.7,4-8.4,9.2-8.4C129.3,156.3,133.3,160,133.3,164.8z"/>
								</defs>
								<clipPath id="SVGID_2_">
									<use xlink:href="#SVGID_1_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st5">
									<path class="st6" d="M133.3,164.8v2.6c0-4.7-4-8.4-9.2-8.4c-3.6,0-6.6,1.8-8.1,4.5v-2.6c1.5-2.7,4.6-4.5,8.1-4.5
										C129.3,156.3,133.3,160,133.3,164.8"/>
									<path class="st7" d="M116,160.8v2.6c-0.3,0.5-0.5,0.9-0.6,1.4v-2.6C115.5,161.7,115.7,161.3,116,160.8"/>
									<path class="st8" d="M115.3,162.2v2.6c-0.1,0.4-0.3,0.9-0.3,1.3v-2.6C115.1,163.1,115.2,162.7,115.3,162.2"/>
									<path class="st9" d="M115,163.6v2.6c-0.1,0.4-0.1,0.8-0.1,1.2v-2.6C114.9,164.4,115,164,115,163.6"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_3_" class="st0" d="M120.9,167.5c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3v-0.3c0-1.8,1.4-3.2,3.2-3.2
										c1.8,0,3.2,1.4,3.2,3.2l0,2.6c0-1.8-1.4-3.2-3.2-3.2C122.4,164.3,120.9,165.8,120.9,167.5z"/>
								</defs>
								<clipPath id="SVGID_4_">
									<use xlink:href="#SVGID_3_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st10">
									<path class="st11" d="M127.3,164.9v2.6c0-1.8-1.4-3.2-3.2-3.2c-1.2,0-2.2,0.7-2.8,1.6v-2.6c0.5-1,1.6-1.6,2.8-1.6
										C125.9,161.7,127.3,163.1,127.3,164.9"/>
									<path class="st12" d="M121.3,163.3v2.6c-0.1,0.2-0.2,0.4-0.3,0.6v-2.6C121.2,163.7,121.2,163.5,121.3,163.3"/>
									<path class="st13" d="M121.1,163.9v2.6c-0.1,0.2-0.1,0.3-0.1,0.5v-2.6C121,164.2,121,164.1,121.1,163.9"/>
									<path class="st14" d="M121,164.4v2.6c0,0.2,0,0.3,0,0.5v-2.6C120.9,164.7,120.9,164.6,121,164.4"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_5_" class="st0" d="M126.3,164.9l0,2.6c0-1.2-1-2.2-2.2-2.2c-1.2,0-2.2,1-2.2,2.2l0-2.6
										c0-1.2,1-2.2,2.2-2.2C125.3,162.7,126.3,163.7,126.3,164.9z"/>
								</defs>
								<clipPath id="SVGID_6_">
									<use xlink:href="#SVGID_5_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st15">
									<path class="st6" d="M126.3,164.9v2.6c0-1.2-1-2.2-2.2-2.2c-0.8,0-1.5,0.4-1.9,1.1v-2.6c0.4-0.7,1.1-1.1,1.9-1.1
										C125.3,162.7,126.3,163.7,126.3,164.9"/>
									<path class="st7" d="M122.2,163.8v2.6c-0.1,0.1-0.1,0.3-0.2,0.4v-2.6C122.1,164.1,122.1,163.9,122.2,163.8"/>
									<path class="st8" d="M122,164.2v2.6c0,0.1-0.1,0.2-0.1,0.4v-2.6C122,164.4,122,164.3,122,164.2"/>
									<path class="st9" d="M121.9,164.6v2.6c0,0.1,0,0.2,0,0.3v-2.6C121.9,164.8,121.9,164.7,121.9,164.6"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_7_" class="st0" d="M127.3,164.9l0,2.6c0,1.8-1.4,3.2-3.2,3.2c-1.8,0-3.2-1.4-3.2-3.2v-2.6
										c0,1.8,1.4,3.2,3.2,3.2C125.9,168.1,127.3,166.6,127.3,164.9z"/>
								</defs>
								<clipPath id="SVGID_8_">
									<use xlink:href="#SVGID_7_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st16">
									<path class="st9" d="M127.3,164.9v2.6c0,0.2,0,0.3,0,0.5v-2.6C127.3,165.2,127.3,165.1,127.3,164.9"/>
									<path class="st8" d="M127.3,165.4v2.6c0,0.2-0.1,0.4-0.1,0.5v-2.6C127.2,165.7,127.2,165.5,127.3,165.4"/>
									<path class="st7" d="M127.1,165.9v2.6c-0.1,0.2-0.2,0.4-0.3,0.6v-2.6C127,166.3,127.1,166.1,127.1,165.9"/>
									<path class="st6" d="M126.9,166.5v2.6c-0.5,1-1.6,1.6-2.8,1.6c-1.8,0-3.2-1.4-3.2-3.2v-2.6c0,1.8,1.4,3.2,3.2,3.2
										C125.3,168.1,126.3,167.4,126.9,166.5"/>
								</g>
							</g>
							<g>
								<path class="st3" d="M124.1,161.7c1.8,0,3.2,1.4,3.2,3.2c0,1.8-1.4,3.2-3.2,3.2c-1.8,0-3.2-1.4-3.2-3.2
									C120.9,163.1,122.4,161.7,124.1,161.7z M124.1,167.1c1.2,0,2.2-1,2.2-2.2c0-1.2-1-2.2-2.2-2.2c-1.2,0-2.2,1-2.2,2.2
									C121.9,166.1,122.9,167.1,124.1,167.1"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_9_" class="st0" d="M124.1,175.8c-5.2,0-9.2-3.7-9.2-8.4l0-2.6c0,4.7,4,8.4,9.2,8.4c5.2,0,9.2-3.7,9.2-8.4
										l0,0.3v2.3C133.3,172.1,129.3,175.8,124.1,175.8z"/>
								</defs>
								<clipPath id="SVGID_10_">
									<use xlink:href="#SVGID_9_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st17">
									<path class="st14" d="M133.3,164.8v2.6c0,0.4,0,0.8-0.1,1.2V166C133.3,165.6,133.3,165.2,133.3,164.8"/>
									<path class="st13" d="M133.2,166v2.6c-0.1,0.5-0.2,0.9-0.3,1.3v-2.6C133,166.9,133.2,166.4,133.2,166"/>
									<path class="st12" d="M132.9,167.3v2.6c-0.2,0.5-0.4,1-0.6,1.4v-2.6C132.5,168.3,132.7,167.8,132.9,167.3"/>
									<path class="st11" d="M132.3,168.8v2.6c-1.5,2.7-4.6,4.5-8.1,4.5c-5.2,0-9.2-3.7-9.2-8.4v-2.6c0,4.7,4,8.4,9.2,8.4
										C127.7,173.2,130.7,171.4,132.3,168.8"/>
								</g>
							</g>
							<g>
								<path class="st3" d="M124.1,155.3c5.7,0,10.2,4.2,10.2,9.4c0,5.3-4.5,9.4-10.2,9.4c-5.7,0-10.2-4.1-10.2-9.4
									C113.9,159.5,118.4,155.3,124.1,155.3z M124.1,173.2c5.2,0,9.2-3.7,9.2-8.4c0-4.7-4-8.4-9.2-8.4c-5.2,0-9.2,3.7-9.2,8.4
									C114.9,169.5,119,173.2,124.1,173.2"/>
							</g>
							<g>
								<path class="st4" d="M124.1,156.3c5.2,0,9.2,3.7,9.2,8.4c0,4.7-4,8.4-9.2,8.4c-5.2,0-9.2-3.7-9.2-8.4
									C114.9,160,119,156.3,124.1,156.3z M124.1,168.1c1.8,0,3.2-1.4,3.2-3.2c0-1.8-1.4-3.2-3.2-3.2c-1.8,0-3.2,1.4-3.2,3.2
									C120.9,166.6,122.4,168.1,124.1,168.1"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_11_" class="st0" d="M134.3,164.8v2.6c0,5.3-4.5,9.4-10.2,9.4c-5.7,0-10.2-4.1-10.2-9.4l0-2.6
										c0,5.3,4.5,9.4,10.2,9.4C129.8,174.2,134.3,170.1,134.3,164.8z"/>
								</defs>
								<clipPath id="SVGID_12_">
									<use xlink:href="#SVGID_11_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st18">
									<path class="st9" d="M134.3,164.8v2.6c0,0.5,0,0.9-0.1,1.4v-2.6C134.3,165.7,134.3,165.2,134.3,164.8"/>
									<path class="st8" d="M134.2,166.1v2.6c-0.1,0.5-0.2,1-0.4,1.5v-2.6C134,167.2,134.1,166.7,134.2,166.1"/>
									<path class="st7" d="M133.9,167.6v2.6c-0.2,0.6-0.4,1.1-0.7,1.6v-2.6C133.4,168.7,133.7,168.2,133.9,167.6"/>
									<path class="st6" d="M133.1,169.3v2.6c-1.7,3-5.1,5-9,5c-5.7,0-10.2-4.1-10.2-9.4v-2.6c0,5.3,4.5,9.4,10.2,9.4
										C128.1,174.2,131.4,172.2,133.1,169.3"/>
								</g>
							</g>
						</g>
						<g class="st0">
							<g>
								<rect x="135.3" y="156.6" class="st1" width="15.8" height="2.6"/>
							</g>
							<g>
								<rect x="135.3" y="161.5" class="st2" width="4.9" height="2.6"/>
							</g>
							<g>
								<rect x="146.3" y="161.5" class="st2" width="4.8" height="2.6"/>
							</g>
							<g>
								<rect x="147.3" y="162.5" class="st1" width="4.8" height="2.6"/>
							</g>
							<g>
								<rect x="134.3" y="162.5" class="st1" width="4.9" height="2.6"/>
							</g>
							<g>
								<rect x="140.2" y="173" class="st2" width="6.2" height="2.6"/>
							</g>
							<g>
								<path class="st3" d="M134.3,155.6h17.8v6.9h-4.8V174h-8.2v-11.5h-4.9V155.6z M151.2,161.5v-4.9h-15.8v4.9h4.9V173h6.2v-11.5
									H151.2"/>
							</g>
							<g>
								<polygon class="st4" points="151.2,156.6 151.2,161.5 146.3,161.5 146.3,173 140.2,173 140.2,161.5 135.3,161.5 
									135.3,156.6 								"/>
							</g>
							<g>
								<rect x="139.2" y="174" class="st1" width="8.2" height="2.6"/>
							</g>
						</g>
						<g class="st0">
							<g>
								<rect x="154" y="156.6" class="st1" width="11.5" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="161.4" class="st2" width="5.4" height="2.6"/>
							</g>
							<g>
								<rect x="165.5" y="162.4" class="st1" width="1" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="163.3" class="st1" width="4.3" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="166.5" class="st2" width="4.3" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="168.4" class="st1" width="5.7" height="2.6"/>
							</g>
							<g>
								<polygon class="st4" points="165.5,156.6 165.5,161.4 160.2,161.4 160.2,163.3 164.5,163.3 164.5,166.5 160.2,166.5 
									160.2,168.4 165.9,168.4 165.9,173 154,173 154,156.6 								"/>
							</g>
							<g>
								<rect x="154" y="173" class="st2" width="11.8" height="2.6"/>
							</g>
							<g>
								<rect x="153" y="174" class="st1" width="13.8" height="2.6"/>
							</g>
							<g>
								<path class="st3" d="M165.5,167.4h1.4v6.5H153v-18.4h13.5v6.8h-1V167.4z M165.9,173v-4.5h-5.7v-2h4.3v-3.1h-4.3v-1.9h5.4
									v-4.8H154V173H165.9"/>
							</g>
						</g>
						<g class="st0">
							<g class="st0">
								<defs>
									<path id="SVGID_13_" class="st0" d="M180.5,157.5l0,2.6c-1.5-0.7-3.8-1.2-5.7-1.2c-4,0-6.6,2.1-6.6,5.4l0-2.6
										c0-3.3,2.6-5.4,6.6-5.4C176.7,156.3,178.9,156.8,180.5,157.5z"/>
								</defs>
								<clipPath id="SVGID_14_">
									<use xlink:href="#SVGID_13_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st19">
									<path class="st6" d="M180.5,157.5v2.6c-1.5-0.7-3.8-1.2-5.7-1.2c-2.8,0-4.9,1-5.9,2.8v-2.6c1-1.8,3.1-2.8,5.9-2.8
										C176.7,156.3,178.9,156.8,180.5,157.5"/>
									<path class="st7" d="M168.8,159.1v2.6c-0.2,0.3-0.3,0.6-0.4,0.9V160C168.6,159.7,168.7,159.4,168.8,159.1"/>
									<path class="st8" d="M168.5,160v2.6c-0.1,0.3-0.2,0.6-0.2,0.9v-2.6C168.3,160.5,168.4,160.3,168.5,160"/>
									<path class="st9" d="M168.3,160.8v2.6c0,0.3-0.1,0.6-0.1,0.8v-2.6C168.2,161.4,168.2,161.1,168.3,160.8"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_15_" class="st0" d="M175.7,163.5c-1.3,0-1.4,0.8-1.4,1v-2.6c0-0.2,0.1-1,1.4-1c1,0,2.8,0.4,4.8,1.2v2.6
										C178.5,163.9,176.7,163.5,175.7,163.5z"/>
								</defs>
								<clipPath id="SVGID_16_">
									<use xlink:href="#SVGID_15_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st20">
									<path class="st11" d="M180.5,162.1v2.6c-1.9-0.9-3.8-1.2-4.8-1.2c-0.8,0-1.1,0.3-1.3,0.5v-2.6c0.2-0.3,0.5-0.5,1.3-0.5
										C176.7,160.8,178.5,161.2,180.5,162.1"/>
									<path class="st12" d="M174.5,161.4v2.6c0,0.1-0.1,0.1-0.1,0.2v-2.6C174.4,161.5,174.4,161.5,174.5,161.4"/>
									<path class="st13" d="M174.4,161.6v2.6c0,0.1,0,0.1,0,0.2v-2.6C174.3,161.7,174.4,161.6,174.4,161.6"/>
									<path class="st14" d="M174.3,161.7v2.6c0,0.1,0,0.1,0,0.1v-2.6C174.3,161.8,174.3,161.8,174.3,161.7"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_17_" class="st0" d="M175.2,163v2.6c-0.6-0.2-0.9-0.6-0.9-1.1l0-2.6C174.3,162.4,174.6,162.7,175.2,163z"/>
								</defs>
								<clipPath id="SVGID_18_">
									<use xlink:href="#SVGID_17_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st21">
									<path class="st6" d="M175.2,163v2.6c-0.6-0.2-0.9-0.6-0.9-1.1v-2.6C174.3,162.4,174.6,162.7,175.2,163"/>
								</g>
							</g>
							<g>
								<polygon class="st1" points="181.5,163.7 181.5,166.3 180.7,165.9 180.7,163.3 								"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_19_" class="st0" d="M169.6,167.5c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6c0,1.4,0.4,2.4,1.4,3.2
										C169.6,165.8,169.6,166.7,169.6,167.5c0-0.9,0-1.7,0-2.6c1.4,1.2,3.2,1.6,4.3,1.8c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										C172.8,169.1,171,168.8,169.6,167.5z"/>
								</defs>
								<clipPath id="SVGID_20_">
									<use xlink:href="#SVGID_19_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st22">
									<path class="st11" d="M174.4,166.9v2.6c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6
										c0,1.4,0.4,2.4,1.4,3.2c1.4,1.2,3.2,1.6,4.3,1.8C174.1,166.8,174.3,166.8,174.4,166.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_21_" class="st0" d="M174.4,169.5c-0.1,0-0.3-0.1-0.5-0.1c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0.2,0,0.4,0.1,0.5,0.1C174.4,167.7,174.4,168.6,174.4,169.5z"/>
								</defs>
								<clipPath id="SVGID_22_">
									<use xlink:href="#SVGID_21_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st23">
									<path class="st11" d="M174.4,166.9v2.6c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6
										c0,1.4,0.4,2.4,1.4,3.2c1.4,1.2,3.2,1.6,4.3,1.8C174.1,166.8,174.3,166.8,174.4,166.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_23_" class="st0" d="M173.7,167.7v2.6c-1.1-0.2-3.2-0.6-4.8-2c-1.2-1-1.7-2.3-1.7-4l0-2.6
										c0,1.7,0.6,3,1.7,4C170.5,167.1,172.5,167.5,173.7,167.7z"/>
								</defs>
								<clipPath id="SVGID_24_">
									<use xlink:href="#SVGID_23_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st24">
									<path class="st6" d="M173.7,167.7v2.6c-1.1-0.2-3.2-0.6-4.8-2c-1.2-1-1.7-2.3-1.7-4v-2.6c0,1.7,0.6,3,1.7,4
										C170.5,167.1,172.5,167.5,173.7,167.7"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_25_" class="st0" d="M174.2,167.8v2.6c-0.1,0-0.3-0.1-0.5-0.1v-2.6C173.9,167.8,174,167.8,174.2,167.8z"/>
								</defs>
								<clipPath id="SVGID_26_">
									<use xlink:href="#SVGID_25_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st25">
									<path class="st6" d="M174.2,167.8v2.6c-0.1,0-0.3-0.1-0.5-0.1v-2.6C173.9,167.8,174,167.8,174.2,167.8"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_27_" class="st0" d="M175.3,170.5c0-0.3-0.1-0.9-0.9-1c0-0.8,0-1.5,0-2.3v-0.3c0.8,0.2,0.9,0.7,0.9,1v0.3
										C175.3,168.9,175.3,169.8,175.3,170.5z"/>
								</defs>
								<clipPath id="SVGID_28_">
									<use xlink:href="#SVGID_27_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st26">
									<path class="st11" d="M175.3,167.9v2.6c0-0.3-0.1-0.9-0.9-1v-2.6C175.2,167,175.3,167.6,175.3,167.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_29_" class="st0" d="M181.5,168.1v2.6c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2
										V163c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6C180.9,165.6,181.5,166.7,181.5,168.1z"/>
								</defs>
								<clipPath id="SVGID_30_">
									<use xlink:href="#SVGID_29_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st27">
									<path class="st6" d="M181.5,168.1v2.6c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2V163
										c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6C180.9,165.6,181.5,166.7,181.5,168.1"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_31_" class="st0" d="M175.3,167.9l0,2.6c0,0.3-0.1,1.1-1.4,1.1c-1.2,0-3.2-0.5-5.2-1.7l0-2.6
										c2,1.1,4.1,1.7,5.2,1.7C175.2,169,175.3,168.1,175.3,167.9z"/>
								</defs>
								<clipPath id="SVGID_32_">
									<use xlink:href="#SVGID_31_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st28">
									<path class="st9" d="M175.3,167.9v2.6c0,0,0,0.1,0,0.2V168C175.3,168,175.3,167.9,175.3,167.9"/>
									<path class="st8" d="M175.3,168v2.6c0,0.1,0,0.1,0,0.2v-2.6C175.3,168.2,175.3,168.1,175.3,168"/>
									<path class="st7" d="M175.2,168.2v2.6c0,0.1,0,0.1-0.1,0.2v-2.6C175.2,168.3,175.2,168.3,175.2,168.2"/>
									<path class="st6" d="M175.2,168.4v2.6c-0.2,0.3-0.5,0.6-1.3,0.6c-1.2,0-3.2-0.5-5.2-1.7v-2.6c2,1.1,4.1,1.7,5.2,1.7
										C174.7,169,175,168.7,175.2,168.4"/>
								</g>
							</g>
							<g>
								<polygon class="st1" points="168,172.7 168,175.4 167.7,175.2 167.7,172.6 								"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_33_" class="st0" d="M181.5,170.7c0,3.2-2.6,5.2-6.7,5.2c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c4.2,0,6.7-2,6.7-5.2v0.3C181.5,169.1,181.5,170,181.5,170.7z"/>
								</defs>
								<clipPath id="SVGID_34_">
									<use xlink:href="#SVGID_33_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st29">
									<path class="st14" d="M181.5,168.1v2.6c0,0.3,0,0.5-0.1,0.8v-2.6C181.4,168.6,181.5,168.3,181.5,168.1"/>
									<path class="st13" d="M181.4,168.9v2.6c0,0.3-0.1,0.5-0.2,0.8v-2.6C181.3,169.4,181.4,169.1,181.4,168.9"/>
									<path class="st12" d="M181.2,169.7v2.6c-0.1,0.3-0.2,0.5-0.4,0.8v-2.6C181,170.2,181.1,169.9,181.2,169.7"/>
									<path class="st11" d="M180.9,170.5v2.6c-1,1.7-3.2,2.8-6.1,2.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2
										C177.7,173.2,179.9,172.2,180.9,170.5"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_35_" class="st0" d="M174.7,175.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2c0,0,0,0,0,0
										c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										c0,0.1,0,0.2,0,0.3C174.7,175.6,174.7,175.7,174.7,175.8C174.7,175.8,174.7,175.8,174.7,175.8z"/>
								</defs>
								<clipPath id="SVGID_36_">
									<use xlink:href="#SVGID_35_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st30">
									<path class="st11" d="M180.9,170.5v2.6c-1,1.7-3.2,2.8-6.1,2.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2
										C177.7,173.2,179.9,172.2,180.9,170.5"/>
								</g>
							</g>
							<g>
								<path class="st4" d="M174.8,156.3c1.9,0,4.2,0.5,5.7,1.2v4.6c-1.9-0.9-3.8-1.2-4.8-1.2c-1.3,0-1.4,0.8-1.4,1
									c0,0.5,0.3,0.9,0.9,1.1c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6c1.1,0.9,1.6,2,1.6,3.4c0,3.2-2.6,5.2-6.8,5.2
									c-2.1,0-4.4-0.5-6-1.2v-4.7c2,1.1,4.1,1.7,5.2,1.7c1.3,0,1.4-0.8,1.4-1.1c0-0.3-0.1-0.9-0.9-1c-0.1,0-0.3-0.1-0.5-0.1
									c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2C168.2,158.4,170.8,156.3,174.8,156.3z"/>
							</g>
							<g>
								<path class="st3" d="M180.5,163.9c1.3,1.1,1.9,2.4,1.9,4.1c0,3.7-3,6.2-7.7,6.2c-2.4,0-5-0.6-6.7-1.5l-0.3-0.1v-7.1l0.8,0.5
									c2.2,1.5,4.5,2,5.4,2c0.3,0,0.4-0.1,0.4-0.1c0,0,0,0-0.2-0.1c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-3.2-0.6-4.8-2
									c-1.2-1-1.7-2.3-1.7-4c0-3.9,3-6.4,7.6-6.4c2.2,0,4.8,0.6,6.4,1.4l0.3,0.1v6.8l-0.7-0.4c-2.2-1.1-4.2-1.5-5-1.5
									c-0.3,0-0.4,0.1-0.4,0.1c0,0,0.1,0,0.2,0.1c0.1,0,0.3,0.1,0.5,0.1C177.1,162.4,179.1,162.8,180.5,163.9z M174.7,173.2
									c4.2,0,6.8-2,6.8-5.2c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2c-0.6-0.2-0.9-0.6-0.9-1.1
									c0-0.2,0.1-1,1.4-1c1,0,2.8,0.4,4.8,1.2v-4.6c-1.5-0.7-3.8-1.2-5.7-1.2c-4,0-6.6,2.1-6.6,5.4c0,1.4,0.4,2.4,1.4,3.2
									c1.4,1.2,3.2,1.6,4.3,1.8c0.2,0,0.4,0.1,0.5,0.1c0.8,0.2,0.9,0.7,0.9,1c0,0.3-0.1,1.1-1.4,1.1c-1.2,0-3.2-0.5-5.2-1.7v4.7
									C170.3,172.7,172.6,173.2,174.7,173.2"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_37_" class="st0" d="M182.5,168.1l0,2.6c0,3.7-3,6.2-7.7,6.2c-2.4,0-5-0.6-6.7-1.5v-2.6
										c1.7,0.9,4.4,1.5,6.7,1.5C179.4,174.2,182.5,171.8,182.5,168.1z"/>
								</defs>
								<clipPath id="SVGID_38_">
									<use xlink:href="#SVGID_37_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st31">
									<path class="st9" d="M182.5,168.1v2.6c0,0.3,0,0.6-0.1,1V169C182.4,168.7,182.5,168.4,182.5,168.1"/>
									<path class="st8" d="M182.4,169v2.6c-0.1,0.3-0.1,0.7-0.2,1V170C182.3,169.7,182.3,169.3,182.4,169"/>
									<path class="st7" d="M182.2,170v2.6c-0.1,0.3-0.3,0.7-0.4,1v-2.6C181.9,170.6,182,170.3,182.2,170"/>
									<path class="st6" d="M181.7,170.9v2.6c-1.2,2-3.7,3.3-7,3.3c-2.4,0-5-0.6-6.7-1.5v-2.6c1.7,0.9,4.4,1.5,6.7,1.5
										C178.1,174.2,180.6,173,181.7,170.9"/>
								</g>
							</g>
						</g>
					</g>
					<defs>
						<filter id="Adobe_OpacityMaskFilter" filterUnits="userSpaceOnUse" x="94.1" y="155.3" width="88.4" height="21.5">
							<feFlood  style="flood-color:white;flood-opacity:1" result="back"/>
							<feBlend  in="SourceGraphic" in2="back" mode="normal"/>
						</filter>
					</defs>
					<mask maskUnits="userSpaceOnUse" x="94.1" y="155.3" width="88.4" height="21.5" id="SVGID_39_">
						<g class="st32">
							
								<image style="overflow:visible;" width="93" height="27" xlink:href="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgEASABIAAD/7AARRHVja3kAAQAEAAAAHgAA/+4AIUFkb2JlAGTAAAAAAQMA
EAMCAwYAAAF0AAABfwAAAaD/2wCEABALCwsMCxAMDBAXDw0PFxsUEBAUGx8XFxcXFx8eFxoaGhoX
Hh4jJSclIx4vLzMzLy9AQEBAQEBAQEBAQEBAQEABEQ8PERMRFRISFRQRFBEUGhQWFhQaJhoaHBoa
JjAjHh4eHiMwKy4nJycuKzU1MDA1NUBAP0BAQEBAQEBAQEBAQP/CABEIABsAXQMBIgACEQEDEQH/
xABfAAEBAQAAAAAAAAAAAAAAAAAAAQcBAQAAAAAAAAAAAAAAAAAAAAAQAQEAAAAAAAAAAAAAAAAA
ADBQEQEAAAAAAAAAAAAAAAAAAABQEgEAAAAAAAAAAAAAAAAAAAAw/9oADAMBAAIRAxEAAADPykWA
AABYAAAAAAf/2gAIAQIAAQUAT//aAAgBAwABBQBP/9oACAEBAAEFAKH/2gAIAQICBj8AT//aAAgB
AwIGPwBP/9oACAEBAQY/AEP/2Q==" transform="matrix(1 0 0 1 91.75 152.5)">
							</image>
						</g>
					</mask>
					<g class="st33">
						<g class="st0">
							<g>
								<rect x="95.1" y="156.6" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<rect x="106.8" y="156.6" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<polygon class="st34" points="105.8,163.8 105.8,166.4 100.8,159.2 100.8,156.6 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.8,163.8 106.8,166.4 106.1,166.7 106.1,164.1 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.1,164.1 106.1,166.7 105.8,166.4 105.8,163.8 								"/>
							</g>
							<g>
								<polygon class="st34" points="101.2,165.4 101.2,168 100.5,168.4 100.5,165.8 								"/>
							</g>
							<g>
								<polygon class="st34" points="101.5,165.8 101.5,168.4 101.2,168 101.2,165.4 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.5,173 106.5,175.6 101.5,168.4 101.5,165.8 								"/>
							</g>
							<g>
								<rect x="106.5" y="173" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<rect x="95.1" y="173" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M105.8,155.6h7.4V174h-7.3l-4.4-6.4v6.4h-7.4v-18.4h7.3l4.4,6.4V155.6z M112.2,173v-16.4h-5.4v7.2
									l-0.7,0.3l-0.3-0.3l-5-7.2h-5.7V173h5.4v-7.2l0.7-0.3l0.3,0.3l5,7.2H112.2"/>
							</g>
							<g>
								<polygon class="st34" points="112.2,156.6 112.2,173 106.5,173 101.5,165.8 101.2,165.4 100.5,165.8 100.5,173 95.1,173 
									95.1,156.6 100.8,156.6 105.8,163.8 106.1,164.1 106.8,163.8 106.8,156.6 								"/>
							</g>
							<g>
								<polygon class="st34" points="105.9,174 105.9,176.6 101.5,170.2 101.5,167.6 								"/>
							</g>
							<g>
								<rect x="105.9" y="174" class="st34" width="7.3" height="2.6"/>
							</g>
							<g>
								<rect x="94.1" y="174" class="st34" width="7.4" height="2.6"/>
							</g>
						</g>
						<g class="st0">
							<g class="st0">
								<defs>
									<path id="SVGID_40_" class="st0" d="M133.3,164.8l0,2.6c0-4.7-4-8.4-9.2-8.4c-5.2,0-9.2,3.7-9.2,8.4l0-2.6
										c0-4.7,4-8.4,9.2-8.4C129.3,156.3,133.3,160,133.3,164.8z"/>
								</defs>
								<clipPath id="SVGID_41_">
									<use xlink:href="#SVGID_40_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st35">
									<path class="st34" d="M133.3,164.8v2.6c0-4.7-4-8.4-9.2-8.4c-3.6,0-6.6,1.8-8.1,4.5v-2.6c1.5-2.7,4.6-4.5,8.1-4.5
										C129.3,156.3,133.3,160,133.3,164.8"/>
									<path class="st34" d="M116,160.8v2.6c-0.3,0.5-0.5,0.9-0.6,1.4v-2.6C115.5,161.7,115.7,161.3,116,160.8"/>
									<path class="st34" d="M115.3,162.2v2.6c-0.1,0.4-0.3,0.9-0.3,1.3v-2.6C115.1,163.1,115.2,162.7,115.3,162.2"/>
									<path class="st34" d="M115,163.6v2.6c-0.1,0.4-0.1,0.8-0.1,1.2v-2.6C114.9,164.4,115,164,115,163.6"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_42_" class="st0" d="M120.9,167.5c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3v-0.3c0-1.8,1.4-3.2,3.2-3.2
										c1.8,0,3.2,1.4,3.2,3.2l0,2.6c0-1.8-1.4-3.2-3.2-3.2C122.4,164.3,120.9,165.8,120.9,167.5z"/>
								</defs>
								<clipPath id="SVGID_43_">
									<use xlink:href="#SVGID_42_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st36">
									<path class="st34" d="M127.3,164.9v2.6c0-1.8-1.4-3.2-3.2-3.2c-1.2,0-2.2,0.7-2.8,1.6v-2.6c0.5-1,1.6-1.6,2.8-1.6
										C125.9,161.7,127.3,163.1,127.3,164.9"/>
									<path class="st34" d="M121.3,163.3v2.6c-0.1,0.2-0.2,0.4-0.3,0.6v-2.6C121.2,163.7,121.2,163.5,121.3,163.3"/>
									<path class="st34" d="M121.1,163.9v2.6c-0.1,0.2-0.1,0.3-0.1,0.5v-2.6C121,164.2,121,164.1,121.1,163.9"/>
									<path class="st34" d="M121,164.4v2.6c0,0.2,0,0.3,0,0.5v-2.6C120.9,164.7,120.9,164.6,121,164.4"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_44_" class="st0" d="M126.3,164.9l0,2.6c0-1.2-1-2.2-2.2-2.2c-1.2,0-2.2,1-2.2,2.2l0-2.6
										c0-1.2,1-2.2,2.2-2.2C125.3,162.7,126.3,163.7,126.3,164.9z"/>
								</defs>
								<clipPath id="SVGID_45_">
									<use xlink:href="#SVGID_44_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st37">
									<path class="st34" d="M126.3,164.9v2.6c0-1.2-1-2.2-2.2-2.2c-0.8,0-1.5,0.4-1.9,1.1v-2.6c0.4-0.7,1.1-1.1,1.9-1.1
										C125.3,162.7,126.3,163.7,126.3,164.9"/>
									<path class="st34" d="M122.2,163.8v2.6c-0.1,0.1-0.1,0.3-0.2,0.4v-2.6C122.1,164.1,122.1,163.9,122.2,163.8"/>
									<path class="st34" d="M122,164.2v2.6c0,0.1-0.1,0.2-0.1,0.4v-2.6C122,164.4,122,164.3,122,164.2"/>
									<path class="st34" d="M121.9,164.6v2.6c0,0.1,0,0.2,0,0.3v-2.6C121.9,164.8,121.9,164.7,121.9,164.6"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_46_" class="st0" d="M127.3,164.9l0,2.6c0,1.8-1.4,3.2-3.2,3.2c-1.8,0-3.2-1.4-3.2-3.2v-2.6
										c0,1.8,1.4,3.2,3.2,3.2C125.9,168.1,127.3,166.6,127.3,164.9z"/>
								</defs>
								<clipPath id="SVGID_47_">
									<use xlink:href="#SVGID_46_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st38">
									<path class="st34" d="M127.3,164.9v2.6c0,0.2,0,0.3,0,0.5v-2.6C127.3,165.2,127.3,165.1,127.3,164.9"/>
									<path class="st34" d="M127.3,165.4v2.6c0,0.2-0.1,0.4-0.1,0.5v-2.6C127.2,165.7,127.2,165.5,127.3,165.4"/>
									<path class="st34" d="M127.1,165.9v2.6c-0.1,0.2-0.2,0.4-0.3,0.6v-2.6C127,166.3,127.1,166.1,127.1,165.9"/>
									<path class="st34" d="M126.9,166.5v2.6c-0.5,1-1.6,1.6-2.8,1.6c-1.8,0-3.2-1.4-3.2-3.2v-2.6c0,1.8,1.4,3.2,3.2,3.2
										C125.3,168.1,126.3,167.4,126.9,166.5"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M124.1,161.7c1.8,0,3.2,1.4,3.2,3.2c0,1.8-1.4,3.2-3.2,3.2c-1.8,0-3.2-1.4-3.2-3.2
									C120.9,163.1,122.4,161.7,124.1,161.7z M124.1,167.1c1.2,0,2.2-1,2.2-2.2c0-1.2-1-2.2-2.2-2.2c-1.2,0-2.2,1-2.2,2.2
									C121.9,166.1,122.9,167.1,124.1,167.1"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_48_" class="st0" d="M124.1,175.8c-5.2,0-9.2-3.7-9.2-8.4l0-2.6c0,4.7,4,8.4,9.2,8.4c5.2,0,9.2-3.7,9.2-8.4
										l0,0.3v2.3C133.3,172.1,129.3,175.8,124.1,175.8z"/>
								</defs>
								<clipPath id="SVGID_49_">
									<use xlink:href="#SVGID_48_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st39">
									<path class="st34" d="M133.3,164.8v2.6c0,0.4,0,0.8-0.1,1.2V166C133.3,165.6,133.3,165.2,133.3,164.8"/>
									<path class="st34" d="M133.2,166v2.6c-0.1,0.5-0.2,0.9-0.3,1.3v-2.6C133,166.9,133.2,166.4,133.2,166"/>
									<path class="st34" d="M132.9,167.3v2.6c-0.2,0.5-0.4,1-0.6,1.4v-2.6C132.5,168.3,132.7,167.8,132.9,167.3"/>
									<path class="st34" d="M132.3,168.8v2.6c-1.5,2.7-4.6,4.5-8.1,4.5c-5.2,0-9.2-3.7-9.2-8.4v-2.6c0,4.7,4,8.4,9.2,8.4
										C127.7,173.2,130.7,171.4,132.3,168.8"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M124.1,155.3c5.7,0,10.2,4.2,10.2,9.4c0,5.3-4.5,9.4-10.2,9.4c-5.7,0-10.2-4.1-10.2-9.4
									C113.9,159.5,118.4,155.3,124.1,155.3z M124.1,173.2c5.2,0,9.2-3.7,9.2-8.4c0-4.7-4-8.4-9.2-8.4c-5.2,0-9.2,3.7-9.2,8.4
									C114.9,169.5,119,173.2,124.1,173.2"/>
							</g>
							<g>
								<path class="st34" d="M124.1,156.3c5.2,0,9.2,3.7,9.2,8.4c0,4.7-4,8.4-9.2,8.4c-5.2,0-9.2-3.7-9.2-8.4
									C114.9,160,119,156.3,124.1,156.3z M124.1,168.1c1.8,0,3.2-1.4,3.2-3.2c0-1.8-1.4-3.2-3.2-3.2c-1.8,0-3.2,1.4-3.2,3.2
									C120.9,166.6,122.4,168.1,124.1,168.1"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_50_" class="st0" d="M134.3,164.8v2.6c0,5.3-4.5,9.4-10.2,9.4c-5.7,0-10.2-4.1-10.2-9.4l0-2.6
										c0,5.3,4.5,9.4,10.2,9.4C129.8,174.2,134.3,170.1,134.3,164.8z"/>
								</defs>
								<clipPath id="SVGID_51_">
									<use xlink:href="#SVGID_50_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st40">
									<path class="st34" d="M134.3,164.8v2.6c0,0.5,0,0.9-0.1,1.4v-2.6C134.3,165.7,134.3,165.2,134.3,164.8"/>
									<path class="st34" d="M134.2,166.1v2.6c-0.1,0.5-0.2,1-0.4,1.5v-2.6C134,167.2,134.1,166.7,134.2,166.1"/>
									<path class="st34" d="M133.9,167.6v2.6c-0.2,0.6-0.4,1.1-0.7,1.6v-2.6C133.4,168.7,133.7,168.2,133.9,167.6"/>
									<path class="st34" d="M133.1,169.3v2.6c-1.7,3-5.1,5-9,5c-5.7,0-10.2-4.1-10.2-9.4v-2.6c0,5.3,4.5,9.4,10.2,9.4
										C128.1,174.2,131.4,172.2,133.1,169.3"/>
								</g>
							</g>
						</g>
						<g class="st0">
							<g>
								<rect x="135.3" y="156.6" class="st34" width="15.8" height="2.6"/>
							</g>
							<g>
								<rect x="135.3" y="161.5" class="st34" width="4.9" height="2.6"/>
							</g>
							<g>
								<rect x="146.3" y="161.5" class="st34" width="4.8" height="2.6"/>
							</g>
							<g>
								<rect x="147.3" y="162.5" class="st34" width="4.8" height="2.6"/>
							</g>
							<g>
								<rect x="134.3" y="162.5" class="st34" width="4.9" height="2.6"/>
							</g>
							<g>
								<rect x="140.2" y="173" class="st34" width="6.2" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M134.3,155.6h17.8v6.9h-4.8V174h-8.2v-11.5h-4.9V155.6z M151.2,161.5v-4.9h-15.8v4.9h4.9V173h6.2
									v-11.5H151.2"/>
							</g>
							<g>
								<polygon class="st34" points="151.2,156.6 151.2,161.5 146.3,161.5 146.3,173 140.2,173 140.2,161.5 135.3,161.5 
									135.3,156.6 								"/>
							</g>
							<g>
								<rect x="139.2" y="174" class="st34" width="8.2" height="2.6"/>
							</g>
						</g>
						<g class="st0">
							<g>
								<rect x="154" y="156.6" class="st34" width="11.5" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="161.4" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<rect x="165.5" y="162.4" class="st34" width="1" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="163.3" class="st34" width="4.3" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="166.5" class="st34" width="4.3" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="168.4" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<polygon class="st34" points="165.5,156.6 165.5,161.4 160.2,161.4 160.2,163.3 164.5,163.3 164.5,166.5 160.2,166.5 
									160.2,168.4 165.9,168.4 165.9,173 154,173 154,156.6 								"/>
							</g>
							<g>
								<rect x="154" y="173" class="st34" width="11.8" height="2.6"/>
							</g>
							<g>
								<rect x="153" y="174" class="st34" width="13.8" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M165.5,167.4h1.4v6.5H153v-18.4h13.5v6.8h-1V167.4z M165.9,173v-4.5h-5.7v-2h4.3v-3.1h-4.3v-1.9h5.4
									v-4.8H154V173H165.9"/>
							</g>
						</g>
						<g class="st0">
							<g class="st0">
								<defs>
									<path id="SVGID_52_" class="st0" d="M180.5,157.5l0,2.6c-1.5-0.7-3.8-1.2-5.7-1.2c-4,0-6.6,2.1-6.6,5.4l0-2.6
										c0-3.3,2.6-5.4,6.6-5.4C176.7,156.3,178.9,156.8,180.5,157.5z"/>
								</defs>
								<clipPath id="SVGID_53_">
									<use xlink:href="#SVGID_52_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st41">
									<path class="st34" d="M180.5,157.5v2.6c-1.5-0.7-3.8-1.2-5.7-1.2c-2.8,0-4.9,1-5.9,2.8v-2.6c1-1.8,3.1-2.8,5.9-2.8
										C176.7,156.3,178.9,156.8,180.5,157.5"/>
									<path class="st34" d="M168.8,159.1v2.6c-0.2,0.3-0.3,0.6-0.4,0.9V160C168.6,159.7,168.7,159.4,168.8,159.1"/>
									<path class="st34" d="M168.5,160v2.6c-0.1,0.3-0.2,0.6-0.2,0.9v-2.6C168.3,160.5,168.4,160.3,168.5,160"/>
									<path class="st34" d="M168.3,160.8v2.6c0,0.3-0.1,0.6-0.1,0.8v-2.6C168.2,161.4,168.2,161.1,168.3,160.8"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_54_" class="st0" d="M175.7,163.5c-1.3,0-1.4,0.8-1.4,1v-2.6c0-0.2,0.1-1,1.4-1c1,0,2.8,0.4,4.8,1.2v2.6
										C178.5,163.9,176.7,163.5,175.7,163.5z"/>
								</defs>
								<clipPath id="SVGID_55_">
									<use xlink:href="#SVGID_54_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st42">
									<path class="st34" d="M180.5,162.1v2.6c-1.9-0.9-3.8-1.2-4.8-1.2c-0.8,0-1.1,0.3-1.3,0.5v-2.6c0.2-0.3,0.5-0.5,1.3-0.5
										C176.7,160.8,178.5,161.2,180.5,162.1"/>
									<path class="st34" d="M174.5,161.4v2.6c0,0.1-0.1,0.1-0.1,0.2v-2.6C174.4,161.5,174.4,161.5,174.5,161.4"/>
									<path class="st34" d="M174.4,161.6v2.6c0,0.1,0,0.1,0,0.2v-2.6C174.3,161.7,174.4,161.6,174.4,161.6"/>
									<path class="st34" d="M174.3,161.7v2.6c0,0.1,0,0.1,0,0.1v-2.6C174.3,161.8,174.3,161.8,174.3,161.7"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_56_" class="st0" d="M175.2,163v2.6c-0.6-0.2-0.9-0.6-0.9-1.1l0-2.6C174.3,162.4,174.6,162.7,175.2,163z"/>
								</defs>
								<clipPath id="SVGID_57_">
									<use xlink:href="#SVGID_56_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st43">
									<path class="st34" d="M175.2,163v2.6c-0.6-0.2-0.9-0.6-0.9-1.1v-2.6C174.3,162.4,174.6,162.7,175.2,163"/>
								</g>
							</g>
							<g>
								<polygon class="st34" points="181.5,163.7 181.5,166.3 180.7,165.9 180.7,163.3 								"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_58_" class="st0" d="M169.6,167.5c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6c0,1.4,0.4,2.4,1.4,3.2
										C169.6,165.8,169.6,166.7,169.6,167.5c0-0.9,0-1.7,0-2.6c1.4,1.2,3.2,1.6,4.3,1.8c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										C172.8,169.1,171,168.8,169.6,167.5z"/>
								</defs>
								<clipPath id="SVGID_59_">
									<use xlink:href="#SVGID_58_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st44">
									<path class="st34" d="M174.4,166.9v2.6c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6
										c0,1.4,0.4,2.4,1.4,3.2c1.4,1.2,3.2,1.6,4.3,1.8C174.1,166.8,174.3,166.8,174.4,166.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_60_" class="st0" d="M174.4,169.5c-0.1,0-0.3-0.1-0.5-0.1c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0.2,0,0.4,0.1,0.5,0.1C174.4,167.7,174.4,168.6,174.4,169.5z"/>
								</defs>
								<clipPath id="SVGID_61_">
									<use xlink:href="#SVGID_60_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st45">
									<path class="st34" d="M174.4,166.9v2.6c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6
										c0,1.4,0.4,2.4,1.4,3.2c1.4,1.2,3.2,1.6,4.3,1.8C174.1,166.8,174.3,166.8,174.4,166.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_62_" class="st0" d="M173.7,167.7v2.6c-1.1-0.2-3.2-0.6-4.8-2c-1.2-1-1.7-2.3-1.7-4l0-2.6
										c0,1.7,0.6,3,1.7,4C170.5,167.1,172.5,167.5,173.7,167.7z"/>
								</defs>
								<clipPath id="SVGID_63_">
									<use xlink:href="#SVGID_62_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st46">
									<path class="st34" d="M173.7,167.7v2.6c-1.1-0.2-3.2-0.6-4.8-2c-1.2-1-1.7-2.3-1.7-4v-2.6c0,1.7,0.6,3,1.7,4
										C170.5,167.1,172.5,167.5,173.7,167.7"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_64_" class="st0" d="M174.2,167.8v2.6c-0.1,0-0.3-0.1-0.5-0.1v-2.6C173.9,167.8,174,167.8,174.2,167.8z"/>
								</defs>
								<clipPath id="SVGID_65_">
									<use xlink:href="#SVGID_64_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st47">
									<path class="st34" d="M174.2,167.8v2.6c-0.1,0-0.3-0.1-0.5-0.1v-2.6C173.9,167.8,174,167.8,174.2,167.8"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_66_" class="st0" d="M175.3,170.5c0-0.3-0.1-0.9-0.9-1c0-0.8,0-1.5,0-2.3v-0.3c0.8,0.2,0.9,0.7,0.9,1v0.3
										C175.3,168.9,175.3,169.8,175.3,170.5z"/>
								</defs>
								<clipPath id="SVGID_67_">
									<use xlink:href="#SVGID_66_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st48">
									<path class="st34" d="M175.3,167.9v2.6c0-0.3-0.1-0.9-0.9-1v-2.6C175.2,167,175.3,167.6,175.3,167.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_68_" class="st0" d="M181.5,168.1v2.6c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2
										V163c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6C180.9,165.6,181.5,166.7,181.5,168.1z"/>
								</defs>
								<clipPath id="SVGID_69_">
									<use xlink:href="#SVGID_68_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st49">
									<path class="st34" d="M181.5,168.1v2.6c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2V163
										c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6C180.9,165.6,181.5,166.7,181.5,168.1"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_70_" class="st0" d="M175.3,167.9l0,2.6c0,0.3-0.1,1.1-1.4,1.1c-1.2,0-3.2-0.5-5.2-1.7l0-2.6
										c2,1.1,4.1,1.7,5.2,1.7C175.2,169,175.3,168.1,175.3,167.9z"/>
								</defs>
								<clipPath id="SVGID_71_">
									<use xlink:href="#SVGID_70_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st50">
									<path class="st34" d="M175.3,167.9v2.6c0,0,0,0.1,0,0.2V168C175.3,168,175.3,167.9,175.3,167.9"/>
									<path class="st34" d="M175.3,168v2.6c0,0.1,0,0.1,0,0.2v-2.6C175.3,168.2,175.3,168.1,175.3,168"/>
									<path class="st34" d="M175.2,168.2v2.6c0,0.1,0,0.1-0.1,0.2v-2.6C175.2,168.3,175.2,168.3,175.2,168.2"/>
									<path class="st34" d="M175.2,168.4v2.6c-0.2,0.3-0.5,0.6-1.3,0.6c-1.2,0-3.2-0.5-5.2-1.7v-2.6c2,1.1,4.1,1.7,5.2,1.7
										C174.7,169,175,168.7,175.2,168.4"/>
								</g>
							</g>
							<g>
								<polygon class="st34" points="168,172.7 168,175.4 167.7,175.2 167.7,172.6 								"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_72_" class="st0" d="M181.5,170.7c0,3.2-2.6,5.2-6.7,5.2c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c4.2,0,6.7-2,6.7-5.2v0.3C181.5,169.1,181.5,170,181.5,170.7z"/>
								</defs>
								<clipPath id="SVGID_73_">
									<use xlink:href="#SVGID_72_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st51">
									<path class="st34" d="M181.5,168.1v2.6c0,0.3,0,0.5-0.1,0.8v-2.6C181.4,168.6,181.5,168.3,181.5,168.1"/>
									<path class="st34" d="M181.4,168.9v2.6c0,0.3-0.1,0.5-0.2,0.8v-2.6C181.3,169.4,181.4,169.1,181.4,168.9"/>
									<path class="st34" d="M181.2,169.7v2.6c-0.1,0.3-0.2,0.5-0.4,0.8v-2.6C181,170.2,181.1,169.9,181.2,169.7"/>
									<path class="st34" d="M180.9,170.5v2.6c-1,1.7-3.2,2.8-6.1,2.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2
										C177.7,173.2,179.9,172.2,180.9,170.5"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_74_" class="st0" d="M174.7,175.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2c0,0,0,0,0,0
										c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										c0,0.1,0,0.2,0,0.3C174.7,175.6,174.7,175.7,174.7,175.8C174.7,175.8,174.7,175.8,174.7,175.8z"/>
								</defs>
								<clipPath id="SVGID_75_">
									<use xlink:href="#SVGID_74_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st52">
									<path class="st34" d="M180.9,170.5v2.6c-1,1.7-3.2,2.8-6.1,2.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2
										C177.7,173.2,179.9,172.2,180.9,170.5"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M174.8,156.3c1.9,0,4.2,0.5,5.7,1.2v4.6c-1.9-0.9-3.8-1.2-4.8-1.2c-1.3,0-1.4,0.8-1.4,1
									c0,0.5,0.3,0.9,0.9,1.1c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6c1.1,0.9,1.6,2,1.6,3.4c0,3.2-2.6,5.2-6.8,5.2
									c-2.1,0-4.4-0.5-6-1.2v-4.7c2,1.1,4.1,1.7,5.2,1.7c1.3,0,1.4-0.8,1.4-1.1c0-0.3-0.1-0.9-0.9-1c-0.1,0-0.3-0.1-0.5-0.1
									c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2C168.2,158.4,170.8,156.3,174.8,156.3z"/>
							</g>
							<g>
								<path class="st34" d="M180.5,163.9c1.3,1.1,1.9,2.4,1.9,4.1c0,3.7-3,6.2-7.7,6.2c-2.4,0-5-0.6-6.7-1.5l-0.3-0.1v-7.1
									l0.8,0.5c2.2,1.5,4.5,2,5.4,2c0.3,0,0.4-0.1,0.4-0.1c0,0,0,0-0.2-0.1c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-3.2-0.6-4.8-2
									c-1.2-1-1.7-2.3-1.7-4c0-3.9,3-6.4,7.6-6.4c2.2,0,4.8,0.6,6.4,1.4l0.3,0.1v6.8l-0.7-0.4c-2.2-1.1-4.2-1.5-5-1.5
									c-0.3,0-0.4,0.1-0.4,0.1c0,0,0.1,0,0.2,0.1c0.1,0,0.3,0.1,0.5,0.1C177.1,162.4,179.1,162.8,180.5,163.9z M174.7,173.2
									c4.2,0,6.8-2,6.8-5.2c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2c-0.6-0.2-0.9-0.6-0.9-1.1
									c0-0.2,0.1-1,1.4-1c1,0,2.8,0.4,4.8,1.2v-4.6c-1.5-0.7-3.8-1.2-5.7-1.2c-4,0-6.6,2.1-6.6,5.4c0,1.4,0.4,2.4,1.4,3.2
									c1.4,1.2,3.2,1.6,4.3,1.8c0.2,0,0.4,0.1,0.5,0.1c0.8,0.2,0.9,0.7,0.9,1c0,0.3-0.1,1.1-1.4,1.1c-1.2,0-3.2-0.5-5.2-1.7v4.7
									C170.3,172.7,172.6,173.2,174.7,173.2"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_76_" class="st0" d="M182.5,168.1l0,2.6c0,3.7-3,6.2-7.7,6.2c-2.4,0-5-0.6-6.7-1.5v-2.6
										c1.7,0.9,4.4,1.5,6.7,1.5C179.4,174.2,182.5,171.8,182.5,168.1z"/>
								</defs>
								<clipPath id="SVGID_77_">
									<use xlink:href="#SVGID_76_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st53">
									<path class="st34" d="M182.5,168.1v2.6c0,0.3,0,0.6-0.1,1V169C182.4,168.7,182.5,168.4,182.5,168.1"/>
									<path class="st34" d="M182.4,169v2.6c-0.1,0.3-0.1,0.7-0.2,1V170C182.3,169.7,182.3,169.3,182.4,169"/>
									<path class="st34" d="M182.2,170v2.6c-0.1,0.3-0.3,0.7-0.4,1v-2.6C181.9,170.6,182,170.3,182.2,170"/>
									<path class="st34" d="M181.7,170.9v2.6c-1.2,2-3.7,3.3-7,3.3c-2.4,0-5-0.6-6.7-1.5v-2.6c1.7,0.9,4.4,1.5,6.7,1.5
										C178.1,174.2,180.6,173,181.7,170.9"/>
								</g>
							</g>
						</g>
					</g>
				</g>
				<defs>
					<filter id="Adobe_OpacityMaskFilter_1_" filterUnits="userSpaceOnUse" x="94.1" y="155.3" width="88.4" height="21.5">
						<feFlood  style="flood-color:white;flood-opacity:1" result="back"/>
						<feBlend  in="SourceGraphic" in2="back" mode="normal"/>
					</filter>
				</defs>
				<mask maskUnits="userSpaceOnUse" x="94.1" y="155.3" width="88.4" height="21.5" id="SVGID_78_">
					<g class="st54">
						
							<image style="overflow:visible;" width="93" height="27" xlink:href="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgEASABIAAD/7AARRHVja3kAAQAEAAAAHgAA/+4AIUFkb2JlAGTAAAAAAQMA
EAMCAwYAAAF0AAABfwAAAaD/2wCEABALCwsMCxAMDBAXDw0PFxsUEBAUGx8XFxcXFx8eFxoaGhoX
Hh4jJSclIx4vLzMzLy9AQEBAQEBAQEBAQEBAQEABEQ8PERMRFRISFRQRFBEUGhQWFhQaJhoaHBoa
JjAjHh4eHiMwKy4nJycuKzU1MDA1NUBAP0BAQEBAQEBAQEBAQP/CABEIABsAXQMBIgACEQEDEQH/
xABfAAEBAQAAAAAAAAAAAAAAAAAAAQcBAQAAAAAAAAAAAAAAAAAAAAAQAQEAAAAAAAAAAAAAAAAA
ADBQEQEAAAAAAAAAAAAAAAAAAABQEgEAAAAAAAAAAAAAAAAAAAAw/9oADAMBAAIRAxEAAADPykWA
AABYAAAAAAf/2gAIAQIAAQUAT//aAAgBAwABBQBP/9oACAEBAAEFAKH/2gAIAQICBj8AT//aAAgB
AwIGPwBP/9oACAEBAQY/AEP/2Q==" transform="matrix(1 0 0 1 91.75 152.5)">
						</image>
					</g>
				</mask>
				<g class="st55">
					<g>
						<g class="st0">
							<g>
								<rect x="95.1" y="156.6" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<rect x="106.8" y="156.6" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<polygon class="st34" points="105.8,163.8 105.8,166.4 100.8,159.2 100.8,156.6 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.8,163.8 106.8,166.4 106.1,166.7 106.1,164.1 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.1,164.1 106.1,166.7 105.8,166.4 105.8,163.8 								"/>
							</g>
							<g>
								<polygon class="st34" points="101.2,165.4 101.2,168 100.5,168.4 100.5,165.8 								"/>
							</g>
							<g>
								<polygon class="st34" points="101.5,165.8 101.5,168.4 101.2,168 101.2,165.4 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.5,173 106.5,175.6 101.5,168.4 101.5,165.8 								"/>
							</g>
							<g>
								<rect x="106.5" y="173" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<rect x="95.1" y="173" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M105.8,155.6h7.4V174h-7.3l-4.4-6.4v6.4h-7.4v-18.4h7.3l4.4,6.4V155.6z M112.2,173v-16.4h-5.4v7.2
									l-0.7,0.3l-0.3-0.3l-5-7.2h-5.7V173h5.4v-7.2l0.7-0.3l0.3,0.3l5,7.2H112.2"/>
							</g>
							<g>
								<polygon class="st34" points="112.2,156.6 112.2,173 106.5,173 101.5,165.8 101.2,165.4 100.5,165.8 100.5,173 95.1,173 
									95.1,156.6 100.8,156.6 105.8,163.8 106.1,164.1 106.8,163.8 106.8,156.6 								"/>
							</g>
							<g>
								<polygon class="st34" points="105.9,174 105.9,176.6 101.5,170.2 101.5,167.6 								"/>
							</g>
							<g>
								<rect x="105.9" y="174" class="st34" width="7.3" height="2.6"/>
							</g>
							<g>
								<rect x="94.1" y="174" class="st34" width="7.4" height="2.6"/>
							</g>
						</g>
						<g class="st0">
							<g class="st0">
								<defs>
									<path id="SVGID_79_" class="st0" d="M133.3,164.8l0,2.6c0-4.7-4-8.4-9.2-8.4c-5.2,0-9.2,3.7-9.2,8.4l0-2.6
										c0-4.7,4-8.4,9.2-8.4C129.3,156.3,133.3,160,133.3,164.8z"/>
								</defs>
								<clipPath id="SVGID_80_">
									<use xlink:href="#SVGID_79_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st56">
									<path class="st34" d="M133.3,164.8v2.6c0-4.7-4-8.4-9.2-8.4c-3.6,0-6.6,1.8-8.1,4.5v-2.6c1.5-2.7,4.6-4.5,8.1-4.5
										C129.3,156.3,133.3,160,133.3,164.8"/>
									<path class="st34" d="M116,160.8v2.6c-0.3,0.5-0.5,0.9-0.6,1.4v-2.6C115.5,161.7,115.7,161.3,116,160.8"/>
									<path class="st34" d="M115.3,162.2v2.6c-0.1,0.4-0.3,0.9-0.3,1.3v-2.6C115.1,163.1,115.2,162.7,115.3,162.2"/>
									<path class="st34" d="M115,163.6v2.6c-0.1,0.4-0.1,0.8-0.1,1.2v-2.6C114.9,164.4,115,164,115,163.6"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_81_" class="st0" d="M120.9,167.5c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3v-0.3c0-1.8,1.4-3.2,3.2-3.2
										c1.8,0,3.2,1.4,3.2,3.2l0,2.6c0-1.8-1.4-3.2-3.2-3.2C122.4,164.3,120.9,165.8,120.9,167.5z"/>
								</defs>
								<clipPath id="SVGID_82_">
									<use xlink:href="#SVGID_81_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st57">
									<path class="st34" d="M127.3,164.9v2.6c0-1.8-1.4-3.2-3.2-3.2c-1.2,0-2.2,0.7-2.8,1.6v-2.6c0.5-1,1.6-1.6,2.8-1.6
										C125.9,161.7,127.3,163.1,127.3,164.9"/>
									<path class="st34" d="M121.3,163.3v2.6c-0.1,0.2-0.2,0.4-0.3,0.6v-2.6C121.2,163.7,121.2,163.5,121.3,163.3"/>
									<path class="st34" d="M121.1,163.9v2.6c-0.1,0.2-0.1,0.3-0.1,0.5v-2.6C121,164.2,121,164.1,121.1,163.9"/>
									<path class="st34" d="M121,164.4v2.6c0,0.2,0,0.3,0,0.5v-2.6C120.9,164.7,120.9,164.6,121,164.4"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_83_" class="st0" d="M126.3,164.9l0,2.6c0-1.2-1-2.2-2.2-2.2c-1.2,0-2.2,1-2.2,2.2l0-2.6
										c0-1.2,1-2.2,2.2-2.2C125.3,162.7,126.3,163.7,126.3,164.9z"/>
								</defs>
								<clipPath id="SVGID_84_">
									<use xlink:href="#SVGID_83_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st58">
									<path class="st34" d="M126.3,164.9v2.6c0-1.2-1-2.2-2.2-2.2c-0.8,0-1.5,0.4-1.9,1.1v-2.6c0.4-0.7,1.1-1.1,1.9-1.1
										C125.3,162.7,126.3,163.7,126.3,164.9"/>
									<path class="st34" d="M122.2,163.8v2.6c-0.1,0.1-0.1,0.3-0.2,0.4v-2.6C122.1,164.1,122.1,163.9,122.2,163.8"/>
									<path class="st34" d="M122,164.2v2.6c0,0.1-0.1,0.2-0.1,0.4v-2.6C122,164.4,122,164.3,122,164.2"/>
									<path class="st34" d="M121.9,164.6v2.6c0,0.1,0,0.2,0,0.3v-2.6C121.9,164.8,121.9,164.7,121.9,164.6"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_85_" class="st0" d="M127.3,164.9l0,2.6c0,1.8-1.4,3.2-3.2,3.2c-1.8,0-3.2-1.4-3.2-3.2v-2.6
										c0,1.8,1.4,3.2,3.2,3.2C125.9,168.1,127.3,166.6,127.3,164.9z"/>
								</defs>
								<clipPath id="SVGID_86_">
									<use xlink:href="#SVGID_85_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st59">
									<path class="st34" d="M127.3,164.9v2.6c0,0.2,0,0.3,0,0.5v-2.6C127.3,165.2,127.3,165.1,127.3,164.9"/>
									<path class="st34" d="M127.3,165.4v2.6c0,0.2-0.1,0.4-0.1,0.5v-2.6C127.2,165.7,127.2,165.5,127.3,165.4"/>
									<path class="st34" d="M127.1,165.9v2.6c-0.1,0.2-0.2,0.4-0.3,0.6v-2.6C127,166.3,127.1,166.1,127.1,165.9"/>
									<path class="st34" d="M126.9,166.5v2.6c-0.5,1-1.6,1.6-2.8,1.6c-1.8,0-3.2-1.4-3.2-3.2v-2.6c0,1.8,1.4,3.2,3.2,3.2
										C125.3,168.1,126.3,167.4,126.9,166.5"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M124.1,161.7c1.8,0,3.2,1.4,3.2,3.2c0,1.8-1.4,3.2-3.2,3.2c-1.8,0-3.2-1.4-3.2-3.2
									C120.9,163.1,122.4,161.7,124.1,161.7z M124.1,167.1c1.2,0,2.2-1,2.2-2.2c0-1.2-1-2.2-2.2-2.2c-1.2,0-2.2,1-2.2,2.2
									C121.9,166.1,122.9,167.1,124.1,167.1"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_87_" class="st0" d="M124.1,175.8c-5.2,0-9.2-3.7-9.2-8.4l0-2.6c0,4.7,4,8.4,9.2,8.4c5.2,0,9.2-3.7,9.2-8.4
										l0,0.3v2.3C133.3,172.1,129.3,175.8,124.1,175.8z"/>
								</defs>
								<clipPath id="SVGID_88_">
									<use xlink:href="#SVGID_87_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st60">
									<path class="st34" d="M133.3,164.8v2.6c0,0.4,0,0.8-0.1,1.2V166C133.3,165.6,133.3,165.2,133.3,164.8"/>
									<path class="st34" d="M133.2,166v2.6c-0.1,0.5-0.2,0.9-0.3,1.3v-2.6C133,166.9,133.2,166.4,133.2,166"/>
									<path class="st34" d="M132.9,167.3v2.6c-0.2,0.5-0.4,1-0.6,1.4v-2.6C132.5,168.3,132.7,167.8,132.9,167.3"/>
									<path class="st34" d="M132.3,168.8v2.6c-1.5,2.7-4.6,4.5-8.1,4.5c-5.2,0-9.2-3.7-9.2-8.4v-2.6c0,4.7,4,8.4,9.2,8.4
										C127.7,173.2,130.7,171.4,132.3,168.8"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M124.1,155.3c5.7,0,10.2,4.2,10.2,9.4c0,5.3-4.5,9.4-10.2,9.4c-5.7,0-10.2-4.1-10.2-9.4
									C113.9,159.5,118.4,155.3,124.1,155.3z M124.1,173.2c5.2,0,9.2-3.7,9.2-8.4c0-4.7-4-8.4-9.2-8.4c-5.2,0-9.2,3.7-9.2,8.4
									C114.9,169.5,119,173.2,124.1,173.2"/>
							</g>
							<g>
								<path class="st34" d="M124.1,156.3c5.2,0,9.2,3.7,9.2,8.4c0,4.7-4,8.4-9.2,8.4c-5.2,0-9.2-3.7-9.2-8.4
									C114.9,160,119,156.3,124.1,156.3z M124.1,168.1c1.8,0,3.2-1.4,3.2-3.2c0-1.8-1.4-3.2-3.2-3.2c-1.8,0-3.2,1.4-3.2,3.2
									C120.9,166.6,122.4,168.1,124.1,168.1"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_89_" class="st0" d="M134.3,164.8v2.6c0,5.3-4.5,9.4-10.2,9.4c-5.7,0-10.2-4.1-10.2-9.4l0-2.6
										c0,5.3,4.5,9.4,10.2,9.4C129.8,174.2,134.3,170.1,134.3,164.8z"/>
								</defs>
								<clipPath id="SVGID_90_">
									<use xlink:href="#SVGID_89_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st61">
									<path class="st34" d="M134.3,164.8v2.6c0,0.5,0,0.9-0.1,1.4v-2.6C134.3,165.7,134.3,165.2,134.3,164.8"/>
									<path class="st34" d="M134.2,166.1v2.6c-0.1,0.5-0.2,1-0.4,1.5v-2.6C134,167.2,134.1,166.7,134.2,166.1"/>
									<path class="st34" d="M133.9,167.6v2.6c-0.2,0.6-0.4,1.1-0.7,1.6v-2.6C133.4,168.7,133.7,168.2,133.9,167.6"/>
									<path class="st34" d="M133.1,169.3v2.6c-1.7,3-5.1,5-9,5c-5.7,0-10.2-4.1-10.2-9.4v-2.6c0,5.3,4.5,9.4,10.2,9.4
										C128.1,174.2,131.4,172.2,133.1,169.3"/>
								</g>
							</g>
						</g>
						<g class="st0">
							<g>
								<rect x="135.3" y="156.6" class="st34" width="15.8" height="2.6"/>
							</g>
							<g>
								<rect x="135.3" y="161.5" class="st34" width="4.9" height="2.6"/>
							</g>
							<g>
								<rect x="146.3" y="161.5" class="st34" width="4.8" height="2.6"/>
							</g>
							<g>
								<rect x="147.3" y="162.5" class="st34" width="4.8" height="2.6"/>
							</g>
							<g>
								<rect x="134.3" y="162.5" class="st34" width="4.9" height="2.6"/>
							</g>
							<g>
								<rect x="140.2" y="173" class="st34" width="6.2" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M134.3,155.6h17.8v6.9h-4.8V174h-8.2v-11.5h-4.9V155.6z M151.2,161.5v-4.9h-15.8v4.9h4.9V173h6.2
									v-11.5H151.2"/>
							</g>
							<g>
								<polygon class="st34" points="151.2,156.6 151.2,161.5 146.3,161.5 146.3,173 140.2,173 140.2,161.5 135.3,161.5 
									135.3,156.6 								"/>
							</g>
							<g>
								<rect x="139.2" y="174" class="st34" width="8.2" height="2.6"/>
							</g>
						</g>
						<g class="st0">
							<g>
								<rect x="154" y="156.6" class="st34" width="11.5" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="161.4" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<rect x="165.5" y="162.4" class="st34" width="1" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="163.3" class="st34" width="4.3" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="166.5" class="st34" width="4.3" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="168.4" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<polygon class="st34" points="165.5,156.6 165.5,161.4 160.2,161.4 160.2,163.3 164.5,163.3 164.5,166.5 160.2,166.5 
									160.2,168.4 165.9,168.4 165.9,173 154,173 154,156.6 								"/>
							</g>
							<g>
								<rect x="154" y="173" class="st34" width="11.8" height="2.6"/>
							</g>
							<g>
								<rect x="153" y="174" class="st34" width="13.8" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M165.5,167.4h1.4v6.5H153v-18.4h13.5v6.8h-1V167.4z M165.9,173v-4.5h-5.7v-2h4.3v-3.1h-4.3v-1.9h5.4
									v-4.8H154V173H165.9"/>
							</g>
						</g>
						<g class="st0">
							<g class="st0">
								<defs>
									<path id="SVGID_91_" class="st0" d="M180.5,157.5l0,2.6c-1.5-0.7-3.8-1.2-5.7-1.2c-4,0-6.6,2.1-6.6,5.4l0-2.6
										c0-3.3,2.6-5.4,6.6-5.4C176.7,156.3,178.9,156.8,180.5,157.5z"/>
								</defs>
								<clipPath id="SVGID_92_">
									<use xlink:href="#SVGID_91_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st62">
									<path class="st34" d="M180.5,157.5v2.6c-1.5-0.7-3.8-1.2-5.7-1.2c-2.8,0-4.9,1-5.9,2.8v-2.6c1-1.8,3.1-2.8,5.9-2.8
										C176.7,156.3,178.9,156.8,180.5,157.5"/>
									<path class="st34" d="M168.8,159.1v2.6c-0.2,0.3-0.3,0.6-0.4,0.9V160C168.6,159.7,168.7,159.4,168.8,159.1"/>
									<path class="st34" d="M168.5,160v2.6c-0.1,0.3-0.2,0.6-0.2,0.9v-2.6C168.3,160.5,168.4,160.3,168.5,160"/>
									<path class="st34" d="M168.3,160.8v2.6c0,0.3-0.1,0.6-0.1,0.8v-2.6C168.2,161.4,168.2,161.1,168.3,160.8"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_93_" class="st0" d="M175.7,163.5c-1.3,0-1.4,0.8-1.4,1v-2.6c0-0.2,0.1-1,1.4-1c1,0,2.8,0.4,4.8,1.2v2.6
										C178.5,163.9,176.7,163.5,175.7,163.5z"/>
								</defs>
								<clipPath id="SVGID_94_">
									<use xlink:href="#SVGID_93_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st63">
									<path class="st34" d="M180.5,162.1v2.6c-1.9-0.9-3.8-1.2-4.8-1.2c-0.8,0-1.1,0.3-1.3,0.5v-2.6c0.2-0.3,0.5-0.5,1.3-0.5
										C176.7,160.8,178.5,161.2,180.5,162.1"/>
									<path class="st34" d="M174.5,161.4v2.6c0,0.1-0.1,0.1-0.1,0.2v-2.6C174.4,161.5,174.4,161.5,174.5,161.4"/>
									<path class="st34" d="M174.4,161.6v2.6c0,0.1,0,0.1,0,0.2v-2.6C174.3,161.7,174.4,161.6,174.4,161.6"/>
									<path class="st34" d="M174.3,161.7v2.6c0,0.1,0,0.1,0,0.1v-2.6C174.3,161.8,174.3,161.8,174.3,161.7"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_95_" class="st0" d="M175.2,163v2.6c-0.6-0.2-0.9-0.6-0.9-1.1l0-2.6C174.3,162.4,174.6,162.7,175.2,163z"/>
								</defs>
								<clipPath id="SVGID_96_">
									<use xlink:href="#SVGID_95_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st64">
									<path class="st34" d="M175.2,163v2.6c-0.6-0.2-0.9-0.6-0.9-1.1v-2.6C174.3,162.4,174.6,162.7,175.2,163"/>
								</g>
							</g>
							<g>
								<polygon class="st34" points="181.5,163.7 181.5,166.3 180.7,165.9 180.7,163.3 								"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_97_" class="st0" d="M169.6,167.5c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6c0,1.4,0.4,2.4,1.4,3.2
										C169.6,165.8,169.6,166.7,169.6,167.5c0-0.9,0-1.7,0-2.6c1.4,1.2,3.2,1.6,4.3,1.8c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										C172.8,169.1,171,168.8,169.6,167.5z"/>
								</defs>
								<clipPath id="SVGID_98_">
									<use xlink:href="#SVGID_97_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st65">
									<path class="st34" d="M174.4,166.9v2.6c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6
										c0,1.4,0.4,2.4,1.4,3.2c1.4,1.2,3.2,1.6,4.3,1.8C174.1,166.8,174.3,166.8,174.4,166.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_99_" class="st0" d="M174.4,169.5c-0.1,0-0.3-0.1-0.5-0.1c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0.2,0,0.4,0.1,0.5,0.1C174.4,167.7,174.4,168.6,174.4,169.5z"/>
								</defs>
								<clipPath id="SVGID_100_">
									<use xlink:href="#SVGID_99_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st66">
									<path class="st34" d="M174.4,166.9v2.6c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6
										c0,1.4,0.4,2.4,1.4,3.2c1.4,1.2,3.2,1.6,4.3,1.8C174.1,166.8,174.3,166.8,174.4,166.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_101_" class="st0" d="M173.7,167.7v2.6c-1.1-0.2-3.2-0.6-4.8-2c-1.2-1-1.7-2.3-1.7-4l0-2.6
										c0,1.7,0.6,3,1.7,4C170.5,167.1,172.5,167.5,173.7,167.7z"/>
								</defs>
								<clipPath id="SVGID_102_">
									<use xlink:href="#SVGID_101_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st67">
									<path class="st34" d="M173.7,167.7v2.6c-1.1-0.2-3.2-0.6-4.8-2c-1.2-1-1.7-2.3-1.7-4v-2.6c0,1.7,0.6,3,1.7,4
										C170.5,167.1,172.5,167.5,173.7,167.7"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_103_" class="st0" d="M174.2,167.8v2.6c-0.1,0-0.3-0.1-0.5-0.1v-2.6C173.9,167.8,174,167.8,174.2,167.8z"/>
								</defs>
								<clipPath id="SVGID_104_">
									<use xlink:href="#SVGID_103_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st68">
									<path class="st34" d="M174.2,167.8v2.6c-0.1,0-0.3-0.1-0.5-0.1v-2.6C173.9,167.8,174,167.8,174.2,167.8"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_105_" class="st0" d="M175.3,170.5c0-0.3-0.1-0.9-0.9-1c0-0.8,0-1.5,0-2.3v-0.3c0.8,0.2,0.9,0.7,0.9,1v0.3
										C175.3,168.9,175.3,169.8,175.3,170.5z"/>
								</defs>
								<clipPath id="SVGID_106_">
									<use xlink:href="#SVGID_105_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st69">
									<path class="st34" d="M175.3,167.9v2.6c0-0.3-0.1-0.9-0.9-1v-2.6C175.2,167,175.3,167.6,175.3,167.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_107_" class="st0" d="M181.5,168.1v2.6c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2
										V163c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6C180.9,165.6,181.5,166.7,181.5,168.1z"/>
								</defs>
								<clipPath id="SVGID_108_">
									<use xlink:href="#SVGID_107_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st70">
									<path class="st34" d="M181.5,168.1v2.6c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2V163
										c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6C180.9,165.6,181.5,166.7,181.5,168.1"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_109_" class="st0" d="M175.3,167.9l0,2.6c0,0.3-0.1,1.1-1.4,1.1c-1.2,0-3.2-0.5-5.2-1.7l0-2.6
										c2,1.1,4.1,1.7,5.2,1.7C175.2,169,175.3,168.1,175.3,167.9z"/>
								</defs>
								<clipPath id="SVGID_110_">
									<use xlink:href="#SVGID_109_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st71">
									<path class="st34" d="M175.3,167.9v2.6c0,0,0,0.1,0,0.2V168C175.3,168,175.3,167.9,175.3,167.9"/>
									<path class="st34" d="M175.3,168v2.6c0,0.1,0,0.1,0,0.2v-2.6C175.3,168.2,175.3,168.1,175.3,168"/>
									<path class="st34" d="M175.2,168.2v2.6c0,0.1,0,0.1-0.1,0.2v-2.6C175.2,168.3,175.2,168.3,175.2,168.2"/>
									<path class="st34" d="M175.2,168.4v2.6c-0.2,0.3-0.5,0.6-1.3,0.6c-1.2,0-3.2-0.5-5.2-1.7v-2.6c2,1.1,4.1,1.7,5.2,1.7
										C174.7,169,175,168.7,175.2,168.4"/>
								</g>
							</g>
							<g>
								<polygon class="st34" points="168,172.7 168,175.4 167.7,175.2 167.7,172.6 								"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_111_" class="st0" d="M181.5,170.7c0,3.2-2.6,5.2-6.7,5.2c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c4.2,0,6.7-2,6.7-5.2v0.3C181.5,169.1,181.5,170,181.5,170.7z"/>
								</defs>
								<clipPath id="SVGID_112_">
									<use xlink:href="#SVGID_111_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st72">
									<path class="st34" d="M181.5,168.1v2.6c0,0.3,0,0.5-0.1,0.8v-2.6C181.4,168.6,181.5,168.3,181.5,168.1"/>
									<path class="st34" d="M181.4,168.9v2.6c0,0.3-0.1,0.5-0.2,0.8v-2.6C181.3,169.4,181.4,169.1,181.4,168.9"/>
									<path class="st34" d="M181.2,169.7v2.6c-0.1,0.3-0.2,0.5-0.4,0.8v-2.6C181,170.2,181.1,169.9,181.2,169.7"/>
									<path class="st34" d="M180.9,170.5v2.6c-1,1.7-3.2,2.8-6.1,2.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2
										C177.7,173.2,179.9,172.2,180.9,170.5"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_113_" class="st0" d="M174.7,175.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2c0,0,0,0,0,0
										c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										c0,0.1,0,0.2,0,0.3C174.7,175.6,174.7,175.7,174.7,175.8C174.7,175.8,174.7,175.8,174.7,175.8z"/>
								</defs>
								<clipPath id="SVGID_114_">
									<use xlink:href="#SVGID_113_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st73">
									<path class="st34" d="M180.9,170.5v2.6c-1,1.7-3.2,2.8-6.1,2.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2
										C177.7,173.2,179.9,172.2,180.9,170.5"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M174.8,156.3c1.9,0,4.2,0.5,5.7,1.2v4.6c-1.9-0.9-3.8-1.2-4.8-1.2c-1.3,0-1.4,0.8-1.4,1
									c0,0.5,0.3,0.9,0.9,1.1c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6c1.1,0.9,1.6,2,1.6,3.4c0,3.2-2.6,5.2-6.8,5.2
									c-2.1,0-4.4-0.5-6-1.2v-4.7c2,1.1,4.1,1.7,5.2,1.7c1.3,0,1.4-0.8,1.4-1.1c0-0.3-0.1-0.9-0.9-1c-0.1,0-0.3-0.1-0.5-0.1
									c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2C168.2,158.4,170.8,156.3,174.8,156.3z"/>
							</g>
							<g>
								<path class="st34" d="M180.5,163.9c1.3,1.1,1.9,2.4,1.9,4.1c0,3.7-3,6.2-7.7,6.2c-2.4,0-5-0.6-6.7-1.5l-0.3-0.1v-7.1
									l0.8,0.5c2.2,1.5,4.5,2,5.4,2c0.3,0,0.4-0.1,0.4-0.1c0,0,0,0-0.2-0.1c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-3.2-0.6-4.8-2
									c-1.2-1-1.7-2.3-1.7-4c0-3.9,3-6.4,7.6-6.4c2.2,0,4.8,0.6,6.4,1.4l0.3,0.1v6.8l-0.7-0.4c-2.2-1.1-4.2-1.5-5-1.5
									c-0.3,0-0.4,0.1-0.4,0.1c0,0,0.1,0,0.2,0.1c0.1,0,0.3,0.1,0.5,0.1C177.1,162.4,179.1,162.8,180.5,163.9z M174.7,173.2
									c4.2,0,6.8-2,6.8-5.2c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2c-0.6-0.2-0.9-0.6-0.9-1.1
									c0-0.2,0.1-1,1.4-1c1,0,2.8,0.4,4.8,1.2v-4.6c-1.5-0.7-3.8-1.2-5.7-1.2c-4,0-6.6,2.1-6.6,5.4c0,1.4,0.4,2.4,1.4,3.2
									c1.4,1.2,3.2,1.6,4.3,1.8c0.2,0,0.4,0.1,0.5,0.1c0.8,0.2,0.9,0.7,0.9,1c0,0.3-0.1,1.1-1.4,1.1c-1.2,0-3.2-0.5-5.2-1.7v4.7
									C170.3,172.7,172.6,173.2,174.7,173.2"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_115_" class="st0" d="M182.5,168.1l0,2.6c0,3.7-3,6.2-7.7,6.2c-2.4,0-5-0.6-6.7-1.5v-2.6
										c1.7,0.9,4.4,1.5,6.7,1.5C179.4,174.2,182.5,171.8,182.5,168.1z"/>
								</defs>
								<clipPath id="SVGID_116_">
									<use xlink:href="#SVGID_115_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st74">
									<path class="st34" d="M182.5,168.1v2.6c0,0.3,0,0.6-0.1,1V169C182.4,168.7,182.5,168.4,182.5,168.1"/>
									<path class="st34" d="M182.4,169v2.6c-0.1,0.3-0.1,0.7-0.2,1V170C182.3,169.7,182.3,169.3,182.4,169"/>
									<path class="st34" d="M182.2,170v2.6c-0.1,0.3-0.3,0.7-0.4,1v-2.6C181.9,170.6,182,170.3,182.2,170"/>
									<path class="st34" d="M181.7,170.9v2.6c-1.2,2-3.7,3.3-7,3.3c-2.4,0-5-0.6-6.7-1.5v-2.6c1.7,0.9,4.4,1.5,6.7,1.5
										C178.1,174.2,180.6,173,181.7,170.9"/>
								</g>
							</g>
						</g>
					</g>
					<defs>
						<filter id="Adobe_OpacityMaskFilter_2_" filterUnits="userSpaceOnUse" x="94.1" y="155.3" width="88.4" height="21.5">
							<feFlood  style="flood-color:white;flood-opacity:1" result="back"/>
							<feBlend  in="SourceGraphic" in2="back" mode="normal"/>
						</filter>
					</defs>
					<mask maskUnits="userSpaceOnUse" x="94.1" y="155.3" width="88.4" height="21.5" id="SVGID_78_">
						<g class="st75">
							
								<image style="overflow:visible;" width="93" height="27" xlink:href="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgEASABIAAD/7AARRHVja3kAAQAEAAAAHgAA/+4AIUFkb2JlAGTAAAAAAQMA
EAMCAwYAAAF0AAABfwAAAaD/2wCEABALCwsMCxAMDBAXDw0PFxsUEBAUGx8XFxcXFx8eFxoaGhoX
Hh4jJSclIx4vLzMzLy9AQEBAQEBAQEBAQEBAQEABEQ8PERMRFRISFRQRFBEUGhQWFhQaJhoaHBoa
JjAjHh4eHiMwKy4nJycuKzU1MDA1NUBAP0BAQEBAQEBAQEBAQP/CABEIABsAXQMBIgACEQEDEQH/
xABfAAEBAQAAAAAAAAAAAAAAAAAAAQcBAQAAAAAAAAAAAAAAAAAAAAAQAQEAAAAAAAAAAAAAAAAA
ADBQEQEAAAAAAAAAAAAAAAAAAABQEgEAAAAAAAAAAAAAAAAAAAAw/9oADAMBAAIRAxEAAADPykWA
AABYAAAAAAf/2gAIAQIAAQUAT//aAAgBAwABBQBP/9oACAEBAAEFAKH/2gAIAQICBj8AT//aAAgB
AwIGPwBP/9oACAEBAQY/AEP/2Q==" transform="matrix(1 0 0 1 91.75 152.5)">
							</image>
						</g>
					</mask>
					<g>
						<g class="st0">
							<g>
								<rect x="95.1" y="156.6" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<rect x="106.8" y="156.6" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<polygon class="st34" points="105.8,163.8 105.8,166.4 100.8,159.2 100.8,156.6 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.8,163.8 106.8,166.4 106.1,166.7 106.1,164.1 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.1,164.1 106.1,166.7 105.8,166.4 105.8,163.8 								"/>
							</g>
							<g>
								<polygon class="st34" points="101.2,165.4 101.2,168 100.5,168.4 100.5,165.8 								"/>
							</g>
							<g>
								<polygon class="st34" points="101.5,165.8 101.5,168.4 101.2,168 101.2,165.4 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.5,173 106.5,175.6 101.5,168.4 101.5,165.8 								"/>
							</g>
							<g>
								<rect x="106.5" y="173" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<rect x="95.1" y="173" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M105.8,155.6h7.4V174h-7.3l-4.4-6.4v6.4h-7.4v-18.4h7.3l4.4,6.4V155.6z M112.2,173v-16.4h-5.4v7.2
									l-0.7,0.3l-0.3-0.3l-5-7.2h-5.7V173h5.4v-7.2l0.7-0.3l0.3,0.3l5,7.2H112.2"/>
							</g>
							<g>
								<polygon class="st34" points="112.2,156.6 112.2,173 106.5,173 101.5,165.8 101.2,165.4 100.5,165.8 100.5,173 95.1,173 
									95.1,156.6 100.8,156.6 105.8,163.8 106.1,164.1 106.8,163.8 106.8,156.6 								"/>
							</g>
							<g>
								<polygon class="st34" points="105.9,174 105.9,176.6 101.5,170.2 101.5,167.6 								"/>
							</g>
							<g>
								<rect x="105.9" y="174" class="st34" width="7.3" height="2.6"/>
							</g>
							<g>
								<rect x="94.1" y="174" class="st34" width="7.4" height="2.6"/>
							</g>
						</g>
						<g class="st0">
							<g class="st0">
								<defs>
									<path id="SVGID_117_" class="st0" d="M133.3,164.8l0,2.6c0-4.7-4-8.4-9.2-8.4c-5.2,0-9.2,3.7-9.2,8.4l0-2.6
										c0-4.7,4-8.4,9.2-8.4C129.3,156.3,133.3,160,133.3,164.8z"/>
								</defs>
								<clipPath id="SVGID_118_">
									<use xlink:href="#SVGID_117_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st76">
									<path class="st34" d="M133.3,164.8v2.6c0-4.7-4-8.4-9.2-8.4c-3.6,0-6.6,1.8-8.1,4.5v-2.6c1.5-2.7,4.6-4.5,8.1-4.5
										C129.3,156.3,133.3,160,133.3,164.8"/>
									<path class="st34" d="M116,160.8v2.6c-0.3,0.5-0.5,0.9-0.6,1.4v-2.6C115.5,161.7,115.7,161.3,116,160.8"/>
									<path class="st34" d="M115.3,162.2v2.6c-0.1,0.4-0.3,0.9-0.3,1.3v-2.6C115.1,163.1,115.2,162.7,115.3,162.2"/>
									<path class="st34" d="M115,163.6v2.6c-0.1,0.4-0.1,0.8-0.1,1.2v-2.6C114.9,164.4,115,164,115,163.6"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_119_" class="st0" d="M120.9,167.5c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3v-0.3c0-1.8,1.4-3.2,3.2-3.2
										c1.8,0,3.2,1.4,3.2,3.2l0,2.6c0-1.8-1.4-3.2-3.2-3.2C122.4,164.3,120.9,165.8,120.9,167.5z"/>
								</defs>
								<clipPath id="SVGID_120_">
									<use xlink:href="#SVGID_119_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st77">
									<path class="st34" d="M127.3,164.9v2.6c0-1.8-1.4-3.2-3.2-3.2c-1.2,0-2.2,0.7-2.8,1.6v-2.6c0.5-1,1.6-1.6,2.8-1.6
										C125.9,161.7,127.3,163.1,127.3,164.9"/>
									<path class="st34" d="M121.3,163.3v2.6c-0.1,0.2-0.2,0.4-0.3,0.6v-2.6C121.2,163.7,121.2,163.5,121.3,163.3"/>
									<path class="st34" d="M121.1,163.9v2.6c-0.1,0.2-0.1,0.3-0.1,0.5v-2.6C121,164.2,121,164.1,121.1,163.9"/>
									<path class="st34" d="M121,164.4v2.6c0,0.2,0,0.3,0,0.5v-2.6C120.9,164.7,120.9,164.6,121,164.4"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_121_" class="st0" d="M126.3,164.9l0,2.6c0-1.2-1-2.2-2.2-2.2c-1.2,0-2.2,1-2.2,2.2l0-2.6
										c0-1.2,1-2.2,2.2-2.2C125.3,162.7,126.3,163.7,126.3,164.9z"/>
								</defs>
								<clipPath id="SVGID_122_">
									<use xlink:href="#SVGID_121_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st78">
									<path class="st34" d="M126.3,164.9v2.6c0-1.2-1-2.2-2.2-2.2c-0.8,0-1.5,0.4-1.9,1.1v-2.6c0.4-0.7,1.1-1.1,1.9-1.1
										C125.3,162.7,126.3,163.7,126.3,164.9"/>
									<path class="st34" d="M122.2,163.8v2.6c-0.1,0.1-0.1,0.3-0.2,0.4v-2.6C122.1,164.1,122.1,163.9,122.2,163.8"/>
									<path class="st34" d="M122,164.2v2.6c0,0.1-0.1,0.2-0.1,0.4v-2.6C122,164.4,122,164.3,122,164.2"/>
									<path class="st34" d="M121.9,164.6v2.6c0,0.1,0,0.2,0,0.3v-2.6C121.9,164.8,121.9,164.7,121.9,164.6"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_123_" class="st0" d="M127.3,164.9l0,2.6c0,1.8-1.4,3.2-3.2,3.2c-1.8,0-3.2-1.4-3.2-3.2v-2.6
										c0,1.8,1.4,3.2,3.2,3.2C125.9,168.1,127.3,166.6,127.3,164.9z"/>
								</defs>
								<clipPath id="SVGID_124_">
									<use xlink:href="#SVGID_123_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st79">
									<path class="st34" d="M127.3,164.9v2.6c0,0.2,0,0.3,0,0.5v-2.6C127.3,165.2,127.3,165.1,127.3,164.9"/>
									<path class="st34" d="M127.3,165.4v2.6c0,0.2-0.1,0.4-0.1,0.5v-2.6C127.2,165.7,127.2,165.5,127.3,165.4"/>
									<path class="st34" d="M127.1,165.9v2.6c-0.1,0.2-0.2,0.4-0.3,0.6v-2.6C127,166.3,127.1,166.1,127.1,165.9"/>
									<path class="st34" d="M126.9,166.5v2.6c-0.5,1-1.6,1.6-2.8,1.6c-1.8,0-3.2-1.4-3.2-3.2v-2.6c0,1.8,1.4,3.2,3.2,3.2
										C125.3,168.1,126.3,167.4,126.9,166.5"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M124.1,161.7c1.8,0,3.2,1.4,3.2,3.2c0,1.8-1.4,3.2-3.2,3.2c-1.8,0-3.2-1.4-3.2-3.2
									C120.9,163.1,122.4,161.7,124.1,161.7z M124.1,167.1c1.2,0,2.2-1,2.2-2.2c0-1.2-1-2.2-2.2-2.2c-1.2,0-2.2,1-2.2,2.2
									C121.9,166.1,122.9,167.1,124.1,167.1"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_125_" class="st0" d="M124.1,175.8c-5.2,0-9.2-3.7-9.2-8.4l0-2.6c0,4.7,4,8.4,9.2,8.4
										c5.2,0,9.2-3.7,9.2-8.4l0,0.3v2.3C133.3,172.1,129.3,175.8,124.1,175.8z"/>
								</defs>
								<clipPath id="SVGID_126_">
									<use xlink:href="#SVGID_125_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st80">
									<path class="st34" d="M133.3,164.8v2.6c0,0.4,0,0.8-0.1,1.2V166C133.3,165.6,133.3,165.2,133.3,164.8"/>
									<path class="st34" d="M133.2,166v2.6c-0.1,0.5-0.2,0.9-0.3,1.3v-2.6C133,166.9,133.2,166.4,133.2,166"/>
									<path class="st34" d="M132.9,167.3v2.6c-0.2,0.5-0.4,1-0.6,1.4v-2.6C132.5,168.3,132.7,167.8,132.9,167.3"/>
									<path class="st34" d="M132.3,168.8v2.6c-1.5,2.7-4.6,4.5-8.1,4.5c-5.2,0-9.2-3.7-9.2-8.4v-2.6c0,4.7,4,8.4,9.2,8.4
										C127.7,173.2,130.7,171.4,132.3,168.8"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M124.1,155.3c5.7,0,10.2,4.2,10.2,9.4c0,5.3-4.5,9.4-10.2,9.4c-5.7,0-10.2-4.1-10.2-9.4
									C113.9,159.5,118.4,155.3,124.1,155.3z M124.1,173.2c5.2,0,9.2-3.7,9.2-8.4c0-4.7-4-8.4-9.2-8.4c-5.2,0-9.2,3.7-9.2,8.4
									C114.9,169.5,119,173.2,124.1,173.2"/>
							</g>
							<g>
								<path class="st34" d="M124.1,156.3c5.2,0,9.2,3.7,9.2,8.4c0,4.7-4,8.4-9.2,8.4c-5.2,0-9.2-3.7-9.2-8.4
									C114.9,160,119,156.3,124.1,156.3z M124.1,168.1c1.8,0,3.2-1.4,3.2-3.2c0-1.8-1.4-3.2-3.2-3.2c-1.8,0-3.2,1.4-3.2,3.2
									C120.9,166.6,122.4,168.1,124.1,168.1"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_127_" class="st0" d="M134.3,164.8v2.6c0,5.3-4.5,9.4-10.2,9.4c-5.7,0-10.2-4.1-10.2-9.4l0-2.6
										c0,5.3,4.5,9.4,10.2,9.4C129.8,174.2,134.3,170.1,134.3,164.8z"/>
								</defs>
								<clipPath id="SVGID_128_">
									<use xlink:href="#SVGID_127_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st81">
									<path class="st34" d="M134.3,164.8v2.6c0,0.5,0,0.9-0.1,1.4v-2.6C134.3,165.7,134.3,165.2,134.3,164.8"/>
									<path class="st34" d="M134.2,166.1v2.6c-0.1,0.5-0.2,1-0.4,1.5v-2.6C134,167.2,134.1,166.7,134.2,166.1"/>
									<path class="st34" d="M133.9,167.6v2.6c-0.2,0.6-0.4,1.1-0.7,1.6v-2.6C133.4,168.7,133.7,168.2,133.9,167.6"/>
									<path class="st34" d="M133.1,169.3v2.6c-1.7,3-5.1,5-9,5c-5.7,0-10.2-4.1-10.2-9.4v-2.6c0,5.3,4.5,9.4,10.2,9.4
										C128.1,174.2,131.4,172.2,133.1,169.3"/>
								</g>
							</g>
						</g>
						<g class="st0">
							<g>
								<rect x="135.3" y="156.6" class="st34" width="15.8" height="2.6"/>
							</g>
							<g>
								<rect x="135.3" y="161.5" class="st34" width="4.9" height="2.6"/>
							</g>
							<g>
								<rect x="146.3" y="161.5" class="st34" width="4.8" height="2.6"/>
							</g>
							<g>
								<rect x="147.3" y="162.5" class="st34" width="4.8" height="2.6"/>
							</g>
							<g>
								<rect x="134.3" y="162.5" class="st34" width="4.9" height="2.6"/>
							</g>
							<g>
								<rect x="140.2" y="173" class="st34" width="6.2" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M134.3,155.6h17.8v6.9h-4.8V174h-8.2v-11.5h-4.9V155.6z M151.2,161.5v-4.9h-15.8v4.9h4.9V173h6.2
									v-11.5H151.2"/>
							</g>
							<g>
								<polygon class="st34" points="151.2,156.6 151.2,161.5 146.3,161.5 146.3,173 140.2,173 140.2,161.5 135.3,161.5 
									135.3,156.6 								"/>
							</g>
							<g>
								<rect x="139.2" y="174" class="st34" width="8.2" height="2.6"/>
							</g>
						</g>
						<g class="st0">
							<g>
								<rect x="154" y="156.6" class="st34" width="11.5" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="161.4" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<rect x="165.5" y="162.4" class="st34" width="1" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="163.3" class="st34" width="4.3" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="166.5" class="st34" width="4.3" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="168.4" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<polygon class="st34" points="165.5,156.6 165.5,161.4 160.2,161.4 160.2,163.3 164.5,163.3 164.5,166.5 160.2,166.5 
									160.2,168.4 165.9,168.4 165.9,173 154,173 154,156.6 								"/>
							</g>
							<g>
								<rect x="154" y="173" class="st34" width="11.8" height="2.6"/>
							</g>
							<g>
								<rect x="153" y="174" class="st34" width="13.8" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M165.5,167.4h1.4v6.5H153v-18.4h13.5v6.8h-1V167.4z M165.9,173v-4.5h-5.7v-2h4.3v-3.1h-4.3v-1.9h5.4
									v-4.8H154V173H165.9"/>
							</g>
						</g>
						<g class="st0">
							<g class="st0">
								<defs>
									<path id="SVGID_129_" class="st0" d="M180.5,157.5l0,2.6c-1.5-0.7-3.8-1.2-5.7-1.2c-4,0-6.6,2.1-6.6,5.4l0-2.6
										c0-3.3,2.6-5.4,6.6-5.4C176.7,156.3,178.9,156.8,180.5,157.5z"/>
								</defs>
								<clipPath id="SVGID_130_">
									<use xlink:href="#SVGID_129_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st82">
									<path class="st34" d="M180.5,157.5v2.6c-1.5-0.7-3.8-1.2-5.7-1.2c-2.8,0-4.9,1-5.9,2.8v-2.6c1-1.8,3.1-2.8,5.9-2.8
										C176.7,156.3,178.9,156.8,180.5,157.5"/>
									<path class="st34" d="M168.8,159.1v2.6c-0.2,0.3-0.3,0.6-0.4,0.9V160C168.6,159.7,168.7,159.4,168.8,159.1"/>
									<path class="st34" d="M168.5,160v2.6c-0.1,0.3-0.2,0.6-0.2,0.9v-2.6C168.3,160.5,168.4,160.3,168.5,160"/>
									<path class="st34" d="M168.3,160.8v2.6c0,0.3-0.1,0.6-0.1,0.8v-2.6C168.2,161.4,168.2,161.1,168.3,160.8"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_131_" class="st0" d="M175.7,163.5c-1.3,0-1.4,0.8-1.4,1v-2.6c0-0.2,0.1-1,1.4-1c1,0,2.8,0.4,4.8,1.2v2.6
										C178.5,163.9,176.7,163.5,175.7,163.5z"/>
								</defs>
								<clipPath id="SVGID_132_">
									<use xlink:href="#SVGID_131_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st83">
									<path class="st34" d="M180.5,162.1v2.6c-1.9-0.9-3.8-1.2-4.8-1.2c-0.8,0-1.1,0.3-1.3,0.5v-2.6c0.2-0.3,0.5-0.5,1.3-0.5
										C176.7,160.8,178.5,161.2,180.5,162.1"/>
									<path class="st34" d="M174.5,161.4v2.6c0,0.1-0.1,0.1-0.1,0.2v-2.6C174.4,161.5,174.4,161.5,174.5,161.4"/>
									<path class="st34" d="M174.4,161.6v2.6c0,0.1,0,0.1,0,0.2v-2.6C174.3,161.7,174.4,161.6,174.4,161.6"/>
									<path class="st34" d="M174.3,161.7v2.6c0,0.1,0,0.1,0,0.1v-2.6C174.3,161.8,174.3,161.8,174.3,161.7"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_133_" class="st0" d="M175.2,163v2.6c-0.6-0.2-0.9-0.6-0.9-1.1l0-2.6C174.3,162.4,174.6,162.7,175.2,163z"
										/>
								</defs>
								<clipPath id="SVGID_134_">
									<use xlink:href="#SVGID_133_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st84">
									<path class="st34" d="M175.2,163v2.6c-0.6-0.2-0.9-0.6-0.9-1.1v-2.6C174.3,162.4,174.6,162.7,175.2,163"/>
								</g>
							</g>
							<g>
								<polygon class="st34" points="181.5,163.7 181.5,166.3 180.7,165.9 180.7,163.3 								"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_135_" class="st0" d="M169.6,167.5c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6c0,1.4,0.4,2.4,1.4,3.2
										C169.6,165.8,169.6,166.7,169.6,167.5c0-0.9,0-1.7,0-2.6c1.4,1.2,3.2,1.6,4.3,1.8c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										C172.8,169.1,171,168.8,169.6,167.5z"/>
								</defs>
								<clipPath id="SVGID_136_">
									<use xlink:href="#SVGID_135_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st85">
									<path class="st34" d="M174.4,166.9v2.6c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6
										c0,1.4,0.4,2.4,1.4,3.2c1.4,1.2,3.2,1.6,4.3,1.8C174.1,166.8,174.3,166.8,174.4,166.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_137_" class="st0" d="M174.4,169.5c-0.1,0-0.3-0.1-0.5-0.1c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0.2,0,0.4,0.1,0.5,0.1C174.4,167.7,174.4,168.6,174.4,169.5z"/>
								</defs>
								<clipPath id="SVGID_138_">
									<use xlink:href="#SVGID_137_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st86">
									<path class="st34" d="M174.4,166.9v2.6c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6
										c0,1.4,0.4,2.4,1.4,3.2c1.4,1.2,3.2,1.6,4.3,1.8C174.1,166.8,174.3,166.8,174.4,166.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_139_" class="st0" d="M173.7,167.7v2.6c-1.1-0.2-3.2-0.6-4.8-2c-1.2-1-1.7-2.3-1.7-4l0-2.6
										c0,1.7,0.6,3,1.7,4C170.5,167.1,172.5,167.5,173.7,167.7z"/>
								</defs>
								<clipPath id="SVGID_140_">
									<use xlink:href="#SVGID_139_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st87">
									<path class="st34" d="M173.7,167.7v2.6c-1.1-0.2-3.2-0.6-4.8-2c-1.2-1-1.7-2.3-1.7-4v-2.6c0,1.7,0.6,3,1.7,4
										C170.5,167.1,172.5,167.5,173.7,167.7"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_141_" class="st0" d="M174.2,167.8v2.6c-0.1,0-0.3-0.1-0.5-0.1v-2.6C173.9,167.8,174,167.8,174.2,167.8z"/>
								</defs>
								<clipPath id="SVGID_142_">
									<use xlink:href="#SVGID_141_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st88">
									<path class="st34" d="M174.2,167.8v2.6c-0.1,0-0.3-0.1-0.5-0.1v-2.6C173.9,167.8,174,167.8,174.2,167.8"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_143_" class="st0" d="M175.3,170.5c0-0.3-0.1-0.9-0.9-1c0-0.8,0-1.5,0-2.3v-0.3c0.8,0.2,0.9,0.7,0.9,1v0.3
										C175.3,168.9,175.3,169.8,175.3,170.5z"/>
								</defs>
								<clipPath id="SVGID_144_">
									<use xlink:href="#SVGID_143_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st89">
									<path class="st34" d="M175.3,167.9v2.6c0-0.3-0.1-0.9-0.9-1v-2.6C175.2,167,175.3,167.6,175.3,167.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_145_" class="st0" d="M181.5,168.1v2.6c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2
										V163c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6C180.9,165.6,181.5,166.7,181.5,168.1z"/>
								</defs>
								<clipPath id="SVGID_146_">
									<use xlink:href="#SVGID_145_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st90">
									<path class="st34" d="M181.5,168.1v2.6c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2V163
										c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6C180.9,165.6,181.5,166.7,181.5,168.1"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_147_" class="st0" d="M175.3,167.9l0,2.6c0,0.3-0.1,1.1-1.4,1.1c-1.2,0-3.2-0.5-5.2-1.7l0-2.6
										c2,1.1,4.1,1.7,5.2,1.7C175.2,169,175.3,168.1,175.3,167.9z"/>
								</defs>
								<clipPath id="SVGID_148_">
									<use xlink:href="#SVGID_147_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st91">
									<path class="st34" d="M175.3,167.9v2.6c0,0,0,0.1,0,0.2V168C175.3,168,175.3,167.9,175.3,167.9"/>
									<path class="st34" d="M175.3,168v2.6c0,0.1,0,0.1,0,0.2v-2.6C175.3,168.2,175.3,168.1,175.3,168"/>
									<path class="st34" d="M175.2,168.2v2.6c0,0.1,0,0.1-0.1,0.2v-2.6C175.2,168.3,175.2,168.3,175.2,168.2"/>
									<path class="st34" d="M175.2,168.4v2.6c-0.2,0.3-0.5,0.6-1.3,0.6c-1.2,0-3.2-0.5-5.2-1.7v-2.6c2,1.1,4.1,1.7,5.2,1.7
										C174.7,169,175,168.7,175.2,168.4"/>
								</g>
							</g>
							<g>
								<polygon class="st34" points="168,172.7 168,175.4 167.7,175.2 167.7,172.6 								"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_149_" class="st0" d="M181.5,170.7c0,3.2-2.6,5.2-6.7,5.2c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c4.2,0,6.7-2,6.7-5.2v0.3C181.5,169.1,181.5,170,181.5,170.7z"/>
								</defs>
								<clipPath id="SVGID_150_">
									<use xlink:href="#SVGID_149_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st92">
									<path class="st34" d="M181.5,168.1v2.6c0,0.3,0,0.5-0.1,0.8v-2.6C181.4,168.6,181.5,168.3,181.5,168.1"/>
									<path class="st34" d="M181.4,168.9v2.6c0,0.3-0.1,0.5-0.2,0.8v-2.6C181.3,169.4,181.4,169.1,181.4,168.9"/>
									<path class="st34" d="M181.2,169.7v2.6c-0.1,0.3-0.2,0.5-0.4,0.8v-2.6C181,170.2,181.1,169.9,181.2,169.7"/>
									<path class="st34" d="M180.9,170.5v2.6c-1,1.7-3.2,2.8-6.1,2.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2
										C177.7,173.2,179.9,172.2,180.9,170.5"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_151_" class="st0" d="M174.7,175.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2c0,0,0,0,0,0
										c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										c0,0.1,0,0.2,0,0.3C174.7,175.6,174.7,175.7,174.7,175.8C174.7,175.8,174.7,175.8,174.7,175.8z"/>
								</defs>
								<clipPath id="SVGID_152_">
									<use xlink:href="#SVGID_151_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st93">
									<path class="st34" d="M180.9,170.5v2.6c-1,1.7-3.2,2.8-6.1,2.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2
										C177.7,173.2,179.9,172.2,180.9,170.5"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M174.8,156.3c1.9,0,4.2,0.5,5.7,1.2v4.6c-1.9-0.9-3.8-1.2-4.8-1.2c-1.3,0-1.4,0.8-1.4,1
									c0,0.5,0.3,0.9,0.9,1.1c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6c1.1,0.9,1.6,2,1.6,3.4c0,3.2-2.6,5.2-6.8,5.2
									c-2.1,0-4.4-0.5-6-1.2v-4.7c2,1.1,4.1,1.7,5.2,1.7c1.3,0,1.4-0.8,1.4-1.1c0-0.3-0.1-0.9-0.9-1c-0.1,0-0.3-0.1-0.5-0.1
									c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2C168.2,158.4,170.8,156.3,174.8,156.3z"/>
							</g>
							<g>
								<path class="st34" d="M180.5,163.9c1.3,1.1,1.9,2.4,1.9,4.1c0,3.7-3,6.2-7.7,6.2c-2.4,0-5-0.6-6.7-1.5l-0.3-0.1v-7.1
									l0.8,0.5c2.2,1.5,4.5,2,5.4,2c0.3,0,0.4-0.1,0.4-0.1c0,0,0,0-0.2-0.1c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-3.2-0.6-4.8-2
									c-1.2-1-1.7-2.3-1.7-4c0-3.9,3-6.4,7.6-6.4c2.2,0,4.8,0.6,6.4,1.4l0.3,0.1v6.8l-0.7-0.4c-2.2-1.1-4.2-1.5-5-1.5
									c-0.3,0-0.4,0.1-0.4,0.1c0,0,0.1,0,0.2,0.1c0.1,0,0.3,0.1,0.5,0.1C177.1,162.4,179.1,162.8,180.5,163.9z M174.7,173.2
									c4.2,0,6.8-2,6.8-5.2c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2c-0.6-0.2-0.9-0.6-0.9-1.1
									c0-0.2,0.1-1,1.4-1c1,0,2.8,0.4,4.8,1.2v-4.6c-1.5-0.7-3.8-1.2-5.7-1.2c-4,0-6.6,2.1-6.6,5.4c0,1.4,0.4,2.4,1.4,3.2
									c1.4,1.2,3.2,1.6,4.3,1.8c0.2,0,0.4,0.1,0.5,0.1c0.8,0.2,0.9,0.7,0.9,1c0,0.3-0.1,1.1-1.4,1.1c-1.2,0-3.2-0.5-5.2-1.7v4.7
									C170.3,172.7,172.6,173.2,174.7,173.2"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_153_" class="st0" d="M182.5,168.1l0,2.6c0,3.7-3,6.2-7.7,6.2c-2.4,0-5-0.6-6.7-1.5v-2.6
										c1.7,0.9,4.4,1.5,6.7,1.5C179.4,174.2,182.5,171.8,182.5,168.1z"/>
								</defs>
								<clipPath id="SVGID_154_">
									<use xlink:href="#SVGID_153_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st94">
									<path class="st34" d="M182.5,168.1v2.6c0,0.3,0,0.6-0.1,1V169C182.4,168.7,182.5,168.4,182.5,168.1"/>
									<path class="st34" d="M182.4,169v2.6c-0.1,0.3-0.1,0.7-0.2,1V170C182.3,169.7,182.3,169.3,182.4,169"/>
									<path class="st34" d="M182.2,170v2.6c-0.1,0.3-0.3,0.7-0.4,1v-2.6C181.9,170.6,182,170.3,182.2,170"/>
									<path class="st34" d="M181.7,170.9v2.6c-1.2,2-3.7,3.3-7,3.3c-2.4,0-5-0.6-6.7-1.5v-2.6c1.7,0.9,4.4,1.5,6.7,1.5
										C178.1,174.2,180.6,173,181.7,170.9"/>
								</g>
							</g>
						</g>
					</g>
				</g>
			</g>
			<defs>
				<filter id="Adobe_OpacityMaskFilter_3_" filterUnits="userSpaceOnUse" x="94.1" y="155.3" width="88.4" height="21.5">
					<feFlood  style="flood-color:white;flood-opacity:1" result="back"/>
					<feBlend  in="SourceGraphic" in2="back" mode="normal"/>
				</filter>
			</defs>
			<mask maskUnits="userSpaceOnUse" x="94.1" y="155.3" width="88.4" height="21.5" id="SVGID_155_">
				<g class="st95">
					
						<image style="overflow:visible;" width="93" height="27" xlink:href="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgEASABIAAD/7AARRHVja3kAAQAEAAAAHgAA/+4AIUFkb2JlAGTAAAAAAQMA
EAMCAwYAAAF0AAABfwAAAaD/2wCEABALCwsMCxAMDBAXDw0PFxsUEBAUGx8XFxcXFx8eFxoaGhoX
Hh4jJSclIx4vLzMzLy9AQEBAQEBAQEBAQEBAQEABEQ8PERMRFRISFRQRFBEUGhQWFhQaJhoaHBoa
JjAjHh4eHiMwKy4nJycuKzU1MDA1NUBAP0BAQEBAQEBAQEBAQP/CABEIABsAXQMBIgACEQEDEQH/
xABfAAEBAQAAAAAAAAAAAAAAAAAAAQcBAQAAAAAAAAAAAAAAAAAAAAAQAQEAAAAAAAAAAAAAAAAA
ADBQEQEAAAAAAAAAAAAAAAAAAABQEgEAAAAAAAAAAAAAAAAAAAAw/9oADAMBAAIRAxEAAADPykWA
AABYAAAAAAf/2gAIAQIAAQUAT//aAAgBAwABBQBP/9oACAEBAAEFAKH/2gAIAQICBj8AT//aAAgB
AwIGPwBP/9oACAEBAQY/AEP/2Q==" transform="matrix(1 0 0 1 91.75 152.5)">
					</image>
				</g>
			</mask>
			<g class="st96">
				<g>
					<g>
						<g class="st0">
							<g>
								<rect x="95.1" y="156.6" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<rect x="106.8" y="156.6" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<polygon class="st34" points="105.8,163.8 105.8,166.4 100.8,159.2 100.8,156.6 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.8,163.8 106.8,166.4 106.1,166.7 106.1,164.1 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.1,164.1 106.1,166.7 105.8,166.4 105.8,163.8 								"/>
							</g>
							<g>
								<polygon class="st34" points="101.2,165.4 101.2,168 100.5,168.4 100.5,165.8 								"/>
							</g>
							<g>
								<polygon class="st34" points="101.5,165.8 101.5,168.4 101.2,168 101.2,165.4 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.5,173 106.5,175.6 101.5,168.4 101.5,165.8 								"/>
							</g>
							<g>
								<rect x="106.5" y="173" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<rect x="95.1" y="173" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M105.8,155.6h7.4V174h-7.3l-4.4-6.4v6.4h-7.4v-18.4h7.3l4.4,6.4V155.6z M112.2,173v-16.4h-5.4v7.2
									l-0.7,0.3l-0.3-0.3l-5-7.2h-5.7V173h5.4v-7.2l0.7-0.3l0.3,0.3l5,7.2H112.2"/>
							</g>
							<g>
								<polygon class="st34" points="112.2,156.6 112.2,173 106.5,173 101.5,165.8 101.2,165.4 100.5,165.8 100.5,173 95.1,173 
									95.1,156.6 100.8,156.6 105.8,163.8 106.1,164.1 106.8,163.8 106.8,156.6 								"/>
							</g>
							<g>
								<polygon class="st34" points="105.9,174 105.9,176.6 101.5,170.2 101.5,167.6 								"/>
							</g>
							<g>
								<rect x="105.9" y="174" class="st34" width="7.3" height="2.6"/>
							</g>
							<g>
								<rect x="94.1" y="174" class="st34" width="7.4" height="2.6"/>
							</g>
						</g>
						<g class="st0">
							<g class="st0">
								<defs>
									<path id="SVGID_156_" class="st0" d="M133.3,164.8l0,2.6c0-4.7-4-8.4-9.2-8.4c-5.2,0-9.2,3.7-9.2,8.4l0-2.6
										c0-4.7,4-8.4,9.2-8.4C129.3,156.3,133.3,160,133.3,164.8z"/>
								</defs>
								<clipPath id="SVGID_157_">
									<use xlink:href="#SVGID_156_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st97">
									<path class="st34" d="M133.3,164.8v2.6c0-4.7-4-8.4-9.2-8.4c-3.6,0-6.6,1.8-8.1,4.5v-2.6c1.5-2.7,4.6-4.5,8.1-4.5
										C129.3,156.3,133.3,160,133.3,164.8"/>
									<path class="st34" d="M116,160.8v2.6c-0.3,0.5-0.5,0.9-0.6,1.4v-2.6C115.5,161.7,115.7,161.3,116,160.8"/>
									<path class="st34" d="M115.3,162.2v2.6c-0.1,0.4-0.3,0.9-0.3,1.3v-2.6C115.1,163.1,115.2,162.7,115.3,162.2"/>
									<path class="st34" d="M115,163.6v2.6c-0.1,0.4-0.1,0.8-0.1,1.2v-2.6C114.9,164.4,115,164,115,163.6"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_158_" class="st0" d="M120.9,167.5c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3v-0.3c0-1.8,1.4-3.2,3.2-3.2
										c1.8,0,3.2,1.4,3.2,3.2l0,2.6c0-1.8-1.4-3.2-3.2-3.2C122.4,164.3,120.9,165.8,120.9,167.5z"/>
								</defs>
								<clipPath id="SVGID_159_">
									<use xlink:href="#SVGID_158_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st98">
									<path class="st34" d="M127.3,164.9v2.6c0-1.8-1.4-3.2-3.2-3.2c-1.2,0-2.2,0.7-2.8,1.6v-2.6c0.5-1,1.6-1.6,2.8-1.6
										C125.9,161.7,127.3,163.1,127.3,164.9"/>
									<path class="st34" d="M121.3,163.3v2.6c-0.1,0.2-0.2,0.4-0.3,0.6v-2.6C121.2,163.7,121.2,163.5,121.3,163.3"/>
									<path class="st34" d="M121.1,163.9v2.6c-0.1,0.2-0.1,0.3-0.1,0.5v-2.6C121,164.2,121,164.1,121.1,163.9"/>
									<path class="st34" d="M121,164.4v2.6c0,0.2,0,0.3,0,0.5v-2.6C120.9,164.7,120.9,164.6,121,164.4"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_160_" class="st0" d="M126.3,164.9l0,2.6c0-1.2-1-2.2-2.2-2.2c-1.2,0-2.2,1-2.2,2.2l0-2.6
										c0-1.2,1-2.2,2.2-2.2C125.3,162.7,126.3,163.7,126.3,164.9z"/>
								</defs>
								<clipPath id="SVGID_161_">
									<use xlink:href="#SVGID_160_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st99">
									<path class="st34" d="M126.3,164.9v2.6c0-1.2-1-2.2-2.2-2.2c-0.8,0-1.5,0.4-1.9,1.1v-2.6c0.4-0.7,1.1-1.1,1.9-1.1
										C125.3,162.7,126.3,163.7,126.3,164.9"/>
									<path class="st34" d="M122.2,163.8v2.6c-0.1,0.1-0.1,0.3-0.2,0.4v-2.6C122.1,164.1,122.1,163.9,122.2,163.8"/>
									<path class="st34" d="M122,164.2v2.6c0,0.1-0.1,0.2-0.1,0.4v-2.6C122,164.4,122,164.3,122,164.2"/>
									<path class="st34" d="M121.9,164.6v2.6c0,0.1,0,0.2,0,0.3v-2.6C121.9,164.8,121.9,164.7,121.9,164.6"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_162_" class="st0" d="M127.3,164.9l0,2.6c0,1.8-1.4,3.2-3.2,3.2c-1.8,0-3.2-1.4-3.2-3.2v-2.6
										c0,1.8,1.4,3.2,3.2,3.2C125.9,168.1,127.3,166.6,127.3,164.9z"/>
								</defs>
								<clipPath id="SVGID_163_">
									<use xlink:href="#SVGID_162_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st100">
									<path class="st34" d="M127.3,164.9v2.6c0,0.2,0,0.3,0,0.5v-2.6C127.3,165.2,127.3,165.1,127.3,164.9"/>
									<path class="st34" d="M127.3,165.4v2.6c0,0.2-0.1,0.4-0.1,0.5v-2.6C127.2,165.7,127.2,165.5,127.3,165.4"/>
									<path class="st34" d="M127.1,165.9v2.6c-0.1,0.2-0.2,0.4-0.3,0.6v-2.6C127,166.3,127.1,166.1,127.1,165.9"/>
									<path class="st34" d="M126.9,166.5v2.6c-0.5,1-1.6,1.6-2.8,1.6c-1.8,0-3.2-1.4-3.2-3.2v-2.6c0,1.8,1.4,3.2,3.2,3.2
										C125.3,168.1,126.3,167.4,126.9,166.5"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M124.1,161.7c1.8,0,3.2,1.4,3.2,3.2c0,1.8-1.4,3.2-3.2,3.2c-1.8,0-3.2-1.4-3.2-3.2
									C120.9,163.1,122.4,161.7,124.1,161.7z M124.1,167.1c1.2,0,2.2-1,2.2-2.2c0-1.2-1-2.2-2.2-2.2c-1.2,0-2.2,1-2.2,2.2
									C121.9,166.1,122.9,167.1,124.1,167.1"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_164_" class="st0" d="M124.1,175.8c-5.2,0-9.2-3.7-9.2-8.4l0-2.6c0,4.7,4,8.4,9.2,8.4
										c5.2,0,9.2-3.7,9.2-8.4l0,0.3v2.3C133.3,172.1,129.3,175.8,124.1,175.8z"/>
								</defs>
								<clipPath id="SVGID_165_">
									<use xlink:href="#SVGID_164_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st101">
									<path class="st34" d="M133.3,164.8v2.6c0,0.4,0,0.8-0.1,1.2V166C133.3,165.6,133.3,165.2,133.3,164.8"/>
									<path class="st34" d="M133.2,166v2.6c-0.1,0.5-0.2,0.9-0.3,1.3v-2.6C133,166.9,133.2,166.4,133.2,166"/>
									<path class="st34" d="M132.9,167.3v2.6c-0.2,0.5-0.4,1-0.6,1.4v-2.6C132.5,168.3,132.7,167.8,132.9,167.3"/>
									<path class="st34" d="M132.3,168.8v2.6c-1.5,2.7-4.6,4.5-8.1,4.5c-5.2,0-9.2-3.7-9.2-8.4v-2.6c0,4.7,4,8.4,9.2,8.4
										C127.7,173.2,130.7,171.4,132.3,168.8"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M124.1,155.3c5.7,0,10.2,4.2,10.2,9.4c0,5.3-4.5,9.4-10.2,9.4c-5.7,0-10.2-4.1-10.2-9.4
									C113.9,159.5,118.4,155.3,124.1,155.3z M124.1,173.2c5.2,0,9.2-3.7,9.2-8.4c0-4.7-4-8.4-9.2-8.4c-5.2,0-9.2,3.7-9.2,8.4
									C114.9,169.5,119,173.2,124.1,173.2"/>
							</g>
							<g>
								<path class="st34" d="M124.1,156.3c5.2,0,9.2,3.7,9.2,8.4c0,4.7-4,8.4-9.2,8.4c-5.2,0-9.2-3.7-9.2-8.4
									C114.9,160,119,156.3,124.1,156.3z M124.1,168.1c1.8,0,3.2-1.4,3.2-3.2c0-1.8-1.4-3.2-3.2-3.2c-1.8,0-3.2,1.4-3.2,3.2
									C120.9,166.6,122.4,168.1,124.1,168.1"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_166_" class="st0" d="M134.3,164.8v2.6c0,5.3-4.5,9.4-10.2,9.4c-5.7,0-10.2-4.1-10.2-9.4l0-2.6
										c0,5.3,4.5,9.4,10.2,9.4C129.8,174.2,134.3,170.1,134.3,164.8z"/>
								</defs>
								<clipPath id="SVGID_167_">
									<use xlink:href="#SVGID_166_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st102">
									<path class="st34" d="M134.3,164.8v2.6c0,0.5,0,0.9-0.1,1.4v-2.6C134.3,165.7,134.3,165.2,134.3,164.8"/>
									<path class="st34" d="M134.2,166.1v2.6c-0.1,0.5-0.2,1-0.4,1.5v-2.6C134,167.2,134.1,166.7,134.2,166.1"/>
									<path class="st34" d="M133.9,167.6v2.6c-0.2,0.6-0.4,1.1-0.7,1.6v-2.6C133.4,168.7,133.7,168.2,133.9,167.6"/>
									<path class="st34" d="M133.1,169.3v2.6c-1.7,3-5.1,5-9,5c-5.7,0-10.2-4.1-10.2-9.4v-2.6c0,5.3,4.5,9.4,10.2,9.4
										C128.1,174.2,131.4,172.2,133.1,169.3"/>
								</g>
							</g>
						</g>
						<g class="st0">
							<g>
								<rect x="135.3" y="156.6" class="st34" width="15.8" height="2.6"/>
							</g>
							<g>
								<rect x="135.3" y="161.5" class="st34" width="4.9" height="2.6"/>
							</g>
							<g>
								<rect x="146.3" y="161.5" class="st34" width="4.8" height="2.6"/>
							</g>
							<g>
								<rect x="147.3" y="162.5" class="st34" width="4.8" height="2.6"/>
							</g>
							<g>
								<rect x="134.3" y="162.5" class="st34" width="4.9" height="2.6"/>
							</g>
							<g>
								<rect x="140.2" y="173" class="st34" width="6.2" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M134.3,155.6h17.8v6.9h-4.8V174h-8.2v-11.5h-4.9V155.6z M151.2,161.5v-4.9h-15.8v4.9h4.9V173h6.2
									v-11.5H151.2"/>
							</g>
							<g>
								<polygon class="st34" points="151.2,156.6 151.2,161.5 146.3,161.5 146.3,173 140.2,173 140.2,161.5 135.3,161.5 
									135.3,156.6 								"/>
							</g>
							<g>
								<rect x="139.2" y="174" class="st34" width="8.2" height="2.6"/>
							</g>
						</g>
						<g class="st0">
							<g>
								<rect x="154" y="156.6" class="st34" width="11.5" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="161.4" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<rect x="165.5" y="162.4" class="st34" width="1" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="163.3" class="st34" width="4.3" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="166.5" class="st34" width="4.3" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="168.4" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<polygon class="st34" points="165.5,156.6 165.5,161.4 160.2,161.4 160.2,163.3 164.5,163.3 164.5,166.5 160.2,166.5 
									160.2,168.4 165.9,168.4 165.9,173 154,173 154,156.6 								"/>
							</g>
							<g>
								<rect x="154" y="173" class="st34" width="11.8" height="2.6"/>
							</g>
							<g>
								<rect x="153" y="174" class="st34" width="13.8" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M165.5,167.4h1.4v6.5H153v-18.4h13.5v6.8h-1V167.4z M165.9,173v-4.5h-5.7v-2h4.3v-3.1h-4.3v-1.9h5.4
									v-4.8H154V173H165.9"/>
							</g>
						</g>
						<g class="st0">
							<g class="st0">
								<defs>
									<path id="SVGID_168_" class="st0" d="M180.5,157.5l0,2.6c-1.5-0.7-3.8-1.2-5.7-1.2c-4,0-6.6,2.1-6.6,5.4l0-2.6
										c0-3.3,2.6-5.4,6.6-5.4C176.7,156.3,178.9,156.8,180.5,157.5z"/>
								</defs>
								<clipPath id="SVGID_169_">
									<use xlink:href="#SVGID_168_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st103">
									<path class="st34" d="M180.5,157.5v2.6c-1.5-0.7-3.8-1.2-5.7-1.2c-2.8,0-4.9,1-5.9,2.8v-2.6c1-1.8,3.1-2.8,5.9-2.8
										C176.7,156.3,178.9,156.8,180.5,157.5"/>
									<path class="st34" d="M168.8,159.1v2.6c-0.2,0.3-0.3,0.6-0.4,0.9V160C168.6,159.7,168.7,159.4,168.8,159.1"/>
									<path class="st34" d="M168.5,160v2.6c-0.1,0.3-0.2,0.6-0.2,0.9v-2.6C168.3,160.5,168.4,160.3,168.5,160"/>
									<path class="st34" d="M168.3,160.8v2.6c0,0.3-0.1,0.6-0.1,0.8v-2.6C168.2,161.4,168.2,161.1,168.3,160.8"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_170_" class="st0" d="M175.7,163.5c-1.3,0-1.4,0.8-1.4,1v-2.6c0-0.2,0.1-1,1.4-1c1,0,2.8,0.4,4.8,1.2v2.6
										C178.5,163.9,176.7,163.5,175.7,163.5z"/>
								</defs>
								<clipPath id="SVGID_171_">
									<use xlink:href="#SVGID_170_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st104">
									<path class="st34" d="M180.5,162.1v2.6c-1.9-0.9-3.8-1.2-4.8-1.2c-0.8,0-1.1,0.3-1.3,0.5v-2.6c0.2-0.3,0.5-0.5,1.3-0.5
										C176.7,160.8,178.5,161.2,180.5,162.1"/>
									<path class="st34" d="M174.5,161.4v2.6c0,0.1-0.1,0.1-0.1,0.2v-2.6C174.4,161.5,174.4,161.5,174.5,161.4"/>
									<path class="st34" d="M174.4,161.6v2.6c0,0.1,0,0.1,0,0.2v-2.6C174.3,161.7,174.4,161.6,174.4,161.6"/>
									<path class="st34" d="M174.3,161.7v2.6c0,0.1,0,0.1,0,0.1v-2.6C174.3,161.8,174.3,161.8,174.3,161.7"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_172_" class="st0" d="M175.2,163v2.6c-0.6-0.2-0.9-0.6-0.9-1.1l0-2.6C174.3,162.4,174.6,162.7,175.2,163z"
										/>
								</defs>
								<clipPath id="SVGID_173_">
									<use xlink:href="#SVGID_172_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st105">
									<path class="st34" d="M175.2,163v2.6c-0.6-0.2-0.9-0.6-0.9-1.1v-2.6C174.3,162.4,174.6,162.7,175.2,163"/>
								</g>
							</g>
							<g>
								<polygon class="st34" points="181.5,163.7 181.5,166.3 180.7,165.9 180.7,163.3 								"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_174_" class="st0" d="M169.6,167.5c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6c0,1.4,0.4,2.4,1.4,3.2
										C169.6,165.8,169.6,166.7,169.6,167.5c0-0.9,0-1.7,0-2.6c1.4,1.2,3.2,1.6,4.3,1.8c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										C172.8,169.1,171,168.8,169.6,167.5z"/>
								</defs>
								<clipPath id="SVGID_175_">
									<use xlink:href="#SVGID_174_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st106">
									<path class="st34" d="M174.4,166.9v2.6c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6
										c0,1.4,0.4,2.4,1.4,3.2c1.4,1.2,3.2,1.6,4.3,1.8C174.1,166.8,174.3,166.8,174.4,166.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_176_" class="st0" d="M174.4,169.5c-0.1,0-0.3-0.1-0.5-0.1c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0.2,0,0.4,0.1,0.5,0.1C174.4,167.7,174.4,168.6,174.4,169.5z"/>
								</defs>
								<clipPath id="SVGID_177_">
									<use xlink:href="#SVGID_176_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st107">
									<path class="st34" d="M174.4,166.9v2.6c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6
										c0,1.4,0.4,2.4,1.4,3.2c1.4,1.2,3.2,1.6,4.3,1.8C174.1,166.8,174.3,166.8,174.4,166.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_178_" class="st0" d="M173.7,167.7v2.6c-1.1-0.2-3.2-0.6-4.8-2c-1.2-1-1.7-2.3-1.7-4l0-2.6
										c0,1.7,0.6,3,1.7,4C170.5,167.1,172.5,167.5,173.7,167.7z"/>
								</defs>
								<clipPath id="SVGID_179_">
									<use xlink:href="#SVGID_178_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st108">
									<path class="st34" d="M173.7,167.7v2.6c-1.1-0.2-3.2-0.6-4.8-2c-1.2-1-1.7-2.3-1.7-4v-2.6c0,1.7,0.6,3,1.7,4
										C170.5,167.1,172.5,167.5,173.7,167.7"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_180_" class="st0" d="M174.2,167.8v2.6c-0.1,0-0.3-0.1-0.5-0.1v-2.6C173.9,167.8,174,167.8,174.2,167.8z"/>
								</defs>
								<clipPath id="SVGID_181_">
									<use xlink:href="#SVGID_180_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st109">
									<path class="st34" d="M174.2,167.8v2.6c-0.1,0-0.3-0.1-0.5-0.1v-2.6C173.9,167.8,174,167.8,174.2,167.8"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_182_" class="st0" d="M175.3,170.5c0-0.3-0.1-0.9-0.9-1c0-0.8,0-1.5,0-2.3v-0.3c0.8,0.2,0.9,0.7,0.9,1v0.3
										C175.3,168.9,175.3,169.8,175.3,170.5z"/>
								</defs>
								<clipPath id="SVGID_183_">
									<use xlink:href="#SVGID_182_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st110">
									<path class="st34" d="M175.3,167.9v2.6c0-0.3-0.1-0.9-0.9-1v-2.6C175.2,167,175.3,167.6,175.3,167.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_184_" class="st0" d="M181.5,168.1v2.6c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2
										V163c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6C180.9,165.6,181.5,166.7,181.5,168.1z"/>
								</defs>
								<clipPath id="SVGID_185_">
									<use xlink:href="#SVGID_184_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st111">
									<path class="st34" d="M181.5,168.1v2.6c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2V163
										c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6C180.9,165.6,181.5,166.7,181.5,168.1"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_186_" class="st0" d="M175.3,167.9l0,2.6c0,0.3-0.1,1.1-1.4,1.1c-1.2,0-3.2-0.5-5.2-1.7l0-2.6
										c2,1.1,4.1,1.7,5.2,1.7C175.2,169,175.3,168.1,175.3,167.9z"/>
								</defs>
								<clipPath id="SVGID_187_">
									<use xlink:href="#SVGID_186_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st112">
									<path class="st34" d="M175.3,167.9v2.6c0,0,0,0.1,0,0.2V168C175.3,168,175.3,167.9,175.3,167.9"/>
									<path class="st34" d="M175.3,168v2.6c0,0.1,0,0.1,0,0.2v-2.6C175.3,168.2,175.3,168.1,175.3,168"/>
									<path class="st34" d="M175.2,168.2v2.6c0,0.1,0,0.1-0.1,0.2v-2.6C175.2,168.3,175.2,168.3,175.2,168.2"/>
									<path class="st34" d="M175.2,168.4v2.6c-0.2,0.3-0.5,0.6-1.3,0.6c-1.2,0-3.2-0.5-5.2-1.7v-2.6c2,1.1,4.1,1.7,5.2,1.7
										C174.7,169,175,168.7,175.2,168.4"/>
								</g>
							</g>
							<g>
								<polygon class="st34" points="168,172.7 168,175.4 167.7,175.2 167.7,172.6 								"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_188_" class="st0" d="M181.5,170.7c0,3.2-2.6,5.2-6.7,5.2c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c4.2,0,6.7-2,6.7-5.2v0.3C181.5,169.1,181.5,170,181.5,170.7z"/>
								</defs>
								<clipPath id="SVGID_189_">
									<use xlink:href="#SVGID_188_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st113">
									<path class="st34" d="M181.5,168.1v2.6c0,0.3,0,0.5-0.1,0.8v-2.6C181.4,168.6,181.5,168.3,181.5,168.1"/>
									<path class="st34" d="M181.4,168.9v2.6c0,0.3-0.1,0.5-0.2,0.8v-2.6C181.3,169.4,181.4,169.1,181.4,168.9"/>
									<path class="st34" d="M181.2,169.7v2.6c-0.1,0.3-0.2,0.5-0.4,0.8v-2.6C181,170.2,181.1,169.9,181.2,169.7"/>
									<path class="st34" d="M180.9,170.5v2.6c-1,1.7-3.2,2.8-6.1,2.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2
										C177.7,173.2,179.9,172.2,180.9,170.5"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_190_" class="st0" d="M174.7,175.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2c0,0,0,0,0,0
										c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										c0,0.1,0,0.2,0,0.3C174.7,175.6,174.7,175.7,174.7,175.8C174.7,175.8,174.7,175.8,174.7,175.8z"/>
								</defs>
								<clipPath id="SVGID_191_">
									<use xlink:href="#SVGID_190_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st114">
									<path class="st34" d="M180.9,170.5v2.6c-1,1.7-3.2,2.8-6.1,2.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2
										C177.7,173.2,179.9,172.2,180.9,170.5"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M174.8,156.3c1.9,0,4.2,0.5,5.7,1.2v4.6c-1.9-0.9-3.8-1.2-4.8-1.2c-1.3,0-1.4,0.8-1.4,1
									c0,0.5,0.3,0.9,0.9,1.1c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6c1.1,0.9,1.6,2,1.6,3.4c0,3.2-2.6,5.2-6.8,5.2
									c-2.1,0-4.4-0.5-6-1.2v-4.7c2,1.1,4.1,1.7,5.2,1.7c1.3,0,1.4-0.8,1.4-1.1c0-0.3-0.1-0.9-0.9-1c-0.1,0-0.3-0.1-0.5-0.1
									c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2C168.2,158.4,170.8,156.3,174.8,156.3z"/>
							</g>
							<g>
								<path class="st34" d="M180.5,163.9c1.3,1.1,1.9,2.4,1.9,4.1c0,3.7-3,6.2-7.7,6.2c-2.4,0-5-0.6-6.7-1.5l-0.3-0.1v-7.1
									l0.8,0.5c2.2,1.5,4.5,2,5.4,2c0.3,0,0.4-0.1,0.4-0.1c0,0,0,0-0.2-0.1c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-3.2-0.6-4.8-2
									c-1.2-1-1.7-2.3-1.7-4c0-3.9,3-6.4,7.6-6.4c2.2,0,4.8,0.6,6.4,1.4l0.3,0.1v6.8l-0.7-0.4c-2.2-1.1-4.2-1.5-5-1.5
									c-0.3,0-0.4,0.1-0.4,0.1c0,0,0.1,0,0.2,0.1c0.1,0,0.3,0.1,0.5,0.1C177.1,162.4,179.1,162.8,180.5,163.9z M174.7,173.2
									c4.2,0,6.8-2,6.8-5.2c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2c-0.6-0.2-0.9-0.6-0.9-1.1
									c0-0.2,0.1-1,1.4-1c1,0,2.8,0.4,4.8,1.2v-4.6c-1.5-0.7-3.8-1.2-5.7-1.2c-4,0-6.6,2.1-6.6,5.4c0,1.4,0.4,2.4,1.4,3.2
									c1.4,1.2,3.2,1.6,4.3,1.8c0.2,0,0.4,0.1,0.5,0.1c0.8,0.2,0.9,0.7,0.9,1c0,0.3-0.1,1.1-1.4,1.1c-1.2,0-3.2-0.5-5.2-1.7v4.7
									C170.3,172.7,172.6,173.2,174.7,173.2"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_192_" class="st0" d="M182.5,168.1l0,2.6c0,3.7-3,6.2-7.7,6.2c-2.4,0-5-0.6-6.7-1.5v-2.6
										c1.7,0.9,4.4,1.5,6.7,1.5C179.4,174.2,182.5,171.8,182.5,168.1z"/>
								</defs>
								<clipPath id="SVGID_193_">
									<use xlink:href="#SVGID_192_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st115">
									<path class="st34" d="M182.5,168.1v2.6c0,0.3,0,0.6-0.1,1V169C182.4,168.7,182.5,168.4,182.5,168.1"/>
									<path class="st34" d="M182.4,169v2.6c-0.1,0.3-0.1,0.7-0.2,1V170C182.3,169.7,182.3,169.3,182.4,169"/>
									<path class="st34" d="M182.2,170v2.6c-0.1,0.3-0.3,0.7-0.4,1v-2.6C181.9,170.6,182,170.3,182.2,170"/>
									<path class="st34" d="M181.7,170.9v2.6c-1.2,2-3.7,3.3-7,3.3c-2.4,0-5-0.6-6.7-1.5v-2.6c1.7,0.9,4.4,1.5,6.7,1.5
										C178.1,174.2,180.6,173,181.7,170.9"/>
								</g>
							</g>
						</g>
					</g>
					<defs>
						<filter id="Adobe_OpacityMaskFilter_4_" filterUnits="userSpaceOnUse" x="94.1" y="155.3" width="88.4" height="21.5">
							<feFlood  style="flood-color:white;flood-opacity:1" result="back"/>
							<feBlend  in="SourceGraphic" in2="back" mode="normal"/>
						</filter>
					</defs>
					<mask maskUnits="userSpaceOnUse" x="94.1" y="155.3" width="88.4" height="21.5" id="SVGID_155_">
						<g class="st116">
							
								<image style="overflow:visible;" width="93" height="27" xlink:href="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgEASABIAAD/7AARRHVja3kAAQAEAAAAHgAA/+4AIUFkb2JlAGTAAAAAAQMA
EAMCAwYAAAF0AAABfwAAAaD/2wCEABALCwsMCxAMDBAXDw0PFxsUEBAUGx8XFxcXFx8eFxoaGhoX
Hh4jJSclIx4vLzMzLy9AQEBAQEBAQEBAQEBAQEABEQ8PERMRFRISFRQRFBEUGhQWFhQaJhoaHBoa
JjAjHh4eHiMwKy4nJycuKzU1MDA1NUBAP0BAQEBAQEBAQEBAQP/CABEIABsAXQMBIgACEQEDEQH/
xABfAAEBAQAAAAAAAAAAAAAAAAAAAQcBAQAAAAAAAAAAAAAAAAAAAAAQAQEAAAAAAAAAAAAAAAAA
ADBQEQEAAAAAAAAAAAAAAAAAAABQEgEAAAAAAAAAAAAAAAAAAAAw/9oADAMBAAIRAxEAAADPykWA
AABYAAAAAAf/2gAIAQIAAQUAT//aAAgBAwABBQBP/9oACAEBAAEFAKH/2gAIAQICBj8AT//aAAgB
AwIGPwBP/9oACAEBAQY/AEP/2Q==" transform="matrix(1 0 0 1 91.75 152.5)">
							</image>
						</g>
					</mask>
					<g>
						<g class="st0">
							<g>
								<rect x="95.1" y="156.6" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<rect x="106.8" y="156.6" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<polygon class="st34" points="105.8,163.8 105.8,166.4 100.8,159.2 100.8,156.6 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.8,163.8 106.8,166.4 106.1,166.7 106.1,164.1 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.1,164.1 106.1,166.7 105.8,166.4 105.8,163.8 								"/>
							</g>
							<g>
								<polygon class="st34" points="101.2,165.4 101.2,168 100.5,168.4 100.5,165.8 								"/>
							</g>
							<g>
								<polygon class="st34" points="101.5,165.8 101.5,168.4 101.2,168 101.2,165.4 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.5,173 106.5,175.6 101.5,168.4 101.5,165.8 								"/>
							</g>
							<g>
								<rect x="106.5" y="173" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<rect x="95.1" y="173" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M105.8,155.6h7.4V174h-7.3l-4.4-6.4v6.4h-7.4v-18.4h7.3l4.4,6.4V155.6z M112.2,173v-16.4h-5.4v7.2
									l-0.7,0.3l-0.3-0.3l-5-7.2h-5.7V173h5.4v-7.2l0.7-0.3l0.3,0.3l5,7.2H112.2"/>
							</g>
							<g>
								<polygon class="st34" points="112.2,156.6 112.2,173 106.5,173 101.5,165.8 101.2,165.4 100.5,165.8 100.5,173 95.1,173 
									95.1,156.6 100.8,156.6 105.8,163.8 106.1,164.1 106.8,163.8 106.8,156.6 								"/>
							</g>
							<g>
								<polygon class="st34" points="105.9,174 105.9,176.6 101.5,170.2 101.5,167.6 								"/>
							</g>
							<g>
								<rect x="105.9" y="174" class="st34" width="7.3" height="2.6"/>
							</g>
							<g>
								<rect x="94.1" y="174" class="st34" width="7.4" height="2.6"/>
							</g>
						</g>
						<g class="st0">
							<g class="st0">
								<defs>
									<path id="SVGID_194_" class="st0" d="M133.3,164.8l0,2.6c0-4.7-4-8.4-9.2-8.4c-5.2,0-9.2,3.7-9.2,8.4l0-2.6
										c0-4.7,4-8.4,9.2-8.4C129.3,156.3,133.3,160,133.3,164.8z"/>
								</defs>
								<clipPath id="SVGID_195_">
									<use xlink:href="#SVGID_194_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st117">
									<path class="st34" d="M133.3,164.8v2.6c0-4.7-4-8.4-9.2-8.4c-3.6,0-6.6,1.8-8.1,4.5v-2.6c1.5-2.7,4.6-4.5,8.1-4.5
										C129.3,156.3,133.3,160,133.3,164.8"/>
									<path class="st34" d="M116,160.8v2.6c-0.3,0.5-0.5,0.9-0.6,1.4v-2.6C115.5,161.7,115.7,161.3,116,160.8"/>
									<path class="st34" d="M115.3,162.2v2.6c-0.1,0.4-0.3,0.9-0.3,1.3v-2.6C115.1,163.1,115.2,162.7,115.3,162.2"/>
									<path class="st34" d="M115,163.6v2.6c-0.1,0.4-0.1,0.8-0.1,1.2v-2.6C114.9,164.4,115,164,115,163.6"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_196_" class="st0" d="M120.9,167.5c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3v-0.3c0-1.8,1.4-3.2,3.2-3.2
										c1.8,0,3.2,1.4,3.2,3.2l0,2.6c0-1.8-1.4-3.2-3.2-3.2C122.4,164.3,120.9,165.8,120.9,167.5z"/>
								</defs>
								<clipPath id="SVGID_197_">
									<use xlink:href="#SVGID_196_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st118">
									<path class="st34" d="M127.3,164.9v2.6c0-1.8-1.4-3.2-3.2-3.2c-1.2,0-2.2,0.7-2.8,1.6v-2.6c0.5-1,1.6-1.6,2.8-1.6
										C125.9,161.7,127.3,163.1,127.3,164.9"/>
									<path class="st34" d="M121.3,163.3v2.6c-0.1,0.2-0.2,0.4-0.3,0.6v-2.6C121.2,163.7,121.2,163.5,121.3,163.3"/>
									<path class="st34" d="M121.1,163.9v2.6c-0.1,0.2-0.1,0.3-0.1,0.5v-2.6C121,164.2,121,164.1,121.1,163.9"/>
									<path class="st34" d="M121,164.4v2.6c0,0.2,0,0.3,0,0.5v-2.6C120.9,164.7,120.9,164.6,121,164.4"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_198_" class="st0" d="M126.3,164.9l0,2.6c0-1.2-1-2.2-2.2-2.2c-1.2,0-2.2,1-2.2,2.2l0-2.6
										c0-1.2,1-2.2,2.2-2.2C125.3,162.7,126.3,163.7,126.3,164.9z"/>
								</defs>
								<clipPath id="SVGID_199_">
									<use xlink:href="#SVGID_198_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st119">
									<path class="st34" d="M126.3,164.9v2.6c0-1.2-1-2.2-2.2-2.2c-0.8,0-1.5,0.4-1.9,1.1v-2.6c0.4-0.7,1.1-1.1,1.9-1.1
										C125.3,162.7,126.3,163.7,126.3,164.9"/>
									<path class="st34" d="M122.2,163.8v2.6c-0.1,0.1-0.1,0.3-0.2,0.4v-2.6C122.1,164.1,122.1,163.9,122.2,163.8"/>
									<path class="st34" d="M122,164.2v2.6c0,0.1-0.1,0.2-0.1,0.4v-2.6C122,164.4,122,164.3,122,164.2"/>
									<path class="st34" d="M121.9,164.6v2.6c0,0.1,0,0.2,0,0.3v-2.6C121.9,164.8,121.9,164.7,121.9,164.6"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_200_" class="st0" d="M127.3,164.9l0,2.6c0,1.8-1.4,3.2-3.2,3.2c-1.8,0-3.2-1.4-3.2-3.2v-2.6
										c0,1.8,1.4,3.2,3.2,3.2C125.9,168.1,127.3,166.6,127.3,164.9z"/>
								</defs>
								<clipPath id="SVGID_201_">
									<use xlink:href="#SVGID_200_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st120">
									<path class="st34" d="M127.3,164.9v2.6c0,0.2,0,0.3,0,0.5v-2.6C127.3,165.2,127.3,165.1,127.3,164.9"/>
									<path class="st34" d="M127.3,165.4v2.6c0,0.2-0.1,0.4-0.1,0.5v-2.6C127.2,165.7,127.2,165.5,127.3,165.4"/>
									<path class="st34" d="M127.1,165.9v2.6c-0.1,0.2-0.2,0.4-0.3,0.6v-2.6C127,166.3,127.1,166.1,127.1,165.9"/>
									<path class="st34" d="M126.9,166.5v2.6c-0.5,1-1.6,1.6-2.8,1.6c-1.8,0-3.2-1.4-3.2-3.2v-2.6c0,1.8,1.4,3.2,3.2,3.2
										C125.3,168.1,126.3,167.4,126.9,166.5"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M124.1,161.7c1.8,0,3.2,1.4,3.2,3.2c0,1.8-1.4,3.2-3.2,3.2c-1.8,0-3.2-1.4-3.2-3.2
									C120.9,163.1,122.4,161.7,124.1,161.7z M124.1,167.1c1.2,0,2.2-1,2.2-2.2c0-1.2-1-2.2-2.2-2.2c-1.2,0-2.2,1-2.2,2.2
									C121.9,166.1,122.9,167.1,124.1,167.1"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_202_" class="st0" d="M124.1,175.8c-5.2,0-9.2-3.7-9.2-8.4l0-2.6c0,4.7,4,8.4,9.2,8.4
										c5.2,0,9.2-3.7,9.2-8.4l0,0.3v2.3C133.3,172.1,129.3,175.8,124.1,175.8z"/>
								</defs>
								<clipPath id="SVGID_203_">
									<use xlink:href="#SVGID_202_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st121">
									<path class="st34" d="M133.3,164.8v2.6c0,0.4,0,0.8-0.1,1.2V166C133.3,165.6,133.3,165.2,133.3,164.8"/>
									<path class="st34" d="M133.2,166v2.6c-0.1,0.5-0.2,0.9-0.3,1.3v-2.6C133,166.9,133.2,166.4,133.2,166"/>
									<path class="st34" d="M132.9,167.3v2.6c-0.2,0.5-0.4,1-0.6,1.4v-2.6C132.5,168.3,132.7,167.8,132.9,167.3"/>
									<path class="st34" d="M132.3,168.8v2.6c-1.5,2.7-4.6,4.5-8.1,4.5c-5.2,0-9.2-3.7-9.2-8.4v-2.6c0,4.7,4,8.4,9.2,8.4
										C127.7,173.2,130.7,171.4,132.3,168.8"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M124.1,155.3c5.7,0,10.2,4.2,10.2,9.4c0,5.3-4.5,9.4-10.2,9.4c-5.7,0-10.2-4.1-10.2-9.4
									C113.9,159.5,118.4,155.3,124.1,155.3z M124.1,173.2c5.2,0,9.2-3.7,9.2-8.4c0-4.7-4-8.4-9.2-8.4c-5.2,0-9.2,3.7-9.2,8.4
									C114.9,169.5,119,173.2,124.1,173.2"/>
							</g>
							<g>
								<path class="st34" d="M124.1,156.3c5.2,0,9.2,3.7,9.2,8.4c0,4.7-4,8.4-9.2,8.4c-5.2,0-9.2-3.7-9.2-8.4
									C114.9,160,119,156.3,124.1,156.3z M124.1,168.1c1.8,0,3.2-1.4,3.2-3.2c0-1.8-1.4-3.2-3.2-3.2c-1.8,0-3.2,1.4-3.2,3.2
									C120.9,166.6,122.4,168.1,124.1,168.1"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_204_" class="st0" d="M134.3,164.8v2.6c0,5.3-4.5,9.4-10.2,9.4c-5.7,0-10.2-4.1-10.2-9.4l0-2.6
										c0,5.3,4.5,9.4,10.2,9.4C129.8,174.2,134.3,170.1,134.3,164.8z"/>
								</defs>
								<clipPath id="SVGID_205_">
									<use xlink:href="#SVGID_204_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st122">
									<path class="st34" d="M134.3,164.8v2.6c0,0.5,0,0.9-0.1,1.4v-2.6C134.3,165.7,134.3,165.2,134.3,164.8"/>
									<path class="st34" d="M134.2,166.1v2.6c-0.1,0.5-0.2,1-0.4,1.5v-2.6C134,167.2,134.1,166.7,134.2,166.1"/>
									<path class="st34" d="M133.9,167.6v2.6c-0.2,0.6-0.4,1.1-0.7,1.6v-2.6C133.4,168.7,133.7,168.2,133.9,167.6"/>
									<path class="st34" d="M133.1,169.3v2.6c-1.7,3-5.1,5-9,5c-5.7,0-10.2-4.1-10.2-9.4v-2.6c0,5.3,4.5,9.4,10.2,9.4
										C128.1,174.2,131.4,172.2,133.1,169.3"/>
								</g>
							</g>
						</g>
						<g class="st0">
							<g>
								<rect x="135.3" y="156.6" class="st34" width="15.8" height="2.6"/>
							</g>
							<g>
								<rect x="135.3" y="161.5" class="st34" width="4.9" height="2.6"/>
							</g>
							<g>
								<rect x="146.3" y="161.5" class="st34" width="4.8" height="2.6"/>
							</g>
							<g>
								<rect x="147.3" y="162.5" class="st34" width="4.8" height="2.6"/>
							</g>
							<g>
								<rect x="134.3" y="162.5" class="st34" width="4.9" height="2.6"/>
							</g>
							<g>
								<rect x="140.2" y="173" class="st34" width="6.2" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M134.3,155.6h17.8v6.9h-4.8V174h-8.2v-11.5h-4.9V155.6z M151.2,161.5v-4.9h-15.8v4.9h4.9V173h6.2
									v-11.5H151.2"/>
							</g>
							<g>
								<polygon class="st34" points="151.2,156.6 151.2,161.5 146.3,161.5 146.3,173 140.2,173 140.2,161.5 135.3,161.5 
									135.3,156.6 								"/>
							</g>
							<g>
								<rect x="139.2" y="174" class="st34" width="8.2" height="2.6"/>
							</g>
						</g>
						<g class="st0">
							<g>
								<rect x="154" y="156.6" class="st34" width="11.5" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="161.4" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<rect x="165.5" y="162.4" class="st34" width="1" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="163.3" class="st34" width="4.3" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="166.5" class="st34" width="4.3" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="168.4" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<polygon class="st34" points="165.5,156.6 165.5,161.4 160.2,161.4 160.2,163.3 164.5,163.3 164.5,166.5 160.2,166.5 
									160.2,168.4 165.9,168.4 165.9,173 154,173 154,156.6 								"/>
							</g>
							<g>
								<rect x="154" y="173" class="st34" width="11.8" height="2.6"/>
							</g>
							<g>
								<rect x="153" y="174" class="st34" width="13.8" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M165.5,167.4h1.4v6.5H153v-18.4h13.5v6.8h-1V167.4z M165.9,173v-4.5h-5.7v-2h4.3v-3.1h-4.3v-1.9h5.4
									v-4.8H154V173H165.9"/>
							</g>
						</g>
						<g class="st0">
							<g class="st0">
								<defs>
									<path id="SVGID_206_" class="st0" d="M180.5,157.5l0,2.6c-1.5-0.7-3.8-1.2-5.7-1.2c-4,0-6.6,2.1-6.6,5.4l0-2.6
										c0-3.3,2.6-5.4,6.6-5.4C176.7,156.3,178.9,156.8,180.5,157.5z"/>
								</defs>
								<clipPath id="SVGID_207_">
									<use xlink:href="#SVGID_206_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st123">
									<path class="st34" d="M180.5,157.5v2.6c-1.5-0.7-3.8-1.2-5.7-1.2c-2.8,0-4.9,1-5.9,2.8v-2.6c1-1.8,3.1-2.8,5.9-2.8
										C176.7,156.3,178.9,156.8,180.5,157.5"/>
									<path class="st34" d="M168.8,159.1v2.6c-0.2,0.3-0.3,0.6-0.4,0.9V160C168.6,159.7,168.7,159.4,168.8,159.1"/>
									<path class="st34" d="M168.5,160v2.6c-0.1,0.3-0.2,0.6-0.2,0.9v-2.6C168.3,160.5,168.4,160.3,168.5,160"/>
									<path class="st34" d="M168.3,160.8v2.6c0,0.3-0.1,0.6-0.1,0.8v-2.6C168.2,161.4,168.2,161.1,168.3,160.8"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_208_" class="st0" d="M175.7,163.5c-1.3,0-1.4,0.8-1.4,1v-2.6c0-0.2,0.1-1,1.4-1c1,0,2.8,0.4,4.8,1.2v2.6
										C178.5,163.9,176.7,163.5,175.7,163.5z"/>
								</defs>
								<clipPath id="SVGID_209_">
									<use xlink:href="#SVGID_208_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st124">
									<path class="st34" d="M180.5,162.1v2.6c-1.9-0.9-3.8-1.2-4.8-1.2c-0.8,0-1.1,0.3-1.3,0.5v-2.6c0.2-0.3,0.5-0.5,1.3-0.5
										C176.7,160.8,178.5,161.2,180.5,162.1"/>
									<path class="st34" d="M174.5,161.4v2.6c0,0.1-0.1,0.1-0.1,0.2v-2.6C174.4,161.5,174.4,161.5,174.5,161.4"/>
									<path class="st34" d="M174.4,161.6v2.6c0,0.1,0,0.1,0,0.2v-2.6C174.3,161.7,174.4,161.6,174.4,161.6"/>
									<path class="st34" d="M174.3,161.7v2.6c0,0.1,0,0.1,0,0.1v-2.6C174.3,161.8,174.3,161.8,174.3,161.7"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_210_" class="st0" d="M175.2,163v2.6c-0.6-0.2-0.9-0.6-0.9-1.1l0-2.6C174.3,162.4,174.6,162.7,175.2,163z"
										/>
								</defs>
								<clipPath id="SVGID_211_">
									<use xlink:href="#SVGID_210_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st125">
									<path class="st34" d="M175.2,163v2.6c-0.6-0.2-0.9-0.6-0.9-1.1v-2.6C174.3,162.4,174.6,162.7,175.2,163"/>
								</g>
							</g>
							<g>
								<polygon class="st34" points="181.5,163.7 181.5,166.3 180.7,165.9 180.7,163.3 								"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_212_" class="st0" d="M169.6,167.5c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6c0,1.4,0.4,2.4,1.4,3.2
										C169.6,165.8,169.6,166.7,169.6,167.5c0-0.9,0-1.7,0-2.6c1.4,1.2,3.2,1.6,4.3,1.8c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										C172.8,169.1,171,168.8,169.6,167.5z"/>
								</defs>
								<clipPath id="SVGID_213_">
									<use xlink:href="#SVGID_212_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st126">
									<path class="st34" d="M174.4,166.9v2.6c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6
										c0,1.4,0.4,2.4,1.4,3.2c1.4,1.2,3.2,1.6,4.3,1.8C174.1,166.8,174.3,166.8,174.4,166.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_214_" class="st0" d="M174.4,169.5c-0.1,0-0.3-0.1-0.5-0.1c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0.2,0,0.4,0.1,0.5,0.1C174.4,167.7,174.4,168.6,174.4,169.5z"/>
								</defs>
								<clipPath id="SVGID_215_">
									<use xlink:href="#SVGID_214_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st127">
									<path class="st34" d="M174.4,166.9v2.6c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6
										c0,1.4,0.4,2.4,1.4,3.2c1.4,1.2,3.2,1.6,4.3,1.8C174.1,166.8,174.3,166.8,174.4,166.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_216_" class="st0" d="M173.7,167.7v2.6c-1.1-0.2-3.2-0.6-4.8-2c-1.2-1-1.7-2.3-1.7-4l0-2.6
										c0,1.7,0.6,3,1.7,4C170.5,167.1,172.5,167.5,173.7,167.7z"/>
								</defs>
								<clipPath id="SVGID_217_">
									<use xlink:href="#SVGID_216_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st128">
									<path class="st34" d="M173.7,167.7v2.6c-1.1-0.2-3.2-0.6-4.8-2c-1.2-1-1.7-2.3-1.7-4v-2.6c0,1.7,0.6,3,1.7,4
										C170.5,167.1,172.5,167.5,173.7,167.7"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_218_" class="st0" d="M174.2,167.8v2.6c-0.1,0-0.3-0.1-0.5-0.1v-2.6C173.9,167.8,174,167.8,174.2,167.8z"/>
								</defs>
								<clipPath id="SVGID_219_">
									<use xlink:href="#SVGID_218_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st129">
									<path class="st34" d="M174.2,167.8v2.6c-0.1,0-0.3-0.1-0.5-0.1v-2.6C173.9,167.8,174,167.8,174.2,167.8"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_220_" class="st0" d="M175.3,170.5c0-0.3-0.1-0.9-0.9-1c0-0.8,0-1.5,0-2.3v-0.3c0.8,0.2,0.9,0.7,0.9,1v0.3
										C175.3,168.9,175.3,169.8,175.3,170.5z"/>
								</defs>
								<clipPath id="SVGID_221_">
									<use xlink:href="#SVGID_220_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st130">
									<path class="st34" d="M175.3,167.9v2.6c0-0.3-0.1-0.9-0.9-1v-2.6C175.2,167,175.3,167.6,175.3,167.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_222_" class="st0" d="M181.5,168.1v2.6c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2
										V163c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6C180.9,165.6,181.5,166.7,181.5,168.1z"/>
								</defs>
								<clipPath id="SVGID_223_">
									<use xlink:href="#SVGID_222_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st131">
									<path class="st34" d="M181.5,168.1v2.6c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2V163
										c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6C180.9,165.6,181.5,166.7,181.5,168.1"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_224_" class="st0" d="M175.3,167.9l0,2.6c0,0.3-0.1,1.1-1.4,1.1c-1.2,0-3.2-0.5-5.2-1.7l0-2.6
										c2,1.1,4.1,1.7,5.2,1.7C175.2,169,175.3,168.1,175.3,167.9z"/>
								</defs>
								<clipPath id="SVGID_225_">
									<use xlink:href="#SVGID_224_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st132">
									<path class="st34" d="M175.3,167.9v2.6c0,0,0,0.1,0,0.2V168C175.3,168,175.3,167.9,175.3,167.9"/>
									<path class="st34" d="M175.3,168v2.6c0,0.1,0,0.1,0,0.2v-2.6C175.3,168.2,175.3,168.1,175.3,168"/>
									<path class="st34" d="M175.2,168.2v2.6c0,0.1,0,0.1-0.1,0.2v-2.6C175.2,168.3,175.2,168.3,175.2,168.2"/>
									<path class="st34" d="M175.2,168.4v2.6c-0.2,0.3-0.5,0.6-1.3,0.6c-1.2,0-3.2-0.5-5.2-1.7v-2.6c2,1.1,4.1,1.7,5.2,1.7
										C174.7,169,175,168.7,175.2,168.4"/>
								</g>
							</g>
							<g>
								<polygon class="st34" points="168,172.7 168,175.4 167.7,175.2 167.7,172.6 								"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_226_" class="st0" d="M181.5,170.7c0,3.2-2.6,5.2-6.7,5.2c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c4.2,0,6.7-2,6.7-5.2v0.3C181.5,169.1,181.5,170,181.5,170.7z"/>
								</defs>
								<clipPath id="SVGID_227_">
									<use xlink:href="#SVGID_226_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st133">
									<path class="st34" d="M181.5,168.1v2.6c0,0.3,0,0.5-0.1,0.8v-2.6C181.4,168.6,181.5,168.3,181.5,168.1"/>
									<path class="st34" d="M181.4,168.9v2.6c0,0.3-0.1,0.5-0.2,0.8v-2.6C181.3,169.4,181.4,169.1,181.4,168.9"/>
									<path class="st34" d="M181.2,169.7v2.6c-0.1,0.3-0.2,0.5-0.4,0.8v-2.6C181,170.2,181.1,169.9,181.2,169.7"/>
									<path class="st34" d="M180.9,170.5v2.6c-1,1.7-3.2,2.8-6.1,2.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2
										C177.7,173.2,179.9,172.2,180.9,170.5"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_228_" class="st0" d="M174.7,175.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2c0,0,0,0,0,0
										c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										c0,0.1,0,0.2,0,0.3C174.7,175.6,174.7,175.7,174.7,175.8C174.7,175.8,174.7,175.8,174.7,175.8z"/>
								</defs>
								<clipPath id="SVGID_229_">
									<use xlink:href="#SVGID_228_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st134">
									<path class="st34" d="M180.9,170.5v2.6c-1,1.7-3.2,2.8-6.1,2.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2
										C177.7,173.2,179.9,172.2,180.9,170.5"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M174.8,156.3c1.9,0,4.2,0.5,5.7,1.2v4.6c-1.9-0.9-3.8-1.2-4.8-1.2c-1.3,0-1.4,0.8-1.4,1
									c0,0.5,0.3,0.9,0.9,1.1c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6c1.1,0.9,1.6,2,1.6,3.4c0,3.2-2.6,5.2-6.8,5.2
									c-2.1,0-4.4-0.5-6-1.2v-4.7c2,1.1,4.1,1.7,5.2,1.7c1.3,0,1.4-0.8,1.4-1.1c0-0.3-0.1-0.9-0.9-1c-0.1,0-0.3-0.1-0.5-0.1
									c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2C168.2,158.4,170.8,156.3,174.8,156.3z"/>
							</g>
							<g>
								<path class="st34" d="M180.5,163.9c1.3,1.1,1.9,2.4,1.9,4.1c0,3.7-3,6.2-7.7,6.2c-2.4,0-5-0.6-6.7-1.5l-0.3-0.1v-7.1
									l0.8,0.5c2.2,1.5,4.5,2,5.4,2c0.3,0,0.4-0.1,0.4-0.1c0,0,0,0-0.2-0.1c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-3.2-0.6-4.8-2
									c-1.2-1-1.7-2.3-1.7-4c0-3.9,3-6.4,7.6-6.4c2.2,0,4.8,0.6,6.4,1.4l0.3,0.1v6.8l-0.7-0.4c-2.2-1.1-4.2-1.5-5-1.5
									c-0.3,0-0.4,0.1-0.4,0.1c0,0,0.1,0,0.2,0.1c0.1,0,0.3,0.1,0.5,0.1C177.1,162.4,179.1,162.8,180.5,163.9z M174.7,173.2
									c4.2,0,6.8-2,6.8-5.2c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2c-0.6-0.2-0.9-0.6-0.9-1.1
									c0-0.2,0.1-1,1.4-1c1,0,2.8,0.4,4.8,1.2v-4.6c-1.5-0.7-3.8-1.2-5.7-1.2c-4,0-6.6,2.1-6.6,5.4c0,1.4,0.4,2.4,1.4,3.2
									c1.4,1.2,3.2,1.6,4.3,1.8c0.2,0,0.4,0.1,0.5,0.1c0.8,0.2,0.9,0.7,0.9,1c0,0.3-0.1,1.1-1.4,1.1c-1.2,0-3.2-0.5-5.2-1.7v4.7
									C170.3,172.7,172.6,173.2,174.7,173.2"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_230_" class="st0" d="M182.5,168.1l0,2.6c0,3.7-3,6.2-7.7,6.2c-2.4,0-5-0.6-6.7-1.5v-2.6
										c1.7,0.9,4.4,1.5,6.7,1.5C179.4,174.2,182.5,171.8,182.5,168.1z"/>
								</defs>
								<clipPath id="SVGID_231_">
									<use xlink:href="#SVGID_230_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st135">
									<path class="st34" d="M182.5,168.1v2.6c0,0.3,0,0.6-0.1,1V169C182.4,168.7,182.5,168.4,182.5,168.1"/>
									<path class="st34" d="M182.4,169v2.6c-0.1,0.3-0.1,0.7-0.2,1V170C182.3,169.7,182.3,169.3,182.4,169"/>
									<path class="st34" d="M182.2,170v2.6c-0.1,0.3-0.3,0.7-0.4,1v-2.6C181.9,170.6,182,170.3,182.2,170"/>
									<path class="st34" d="M181.7,170.9v2.6c-1.2,2-3.7,3.3-7,3.3c-2.4,0-5-0.6-6.7-1.5v-2.6c1.7,0.9,4.4,1.5,6.7,1.5
										C178.1,174.2,180.6,173,181.7,170.9"/>
								</g>
							</g>
						</g>
					</g>
				</g>
				<defs>
					<filter id="Adobe_OpacityMaskFilter_5_" filterUnits="userSpaceOnUse" x="94.1" y="155.3" width="88.4" height="21.5">
						<feFlood  style="flood-color:white;flood-opacity:1" result="back"/>
						<feBlend  in="SourceGraphic" in2="back" mode="normal"/>
					</filter>
				</defs>
				<mask maskUnits="userSpaceOnUse" x="94.1" y="155.3" width="88.4" height="21.5" id="SVGID_232_">
					<g class="st136">
						
							<image style="overflow:visible;" width="93" height="27" xlink:href="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgEASABIAAD/7AARRHVja3kAAQAEAAAAHgAA/+4AIUFkb2JlAGTAAAAAAQMA
EAMCAwYAAAF0AAABfwAAAaD/2wCEABALCwsMCxAMDBAXDw0PFxsUEBAUGx8XFxcXFx8eFxoaGhoX
Hh4jJSclIx4vLzMzLy9AQEBAQEBAQEBAQEBAQEABEQ8PERMRFRISFRQRFBEUGhQWFhQaJhoaHBoa
JjAjHh4eHiMwKy4nJycuKzU1MDA1NUBAP0BAQEBAQEBAQEBAQP/CABEIABsAXQMBIgACEQEDEQH/
xABfAAEBAQAAAAAAAAAAAAAAAAAAAQcBAQAAAAAAAAAAAAAAAAAAAAAQAQEAAAAAAAAAAAAAAAAA
ADBQEQEAAAAAAAAAAAAAAAAAAABQEgEAAAAAAAAAAAAAAAAAAAAw/9oADAMBAAIRAxEAAADPykWA
AABYAAAAAAf/2gAIAQIAAQUAT//aAAgBAwABBQBP/9oACAEBAAEFAKH/2gAIAQICBj8AT//aAAgB
AwIGPwBP/9oACAEBAQY/AEP/2Q==" transform="matrix(1 0 0 1 91.75 152.5)">
						</image>
					</g>
				</mask>
				<g class="st137">
					<g>
						<g class="st0">
							<g>
								<rect x="95.1" y="156.6" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<rect x="106.8" y="156.6" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<polygon class="st34" points="105.8,163.8 105.8,166.4 100.8,159.2 100.8,156.6 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.8,163.8 106.8,166.4 106.1,166.7 106.1,164.1 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.1,164.1 106.1,166.7 105.8,166.4 105.8,163.8 								"/>
							</g>
							<g>
								<polygon class="st34" points="101.2,165.4 101.2,168 100.5,168.4 100.5,165.8 								"/>
							</g>
							<g>
								<polygon class="st34" points="101.5,165.8 101.5,168.4 101.2,168 101.2,165.4 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.5,173 106.5,175.6 101.5,168.4 101.5,165.8 								"/>
							</g>
							<g>
								<rect x="106.5" y="173" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<rect x="95.1" y="173" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M105.8,155.6h7.4V174h-7.3l-4.4-6.4v6.4h-7.4v-18.4h7.3l4.4,6.4V155.6z M112.2,173v-16.4h-5.4v7.2
									l-0.7,0.3l-0.3-0.3l-5-7.2h-5.7V173h5.4v-7.2l0.7-0.3l0.3,0.3l5,7.2H112.2"/>
							</g>
							<g>
								<polygon class="st34" points="112.2,156.6 112.2,173 106.5,173 101.5,165.8 101.2,165.4 100.5,165.8 100.5,173 95.1,173 
									95.1,156.6 100.8,156.6 105.8,163.8 106.1,164.1 106.8,163.8 106.8,156.6 								"/>
							</g>
							<g>
								<polygon class="st34" points="105.9,174 105.9,176.6 101.5,170.2 101.5,167.6 								"/>
							</g>
							<g>
								<rect x="105.9" y="174" class="st34" width="7.3" height="2.6"/>
							</g>
							<g>
								<rect x="94.1" y="174" class="st34" width="7.4" height="2.6"/>
							</g>
						</g>
						<g class="st0">
							<g class="st0">
								<defs>
									<path id="SVGID_233_" class="st0" d="M133.3,164.8l0,2.6c0-4.7-4-8.4-9.2-8.4c-5.2,0-9.2,3.7-9.2,8.4l0-2.6
										c0-4.7,4-8.4,9.2-8.4C129.3,156.3,133.3,160,133.3,164.8z"/>
								</defs>
								<clipPath id="SVGID_234_">
									<use xlink:href="#SVGID_233_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st138">
									<path class="st34" d="M133.3,164.8v2.6c0-4.7-4-8.4-9.2-8.4c-3.6,0-6.6,1.8-8.1,4.5v-2.6c1.5-2.7,4.6-4.5,8.1-4.5
										C129.3,156.3,133.3,160,133.3,164.8"/>
									<path class="st34" d="M116,160.8v2.6c-0.3,0.5-0.5,0.9-0.6,1.4v-2.6C115.5,161.7,115.7,161.3,116,160.8"/>
									<path class="st34" d="M115.3,162.2v2.6c-0.1,0.4-0.3,0.9-0.3,1.3v-2.6C115.1,163.1,115.2,162.7,115.3,162.2"/>
									<path class="st34" d="M115,163.6v2.6c-0.1,0.4-0.1,0.8-0.1,1.2v-2.6C114.9,164.4,115,164,115,163.6"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_235_" class="st0" d="M120.9,167.5c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3v-0.3c0-1.8,1.4-3.2,3.2-3.2
										c1.8,0,3.2,1.4,3.2,3.2l0,2.6c0-1.8-1.4-3.2-3.2-3.2C122.4,164.3,120.9,165.8,120.9,167.5z"/>
								</defs>
								<clipPath id="SVGID_236_">
									<use xlink:href="#SVGID_235_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st139">
									<path class="st34" d="M127.3,164.9v2.6c0-1.8-1.4-3.2-3.2-3.2c-1.2,0-2.2,0.7-2.8,1.6v-2.6c0.5-1,1.6-1.6,2.8-1.6
										C125.9,161.7,127.3,163.1,127.3,164.9"/>
									<path class="st34" d="M121.3,163.3v2.6c-0.1,0.2-0.2,0.4-0.3,0.6v-2.6C121.2,163.7,121.2,163.5,121.3,163.3"/>
									<path class="st34" d="M121.1,163.9v2.6c-0.1,0.2-0.1,0.3-0.1,0.5v-2.6C121,164.2,121,164.1,121.1,163.9"/>
									<path class="st34" d="M121,164.4v2.6c0,0.2,0,0.3,0,0.5v-2.6C120.9,164.7,120.9,164.6,121,164.4"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_237_" class="st0" d="M126.3,164.9l0,2.6c0-1.2-1-2.2-2.2-2.2c-1.2,0-2.2,1-2.2,2.2l0-2.6
										c0-1.2,1-2.2,2.2-2.2C125.3,162.7,126.3,163.7,126.3,164.9z"/>
								</defs>
								<clipPath id="SVGID_238_">
									<use xlink:href="#SVGID_237_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st140">
									<path class="st34" d="M126.3,164.9v2.6c0-1.2-1-2.2-2.2-2.2c-0.8,0-1.5,0.4-1.9,1.1v-2.6c0.4-0.7,1.1-1.1,1.9-1.1
										C125.3,162.7,126.3,163.7,126.3,164.9"/>
									<path class="st34" d="M122.2,163.8v2.6c-0.1,0.1-0.1,0.3-0.2,0.4v-2.6C122.1,164.1,122.1,163.9,122.2,163.8"/>
									<path class="st34" d="M122,164.2v2.6c0,0.1-0.1,0.2-0.1,0.4v-2.6C122,164.4,122,164.3,122,164.2"/>
									<path class="st34" d="M121.9,164.6v2.6c0,0.1,0,0.2,0,0.3v-2.6C121.9,164.8,121.9,164.7,121.9,164.6"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_239_" class="st0" d="M127.3,164.9l0,2.6c0,1.8-1.4,3.2-3.2,3.2c-1.8,0-3.2-1.4-3.2-3.2v-2.6
										c0,1.8,1.4,3.2,3.2,3.2C125.9,168.1,127.3,166.6,127.3,164.9z"/>
								</defs>
								<clipPath id="SVGID_240_">
									<use xlink:href="#SVGID_239_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st141">
									<path class="st34" d="M127.3,164.9v2.6c0,0.2,0,0.3,0,0.5v-2.6C127.3,165.2,127.3,165.1,127.3,164.9"/>
									<path class="st34" d="M127.3,165.4v2.6c0,0.2-0.1,0.4-0.1,0.5v-2.6C127.2,165.7,127.2,165.5,127.3,165.4"/>
									<path class="st34" d="M127.1,165.9v2.6c-0.1,0.2-0.2,0.4-0.3,0.6v-2.6C127,166.3,127.1,166.1,127.1,165.9"/>
									<path class="st34" d="M126.9,166.5v2.6c-0.5,1-1.6,1.6-2.8,1.6c-1.8,0-3.2-1.4-3.2-3.2v-2.6c0,1.8,1.4,3.2,3.2,3.2
										C125.3,168.1,126.3,167.4,126.9,166.5"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M124.1,161.7c1.8,0,3.2,1.4,3.2,3.2c0,1.8-1.4,3.2-3.2,3.2c-1.8,0-3.2-1.4-3.2-3.2
									C120.9,163.1,122.4,161.7,124.1,161.7z M124.1,167.1c1.2,0,2.2-1,2.2-2.2c0-1.2-1-2.2-2.2-2.2c-1.2,0-2.2,1-2.2,2.2
									C121.9,166.1,122.9,167.1,124.1,167.1"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_241_" class="st0" d="M124.1,175.8c-5.2,0-9.2-3.7-9.2-8.4l0-2.6c0,4.7,4,8.4,9.2,8.4
										c5.2,0,9.2-3.7,9.2-8.4l0,0.3v2.3C133.3,172.1,129.3,175.8,124.1,175.8z"/>
								</defs>
								<clipPath id="SVGID_242_">
									<use xlink:href="#SVGID_241_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st142">
									<path class="st34" d="M133.3,164.8v2.6c0,0.4,0,0.8-0.1,1.2V166C133.3,165.6,133.3,165.2,133.3,164.8"/>
									<path class="st34" d="M133.2,166v2.6c-0.1,0.5-0.2,0.9-0.3,1.3v-2.6C133,166.9,133.2,166.4,133.2,166"/>
									<path class="st34" d="M132.9,167.3v2.6c-0.2,0.5-0.4,1-0.6,1.4v-2.6C132.5,168.3,132.7,167.8,132.9,167.3"/>
									<path class="st34" d="M132.3,168.8v2.6c-1.5,2.7-4.6,4.5-8.1,4.5c-5.2,0-9.2-3.7-9.2-8.4v-2.6c0,4.7,4,8.4,9.2,8.4
										C127.7,173.2,130.7,171.4,132.3,168.8"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M124.1,155.3c5.7,0,10.2,4.2,10.2,9.4c0,5.3-4.5,9.4-10.2,9.4c-5.7,0-10.2-4.1-10.2-9.4
									C113.9,159.5,118.4,155.3,124.1,155.3z M124.1,173.2c5.2,0,9.2-3.7,9.2-8.4c0-4.7-4-8.4-9.2-8.4c-5.2,0-9.2,3.7-9.2,8.4
									C114.9,169.5,119,173.2,124.1,173.2"/>
							</g>
							<g>
								<path class="st34" d="M124.1,156.3c5.2,0,9.2,3.7,9.2,8.4c0,4.7-4,8.4-9.2,8.4c-5.2,0-9.2-3.7-9.2-8.4
									C114.9,160,119,156.3,124.1,156.3z M124.1,168.1c1.8,0,3.2-1.4,3.2-3.2c0-1.8-1.4-3.2-3.2-3.2c-1.8,0-3.2,1.4-3.2,3.2
									C120.9,166.6,122.4,168.1,124.1,168.1"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_243_" class="st0" d="M134.3,164.8v2.6c0,5.3-4.5,9.4-10.2,9.4c-5.7,0-10.2-4.1-10.2-9.4l0-2.6
										c0,5.3,4.5,9.4,10.2,9.4C129.8,174.2,134.3,170.1,134.3,164.8z"/>
								</defs>
								<clipPath id="SVGID_244_">
									<use xlink:href="#SVGID_243_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st143">
									<path class="st34" d="M134.3,164.8v2.6c0,0.5,0,0.9-0.1,1.4v-2.6C134.3,165.7,134.3,165.2,134.3,164.8"/>
									<path class="st34" d="M134.2,166.1v2.6c-0.1,0.5-0.2,1-0.4,1.5v-2.6C134,167.2,134.1,166.7,134.2,166.1"/>
									<path class="st34" d="M133.9,167.6v2.6c-0.2,0.6-0.4,1.1-0.7,1.6v-2.6C133.4,168.7,133.7,168.2,133.9,167.6"/>
									<path class="st34" d="M133.1,169.3v2.6c-1.7,3-5.1,5-9,5c-5.7,0-10.2-4.1-10.2-9.4v-2.6c0,5.3,4.5,9.4,10.2,9.4
										C128.1,174.2,131.4,172.2,133.1,169.3"/>
								</g>
							</g>
						</g>
						<g class="st0">
							<g>
								<rect x="135.3" y="156.6" class="st34" width="15.8" height="2.6"/>
							</g>
							<g>
								<rect x="135.3" y="161.5" class="st34" width="4.9" height="2.6"/>
							</g>
							<g>
								<rect x="146.3" y="161.5" class="st34" width="4.8" height="2.6"/>
							</g>
							<g>
								<rect x="147.3" y="162.5" class="st34" width="4.8" height="2.6"/>
							</g>
							<g>
								<rect x="134.3" y="162.5" class="st34" width="4.9" height="2.6"/>
							</g>
							<g>
								<rect x="140.2" y="173" class="st34" width="6.2" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M134.3,155.6h17.8v6.9h-4.8V174h-8.2v-11.5h-4.9V155.6z M151.2,161.5v-4.9h-15.8v4.9h4.9V173h6.2
									v-11.5H151.2"/>
							</g>
							<g>
								<polygon class="st34" points="151.2,156.6 151.2,161.5 146.3,161.5 146.3,173 140.2,173 140.2,161.5 135.3,161.5 
									135.3,156.6 								"/>
							</g>
							<g>
								<rect x="139.2" y="174" class="st34" width="8.2" height="2.6"/>
							</g>
						</g>
						<g class="st0">
							<g>
								<rect x="154" y="156.6" class="st34" width="11.5" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="161.4" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<rect x="165.5" y="162.4" class="st34" width="1" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="163.3" class="st34" width="4.3" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="166.5" class="st34" width="4.3" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="168.4" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<polygon class="st34" points="165.5,156.6 165.5,161.4 160.2,161.4 160.2,163.3 164.5,163.3 164.5,166.5 160.2,166.5 
									160.2,168.4 165.9,168.4 165.9,173 154,173 154,156.6 								"/>
							</g>
							<g>
								<rect x="154" y="173" class="st34" width="11.8" height="2.6"/>
							</g>
							<g>
								<rect x="153" y="174" class="st34" width="13.8" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M165.5,167.4h1.4v6.5H153v-18.4h13.5v6.8h-1V167.4z M165.9,173v-4.5h-5.7v-2h4.3v-3.1h-4.3v-1.9h5.4
									v-4.8H154V173H165.9"/>
							</g>
						</g>
						<g class="st0">
							<g class="st0">
								<defs>
									<path id="SVGID_245_" class="st0" d="M180.5,157.5l0,2.6c-1.5-0.7-3.8-1.2-5.7-1.2c-4,0-6.6,2.1-6.6,5.4l0-2.6
										c0-3.3,2.6-5.4,6.6-5.4C176.7,156.3,178.9,156.8,180.5,157.5z"/>
								</defs>
								<clipPath id="SVGID_246_">
									<use xlink:href="#SVGID_245_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st144">
									<path class="st34" d="M180.5,157.5v2.6c-1.5-0.7-3.8-1.2-5.7-1.2c-2.8,0-4.9,1-5.9,2.8v-2.6c1-1.8,3.1-2.8,5.9-2.8
										C176.7,156.3,178.9,156.8,180.5,157.5"/>
									<path class="st34" d="M168.8,159.1v2.6c-0.2,0.3-0.3,0.6-0.4,0.9V160C168.6,159.7,168.7,159.4,168.8,159.1"/>
									<path class="st34" d="M168.5,160v2.6c-0.1,0.3-0.2,0.6-0.2,0.9v-2.6C168.3,160.5,168.4,160.3,168.5,160"/>
									<path class="st34" d="M168.3,160.8v2.6c0,0.3-0.1,0.6-0.1,0.8v-2.6C168.2,161.4,168.2,161.1,168.3,160.8"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_247_" class="st0" d="M175.7,163.5c-1.3,0-1.4,0.8-1.4,1v-2.6c0-0.2,0.1-1,1.4-1c1,0,2.8,0.4,4.8,1.2v2.6
										C178.5,163.9,176.7,163.5,175.7,163.5z"/>
								</defs>
								<clipPath id="SVGID_248_">
									<use xlink:href="#SVGID_247_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st145">
									<path class="st34" d="M180.5,162.1v2.6c-1.9-0.9-3.8-1.2-4.8-1.2c-0.8,0-1.1,0.3-1.3,0.5v-2.6c0.2-0.3,0.5-0.5,1.3-0.5
										C176.7,160.8,178.5,161.2,180.5,162.1"/>
									<path class="st34" d="M174.5,161.4v2.6c0,0.1-0.1,0.1-0.1,0.2v-2.6C174.4,161.5,174.4,161.5,174.5,161.4"/>
									<path class="st34" d="M174.4,161.6v2.6c0,0.1,0,0.1,0,0.2v-2.6C174.3,161.7,174.4,161.6,174.4,161.6"/>
									<path class="st34" d="M174.3,161.7v2.6c0,0.1,0,0.1,0,0.1v-2.6C174.3,161.8,174.3,161.8,174.3,161.7"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_249_" class="st0" d="M175.2,163v2.6c-0.6-0.2-0.9-0.6-0.9-1.1l0-2.6C174.3,162.4,174.6,162.7,175.2,163z"
										/>
								</defs>
								<clipPath id="SVGID_250_">
									<use xlink:href="#SVGID_249_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st146">
									<path class="st34" d="M175.2,163v2.6c-0.6-0.2-0.9-0.6-0.9-1.1v-2.6C174.3,162.4,174.6,162.7,175.2,163"/>
								</g>
							</g>
							<g>
								<polygon class="st34" points="181.5,163.7 181.5,166.3 180.7,165.9 180.7,163.3 								"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_251_" class="st0" d="M169.6,167.5c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6c0,1.4,0.4,2.4,1.4,3.2
										C169.6,165.8,169.6,166.7,169.6,167.5c0-0.9,0-1.7,0-2.6c1.4,1.2,3.2,1.6,4.3,1.8c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										C172.8,169.1,171,168.8,169.6,167.5z"/>
								</defs>
								<clipPath id="SVGID_252_">
									<use xlink:href="#SVGID_251_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st147">
									<path class="st34" d="M174.4,166.9v2.6c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6
										c0,1.4,0.4,2.4,1.4,3.2c1.4,1.2,3.2,1.6,4.3,1.8C174.1,166.8,174.3,166.8,174.4,166.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_253_" class="st0" d="M174.4,169.5c-0.1,0-0.3-0.1-0.5-0.1c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0.2,0,0.4,0.1,0.5,0.1C174.4,167.7,174.4,168.6,174.4,169.5z"/>
								</defs>
								<clipPath id="SVGID_254_">
									<use xlink:href="#SVGID_253_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st148">
									<path class="st34" d="M174.4,166.9v2.6c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6
										c0,1.4,0.4,2.4,1.4,3.2c1.4,1.2,3.2,1.6,4.3,1.8C174.1,166.8,174.3,166.8,174.4,166.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_255_" class="st0" d="M173.7,167.7v2.6c-1.1-0.2-3.2-0.6-4.8-2c-1.2-1-1.7-2.3-1.7-4l0-2.6
										c0,1.7,0.6,3,1.7,4C170.5,167.1,172.5,167.5,173.7,167.7z"/>
								</defs>
								<clipPath id="SVGID_256_">
									<use xlink:href="#SVGID_255_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st149">
									<path class="st34" d="M173.7,167.7v2.6c-1.1-0.2-3.2-0.6-4.8-2c-1.2-1-1.7-2.3-1.7-4v-2.6c0,1.7,0.6,3,1.7,4
										C170.5,167.1,172.5,167.5,173.7,167.7"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_257_" class="st0" d="M174.2,167.8v2.6c-0.1,0-0.3-0.1-0.5-0.1v-2.6C173.9,167.8,174,167.8,174.2,167.8z"/>
								</defs>
								<clipPath id="SVGID_258_">
									<use xlink:href="#SVGID_257_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st150">
									<path class="st34" d="M174.2,167.8v2.6c-0.1,0-0.3-0.1-0.5-0.1v-2.6C173.9,167.8,174,167.8,174.2,167.8"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_259_" class="st0" d="M175.3,170.5c0-0.3-0.1-0.9-0.9-1c0-0.8,0-1.5,0-2.3v-0.3c0.8,0.2,0.9,0.7,0.9,1v0.3
										C175.3,168.9,175.3,169.8,175.3,170.5z"/>
								</defs>
								<clipPath id="SVGID_260_">
									<use xlink:href="#SVGID_259_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st151">
									<path class="st34" d="M175.3,167.9v2.6c0-0.3-0.1-0.9-0.9-1v-2.6C175.2,167,175.3,167.6,175.3,167.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_261_" class="st0" d="M181.5,168.1v2.6c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2
										V163c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6C180.9,165.6,181.5,166.7,181.5,168.1z"/>
								</defs>
								<clipPath id="SVGID_262_">
									<use xlink:href="#SVGID_261_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st152">
									<path class="st34" d="M181.5,168.1v2.6c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2V163
										c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6C180.9,165.6,181.5,166.7,181.5,168.1"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_263_" class="st0" d="M175.3,167.9l0,2.6c0,0.3-0.1,1.1-1.4,1.1c-1.2,0-3.2-0.5-5.2-1.7l0-2.6
										c2,1.1,4.1,1.7,5.2,1.7C175.2,169,175.3,168.1,175.3,167.9z"/>
								</defs>
								<clipPath id="SVGID_264_">
									<use xlink:href="#SVGID_263_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st153">
									<path class="st34" d="M175.3,167.9v2.6c0,0,0,0.1,0,0.2V168C175.3,168,175.3,167.9,175.3,167.9"/>
									<path class="st34" d="M175.3,168v2.6c0,0.1,0,0.1,0,0.2v-2.6C175.3,168.2,175.3,168.1,175.3,168"/>
									<path class="st34" d="M175.2,168.2v2.6c0,0.1,0,0.1-0.1,0.2v-2.6C175.2,168.3,175.2,168.3,175.2,168.2"/>
									<path class="st34" d="M175.2,168.4v2.6c-0.2,0.3-0.5,0.6-1.3,0.6c-1.2,0-3.2-0.5-5.2-1.7v-2.6c2,1.1,4.1,1.7,5.2,1.7
										C174.7,169,175,168.7,175.2,168.4"/>
								</g>
							</g>
							<g>
								<polygon class="st34" points="168,172.7 168,175.4 167.7,175.2 167.7,172.6 								"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_265_" class="st0" d="M181.5,170.7c0,3.2-2.6,5.2-6.7,5.2c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c4.2,0,6.7-2,6.7-5.2v0.3C181.5,169.1,181.5,170,181.5,170.7z"/>
								</defs>
								<clipPath id="SVGID_266_">
									<use xlink:href="#SVGID_265_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st154">
									<path class="st34" d="M181.5,168.1v2.6c0,0.3,0,0.5-0.1,0.8v-2.6C181.4,168.6,181.5,168.3,181.5,168.1"/>
									<path class="st34" d="M181.4,168.9v2.6c0,0.3-0.1,0.5-0.2,0.8v-2.6C181.3,169.4,181.4,169.1,181.4,168.9"/>
									<path class="st34" d="M181.2,169.7v2.6c-0.1,0.3-0.2,0.5-0.4,0.8v-2.6C181,170.2,181.1,169.9,181.2,169.7"/>
									<path class="st34" d="M180.9,170.5v2.6c-1,1.7-3.2,2.8-6.1,2.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2
										C177.7,173.2,179.9,172.2,180.9,170.5"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_267_" class="st0" d="M174.7,175.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2c0,0,0,0,0,0
										c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										c0,0.1,0,0.2,0,0.3C174.7,175.6,174.7,175.7,174.7,175.8C174.7,175.8,174.7,175.8,174.7,175.8z"/>
								</defs>
								<clipPath id="SVGID_268_">
									<use xlink:href="#SVGID_267_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st155">
									<path class="st34" d="M180.9,170.5v2.6c-1,1.7-3.2,2.8-6.1,2.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2
										C177.7,173.2,179.9,172.2,180.9,170.5"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M174.8,156.3c1.9,0,4.2,0.5,5.7,1.2v4.6c-1.9-0.9-3.8-1.2-4.8-1.2c-1.3,0-1.4,0.8-1.4,1
									c0,0.5,0.3,0.9,0.9,1.1c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6c1.1,0.9,1.6,2,1.6,3.4c0,3.2-2.6,5.2-6.8,5.2
									c-2.1,0-4.4-0.5-6-1.2v-4.7c2,1.1,4.1,1.7,5.2,1.7c1.3,0,1.4-0.8,1.4-1.1c0-0.3-0.1-0.9-0.9-1c-0.1,0-0.3-0.1-0.5-0.1
									c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2C168.2,158.4,170.8,156.3,174.8,156.3z"/>
							</g>
							<g>
								<path class="st34" d="M180.5,163.9c1.3,1.1,1.9,2.4,1.9,4.1c0,3.7-3,6.2-7.7,6.2c-2.4,0-5-0.6-6.7-1.5l-0.3-0.1v-7.1
									l0.8,0.5c2.2,1.5,4.5,2,5.4,2c0.3,0,0.4-0.1,0.4-0.1c0,0,0,0-0.2-0.1c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-3.2-0.6-4.8-2
									c-1.2-1-1.7-2.3-1.7-4c0-3.9,3-6.4,7.6-6.4c2.2,0,4.8,0.6,6.4,1.4l0.3,0.1v6.8l-0.7-0.4c-2.2-1.1-4.2-1.5-5-1.5
									c-0.3,0-0.4,0.1-0.4,0.1c0,0,0.1,0,0.2,0.1c0.1,0,0.3,0.1,0.5,0.1C177.1,162.4,179.1,162.8,180.5,163.9z M174.7,173.2
									c4.2,0,6.8-2,6.8-5.2c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2c-0.6-0.2-0.9-0.6-0.9-1.1
									c0-0.2,0.1-1,1.4-1c1,0,2.8,0.4,4.8,1.2v-4.6c-1.5-0.7-3.8-1.2-5.7-1.2c-4,0-6.6,2.1-6.6,5.4c0,1.4,0.4,2.4,1.4,3.2
									c1.4,1.2,3.2,1.6,4.3,1.8c0.2,0,0.4,0.1,0.5,0.1c0.8,0.2,0.9,0.7,0.9,1c0,0.3-0.1,1.1-1.4,1.1c-1.2,0-3.2-0.5-5.2-1.7v4.7
									C170.3,172.7,172.6,173.2,174.7,173.2"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_269_" class="st0" d="M182.5,168.1l0,2.6c0,3.7-3,6.2-7.7,6.2c-2.4,0-5-0.6-6.7-1.5v-2.6
										c1.7,0.9,4.4,1.5,6.7,1.5C179.4,174.2,182.5,171.8,182.5,168.1z"/>
								</defs>
								<clipPath id="SVGID_270_">
									<use xlink:href="#SVGID_269_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st156">
									<path class="st34" d="M182.5,168.1v2.6c0,0.3,0,0.6-0.1,1V169C182.4,168.7,182.5,168.4,182.5,168.1"/>
									<path class="st34" d="M182.4,169v2.6c-0.1,0.3-0.1,0.7-0.2,1V170C182.3,169.7,182.3,169.3,182.4,169"/>
									<path class="st34" d="M182.2,170v2.6c-0.1,0.3-0.3,0.7-0.4,1v-2.6C181.9,170.6,182,170.3,182.2,170"/>
									<path class="st34" d="M181.7,170.9v2.6c-1.2,2-3.7,3.3-7,3.3c-2.4,0-5-0.6-6.7-1.5v-2.6c1.7,0.9,4.4,1.5,6.7,1.5
										C178.1,174.2,180.6,173,181.7,170.9"/>
								</g>
							</g>
						</g>
					</g>
					<defs>
						<filter id="Adobe_OpacityMaskFilter_6_" filterUnits="userSpaceOnUse" x="94.1" y="155.3" width="88.4" height="21.5">
							<feFlood  style="flood-color:white;flood-opacity:1" result="back"/>
							<feBlend  in="SourceGraphic" in2="back" mode="normal"/>
						</filter>
					</defs>
					<mask maskUnits="userSpaceOnUse" x="94.1" y="155.3" width="88.4" height="21.5" id="SVGID_232_">
						<g class="st157">
							
								<image style="overflow:visible;" width="93" height="27" xlink:href="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgEASABIAAD/7AARRHVja3kAAQAEAAAAHgAA/+4AIUFkb2JlAGTAAAAAAQMA
EAMCAwYAAAF0AAABfwAAAaD/2wCEABALCwsMCxAMDBAXDw0PFxsUEBAUGx8XFxcXFx8eFxoaGhoX
Hh4jJSclIx4vLzMzLy9AQEBAQEBAQEBAQEBAQEABEQ8PERMRFRISFRQRFBEUGhQWFhQaJhoaHBoa
JjAjHh4eHiMwKy4nJycuKzU1MDA1NUBAP0BAQEBAQEBAQEBAQP/CABEIABsAXQMBIgACEQEDEQH/
xABfAAEBAQAAAAAAAAAAAAAAAAAAAQcBAQAAAAAAAAAAAAAAAAAAAAAQAQEAAAAAAAAAAAAAAAAA
ADBQEQEAAAAAAAAAAAAAAAAAAABQEgEAAAAAAAAAAAAAAAAAAAAw/9oADAMBAAIRAxEAAADPykWA
AABYAAAAAAf/2gAIAQIAAQUAT//aAAgBAwABBQBP/9oACAEBAAEFAKH/2gAIAQICBj8AT//aAAgB
AwIGPwBP/9oACAEBAQY/AEP/2Q==" transform="matrix(1 0 0 1 91.75 152.5)">
							</image>
						</g>
					</mask>
					<g>
						<g class="st0">
							<g>
								<rect x="95.1" y="156.6" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<rect x="106.8" y="156.6" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<polygon class="st34" points="105.8,163.8 105.8,166.4 100.8,159.2 100.8,156.6 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.8,163.8 106.8,166.4 106.1,166.7 106.1,164.1 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.1,164.1 106.1,166.7 105.8,166.4 105.8,163.8 								"/>
							</g>
							<g>
								<polygon class="st34" points="101.2,165.4 101.2,168 100.5,168.4 100.5,165.8 								"/>
							</g>
							<g>
								<polygon class="st34" points="101.5,165.8 101.5,168.4 101.2,168 101.2,165.4 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.5,173 106.5,175.6 101.5,168.4 101.5,165.8 								"/>
							</g>
							<g>
								<rect x="106.5" y="173" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<rect x="95.1" y="173" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M105.8,155.6h7.4V174h-7.3l-4.4-6.4v6.4h-7.4v-18.4h7.3l4.4,6.4V155.6z M112.2,173v-16.4h-5.4v7.2
									l-0.7,0.3l-0.3-0.3l-5-7.2h-5.7V173h5.4v-7.2l0.7-0.3l0.3,0.3l5,7.2H112.2"/>
							</g>
							<g>
								<polygon class="st34" points="112.2,156.6 112.2,173 106.5,173 101.5,165.8 101.2,165.4 100.5,165.8 100.5,173 95.1,173 
									95.1,156.6 100.8,156.6 105.8,163.8 106.1,164.1 106.8,163.8 106.8,156.6 								"/>
							</g>
							<g>
								<polygon class="st34" points="105.9,174 105.9,176.6 101.5,170.2 101.5,167.6 								"/>
							</g>
							<g>
								<rect x="105.9" y="174" class="st34" width="7.3" height="2.6"/>
							</g>
							<g>
								<rect x="94.1" y="174" class="st34" width="7.4" height="2.6"/>
							</g>
						</g>
						<g class="st0">
							<g class="st0">
								<defs>
									<path id="SVGID_271_" class="st0" d="M133.3,164.8l0,2.6c0-4.7-4-8.4-9.2-8.4c-5.2,0-9.2,3.7-9.2,8.4l0-2.6
										c0-4.7,4-8.4,9.2-8.4C129.3,156.3,133.3,160,133.3,164.8z"/>
								</defs>
								<clipPath id="SVGID_272_">
									<use xlink:href="#SVGID_271_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st158">
									<path class="st34" d="M133.3,164.8v2.6c0-4.7-4-8.4-9.2-8.4c-3.6,0-6.6,1.8-8.1,4.5v-2.6c1.5-2.7,4.6-4.5,8.1-4.5
										C129.3,156.3,133.3,160,133.3,164.8"/>
									<path class="st34" d="M116,160.8v2.6c-0.3,0.5-0.5,0.9-0.6,1.4v-2.6C115.5,161.7,115.7,161.3,116,160.8"/>
									<path class="st34" d="M115.3,162.2v2.6c-0.1,0.4-0.3,0.9-0.3,1.3v-2.6C115.1,163.1,115.2,162.7,115.3,162.2"/>
									<path class="st34" d="M115,163.6v2.6c-0.1,0.4-0.1,0.8-0.1,1.2v-2.6C114.9,164.4,115,164,115,163.6"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_273_" class="st0" d="M120.9,167.5c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3v-0.3c0-1.8,1.4-3.2,3.2-3.2
										c1.8,0,3.2,1.4,3.2,3.2l0,2.6c0-1.8-1.4-3.2-3.2-3.2C122.4,164.3,120.9,165.8,120.9,167.5z"/>
								</defs>
								<clipPath id="SVGID_274_">
									<use xlink:href="#SVGID_273_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st159">
									<path class="st34" d="M127.3,164.9v2.6c0-1.8-1.4-3.2-3.2-3.2c-1.2,0-2.2,0.7-2.8,1.6v-2.6c0.5-1,1.6-1.6,2.8-1.6
										C125.9,161.7,127.3,163.1,127.3,164.9"/>
									<path class="st34" d="M121.3,163.3v2.6c-0.1,0.2-0.2,0.4-0.3,0.6v-2.6C121.2,163.7,121.2,163.5,121.3,163.3"/>
									<path class="st34" d="M121.1,163.9v2.6c-0.1,0.2-0.1,0.3-0.1,0.5v-2.6C121,164.2,121,164.1,121.1,163.9"/>
									<path class="st34" d="M121,164.4v2.6c0,0.2,0,0.3,0,0.5v-2.6C120.9,164.7,120.9,164.6,121,164.4"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_275_" class="st0" d="M126.3,164.9l0,2.6c0-1.2-1-2.2-2.2-2.2c-1.2,0-2.2,1-2.2,2.2l0-2.6
										c0-1.2,1-2.2,2.2-2.2C125.3,162.7,126.3,163.7,126.3,164.9z"/>
								</defs>
								<clipPath id="SVGID_276_">
									<use xlink:href="#SVGID_275_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st160">
									<path class="st34" d="M126.3,164.9v2.6c0-1.2-1-2.2-2.2-2.2c-0.8,0-1.5,0.4-1.9,1.1v-2.6c0.4-0.7,1.1-1.1,1.9-1.1
										C125.3,162.7,126.3,163.7,126.3,164.9"/>
									<path class="st34" d="M122.2,163.8v2.6c-0.1,0.1-0.1,0.3-0.2,0.4v-2.6C122.1,164.1,122.1,163.9,122.2,163.8"/>
									<path class="st34" d="M122,164.2v2.6c0,0.1-0.1,0.2-0.1,0.4v-2.6C122,164.4,122,164.3,122,164.2"/>
									<path class="st34" d="M121.9,164.6v2.6c0,0.1,0,0.2,0,0.3v-2.6C121.9,164.8,121.9,164.7,121.9,164.6"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_277_" class="st0" d="M127.3,164.9l0,2.6c0,1.8-1.4,3.2-3.2,3.2c-1.8,0-3.2-1.4-3.2-3.2v-2.6
										c0,1.8,1.4,3.2,3.2,3.2C125.9,168.1,127.3,166.6,127.3,164.9z"/>
								</defs>
								<clipPath id="SVGID_278_">
									<use xlink:href="#SVGID_277_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st161">
									<path class="st34" d="M127.3,164.9v2.6c0,0.2,0,0.3,0,0.5v-2.6C127.3,165.2,127.3,165.1,127.3,164.9"/>
									<path class="st34" d="M127.3,165.4v2.6c0,0.2-0.1,0.4-0.1,0.5v-2.6C127.2,165.7,127.2,165.5,127.3,165.4"/>
									<path class="st34" d="M127.1,165.9v2.6c-0.1,0.2-0.2,0.4-0.3,0.6v-2.6C127,166.3,127.1,166.1,127.1,165.9"/>
									<path class="st34" d="M126.9,166.5v2.6c-0.5,1-1.6,1.6-2.8,1.6c-1.8,0-3.2-1.4-3.2-3.2v-2.6c0,1.8,1.4,3.2,3.2,3.2
										C125.3,168.1,126.3,167.4,126.9,166.5"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M124.1,161.7c1.8,0,3.2,1.4,3.2,3.2c0,1.8-1.4,3.2-3.2,3.2c-1.8,0-3.2-1.4-3.2-3.2
									C120.9,163.1,122.4,161.7,124.1,161.7z M124.1,167.1c1.2,0,2.2-1,2.2-2.2c0-1.2-1-2.2-2.2-2.2c-1.2,0-2.2,1-2.2,2.2
									C121.9,166.1,122.9,167.1,124.1,167.1"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_279_" class="st0" d="M124.1,175.8c-5.2,0-9.2-3.7-9.2-8.4l0-2.6c0,4.7,4,8.4,9.2,8.4
										c5.2,0,9.2-3.7,9.2-8.4l0,0.3v2.3C133.3,172.1,129.3,175.8,124.1,175.8z"/>
								</defs>
								<clipPath id="SVGID_280_">
									<use xlink:href="#SVGID_279_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st162">
									<path class="st34" d="M133.3,164.8v2.6c0,0.4,0,0.8-0.1,1.2V166C133.3,165.6,133.3,165.2,133.3,164.8"/>
									<path class="st34" d="M133.2,166v2.6c-0.1,0.5-0.2,0.9-0.3,1.3v-2.6C133,166.9,133.2,166.4,133.2,166"/>
									<path class="st34" d="M132.9,167.3v2.6c-0.2,0.5-0.4,1-0.6,1.4v-2.6C132.5,168.3,132.7,167.8,132.9,167.3"/>
									<path class="st34" d="M132.3,168.8v2.6c-1.5,2.7-4.6,4.5-8.1,4.5c-5.2,0-9.2-3.7-9.2-8.4v-2.6c0,4.7,4,8.4,9.2,8.4
										C127.7,173.2,130.7,171.4,132.3,168.8"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M124.1,155.3c5.7,0,10.2,4.2,10.2,9.4c0,5.3-4.5,9.4-10.2,9.4c-5.7,0-10.2-4.1-10.2-9.4
									C113.9,159.5,118.4,155.3,124.1,155.3z M124.1,173.2c5.2,0,9.2-3.7,9.2-8.4c0-4.7-4-8.4-9.2-8.4c-5.2,0-9.2,3.7-9.2,8.4
									C114.9,169.5,119,173.2,124.1,173.2"/>
							</g>
							<g>
								<path class="st34" d="M124.1,156.3c5.2,0,9.2,3.7,9.2,8.4c0,4.7-4,8.4-9.2,8.4c-5.2,0-9.2-3.7-9.2-8.4
									C114.9,160,119,156.3,124.1,156.3z M124.1,168.1c1.8,0,3.2-1.4,3.2-3.2c0-1.8-1.4-3.2-3.2-3.2c-1.8,0-3.2,1.4-3.2,3.2
									C120.9,166.6,122.4,168.1,124.1,168.1"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_281_" class="st0" d="M134.3,164.8v2.6c0,5.3-4.5,9.4-10.2,9.4c-5.7,0-10.2-4.1-10.2-9.4l0-2.6
										c0,5.3,4.5,9.4,10.2,9.4C129.8,174.2,134.3,170.1,134.3,164.8z"/>
								</defs>
								<clipPath id="SVGID_282_">
									<use xlink:href="#SVGID_281_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st163">
									<path class="st34" d="M134.3,164.8v2.6c0,0.5,0,0.9-0.1,1.4v-2.6C134.3,165.7,134.3,165.2,134.3,164.8"/>
									<path class="st34" d="M134.2,166.1v2.6c-0.1,0.5-0.2,1-0.4,1.5v-2.6C134,167.2,134.1,166.7,134.2,166.1"/>
									<path class="st34" d="M133.9,167.6v2.6c-0.2,0.6-0.4,1.1-0.7,1.6v-2.6C133.4,168.7,133.7,168.2,133.9,167.6"/>
									<path class="st34" d="M133.1,169.3v2.6c-1.7,3-5.1,5-9,5c-5.7,0-10.2-4.1-10.2-9.4v-2.6c0,5.3,4.5,9.4,10.2,9.4
										C128.1,174.2,131.4,172.2,133.1,169.3"/>
								</g>
							</g>
						</g>
						<g class="st0">
							<g>
								<rect x="135.3" y="156.6" class="st34" width="15.8" height="2.6"/>
							</g>
							<g>
								<rect x="135.3" y="161.5" class="st34" width="4.9" height="2.6"/>
							</g>
							<g>
								<rect x="146.3" y="161.5" class="st34" width="4.8" height="2.6"/>
							</g>
							<g>
								<rect x="147.3" y="162.5" class="st34" width="4.8" height="2.6"/>
							</g>
							<g>
								<rect x="134.3" y="162.5" class="st34" width="4.9" height="2.6"/>
							</g>
							<g>
								<rect x="140.2" y="173" class="st34" width="6.2" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M134.3,155.6h17.8v6.9h-4.8V174h-8.2v-11.5h-4.9V155.6z M151.2,161.5v-4.9h-15.8v4.9h4.9V173h6.2
									v-11.5H151.2"/>
							</g>
							<g>
								<polygon class="st34" points="151.2,156.6 151.2,161.5 146.3,161.5 146.3,173 140.2,173 140.2,161.5 135.3,161.5 
									135.3,156.6 								"/>
							</g>
							<g>
								<rect x="139.2" y="174" class="st34" width="8.2" height="2.6"/>
							</g>
						</g>
						<g class="st0">
							<g>
								<rect x="154" y="156.6" class="st34" width="11.5" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="161.4" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<rect x="165.5" y="162.4" class="st34" width="1" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="163.3" class="st34" width="4.3" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="166.5" class="st34" width="4.3" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="168.4" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<polygon class="st34" points="165.5,156.6 165.5,161.4 160.2,161.4 160.2,163.3 164.5,163.3 164.5,166.5 160.2,166.5 
									160.2,168.4 165.9,168.4 165.9,173 154,173 154,156.6 								"/>
							</g>
							<g>
								<rect x="154" y="173" class="st34" width="11.8" height="2.6"/>
							</g>
							<g>
								<rect x="153" y="174" class="st34" width="13.8" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M165.5,167.4h1.4v6.5H153v-18.4h13.5v6.8h-1V167.4z M165.9,173v-4.5h-5.7v-2h4.3v-3.1h-4.3v-1.9h5.4
									v-4.8H154V173H165.9"/>
							</g>
						</g>
						<g class="st0">
							<g class="st0">
								<defs>
									<path id="SVGID_283_" class="st0" d="M180.5,157.5l0,2.6c-1.5-0.7-3.8-1.2-5.7-1.2c-4,0-6.6,2.1-6.6,5.4l0-2.6
										c0-3.3,2.6-5.4,6.6-5.4C176.7,156.3,178.9,156.8,180.5,157.5z"/>
								</defs>
								<clipPath id="SVGID_284_">
									<use xlink:href="#SVGID_283_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st164">
									<path class="st34" d="M180.5,157.5v2.6c-1.5-0.7-3.8-1.2-5.7-1.2c-2.8,0-4.9,1-5.9,2.8v-2.6c1-1.8,3.1-2.8,5.9-2.8
										C176.7,156.3,178.9,156.8,180.5,157.5"/>
									<path class="st34" d="M168.8,159.1v2.6c-0.2,0.3-0.3,0.6-0.4,0.9V160C168.6,159.7,168.7,159.4,168.8,159.1"/>
									<path class="st34" d="M168.5,160v2.6c-0.1,0.3-0.2,0.6-0.2,0.9v-2.6C168.3,160.5,168.4,160.3,168.5,160"/>
									<path class="st34" d="M168.3,160.8v2.6c0,0.3-0.1,0.6-0.1,0.8v-2.6C168.2,161.4,168.2,161.1,168.3,160.8"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_285_" class="st0" d="M175.7,163.5c-1.3,0-1.4,0.8-1.4,1v-2.6c0-0.2,0.1-1,1.4-1c1,0,2.8,0.4,4.8,1.2v2.6
										C178.5,163.9,176.7,163.5,175.7,163.5z"/>
								</defs>
								<clipPath id="SVGID_286_">
									<use xlink:href="#SVGID_285_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st165">
									<path class="st34" d="M180.5,162.1v2.6c-1.9-0.9-3.8-1.2-4.8-1.2c-0.8,0-1.1,0.3-1.3,0.5v-2.6c0.2-0.3,0.5-0.5,1.3-0.5
										C176.7,160.8,178.5,161.2,180.5,162.1"/>
									<path class="st34" d="M174.5,161.4v2.6c0,0.1-0.1,0.1-0.1,0.2v-2.6C174.4,161.5,174.4,161.5,174.5,161.4"/>
									<path class="st34" d="M174.4,161.6v2.6c0,0.1,0,0.1,0,0.2v-2.6C174.3,161.7,174.4,161.6,174.4,161.6"/>
									<path class="st34" d="M174.3,161.7v2.6c0,0.1,0,0.1,0,0.1v-2.6C174.3,161.8,174.3,161.8,174.3,161.7"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_287_" class="st0" d="M175.2,163v2.6c-0.6-0.2-0.9-0.6-0.9-1.1l0-2.6C174.3,162.4,174.6,162.7,175.2,163z"
										/>
								</defs>
								<clipPath id="SVGID_288_">
									<use xlink:href="#SVGID_287_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st166">
									<path class="st34" d="M175.2,163v2.6c-0.6-0.2-0.9-0.6-0.9-1.1v-2.6C174.3,162.4,174.6,162.7,175.2,163"/>
								</g>
							</g>
							<g>
								<polygon class="st34" points="181.5,163.7 181.5,166.3 180.7,165.9 180.7,163.3 								"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_289_" class="st0" d="M169.6,167.5c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6c0,1.4,0.4,2.4,1.4,3.2
										C169.6,165.8,169.6,166.7,169.6,167.5c0-0.9,0-1.7,0-2.6c1.4,1.2,3.2,1.6,4.3,1.8c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										C172.8,169.1,171,168.8,169.6,167.5z"/>
								</defs>
								<clipPath id="SVGID_290_">
									<use xlink:href="#SVGID_289_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st167">
									<path class="st34" d="M174.4,166.9v2.6c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6
										c0,1.4,0.4,2.4,1.4,3.2c1.4,1.2,3.2,1.6,4.3,1.8C174.1,166.8,174.3,166.8,174.4,166.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_291_" class="st0" d="M174.4,169.5c-0.1,0-0.3-0.1-0.5-0.1c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0.2,0,0.4,0.1,0.5,0.1C174.4,167.7,174.4,168.6,174.4,169.5z"/>
								</defs>
								<clipPath id="SVGID_292_">
									<use xlink:href="#SVGID_291_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st168">
									<path class="st34" d="M174.4,166.9v2.6c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6
										c0,1.4,0.4,2.4,1.4,3.2c1.4,1.2,3.2,1.6,4.3,1.8C174.1,166.8,174.3,166.8,174.4,166.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_293_" class="st0" d="M173.7,167.7v2.6c-1.1-0.2-3.2-0.6-4.8-2c-1.2-1-1.7-2.3-1.7-4l0-2.6
										c0,1.7,0.6,3,1.7,4C170.5,167.1,172.5,167.5,173.7,167.7z"/>
								</defs>
								<clipPath id="SVGID_294_">
									<use xlink:href="#SVGID_293_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st169">
									<path class="st34" d="M173.7,167.7v2.6c-1.1-0.2-3.2-0.6-4.8-2c-1.2-1-1.7-2.3-1.7-4v-2.6c0,1.7,0.6,3,1.7,4
										C170.5,167.1,172.5,167.5,173.7,167.7"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_295_" class="st0" d="M174.2,167.8v2.6c-0.1,0-0.3-0.1-0.5-0.1v-2.6C173.9,167.8,174,167.8,174.2,167.8z"/>
								</defs>
								<clipPath id="SVGID_296_">
									<use xlink:href="#SVGID_295_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st170">
									<path class="st34" d="M174.2,167.8v2.6c-0.1,0-0.3-0.1-0.5-0.1v-2.6C173.9,167.8,174,167.8,174.2,167.8"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_297_" class="st0" d="M175.3,170.5c0-0.3-0.1-0.9-0.9-1c0-0.8,0-1.5,0-2.3v-0.3c0.8,0.2,0.9,0.7,0.9,1v0.3
										C175.3,168.9,175.3,169.8,175.3,170.5z"/>
								</defs>
								<clipPath id="SVGID_298_">
									<use xlink:href="#SVGID_297_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st171">
									<path class="st34" d="M175.3,167.9v2.6c0-0.3-0.1-0.9-0.9-1v-2.6C175.2,167,175.3,167.6,175.3,167.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_299_" class="st0" d="M181.5,168.1v2.6c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2
										V163c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6C180.9,165.6,181.5,166.7,181.5,168.1z"/>
								</defs>
								<clipPath id="SVGID_300_">
									<use xlink:href="#SVGID_299_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st172">
									<path class="st34" d="M181.5,168.1v2.6c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2V163
										c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6C180.9,165.6,181.5,166.7,181.5,168.1"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_301_" class="st0" d="M175.3,167.9l0,2.6c0,0.3-0.1,1.1-1.4,1.1c-1.2,0-3.2-0.5-5.2-1.7l0-2.6
										c2,1.1,4.1,1.7,5.2,1.7C175.2,169,175.3,168.1,175.3,167.9z"/>
								</defs>
								<clipPath id="SVGID_302_">
									<use xlink:href="#SVGID_301_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st173">
									<path class="st34" d="M175.3,167.9v2.6c0,0,0,0.1,0,0.2V168C175.3,168,175.3,167.9,175.3,167.9"/>
									<path class="st34" d="M175.3,168v2.6c0,0.1,0,0.1,0,0.2v-2.6C175.3,168.2,175.3,168.1,175.3,168"/>
									<path class="st34" d="M175.2,168.2v2.6c0,0.1,0,0.1-0.1,0.2v-2.6C175.2,168.3,175.2,168.3,175.2,168.2"/>
									<path class="st34" d="M175.2,168.4v2.6c-0.2,0.3-0.5,0.6-1.3,0.6c-1.2,0-3.2-0.5-5.2-1.7v-2.6c2,1.1,4.1,1.7,5.2,1.7
										C174.7,169,175,168.7,175.2,168.4"/>
								</g>
							</g>
							<g>
								<polygon class="st34" points="168,172.7 168,175.4 167.7,175.2 167.7,172.6 								"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_303_" class="st0" d="M181.5,170.7c0,3.2-2.6,5.2-6.7,5.2c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c4.2,0,6.7-2,6.7-5.2v0.3C181.5,169.1,181.5,170,181.5,170.7z"/>
								</defs>
								<clipPath id="SVGID_304_">
									<use xlink:href="#SVGID_303_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st174">
									<path class="st34" d="M181.5,168.1v2.6c0,0.3,0,0.5-0.1,0.8v-2.6C181.4,168.6,181.5,168.3,181.5,168.1"/>
									<path class="st34" d="M181.4,168.9v2.6c0,0.3-0.1,0.5-0.2,0.8v-2.6C181.3,169.4,181.4,169.1,181.4,168.9"/>
									<path class="st34" d="M181.2,169.7v2.6c-0.1,0.3-0.2,0.5-0.4,0.8v-2.6C181,170.2,181.1,169.9,181.2,169.7"/>
									<path class="st34" d="M180.9,170.5v2.6c-1,1.7-3.2,2.8-6.1,2.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2
										C177.7,173.2,179.9,172.2,180.9,170.5"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_305_" class="st0" d="M174.7,175.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2c0,0,0,0,0,0
										c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										c0,0.1,0,0.2,0,0.3C174.7,175.6,174.7,175.7,174.7,175.8C174.7,175.8,174.7,175.8,174.7,175.8z"/>
								</defs>
								<clipPath id="SVGID_306_">
									<use xlink:href="#SVGID_305_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st175">
									<path class="st34" d="M180.9,170.5v2.6c-1,1.7-3.2,2.8-6.1,2.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2
										C177.7,173.2,179.9,172.2,180.9,170.5"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M174.8,156.3c1.9,0,4.2,0.5,5.7,1.2v4.6c-1.9-0.9-3.8-1.2-4.8-1.2c-1.3,0-1.4,0.8-1.4,1
									c0,0.5,0.3,0.9,0.9,1.1c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6c1.1,0.9,1.6,2,1.6,3.4c0,3.2-2.6,5.2-6.8,5.2
									c-2.1,0-4.4-0.5-6-1.2v-4.7c2,1.1,4.1,1.7,5.2,1.7c1.3,0,1.4-0.8,1.4-1.1c0-0.3-0.1-0.9-0.9-1c-0.1,0-0.3-0.1-0.5-0.1
									c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2C168.2,158.4,170.8,156.3,174.8,156.3z"/>
							</g>
							<g>
								<path class="st34" d="M180.5,163.9c1.3,1.1,1.9,2.4,1.9,4.1c0,3.7-3,6.2-7.7,6.2c-2.4,0-5-0.6-6.7-1.5l-0.3-0.1v-7.1
									l0.8,0.5c2.2,1.5,4.5,2,5.4,2c0.3,0,0.4-0.1,0.4-0.1c0,0,0,0-0.2-0.1c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-3.2-0.6-4.8-2
									c-1.2-1-1.7-2.3-1.7-4c0-3.9,3-6.4,7.6-6.4c2.2,0,4.8,0.6,6.4,1.4l0.3,0.1v6.8l-0.7-0.4c-2.2-1.1-4.2-1.5-5-1.5
									c-0.3,0-0.4,0.1-0.4,0.1c0,0,0.1,0,0.2,0.1c0.1,0,0.3,0.1,0.5,0.1C177.1,162.4,179.1,162.8,180.5,163.9z M174.7,173.2
									c4.2,0,6.8-2,6.8-5.2c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2c-0.6-0.2-0.9-0.6-0.9-1.1
									c0-0.2,0.1-1,1.4-1c1,0,2.8,0.4,4.8,1.2v-4.6c-1.5-0.7-3.8-1.2-5.7-1.2c-4,0-6.6,2.1-6.6,5.4c0,1.4,0.4,2.4,1.4,3.2
									c1.4,1.2,3.2,1.6,4.3,1.8c0.2,0,0.4,0.1,0.5,0.1c0.8,0.2,0.9,0.7,0.9,1c0,0.3-0.1,1.1-1.4,1.1c-1.2,0-3.2-0.5-5.2-1.7v4.7
									C170.3,172.7,172.6,173.2,174.7,173.2"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_307_" class="st0" d="M182.5,168.1l0,2.6c0,3.7-3,6.2-7.7,6.2c-2.4,0-5-0.6-6.7-1.5v-2.6
										c1.7,0.9,4.4,1.5,6.7,1.5C179.4,174.2,182.5,171.8,182.5,168.1z"/>
								</defs>
								<clipPath id="SVGID_308_">
									<use xlink:href="#SVGID_307_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st176">
									<path class="st34" d="M182.5,168.1v2.6c0,0.3,0,0.6-0.1,1V169C182.4,168.7,182.5,168.4,182.5,168.1"/>
									<path class="st34" d="M182.4,169v2.6c-0.1,0.3-0.1,0.7-0.2,1V170C182.3,169.7,182.3,169.3,182.4,169"/>
									<path class="st34" d="M182.2,170v2.6c-0.1,0.3-0.3,0.7-0.4,1v-2.6C181.9,170.6,182,170.3,182.2,170"/>
									<path class="st34" d="M181.7,170.9v2.6c-1.2,2-3.7,3.3-7,3.3c-2.4,0-5-0.6-6.7-1.5v-2.6c1.7,0.9,4.4,1.5,6.7,1.5
										C178.1,174.2,180.6,173,181.7,170.9"/>
								</g>
							</g>
						</g>
					</g>
				</g>
			</g>
		</g>
		<defs>
			<filter id="Adobe_OpacityMaskFilter_7_" filterUnits="userSpaceOnUse" x="94.1" y="155.3" width="88.4" height="21.5">
				<feFlood  style="flood-color:white;flood-opacity:1" result="back"/>
				<feBlend  in="SourceGraphic" in2="back" mode="normal"/>
			</filter>
		</defs>
		<mask maskUnits="userSpaceOnUse" x="94.1" y="155.3" width="88.4" height="21.5" id="SVGID_309_">
			<g class="st177">
				
					<image style="overflow:visible;" width="93" height="27" xlink:href="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgEASABIAAD/7AARRHVja3kAAQAEAAAAHgAA/+4AIUFkb2JlAGTAAAAAAQMA
EAMCAwYAAAGQAAAByAAAAev/2wCEABALCwsMCxAMDBAXDw0PFxsUEBAUGx8XFxcXFx8eFxoaGhoX
Hh4jJSclIx4vLzMzLy9AQEBAQEBAQEBAQEBAQEABEQ8PERMRFRISFRQRFBEUGhQWFhQaJhoaHBoa
JjAjHh4eHiMwKy4nJycuKzU1MDA1NUBAP0BAQEBAQEBAQEBAQP/CABEIABsAXQMBIgACEQEDEQH/
xABtAAADAQEAAAAAAAAAAAAAAAAAAQMCBwEBAAAAAAAAAAAAAAAAAAAAABAAAgICAwEAAAAAAAAA
AAAAAAERAjAhEEESExEAAgMAAAAAAAAAAAAAAAAAIDBAITESAQAAAAAAAAAAAAAAAAAAADD/2gAM
AwEAAhEDEQAAAOfmqkHZElvIGkCsiSvENYDZgNZAaAo5BSYH/9oACAECAAEFAMn/2gAIAQMAAQUA
yf/aAAgBAQABBQCCCBVY0QeTyzyyGOr4oKqY6oaSXU7nSttLVVLstdpwK7LXYrjtyhXPoi15J3k/
/9oACAECAgY/AE//2gAIAQMCBj8AT//aAAgBAQEGPwB9Fkv/2Q==" transform="matrix(1 0 0 1 91.75 152.5)">
				</image>
			</g>
		</mask>
		<g class="st178">
			<g>
				<g>
					<g>
						<g class="st0">
							<g>
								<rect x="95.1" y="156.6" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<rect x="106.8" y="156.6" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<polygon class="st34" points="105.8,163.8 105.8,166.4 100.8,159.2 100.8,156.6 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.8,163.8 106.8,166.4 106.1,166.7 106.1,164.1 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.1,164.1 106.1,166.7 105.8,166.4 105.8,163.8 								"/>
							</g>
							<g>
								<polygon class="st34" points="101.2,165.4 101.2,168 100.5,168.4 100.5,165.8 								"/>
							</g>
							<g>
								<polygon class="st34" points="101.5,165.8 101.5,168.4 101.2,168 101.2,165.4 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.5,173 106.5,175.6 101.5,168.4 101.5,165.8 								"/>
							</g>
							<g>
								<rect x="106.5" y="173" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<rect x="95.1" y="173" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M105.8,155.6h7.4V174h-7.3l-4.4-6.4v6.4h-7.4v-18.4h7.3l4.4,6.4V155.6z M112.2,173v-16.4h-5.4v7.2
									l-0.7,0.3l-0.3-0.3l-5-7.2h-5.7V173h5.4v-7.2l0.7-0.3l0.3,0.3l5,7.2H112.2"/>
							</g>
							<g>
								<polygon class="st34" points="112.2,156.6 112.2,173 106.5,173 101.5,165.8 101.2,165.4 100.5,165.8 100.5,173 95.1,173 
									95.1,156.6 100.8,156.6 105.8,163.8 106.1,164.1 106.8,163.8 106.8,156.6 								"/>
							</g>
							<g>
								<polygon class="st34" points="105.9,174 105.9,176.6 101.5,170.2 101.5,167.6 								"/>
							</g>
							<g>
								<rect x="105.9" y="174" class="st34" width="7.3" height="2.6"/>
							</g>
							<g>
								<rect x="94.1" y="174" class="st34" width="7.4" height="2.6"/>
							</g>
						</g>
						<g class="st0">
							<g class="st0">
								<defs>
									<path id="SVGID_310_" class="st0" d="M133.3,164.8l0,2.6c0-4.7-4-8.4-9.2-8.4c-5.2,0-9.2,3.7-9.2,8.4l0-2.6
										c0-4.7,4-8.4,9.2-8.4C129.3,156.3,133.3,160,133.3,164.8z"/>
								</defs>
								<clipPath id="SVGID_311_">
									<use xlink:href="#SVGID_310_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st179">
									<path class="st34" d="M133.3,164.8v2.6c0-4.7-4-8.4-9.2-8.4c-3.6,0-6.6,1.8-8.1,4.5v-2.6c1.5-2.7,4.6-4.5,8.1-4.5
										C129.3,156.3,133.3,160,133.3,164.8"/>
									<path class="st34" d="M116,160.8v2.6c-0.3,0.5-0.5,0.9-0.6,1.4v-2.6C115.5,161.7,115.7,161.3,116,160.8"/>
									<path class="st34" d="M115.3,162.2v2.6c-0.1,0.4-0.3,0.9-0.3,1.3v-2.6C115.1,163.1,115.2,162.7,115.3,162.2"/>
									<path class="st34" d="M115,163.6v2.6c-0.1,0.4-0.1,0.8-0.1,1.2v-2.6C114.9,164.4,115,164,115,163.6"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_312_" class="st0" d="M120.9,167.5c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3v-0.3c0-1.8,1.4-3.2,3.2-3.2
										c1.8,0,3.2,1.4,3.2,3.2l0,2.6c0-1.8-1.4-3.2-3.2-3.2C122.4,164.3,120.9,165.8,120.9,167.5z"/>
								</defs>
								<clipPath id="SVGID_313_">
									<use xlink:href="#SVGID_312_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st180">
									<path class="st34" d="M127.3,164.9v2.6c0-1.8-1.4-3.2-3.2-3.2c-1.2,0-2.2,0.7-2.8,1.6v-2.6c0.5-1,1.6-1.6,2.8-1.6
										C125.9,161.7,127.3,163.1,127.3,164.9"/>
									<path class="st34" d="M121.3,163.3v2.6c-0.1,0.2-0.2,0.4-0.3,0.6v-2.6C121.2,163.7,121.2,163.5,121.3,163.3"/>
									<path class="st34" d="M121.1,163.9v2.6c-0.1,0.2-0.1,0.3-0.1,0.5v-2.6C121,164.2,121,164.1,121.1,163.9"/>
									<path class="st34" d="M121,164.4v2.6c0,0.2,0,0.3,0,0.5v-2.6C120.9,164.7,120.9,164.6,121,164.4"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_314_" class="st0" d="M126.3,164.9l0,2.6c0-1.2-1-2.2-2.2-2.2c-1.2,0-2.2,1-2.2,2.2l0-2.6
										c0-1.2,1-2.2,2.2-2.2C125.3,162.7,126.3,163.7,126.3,164.9z"/>
								</defs>
								<clipPath id="SVGID_315_">
									<use xlink:href="#SVGID_314_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st181">
									<path class="st34" d="M126.3,164.9v2.6c0-1.2-1-2.2-2.2-2.2c-0.8,0-1.5,0.4-1.9,1.1v-2.6c0.4-0.7,1.1-1.1,1.9-1.1
										C125.3,162.7,126.3,163.7,126.3,164.9"/>
									<path class="st34" d="M122.2,163.8v2.6c-0.1,0.1-0.1,0.3-0.2,0.4v-2.6C122.1,164.1,122.1,163.9,122.2,163.8"/>
									<path class="st34" d="M122,164.2v2.6c0,0.1-0.1,0.2-0.1,0.4v-2.6C122,164.4,122,164.3,122,164.2"/>
									<path class="st34" d="M121.9,164.6v2.6c0,0.1,0,0.2,0,0.3v-2.6C121.9,164.8,121.9,164.7,121.9,164.6"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_316_" class="st0" d="M127.3,164.9l0,2.6c0,1.8-1.4,3.2-3.2,3.2c-1.8,0-3.2-1.4-3.2-3.2v-2.6
										c0,1.8,1.4,3.2,3.2,3.2C125.9,168.1,127.3,166.6,127.3,164.9z"/>
								</defs>
								<clipPath id="SVGID_317_">
									<use xlink:href="#SVGID_316_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st182">
									<path class="st34" d="M127.3,164.9v2.6c0,0.2,0,0.3,0,0.5v-2.6C127.3,165.2,127.3,165.1,127.3,164.9"/>
									<path class="st34" d="M127.3,165.4v2.6c0,0.2-0.1,0.4-0.1,0.5v-2.6C127.2,165.7,127.2,165.5,127.3,165.4"/>
									<path class="st34" d="M127.1,165.9v2.6c-0.1,0.2-0.2,0.4-0.3,0.6v-2.6C127,166.3,127.1,166.1,127.1,165.9"/>
									<path class="st34" d="M126.9,166.5v2.6c-0.5,1-1.6,1.6-2.8,1.6c-1.8,0-3.2-1.4-3.2-3.2v-2.6c0,1.8,1.4,3.2,3.2,3.2
										C125.3,168.1,126.3,167.4,126.9,166.5"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M124.1,161.7c1.8,0,3.2,1.4,3.2,3.2c0,1.8-1.4,3.2-3.2,3.2c-1.8,0-3.2-1.4-3.2-3.2
									C120.9,163.1,122.4,161.7,124.1,161.7z M124.1,167.1c1.2,0,2.2-1,2.2-2.2c0-1.2-1-2.2-2.2-2.2c-1.2,0-2.2,1-2.2,2.2
									C121.9,166.1,122.9,167.1,124.1,167.1"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_318_" class="st0" d="M124.1,175.8c-5.2,0-9.2-3.7-9.2-8.4l0-2.6c0,4.7,4,8.4,9.2,8.4
										c5.2,0,9.2-3.7,9.2-8.4l0,0.3v2.3C133.3,172.1,129.3,175.8,124.1,175.8z"/>
								</defs>
								<clipPath id="SVGID_319_">
									<use xlink:href="#SVGID_318_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st183">
									<path class="st34" d="M133.3,164.8v2.6c0,0.4,0,0.8-0.1,1.2V166C133.3,165.6,133.3,165.2,133.3,164.8"/>
									<path class="st34" d="M133.2,166v2.6c-0.1,0.5-0.2,0.9-0.3,1.3v-2.6C133,166.9,133.2,166.4,133.2,166"/>
									<path class="st34" d="M132.9,167.3v2.6c-0.2,0.5-0.4,1-0.6,1.4v-2.6C132.5,168.3,132.7,167.8,132.9,167.3"/>
									<path class="st34" d="M132.3,168.8v2.6c-1.5,2.7-4.6,4.5-8.1,4.5c-5.2,0-9.2-3.7-9.2-8.4v-2.6c0,4.7,4,8.4,9.2,8.4
										C127.7,173.2,130.7,171.4,132.3,168.8"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M124.1,155.3c5.7,0,10.2,4.2,10.2,9.4c0,5.3-4.5,9.4-10.2,9.4c-5.7,0-10.2-4.1-10.2-9.4
									C113.9,159.5,118.4,155.3,124.1,155.3z M124.1,173.2c5.2,0,9.2-3.7,9.2-8.4c0-4.7-4-8.4-9.2-8.4c-5.2,0-9.2,3.7-9.2,8.4
									C114.9,169.5,119,173.2,124.1,173.2"/>
							</g>
							<g>
								<path class="st34" d="M124.1,156.3c5.2,0,9.2,3.7,9.2,8.4c0,4.7-4,8.4-9.2,8.4c-5.2,0-9.2-3.7-9.2-8.4
									C114.9,160,119,156.3,124.1,156.3z M124.1,168.1c1.8,0,3.2-1.4,3.2-3.2c0-1.8-1.4-3.2-3.2-3.2c-1.8,0-3.2,1.4-3.2,3.2
									C120.9,166.6,122.4,168.1,124.1,168.1"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_320_" class="st0" d="M134.3,164.8v2.6c0,5.3-4.5,9.4-10.2,9.4c-5.7,0-10.2-4.1-10.2-9.4l0-2.6
										c0,5.3,4.5,9.4,10.2,9.4C129.8,174.2,134.3,170.1,134.3,164.8z"/>
								</defs>
								<clipPath id="SVGID_321_">
									<use xlink:href="#SVGID_320_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st184">
									<path class="st34" d="M134.3,164.8v2.6c0,0.5,0,0.9-0.1,1.4v-2.6C134.3,165.7,134.3,165.2,134.3,164.8"/>
									<path class="st34" d="M134.2,166.1v2.6c-0.1,0.5-0.2,1-0.4,1.5v-2.6C134,167.2,134.1,166.7,134.2,166.1"/>
									<path class="st34" d="M133.9,167.6v2.6c-0.2,0.6-0.4,1.1-0.7,1.6v-2.6C133.4,168.7,133.7,168.2,133.9,167.6"/>
									<path class="st34" d="M133.1,169.3v2.6c-1.7,3-5.1,5-9,5c-5.7,0-10.2-4.1-10.2-9.4v-2.6c0,5.3,4.5,9.4,10.2,9.4
										C128.1,174.2,131.4,172.2,133.1,169.3"/>
								</g>
							</g>
						</g>
						<g class="st0">
							<g>
								<rect x="135.3" y="156.6" class="st34" width="15.8" height="2.6"/>
							</g>
							<g>
								<rect x="135.3" y="161.5" class="st34" width="4.9" height="2.6"/>
							</g>
							<g>
								<rect x="146.3" y="161.5" class="st34" width="4.8" height="2.6"/>
							</g>
							<g>
								<rect x="147.3" y="162.5" class="st34" width="4.8" height="2.6"/>
							</g>
							<g>
								<rect x="134.3" y="162.5" class="st34" width="4.9" height="2.6"/>
							</g>
							<g>
								<rect x="140.2" y="173" class="st34" width="6.2" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M134.3,155.6h17.8v6.9h-4.8V174h-8.2v-11.5h-4.9V155.6z M151.2,161.5v-4.9h-15.8v4.9h4.9V173h6.2
									v-11.5H151.2"/>
							</g>
							<g>
								<polygon class="st34" points="151.2,156.6 151.2,161.5 146.3,161.5 146.3,173 140.2,173 140.2,161.5 135.3,161.5 
									135.3,156.6 								"/>
							</g>
							<g>
								<rect x="139.2" y="174" class="st34" width="8.2" height="2.6"/>
							</g>
						</g>
						<g class="st0">
							<g>
								<rect x="154" y="156.6" class="st34" width="11.5" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="161.4" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<rect x="165.5" y="162.4" class="st34" width="1" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="163.3" class="st34" width="4.3" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="166.5" class="st34" width="4.3" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="168.4" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<polygon class="st34" points="165.5,156.6 165.5,161.4 160.2,161.4 160.2,163.3 164.5,163.3 164.5,166.5 160.2,166.5 
									160.2,168.4 165.9,168.4 165.9,173 154,173 154,156.6 								"/>
							</g>
							<g>
								<rect x="154" y="173" class="st34" width="11.8" height="2.6"/>
							</g>
							<g>
								<rect x="153" y="174" class="st34" width="13.8" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M165.5,167.4h1.4v6.5H153v-18.4h13.5v6.8h-1V167.4z M165.9,173v-4.5h-5.7v-2h4.3v-3.1h-4.3v-1.9h5.4
									v-4.8H154V173H165.9"/>
							</g>
						</g>
						<g class="st0">
							<g class="st0">
								<defs>
									<path id="SVGID_322_" class="st0" d="M180.5,157.5l0,2.6c-1.5-0.7-3.8-1.2-5.7-1.2c-4,0-6.6,2.1-6.6,5.4l0-2.6
										c0-3.3,2.6-5.4,6.6-5.4C176.7,156.3,178.9,156.8,180.5,157.5z"/>
								</defs>
								<clipPath id="SVGID_323_">
									<use xlink:href="#SVGID_322_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st185">
									<path class="st34" d="M180.5,157.5v2.6c-1.5-0.7-3.8-1.2-5.7-1.2c-2.8,0-4.9,1-5.9,2.8v-2.6c1-1.8,3.1-2.8,5.9-2.8
										C176.7,156.3,178.9,156.8,180.5,157.5"/>
									<path class="st34" d="M168.8,159.1v2.6c-0.2,0.3-0.3,0.6-0.4,0.9V160C168.6,159.7,168.7,159.4,168.8,159.1"/>
									<path class="st34" d="M168.5,160v2.6c-0.1,0.3-0.2,0.6-0.2,0.9v-2.6C168.3,160.5,168.4,160.3,168.5,160"/>
									<path class="st34" d="M168.3,160.8v2.6c0,0.3-0.1,0.6-0.1,0.8v-2.6C168.2,161.4,168.2,161.1,168.3,160.8"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_324_" class="st0" d="M175.7,163.5c-1.3,0-1.4,0.8-1.4,1v-2.6c0-0.2,0.1-1,1.4-1c1,0,2.8,0.4,4.8,1.2v2.6
										C178.5,163.9,176.7,163.5,175.7,163.5z"/>
								</defs>
								<clipPath id="SVGID_325_">
									<use xlink:href="#SVGID_324_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st186">
									<path class="st34" d="M180.5,162.1v2.6c-1.9-0.9-3.8-1.2-4.8-1.2c-0.8,0-1.1,0.3-1.3,0.5v-2.6c0.2-0.3,0.5-0.5,1.3-0.5
										C176.7,160.8,178.5,161.2,180.5,162.1"/>
									<path class="st34" d="M174.5,161.4v2.6c0,0.1-0.1,0.1-0.1,0.2v-2.6C174.4,161.5,174.4,161.5,174.5,161.4"/>
									<path class="st34" d="M174.4,161.6v2.6c0,0.1,0,0.1,0,0.2v-2.6C174.3,161.7,174.4,161.6,174.4,161.6"/>
									<path class="st34" d="M174.3,161.7v2.6c0,0.1,0,0.1,0,0.1v-2.6C174.3,161.8,174.3,161.8,174.3,161.7"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_326_" class="st0" d="M175.2,163v2.6c-0.6-0.2-0.9-0.6-0.9-1.1l0-2.6C174.3,162.4,174.6,162.7,175.2,163z"
										/>
								</defs>
								<clipPath id="SVGID_327_">
									<use xlink:href="#SVGID_326_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st187">
									<path class="st34" d="M175.2,163v2.6c-0.6-0.2-0.9-0.6-0.9-1.1v-2.6C174.3,162.4,174.6,162.7,175.2,163"/>
								</g>
							</g>
							<g>
								<polygon class="st34" points="181.5,163.7 181.5,166.3 180.7,165.9 180.7,163.3 								"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_328_" class="st0" d="M169.6,167.5c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6c0,1.4,0.4,2.4,1.4,3.2
										C169.6,165.8,169.6,166.7,169.6,167.5c0-0.9,0-1.7,0-2.6c1.4,1.2,3.2,1.6,4.3,1.8c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										C172.8,169.1,171,168.8,169.6,167.5z"/>
								</defs>
								<clipPath id="SVGID_329_">
									<use xlink:href="#SVGID_328_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st188">
									<path class="st34" d="M174.4,166.9v2.6c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6
										c0,1.4,0.4,2.4,1.4,3.2c1.4,1.2,3.2,1.6,4.3,1.8C174.1,166.8,174.3,166.8,174.4,166.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_330_" class="st0" d="M174.4,169.5c-0.1,0-0.3-0.1-0.5-0.1c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0.2,0,0.4,0.1,0.5,0.1C174.4,167.7,174.4,168.6,174.4,169.5z"/>
								</defs>
								<clipPath id="SVGID_331_">
									<use xlink:href="#SVGID_330_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st189">
									<path class="st34" d="M174.4,166.9v2.6c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6
										c0,1.4,0.4,2.4,1.4,3.2c1.4,1.2,3.2,1.6,4.3,1.8C174.1,166.8,174.3,166.8,174.4,166.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_332_" class="st0" d="M173.7,167.7v2.6c-1.1-0.2-3.2-0.6-4.8-2c-1.2-1-1.7-2.3-1.7-4l0-2.6
										c0,1.7,0.6,3,1.7,4C170.5,167.1,172.5,167.5,173.7,167.7z"/>
								</defs>
								<clipPath id="SVGID_333_">
									<use xlink:href="#SVGID_332_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st190">
									<path class="st34" d="M173.7,167.7v2.6c-1.1-0.2-3.2-0.6-4.8-2c-1.2-1-1.7-2.3-1.7-4v-2.6c0,1.7,0.6,3,1.7,4
										C170.5,167.1,172.5,167.5,173.7,167.7"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_334_" class="st0" d="M174.2,167.8v2.6c-0.1,0-0.3-0.1-0.5-0.1v-2.6C173.9,167.8,174,167.8,174.2,167.8z"/>
								</defs>
								<clipPath id="SVGID_335_">
									<use xlink:href="#SVGID_334_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st191">
									<path class="st34" d="M174.2,167.8v2.6c-0.1,0-0.3-0.1-0.5-0.1v-2.6C173.9,167.8,174,167.8,174.2,167.8"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_336_" class="st0" d="M175.3,170.5c0-0.3-0.1-0.9-0.9-1c0-0.8,0-1.5,0-2.3v-0.3c0.8,0.2,0.9,0.7,0.9,1v0.3
										C175.3,168.9,175.3,169.8,175.3,170.5z"/>
								</defs>
								<clipPath id="SVGID_337_">
									<use xlink:href="#SVGID_336_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st192">
									<path class="st34" d="M175.3,167.9v2.6c0-0.3-0.1-0.9-0.9-1v-2.6C175.2,167,175.3,167.6,175.3,167.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_338_" class="st0" d="M181.5,168.1v2.6c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2
										V163c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6C180.9,165.6,181.5,166.7,181.5,168.1z"/>
								</defs>
								<clipPath id="SVGID_339_">
									<use xlink:href="#SVGID_338_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st193">
									<path class="st34" d="M181.5,168.1v2.6c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2V163
										c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6C180.9,165.6,181.5,166.7,181.5,168.1"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_340_" class="st0" d="M175.3,167.9l0,2.6c0,0.3-0.1,1.1-1.4,1.1c-1.2,0-3.2-0.5-5.2-1.7l0-2.6
										c2,1.1,4.1,1.7,5.2,1.7C175.2,169,175.3,168.1,175.3,167.9z"/>
								</defs>
								<clipPath id="SVGID_341_">
									<use xlink:href="#SVGID_340_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st194">
									<path class="st34" d="M175.3,167.9v2.6c0,0,0,0.1,0,0.2V168C175.3,168,175.3,167.9,175.3,167.9"/>
									<path class="st34" d="M175.3,168v2.6c0,0.1,0,0.1,0,0.2v-2.6C175.3,168.2,175.3,168.1,175.3,168"/>
									<path class="st34" d="M175.2,168.2v2.6c0,0.1,0,0.1-0.1,0.2v-2.6C175.2,168.3,175.2,168.3,175.2,168.2"/>
									<path class="st34" d="M175.2,168.4v2.6c-0.2,0.3-0.5,0.6-1.3,0.6c-1.2,0-3.2-0.5-5.2-1.7v-2.6c2,1.1,4.1,1.7,5.2,1.7
										C174.7,169,175,168.7,175.2,168.4"/>
								</g>
							</g>
							<g>
								<polygon class="st34" points="168,172.7 168,175.4 167.7,175.2 167.7,172.6 								"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_342_" class="st0" d="M181.5,170.7c0,3.2-2.6,5.2-6.7,5.2c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c4.2,0,6.7-2,6.7-5.2v0.3C181.5,169.1,181.5,170,181.5,170.7z"/>
								</defs>
								<clipPath id="SVGID_343_">
									<use xlink:href="#SVGID_342_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st195">
									<path class="st34" d="M181.5,168.1v2.6c0,0.3,0,0.5-0.1,0.8v-2.6C181.4,168.6,181.5,168.3,181.5,168.1"/>
									<path class="st34" d="M181.4,168.9v2.6c0,0.3-0.1,0.5-0.2,0.8v-2.6C181.3,169.4,181.4,169.1,181.4,168.9"/>
									<path class="st34" d="M181.2,169.7v2.6c-0.1,0.3-0.2,0.5-0.4,0.8v-2.6C181,170.2,181.1,169.9,181.2,169.7"/>
									<path class="st34" d="M180.9,170.5v2.6c-1,1.7-3.2,2.8-6.1,2.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2
										C177.7,173.2,179.9,172.2,180.9,170.5"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_344_" class="st0" d="M174.7,175.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2c0,0,0,0,0,0
										c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										c0,0.1,0,0.2,0,0.3C174.7,175.6,174.7,175.7,174.7,175.8C174.7,175.8,174.7,175.8,174.7,175.8z"/>
								</defs>
								<clipPath id="SVGID_345_">
									<use xlink:href="#SVGID_344_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st196">
									<path class="st34" d="M180.9,170.5v2.6c-1,1.7-3.2,2.8-6.1,2.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2
										C177.7,173.2,179.9,172.2,180.9,170.5"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M174.8,156.3c1.9,0,4.2,0.5,5.7,1.2v4.6c-1.9-0.9-3.8-1.2-4.8-1.2c-1.3,0-1.4,0.8-1.4,1
									c0,0.5,0.3,0.9,0.9,1.1c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6c1.1,0.9,1.6,2,1.6,3.4c0,3.2-2.6,5.2-6.8,5.2
									c-2.1,0-4.4-0.5-6-1.2v-4.7c2,1.1,4.1,1.7,5.2,1.7c1.3,0,1.4-0.8,1.4-1.1c0-0.3-0.1-0.9-0.9-1c-0.1,0-0.3-0.1-0.5-0.1
									c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2C168.2,158.4,170.8,156.3,174.8,156.3z"/>
							</g>
							<g>
								<path class="st34" d="M180.5,163.9c1.3,1.1,1.9,2.4,1.9,4.1c0,3.7-3,6.2-7.7,6.2c-2.4,0-5-0.6-6.7-1.5l-0.3-0.1v-7.1
									l0.8,0.5c2.2,1.5,4.5,2,5.4,2c0.3,0,0.4-0.1,0.4-0.1c0,0,0,0-0.2-0.1c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-3.2-0.6-4.8-2
									c-1.2-1-1.7-2.3-1.7-4c0-3.9,3-6.4,7.6-6.4c2.2,0,4.8,0.6,6.4,1.4l0.3,0.1v6.8l-0.7-0.4c-2.2-1.1-4.2-1.5-5-1.5
									c-0.3,0-0.4,0.1-0.4,0.1c0,0,0.1,0,0.2,0.1c0.1,0,0.3,0.1,0.5,0.1C177.1,162.4,179.1,162.8,180.5,163.9z M174.7,173.2
									c4.2,0,6.8-2,6.8-5.2c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2c-0.6-0.2-0.9-0.6-0.9-1.1
									c0-0.2,0.1-1,1.4-1c1,0,2.8,0.4,4.8,1.2v-4.6c-1.5-0.7-3.8-1.2-5.7-1.2c-4,0-6.6,2.1-6.6,5.4c0,1.4,0.4,2.4,1.4,3.2
									c1.4,1.2,3.2,1.6,4.3,1.8c0.2,0,0.4,0.1,0.5,0.1c0.8,0.2,0.9,0.7,0.9,1c0,0.3-0.1,1.1-1.4,1.1c-1.2,0-3.2-0.5-5.2-1.7v4.7
									C170.3,172.7,172.6,173.2,174.7,173.2"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_346_" class="st0" d="M182.5,168.1l0,2.6c0,3.7-3,6.2-7.7,6.2c-2.4,0-5-0.6-6.7-1.5v-2.6
										c1.7,0.9,4.4,1.5,6.7,1.5C179.4,174.2,182.5,171.8,182.5,168.1z"/>
								</defs>
								<clipPath id="SVGID_347_">
									<use xlink:href="#SVGID_346_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st197">
									<path class="st34" d="M182.5,168.1v2.6c0,0.3,0,0.6-0.1,1V169C182.4,168.7,182.5,168.4,182.5,168.1"/>
									<path class="st34" d="M182.4,169v2.6c-0.1,0.3-0.1,0.7-0.2,1V170C182.3,169.7,182.3,169.3,182.4,169"/>
									<path class="st34" d="M182.2,170v2.6c-0.1,0.3-0.3,0.7-0.4,1v-2.6C181.9,170.6,182,170.3,182.2,170"/>
									<path class="st34" d="M181.7,170.9v2.6c-1.2,2-3.7,3.3-7,3.3c-2.4,0-5-0.6-6.7-1.5v-2.6c1.7,0.9,4.4,1.5,6.7,1.5
										C178.1,174.2,180.6,173,181.7,170.9"/>
								</g>
							</g>
						</g>
					</g>
					<defs>
						<filter id="Adobe_OpacityMaskFilter_8_" filterUnits="userSpaceOnUse" x="94.1" y="155.3" width="88.4" height="21.5">
							<feFlood  style="flood-color:white;flood-opacity:1" result="back"/>
							<feBlend  in="SourceGraphic" in2="back" mode="normal"/>
						</filter>
					</defs>
					<mask maskUnits="userSpaceOnUse" x="94.1" y="155.3" width="88.4" height="21.5" id="SVGID_309_">
						<g class="st198">
							
								<image style="overflow:visible;" width="93" height="27" xlink:href="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgEASABIAAD/7AARRHVja3kAAQAEAAAAHgAA/+4AIUFkb2JlAGTAAAAAAQMA
EAMCAwYAAAF0AAABfwAAAaD/2wCEABALCwsMCxAMDBAXDw0PFxsUEBAUGx8XFxcXFx8eFxoaGhoX
Hh4jJSclIx4vLzMzLy9AQEBAQEBAQEBAQEBAQEABEQ8PERMRFRISFRQRFBEUGhQWFhQaJhoaHBoa
JjAjHh4eHiMwKy4nJycuKzU1MDA1NUBAP0BAQEBAQEBAQEBAQP/CABEIABsAXQMBIgACEQEDEQH/
xABfAAEBAQAAAAAAAAAAAAAAAAAAAQcBAQAAAAAAAAAAAAAAAAAAAAAQAQEAAAAAAAAAAAAAAAAA
ADBQEQEAAAAAAAAAAAAAAAAAAABQEgEAAAAAAAAAAAAAAAAAAAAw/9oADAMBAAIRAxEAAADPykWA
AABYAAAAAAf/2gAIAQIAAQUAT//aAAgBAwABBQBP/9oACAEBAAEFAKH/2gAIAQICBj8AT//aAAgB
AwIGPwBP/9oACAEBAQY/AEP/2Q==" transform="matrix(1 0 0 1 91.75 152.5)">
							</image>
						</g>
					</mask>
					<g>
						<g class="st0">
							<g>
								<rect x="95.1" y="156.6" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<rect x="106.8" y="156.6" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<polygon class="st34" points="105.8,163.8 105.8,166.4 100.8,159.2 100.8,156.6 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.8,163.8 106.8,166.4 106.1,166.7 106.1,164.1 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.1,164.1 106.1,166.7 105.8,166.4 105.8,163.8 								"/>
							</g>
							<g>
								<polygon class="st34" points="101.2,165.4 101.2,168 100.5,168.4 100.5,165.8 								"/>
							</g>
							<g>
								<polygon class="st34" points="101.5,165.8 101.5,168.4 101.2,168 101.2,165.4 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.5,173 106.5,175.6 101.5,168.4 101.5,165.8 								"/>
							</g>
							<g>
								<rect x="106.5" y="173" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<rect x="95.1" y="173" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M105.8,155.6h7.4V174h-7.3l-4.4-6.4v6.4h-7.4v-18.4h7.3l4.4,6.4V155.6z M112.2,173v-16.4h-5.4v7.2
									l-0.7,0.3l-0.3-0.3l-5-7.2h-5.7V173h5.4v-7.2l0.7-0.3l0.3,0.3l5,7.2H112.2"/>
							</g>
							<g>
								<polygon class="st34" points="112.2,156.6 112.2,173 106.5,173 101.5,165.8 101.2,165.4 100.5,165.8 100.5,173 95.1,173 
									95.1,156.6 100.8,156.6 105.8,163.8 106.1,164.1 106.8,163.8 106.8,156.6 								"/>
							</g>
							<g>
								<polygon class="st34" points="105.9,174 105.9,176.6 101.5,170.2 101.5,167.6 								"/>
							</g>
							<g>
								<rect x="105.9" y="174" class="st34" width="7.3" height="2.6"/>
							</g>
							<g>
								<rect x="94.1" y="174" class="st34" width="7.4" height="2.6"/>
							</g>
						</g>
						<g class="st0">
							<g class="st0">
								<defs>
									<path id="SVGID_348_" class="st0" d="M133.3,164.8l0,2.6c0-4.7-4-8.4-9.2-8.4c-5.2,0-9.2,3.7-9.2,8.4l0-2.6
										c0-4.7,4-8.4,9.2-8.4C129.3,156.3,133.3,160,133.3,164.8z"/>
								</defs>
								<clipPath id="SVGID_349_">
									<use xlink:href="#SVGID_348_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st199">
									<path class="st34" d="M133.3,164.8v2.6c0-4.7-4-8.4-9.2-8.4c-3.6,0-6.6,1.8-8.1,4.5v-2.6c1.5-2.7,4.6-4.5,8.1-4.5
										C129.3,156.3,133.3,160,133.3,164.8"/>
									<path class="st34" d="M116,160.8v2.6c-0.3,0.5-0.5,0.9-0.6,1.4v-2.6C115.5,161.7,115.7,161.3,116,160.8"/>
									<path class="st34" d="M115.3,162.2v2.6c-0.1,0.4-0.3,0.9-0.3,1.3v-2.6C115.1,163.1,115.2,162.7,115.3,162.2"/>
									<path class="st34" d="M115,163.6v2.6c-0.1,0.4-0.1,0.8-0.1,1.2v-2.6C114.9,164.4,115,164,115,163.6"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_350_" class="st0" d="M120.9,167.5c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3v-0.3c0-1.8,1.4-3.2,3.2-3.2
										c1.8,0,3.2,1.4,3.2,3.2l0,2.6c0-1.8-1.4-3.2-3.2-3.2C122.4,164.3,120.9,165.8,120.9,167.5z"/>
								</defs>
								<clipPath id="SVGID_351_">
									<use xlink:href="#SVGID_350_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st200">
									<path class="st34" d="M127.3,164.9v2.6c0-1.8-1.4-3.2-3.2-3.2c-1.2,0-2.2,0.7-2.8,1.6v-2.6c0.5-1,1.6-1.6,2.8-1.6
										C125.9,161.7,127.3,163.1,127.3,164.9"/>
									<path class="st34" d="M121.3,163.3v2.6c-0.1,0.2-0.2,0.4-0.3,0.6v-2.6C121.2,163.7,121.2,163.5,121.3,163.3"/>
									<path class="st34" d="M121.1,163.9v2.6c-0.1,0.2-0.1,0.3-0.1,0.5v-2.6C121,164.2,121,164.1,121.1,163.9"/>
									<path class="st34" d="M121,164.4v2.6c0,0.2,0,0.3,0,0.5v-2.6C120.9,164.7,120.9,164.6,121,164.4"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_352_" class="st0" d="M126.3,164.9l0,2.6c0-1.2-1-2.2-2.2-2.2c-1.2,0-2.2,1-2.2,2.2l0-2.6
										c0-1.2,1-2.2,2.2-2.2C125.3,162.7,126.3,163.7,126.3,164.9z"/>
								</defs>
								<clipPath id="SVGID_353_">
									<use xlink:href="#SVGID_352_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st201">
									<path class="st34" d="M126.3,164.9v2.6c0-1.2-1-2.2-2.2-2.2c-0.8,0-1.5,0.4-1.9,1.1v-2.6c0.4-0.7,1.1-1.1,1.9-1.1
										C125.3,162.7,126.3,163.7,126.3,164.9"/>
									<path class="st34" d="M122.2,163.8v2.6c-0.1,0.1-0.1,0.3-0.2,0.4v-2.6C122.1,164.1,122.1,163.9,122.2,163.8"/>
									<path class="st34" d="M122,164.2v2.6c0,0.1-0.1,0.2-0.1,0.4v-2.6C122,164.4,122,164.3,122,164.2"/>
									<path class="st34" d="M121.9,164.6v2.6c0,0.1,0,0.2,0,0.3v-2.6C121.9,164.8,121.9,164.7,121.9,164.6"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_354_" class="st0" d="M127.3,164.9l0,2.6c0,1.8-1.4,3.2-3.2,3.2c-1.8,0-3.2-1.4-3.2-3.2v-2.6
										c0,1.8,1.4,3.2,3.2,3.2C125.9,168.1,127.3,166.6,127.3,164.9z"/>
								</defs>
								<clipPath id="SVGID_355_">
									<use xlink:href="#SVGID_354_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st202">
									<path class="st34" d="M127.3,164.9v2.6c0,0.2,0,0.3,0,0.5v-2.6C127.3,165.2,127.3,165.1,127.3,164.9"/>
									<path class="st34" d="M127.3,165.4v2.6c0,0.2-0.1,0.4-0.1,0.5v-2.6C127.2,165.7,127.2,165.5,127.3,165.4"/>
									<path class="st34" d="M127.1,165.9v2.6c-0.1,0.2-0.2,0.4-0.3,0.6v-2.6C127,166.3,127.1,166.1,127.1,165.9"/>
									<path class="st34" d="M126.9,166.5v2.6c-0.5,1-1.6,1.6-2.8,1.6c-1.8,0-3.2-1.4-3.2-3.2v-2.6c0,1.8,1.4,3.2,3.2,3.2
										C125.3,168.1,126.3,167.4,126.9,166.5"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M124.1,161.7c1.8,0,3.2,1.4,3.2,3.2c0,1.8-1.4,3.2-3.2,3.2c-1.8,0-3.2-1.4-3.2-3.2
									C120.9,163.1,122.4,161.7,124.1,161.7z M124.1,167.1c1.2,0,2.2-1,2.2-2.2c0-1.2-1-2.2-2.2-2.2c-1.2,0-2.2,1-2.2,2.2
									C121.9,166.1,122.9,167.1,124.1,167.1"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_356_" class="st0" d="M124.1,175.8c-5.2,0-9.2-3.7-9.2-8.4l0-2.6c0,4.7,4,8.4,9.2,8.4
										c5.2,0,9.2-3.7,9.2-8.4l0,0.3v2.3C133.3,172.1,129.3,175.8,124.1,175.8z"/>
								</defs>
								<clipPath id="SVGID_357_">
									<use xlink:href="#SVGID_356_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st203">
									<path class="st34" d="M133.3,164.8v2.6c0,0.4,0,0.8-0.1,1.2V166C133.3,165.6,133.3,165.2,133.3,164.8"/>
									<path class="st34" d="M133.2,166v2.6c-0.1,0.5-0.2,0.9-0.3,1.3v-2.6C133,166.9,133.2,166.4,133.2,166"/>
									<path class="st34" d="M132.9,167.3v2.6c-0.2,0.5-0.4,1-0.6,1.4v-2.6C132.5,168.3,132.7,167.8,132.9,167.3"/>
									<path class="st34" d="M132.3,168.8v2.6c-1.5,2.7-4.6,4.5-8.1,4.5c-5.2,0-9.2-3.7-9.2-8.4v-2.6c0,4.7,4,8.4,9.2,8.4
										C127.7,173.2,130.7,171.4,132.3,168.8"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M124.1,155.3c5.7,0,10.2,4.2,10.2,9.4c0,5.3-4.5,9.4-10.2,9.4c-5.7,0-10.2-4.1-10.2-9.4
									C113.9,159.5,118.4,155.3,124.1,155.3z M124.1,173.2c5.2,0,9.2-3.7,9.2-8.4c0-4.7-4-8.4-9.2-8.4c-5.2,0-9.2,3.7-9.2,8.4
									C114.9,169.5,119,173.2,124.1,173.2"/>
							</g>
							<g>
								<path class="st34" d="M124.1,156.3c5.2,0,9.2,3.7,9.2,8.4c0,4.7-4,8.4-9.2,8.4c-5.2,0-9.2-3.7-9.2-8.4
									C114.9,160,119,156.3,124.1,156.3z M124.1,168.1c1.8,0,3.2-1.4,3.2-3.2c0-1.8-1.4-3.2-3.2-3.2c-1.8,0-3.2,1.4-3.2,3.2
									C120.9,166.6,122.4,168.1,124.1,168.1"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_358_" class="st0" d="M134.3,164.8v2.6c0,5.3-4.5,9.4-10.2,9.4c-5.7,0-10.2-4.1-10.2-9.4l0-2.6
										c0,5.3,4.5,9.4,10.2,9.4C129.8,174.2,134.3,170.1,134.3,164.8z"/>
								</defs>
								<clipPath id="SVGID_359_">
									<use xlink:href="#SVGID_358_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st204">
									<path class="st34" d="M134.3,164.8v2.6c0,0.5,0,0.9-0.1,1.4v-2.6C134.3,165.7,134.3,165.2,134.3,164.8"/>
									<path class="st34" d="M134.2,166.1v2.6c-0.1,0.5-0.2,1-0.4,1.5v-2.6C134,167.2,134.1,166.7,134.2,166.1"/>
									<path class="st34" d="M133.9,167.6v2.6c-0.2,0.6-0.4,1.1-0.7,1.6v-2.6C133.4,168.7,133.7,168.2,133.9,167.6"/>
									<path class="st34" d="M133.1,169.3v2.6c-1.7,3-5.1,5-9,5c-5.7,0-10.2-4.1-10.2-9.4v-2.6c0,5.3,4.5,9.4,10.2,9.4
										C128.1,174.2,131.4,172.2,133.1,169.3"/>
								</g>
							</g>
						</g>
						<g class="st0">
							<g>
								<rect x="135.3" y="156.6" class="st34" width="15.8" height="2.6"/>
							</g>
							<g>
								<rect x="135.3" y="161.5" class="st34" width="4.9" height="2.6"/>
							</g>
							<g>
								<rect x="146.3" y="161.5" class="st34" width="4.8" height="2.6"/>
							</g>
							<g>
								<rect x="147.3" y="162.5" class="st34" width="4.8" height="2.6"/>
							</g>
							<g>
								<rect x="134.3" y="162.5" class="st34" width="4.9" height="2.6"/>
							</g>
							<g>
								<rect x="140.2" y="173" class="st34" width="6.2" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M134.3,155.6h17.8v6.9h-4.8V174h-8.2v-11.5h-4.9V155.6z M151.2,161.5v-4.9h-15.8v4.9h4.9V173h6.2
									v-11.5H151.2"/>
							</g>
							<g>
								<polygon class="st34" points="151.2,156.6 151.2,161.5 146.3,161.5 146.3,173 140.2,173 140.2,161.5 135.3,161.5 
									135.3,156.6 								"/>
							</g>
							<g>
								<rect x="139.2" y="174" class="st34" width="8.2" height="2.6"/>
							</g>
						</g>
						<g class="st0">
							<g>
								<rect x="154" y="156.6" class="st34" width="11.5" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="161.4" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<rect x="165.5" y="162.4" class="st34" width="1" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="163.3" class="st34" width="4.3" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="166.5" class="st34" width="4.3" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="168.4" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<polygon class="st34" points="165.5,156.6 165.5,161.4 160.2,161.4 160.2,163.3 164.5,163.3 164.5,166.5 160.2,166.5 
									160.2,168.4 165.9,168.4 165.9,173 154,173 154,156.6 								"/>
							</g>
							<g>
								<rect x="154" y="173" class="st34" width="11.8" height="2.6"/>
							</g>
							<g>
								<rect x="153" y="174" class="st34" width="13.8" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M165.5,167.4h1.4v6.5H153v-18.4h13.5v6.8h-1V167.4z M165.9,173v-4.5h-5.7v-2h4.3v-3.1h-4.3v-1.9h5.4
									v-4.8H154V173H165.9"/>
							</g>
						</g>
						<g class="st0">
							<g class="st0">
								<defs>
									<path id="SVGID_360_" class="st0" d="M180.5,157.5l0,2.6c-1.5-0.7-3.8-1.2-5.7-1.2c-4,0-6.6,2.1-6.6,5.4l0-2.6
										c0-3.3,2.6-5.4,6.6-5.4C176.7,156.3,178.9,156.8,180.5,157.5z"/>
								</defs>
								<clipPath id="SVGID_361_">
									<use xlink:href="#SVGID_360_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st205">
									<path class="st34" d="M180.5,157.5v2.6c-1.5-0.7-3.8-1.2-5.7-1.2c-2.8,0-4.9,1-5.9,2.8v-2.6c1-1.8,3.1-2.8,5.9-2.8
										C176.7,156.3,178.9,156.8,180.5,157.5"/>
									<path class="st34" d="M168.8,159.1v2.6c-0.2,0.3-0.3,0.6-0.4,0.9V160C168.6,159.7,168.7,159.4,168.8,159.1"/>
									<path class="st34" d="M168.5,160v2.6c-0.1,0.3-0.2,0.6-0.2,0.9v-2.6C168.3,160.5,168.4,160.3,168.5,160"/>
									<path class="st34" d="M168.3,160.8v2.6c0,0.3-0.1,0.6-0.1,0.8v-2.6C168.2,161.4,168.2,161.1,168.3,160.8"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_362_" class="st0" d="M175.7,163.5c-1.3,0-1.4,0.8-1.4,1v-2.6c0-0.2,0.1-1,1.4-1c1,0,2.8,0.4,4.8,1.2v2.6
										C178.5,163.9,176.7,163.5,175.7,163.5z"/>
								</defs>
								<clipPath id="SVGID_363_">
									<use xlink:href="#SVGID_362_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st206">
									<path class="st34" d="M180.5,162.1v2.6c-1.9-0.9-3.8-1.2-4.8-1.2c-0.8,0-1.1,0.3-1.3,0.5v-2.6c0.2-0.3,0.5-0.5,1.3-0.5
										C176.7,160.8,178.5,161.2,180.5,162.1"/>
									<path class="st34" d="M174.5,161.4v2.6c0,0.1-0.1,0.1-0.1,0.2v-2.6C174.4,161.5,174.4,161.5,174.5,161.4"/>
									<path class="st34" d="M174.4,161.6v2.6c0,0.1,0,0.1,0,0.2v-2.6C174.3,161.7,174.4,161.6,174.4,161.6"/>
									<path class="st34" d="M174.3,161.7v2.6c0,0.1,0,0.1,0,0.1v-2.6C174.3,161.8,174.3,161.8,174.3,161.7"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_364_" class="st0" d="M175.2,163v2.6c-0.6-0.2-0.9-0.6-0.9-1.1l0-2.6C174.3,162.4,174.6,162.7,175.2,163z"
										/>
								</defs>
								<clipPath id="SVGID_365_">
									<use xlink:href="#SVGID_364_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st207">
									<path class="st34" d="M175.2,163v2.6c-0.6-0.2-0.9-0.6-0.9-1.1v-2.6C174.3,162.4,174.6,162.7,175.2,163"/>
								</g>
							</g>
							<g>
								<polygon class="st34" points="181.5,163.7 181.5,166.3 180.7,165.9 180.7,163.3 								"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_366_" class="st0" d="M169.6,167.5c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6c0,1.4,0.4,2.4,1.4,3.2
										C169.6,165.8,169.6,166.7,169.6,167.5c0-0.9,0-1.7,0-2.6c1.4,1.2,3.2,1.6,4.3,1.8c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										C172.8,169.1,171,168.8,169.6,167.5z"/>
								</defs>
								<clipPath id="SVGID_367_">
									<use xlink:href="#SVGID_366_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st208">
									<path class="st34" d="M174.4,166.9v2.6c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6
										c0,1.4,0.4,2.4,1.4,3.2c1.4,1.2,3.2,1.6,4.3,1.8C174.1,166.8,174.3,166.8,174.4,166.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_368_" class="st0" d="M174.4,169.5c-0.1,0-0.3-0.1-0.5-0.1c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0.2,0,0.4,0.1,0.5,0.1C174.4,167.7,174.4,168.6,174.4,169.5z"/>
								</defs>
								<clipPath id="SVGID_369_">
									<use xlink:href="#SVGID_368_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st209">
									<path class="st34" d="M174.4,166.9v2.6c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6
										c0,1.4,0.4,2.4,1.4,3.2c1.4,1.2,3.2,1.6,4.3,1.8C174.1,166.8,174.3,166.8,174.4,166.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_370_" class="st0" d="M173.7,167.7v2.6c-1.1-0.2-3.2-0.6-4.8-2c-1.2-1-1.7-2.3-1.7-4l0-2.6
										c0,1.7,0.6,3,1.7,4C170.5,167.1,172.5,167.5,173.7,167.7z"/>
								</defs>
								<clipPath id="SVGID_371_">
									<use xlink:href="#SVGID_370_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st210">
									<path class="st34" d="M173.7,167.7v2.6c-1.1-0.2-3.2-0.6-4.8-2c-1.2-1-1.7-2.3-1.7-4v-2.6c0,1.7,0.6,3,1.7,4
										C170.5,167.1,172.5,167.5,173.7,167.7"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_372_" class="st0" d="M174.2,167.8v2.6c-0.1,0-0.3-0.1-0.5-0.1v-2.6C173.9,167.8,174,167.8,174.2,167.8z"/>
								</defs>
								<clipPath id="SVGID_373_">
									<use xlink:href="#SVGID_372_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st211">
									<path class="st34" d="M174.2,167.8v2.6c-0.1,0-0.3-0.1-0.5-0.1v-2.6C173.9,167.8,174,167.8,174.2,167.8"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_374_" class="st0" d="M175.3,170.5c0-0.3-0.1-0.9-0.9-1c0-0.8,0-1.5,0-2.3v-0.3c0.8,0.2,0.9,0.7,0.9,1v0.3
										C175.3,168.9,175.3,169.8,175.3,170.5z"/>
								</defs>
								<clipPath id="SVGID_375_">
									<use xlink:href="#SVGID_374_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st212">
									<path class="st34" d="M175.3,167.9v2.6c0-0.3-0.1-0.9-0.9-1v-2.6C175.2,167,175.3,167.6,175.3,167.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_376_" class="st0" d="M181.5,168.1v2.6c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2
										V163c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6C180.9,165.6,181.5,166.7,181.5,168.1z"/>
								</defs>
								<clipPath id="SVGID_377_">
									<use xlink:href="#SVGID_376_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st213">
									<path class="st34" d="M181.5,168.1v2.6c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2V163
										c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6C180.9,165.6,181.5,166.7,181.5,168.1"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_378_" class="st0" d="M175.3,167.9l0,2.6c0,0.3-0.1,1.1-1.4,1.1c-1.2,0-3.2-0.5-5.2-1.7l0-2.6
										c2,1.1,4.1,1.7,5.2,1.7C175.2,169,175.3,168.1,175.3,167.9z"/>
								</defs>
								<clipPath id="SVGID_379_">
									<use xlink:href="#SVGID_378_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st214">
									<path class="st34" d="M175.3,167.9v2.6c0,0,0,0.1,0,0.2V168C175.3,168,175.3,167.9,175.3,167.9"/>
									<path class="st34" d="M175.3,168v2.6c0,0.1,0,0.1,0,0.2v-2.6C175.3,168.2,175.3,168.1,175.3,168"/>
									<path class="st34" d="M175.2,168.2v2.6c0,0.1,0,0.1-0.1,0.2v-2.6C175.2,168.3,175.2,168.3,175.2,168.2"/>
									<path class="st34" d="M175.2,168.4v2.6c-0.2,0.3-0.5,0.6-1.3,0.6c-1.2,0-3.2-0.5-5.2-1.7v-2.6c2,1.1,4.1,1.7,5.2,1.7
										C174.7,169,175,168.7,175.2,168.4"/>
								</g>
							</g>
							<g>
								<polygon class="st34" points="168,172.7 168,175.4 167.7,175.2 167.7,172.6 								"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_380_" class="st0" d="M181.5,170.7c0,3.2-2.6,5.2-6.7,5.2c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c4.2,0,6.7-2,6.7-5.2v0.3C181.5,169.1,181.5,170,181.5,170.7z"/>
								</defs>
								<clipPath id="SVGID_381_">
									<use xlink:href="#SVGID_380_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st215">
									<path class="st34" d="M181.5,168.1v2.6c0,0.3,0,0.5-0.1,0.8v-2.6C181.4,168.6,181.5,168.3,181.5,168.1"/>
									<path class="st34" d="M181.4,168.9v2.6c0,0.3-0.1,0.5-0.2,0.8v-2.6C181.3,169.4,181.4,169.1,181.4,168.9"/>
									<path class="st34" d="M181.2,169.7v2.6c-0.1,0.3-0.2,0.5-0.4,0.8v-2.6C181,170.2,181.1,169.9,181.2,169.7"/>
									<path class="st34" d="M180.9,170.5v2.6c-1,1.7-3.2,2.8-6.1,2.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2
										C177.7,173.2,179.9,172.2,180.9,170.5"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_382_" class="st0" d="M174.7,175.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2c0,0,0,0,0,0
										c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										c0,0.1,0,0.2,0,0.3C174.7,175.6,174.7,175.7,174.7,175.8C174.7,175.8,174.7,175.8,174.7,175.8z"/>
								</defs>
								<clipPath id="SVGID_383_">
									<use xlink:href="#SVGID_382_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st216">
									<path class="st34" d="M180.9,170.5v2.6c-1,1.7-3.2,2.8-6.1,2.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2
										C177.7,173.2,179.9,172.2,180.9,170.5"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M174.8,156.3c1.9,0,4.2,0.5,5.7,1.2v4.6c-1.9-0.9-3.8-1.2-4.8-1.2c-1.3,0-1.4,0.8-1.4,1
									c0,0.5,0.3,0.9,0.9,1.1c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6c1.1,0.9,1.6,2,1.6,3.4c0,3.2-2.6,5.2-6.8,5.2
									c-2.1,0-4.4-0.5-6-1.2v-4.7c2,1.1,4.1,1.7,5.2,1.7c1.3,0,1.4-0.8,1.4-1.1c0-0.3-0.1-0.9-0.9-1c-0.1,0-0.3-0.1-0.5-0.1
									c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2C168.2,158.4,170.8,156.3,174.8,156.3z"/>
							</g>
							<g>
								<path class="st34" d="M180.5,163.9c1.3,1.1,1.9,2.4,1.9,4.1c0,3.7-3,6.2-7.7,6.2c-2.4,0-5-0.6-6.7-1.5l-0.3-0.1v-7.1
									l0.8,0.5c2.2,1.5,4.5,2,5.4,2c0.3,0,0.4-0.1,0.4-0.1c0,0,0,0-0.2-0.1c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-3.2-0.6-4.8-2
									c-1.2-1-1.7-2.3-1.7-4c0-3.9,3-6.4,7.6-6.4c2.2,0,4.8,0.6,6.4,1.4l0.3,0.1v6.8l-0.7-0.4c-2.2-1.1-4.2-1.5-5-1.5
									c-0.3,0-0.4,0.1-0.4,0.1c0,0,0.1,0,0.2,0.1c0.1,0,0.3,0.1,0.5,0.1C177.1,162.4,179.1,162.8,180.5,163.9z M174.7,173.2
									c4.2,0,6.8-2,6.8-5.2c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2c-0.6-0.2-0.9-0.6-0.9-1.1
									c0-0.2,0.1-1,1.4-1c1,0,2.8,0.4,4.8,1.2v-4.6c-1.5-0.7-3.8-1.2-5.7-1.2c-4,0-6.6,2.1-6.6,5.4c0,1.4,0.4,2.4,1.4,3.2
									c1.4,1.2,3.2,1.6,4.3,1.8c0.2,0,0.4,0.1,0.5,0.1c0.8,0.2,0.9,0.7,0.9,1c0,0.3-0.1,1.1-1.4,1.1c-1.2,0-3.2-0.5-5.2-1.7v4.7
									C170.3,172.7,172.6,173.2,174.7,173.2"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_384_" class="st0" d="M182.5,168.1l0,2.6c0,3.7-3,6.2-7.7,6.2c-2.4,0-5-0.6-6.7-1.5v-2.6
										c1.7,0.9,4.4,1.5,6.7,1.5C179.4,174.2,182.5,171.8,182.5,168.1z"/>
								</defs>
								<clipPath id="SVGID_385_">
									<use xlink:href="#SVGID_384_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st217">
									<path class="st34" d="M182.5,168.1v2.6c0,0.3,0,0.6-0.1,1V169C182.4,168.7,182.5,168.4,182.5,168.1"/>
									<path class="st34" d="M182.4,169v2.6c-0.1,0.3-0.1,0.7-0.2,1V170C182.3,169.7,182.3,169.3,182.4,169"/>
									<path class="st34" d="M182.2,170v2.6c-0.1,0.3-0.3,0.7-0.4,1v-2.6C181.9,170.6,182,170.3,182.2,170"/>
									<path class="st34" d="M181.7,170.9v2.6c-1.2,2-3.7,3.3-7,3.3c-2.4,0-5-0.6-6.7-1.5v-2.6c1.7,0.9,4.4,1.5,6.7,1.5
										C178.1,174.2,180.6,173,181.7,170.9"/>
								</g>
							</g>
						</g>
					</g>
				</g>
				<defs>
					<filter id="Adobe_OpacityMaskFilter_9_" filterUnits="userSpaceOnUse" x="94.1" y="155.3" width="88.4" height="21.5">
						<feFlood  style="flood-color:white;flood-opacity:1" result="back"/>
						<feBlend  in="SourceGraphic" in2="back" mode="normal"/>
					</filter>
				</defs>
				<mask maskUnits="userSpaceOnUse" x="94.1" y="155.3" width="88.4" height="21.5" id="SVGID_386_">
					<g class="st218">
						
							<image style="overflow:visible;" width="93" height="27" xlink:href="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgEASABIAAD/7AARRHVja3kAAQAEAAAAHgAA/+4AIUFkb2JlAGTAAAAAAQMA
EAMCAwYAAAF0AAABfwAAAaD/2wCEABALCwsMCxAMDBAXDw0PFxsUEBAUGx8XFxcXFx8eFxoaGhoX
Hh4jJSclIx4vLzMzLy9AQEBAQEBAQEBAQEBAQEABEQ8PERMRFRISFRQRFBEUGhQWFhQaJhoaHBoa
JjAjHh4eHiMwKy4nJycuKzU1MDA1NUBAP0BAQEBAQEBAQEBAQP/CABEIABsAXQMBIgACEQEDEQH/
xABfAAEBAQAAAAAAAAAAAAAAAAAAAQcBAQAAAAAAAAAAAAAAAAAAAAAQAQEAAAAAAAAAAAAAAAAA
ADBQEQEAAAAAAAAAAAAAAAAAAABQEgEAAAAAAAAAAAAAAAAAAAAw/9oADAMBAAIRAxEAAADPykWA
AABYAAAAAAf/2gAIAQIAAQUAT//aAAgBAwABBQBP/9oACAEBAAEFAKH/2gAIAQICBj8AT//aAAgB
AwIGPwBP/9oACAEBAQY/AEP/2Q==" transform="matrix(1 0 0 1 91.75 152.5)">
						</image>
					</g>
				</mask>
				<g class="st219">
					<g>
						<g class="st0">
							<g>
								<rect x="95.1" y="156.6" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<rect x="106.8" y="156.6" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<polygon class="st34" points="105.8,163.8 105.8,166.4 100.8,159.2 100.8,156.6 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.8,163.8 106.8,166.4 106.1,166.7 106.1,164.1 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.1,164.1 106.1,166.7 105.8,166.4 105.8,163.8 								"/>
							</g>
							<g>
								<polygon class="st34" points="101.2,165.4 101.2,168 100.5,168.4 100.5,165.8 								"/>
							</g>
							<g>
								<polygon class="st34" points="101.5,165.8 101.5,168.4 101.2,168 101.2,165.4 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.5,173 106.5,175.6 101.5,168.4 101.5,165.8 								"/>
							</g>
							<g>
								<rect x="106.5" y="173" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<rect x="95.1" y="173" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M105.8,155.6h7.4V174h-7.3l-4.4-6.4v6.4h-7.4v-18.4h7.3l4.4,6.4V155.6z M112.2,173v-16.4h-5.4v7.2
									l-0.7,0.3l-0.3-0.3l-5-7.2h-5.7V173h5.4v-7.2l0.7-0.3l0.3,0.3l5,7.2H112.2"/>
							</g>
							<g>
								<polygon class="st34" points="112.2,156.6 112.2,173 106.5,173 101.5,165.8 101.2,165.4 100.5,165.8 100.5,173 95.1,173 
									95.1,156.6 100.8,156.6 105.8,163.8 106.1,164.1 106.8,163.8 106.8,156.6 								"/>
							</g>
							<g>
								<polygon class="st34" points="105.9,174 105.9,176.6 101.5,170.2 101.5,167.6 								"/>
							</g>
							<g>
								<rect x="105.9" y="174" class="st34" width="7.3" height="2.6"/>
							</g>
							<g>
								<rect x="94.1" y="174" class="st34" width="7.4" height="2.6"/>
							</g>
						</g>
						<g class="st0">
							<g class="st0">
								<defs>
									<path id="SVGID_387_" class="st0" d="M133.3,164.8l0,2.6c0-4.7-4-8.4-9.2-8.4c-5.2,0-9.2,3.7-9.2,8.4l0-2.6
										c0-4.7,4-8.4,9.2-8.4C129.3,156.3,133.3,160,133.3,164.8z"/>
								</defs>
								<clipPath id="SVGID_388_">
									<use xlink:href="#SVGID_387_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st220">
									<path class="st34" d="M133.3,164.8v2.6c0-4.7-4-8.4-9.2-8.4c-3.6,0-6.6,1.8-8.1,4.5v-2.6c1.5-2.7,4.6-4.5,8.1-4.5
										C129.3,156.3,133.3,160,133.3,164.8"/>
									<path class="st34" d="M116,160.8v2.6c-0.3,0.5-0.5,0.9-0.6,1.4v-2.6C115.5,161.7,115.7,161.3,116,160.8"/>
									<path class="st34" d="M115.3,162.2v2.6c-0.1,0.4-0.3,0.9-0.3,1.3v-2.6C115.1,163.1,115.2,162.7,115.3,162.2"/>
									<path class="st34" d="M115,163.6v2.6c-0.1,0.4-0.1,0.8-0.1,1.2v-2.6C114.9,164.4,115,164,115,163.6"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_389_" class="st0" d="M120.9,167.5c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3v-0.3c0-1.8,1.4-3.2,3.2-3.2
										c1.8,0,3.2,1.4,3.2,3.2l0,2.6c0-1.8-1.4-3.2-3.2-3.2C122.4,164.3,120.9,165.8,120.9,167.5z"/>
								</defs>
								<clipPath id="SVGID_390_">
									<use xlink:href="#SVGID_389_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st221">
									<path class="st34" d="M127.3,164.9v2.6c0-1.8-1.4-3.2-3.2-3.2c-1.2,0-2.2,0.7-2.8,1.6v-2.6c0.5-1,1.6-1.6,2.8-1.6
										C125.9,161.7,127.3,163.1,127.3,164.9"/>
									<path class="st34" d="M121.3,163.3v2.6c-0.1,0.2-0.2,0.4-0.3,0.6v-2.6C121.2,163.7,121.2,163.5,121.3,163.3"/>
									<path class="st34" d="M121.1,163.9v2.6c-0.1,0.2-0.1,0.3-0.1,0.5v-2.6C121,164.2,121,164.1,121.1,163.9"/>
									<path class="st34" d="M121,164.4v2.6c0,0.2,0,0.3,0,0.5v-2.6C120.9,164.7,120.9,164.6,121,164.4"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_391_" class="st0" d="M126.3,164.9l0,2.6c0-1.2-1-2.2-2.2-2.2c-1.2,0-2.2,1-2.2,2.2l0-2.6
										c0-1.2,1-2.2,2.2-2.2C125.3,162.7,126.3,163.7,126.3,164.9z"/>
								</defs>
								<clipPath id="SVGID_392_">
									<use xlink:href="#SVGID_391_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st222">
									<path class="st34" d="M126.3,164.9v2.6c0-1.2-1-2.2-2.2-2.2c-0.8,0-1.5,0.4-1.9,1.1v-2.6c0.4-0.7,1.1-1.1,1.9-1.1
										C125.3,162.7,126.3,163.7,126.3,164.9"/>
									<path class="st34" d="M122.2,163.8v2.6c-0.1,0.1-0.1,0.3-0.2,0.4v-2.6C122.1,164.1,122.1,163.9,122.2,163.8"/>
									<path class="st34" d="M122,164.2v2.6c0,0.1-0.1,0.2-0.1,0.4v-2.6C122,164.4,122,164.3,122,164.2"/>
									<path class="st34" d="M121.9,164.6v2.6c0,0.1,0,0.2,0,0.3v-2.6C121.9,164.8,121.9,164.7,121.9,164.6"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_393_" class="st0" d="M127.3,164.9l0,2.6c0,1.8-1.4,3.2-3.2,3.2c-1.8,0-3.2-1.4-3.2-3.2v-2.6
										c0,1.8,1.4,3.2,3.2,3.2C125.9,168.1,127.3,166.6,127.3,164.9z"/>
								</defs>
								<clipPath id="SVGID_394_">
									<use xlink:href="#SVGID_393_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st223">
									<path class="st34" d="M127.3,164.9v2.6c0,0.2,0,0.3,0,0.5v-2.6C127.3,165.2,127.3,165.1,127.3,164.9"/>
									<path class="st34" d="M127.3,165.4v2.6c0,0.2-0.1,0.4-0.1,0.5v-2.6C127.2,165.7,127.2,165.5,127.3,165.4"/>
									<path class="st34" d="M127.1,165.9v2.6c-0.1,0.2-0.2,0.4-0.3,0.6v-2.6C127,166.3,127.1,166.1,127.1,165.9"/>
									<path class="st34" d="M126.9,166.5v2.6c-0.5,1-1.6,1.6-2.8,1.6c-1.8,0-3.2-1.4-3.2-3.2v-2.6c0,1.8,1.4,3.2,3.2,3.2
										C125.3,168.1,126.3,167.4,126.9,166.5"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M124.1,161.7c1.8,0,3.2,1.4,3.2,3.2c0,1.8-1.4,3.2-3.2,3.2c-1.8,0-3.2-1.4-3.2-3.2
									C120.9,163.1,122.4,161.7,124.1,161.7z M124.1,167.1c1.2,0,2.2-1,2.2-2.2c0-1.2-1-2.2-2.2-2.2c-1.2,0-2.2,1-2.2,2.2
									C121.9,166.1,122.9,167.1,124.1,167.1"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_395_" class="st0" d="M124.1,175.8c-5.2,0-9.2-3.7-9.2-8.4l0-2.6c0,4.7,4,8.4,9.2,8.4
										c5.2,0,9.2-3.7,9.2-8.4l0,0.3v2.3C133.3,172.1,129.3,175.8,124.1,175.8z"/>
								</defs>
								<clipPath id="SVGID_396_">
									<use xlink:href="#SVGID_395_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st224">
									<path class="st34" d="M133.3,164.8v2.6c0,0.4,0,0.8-0.1,1.2V166C133.3,165.6,133.3,165.2,133.3,164.8"/>
									<path class="st34" d="M133.2,166v2.6c-0.1,0.5-0.2,0.9-0.3,1.3v-2.6C133,166.9,133.2,166.4,133.2,166"/>
									<path class="st34" d="M132.9,167.3v2.6c-0.2,0.5-0.4,1-0.6,1.4v-2.6C132.5,168.3,132.7,167.8,132.9,167.3"/>
									<path class="st34" d="M132.3,168.8v2.6c-1.5,2.7-4.6,4.5-8.1,4.5c-5.2,0-9.2-3.7-9.2-8.4v-2.6c0,4.7,4,8.4,9.2,8.4
										C127.7,173.2,130.7,171.4,132.3,168.8"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M124.1,155.3c5.7,0,10.2,4.2,10.2,9.4c0,5.3-4.5,9.4-10.2,9.4c-5.7,0-10.2-4.1-10.2-9.4
									C113.9,159.5,118.4,155.3,124.1,155.3z M124.1,173.2c5.2,0,9.2-3.7,9.2-8.4c0-4.7-4-8.4-9.2-8.4c-5.2,0-9.2,3.7-9.2,8.4
									C114.9,169.5,119,173.2,124.1,173.2"/>
							</g>
							<g>
								<path class="st34" d="M124.1,156.3c5.2,0,9.2,3.7,9.2,8.4c0,4.7-4,8.4-9.2,8.4c-5.2,0-9.2-3.7-9.2-8.4
									C114.9,160,119,156.3,124.1,156.3z M124.1,168.1c1.8,0,3.2-1.4,3.2-3.2c0-1.8-1.4-3.2-3.2-3.2c-1.8,0-3.2,1.4-3.2,3.2
									C120.9,166.6,122.4,168.1,124.1,168.1"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_397_" class="st0" d="M134.3,164.8v2.6c0,5.3-4.5,9.4-10.2,9.4c-5.7,0-10.2-4.1-10.2-9.4l0-2.6
										c0,5.3,4.5,9.4,10.2,9.4C129.8,174.2,134.3,170.1,134.3,164.8z"/>
								</defs>
								<clipPath id="SVGID_398_">
									<use xlink:href="#SVGID_397_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st225">
									<path class="st34" d="M134.3,164.8v2.6c0,0.5,0,0.9-0.1,1.4v-2.6C134.3,165.7,134.3,165.2,134.3,164.8"/>
									<path class="st34" d="M134.2,166.1v2.6c-0.1,0.5-0.2,1-0.4,1.5v-2.6C134,167.2,134.1,166.7,134.2,166.1"/>
									<path class="st34" d="M133.9,167.6v2.6c-0.2,0.6-0.4,1.1-0.7,1.6v-2.6C133.4,168.7,133.7,168.2,133.9,167.6"/>
									<path class="st34" d="M133.1,169.3v2.6c-1.7,3-5.1,5-9,5c-5.7,0-10.2-4.1-10.2-9.4v-2.6c0,5.3,4.5,9.4,10.2,9.4
										C128.1,174.2,131.4,172.2,133.1,169.3"/>
								</g>
							</g>
						</g>
						<g class="st0">
							<g>
								<rect x="135.3" y="156.6" class="st34" width="15.8" height="2.6"/>
							</g>
							<g>
								<rect x="135.3" y="161.5" class="st34" width="4.9" height="2.6"/>
							</g>
							<g>
								<rect x="146.3" y="161.5" class="st34" width="4.8" height="2.6"/>
							</g>
							<g>
								<rect x="147.3" y="162.5" class="st34" width="4.8" height="2.6"/>
							</g>
							<g>
								<rect x="134.3" y="162.5" class="st34" width="4.9" height="2.6"/>
							</g>
							<g>
								<rect x="140.2" y="173" class="st34" width="6.2" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M134.3,155.6h17.8v6.9h-4.8V174h-8.2v-11.5h-4.9V155.6z M151.2,161.5v-4.9h-15.8v4.9h4.9V173h6.2
									v-11.5H151.2"/>
							</g>
							<g>
								<polygon class="st34" points="151.2,156.6 151.2,161.5 146.3,161.5 146.3,173 140.2,173 140.2,161.5 135.3,161.5 
									135.3,156.6 								"/>
							</g>
							<g>
								<rect x="139.2" y="174" class="st34" width="8.2" height="2.6"/>
							</g>
						</g>
						<g class="st0">
							<g>
								<rect x="154" y="156.6" class="st34" width="11.5" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="161.4" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<rect x="165.5" y="162.4" class="st34" width="1" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="163.3" class="st34" width="4.3" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="166.5" class="st34" width="4.3" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="168.4" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<polygon class="st34" points="165.5,156.6 165.5,161.4 160.2,161.4 160.2,163.3 164.5,163.3 164.5,166.5 160.2,166.5 
									160.2,168.4 165.9,168.4 165.9,173 154,173 154,156.6 								"/>
							</g>
							<g>
								<rect x="154" y="173" class="st34" width="11.8" height="2.6"/>
							</g>
							<g>
								<rect x="153" y="174" class="st34" width="13.8" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M165.5,167.4h1.4v6.5H153v-18.4h13.5v6.8h-1V167.4z M165.9,173v-4.5h-5.7v-2h4.3v-3.1h-4.3v-1.9h5.4
									v-4.8H154V173H165.9"/>
							</g>
						</g>
						<g class="st0">
							<g class="st0">
								<defs>
									<path id="SVGID_399_" class="st0" d="M180.5,157.5l0,2.6c-1.5-0.7-3.8-1.2-5.7-1.2c-4,0-6.6,2.1-6.6,5.4l0-2.6
										c0-3.3,2.6-5.4,6.6-5.4C176.7,156.3,178.9,156.8,180.5,157.5z"/>
								</defs>
								<clipPath id="SVGID_400_">
									<use xlink:href="#SVGID_399_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st226">
									<path class="st34" d="M180.5,157.5v2.6c-1.5-0.7-3.8-1.2-5.7-1.2c-2.8,0-4.9,1-5.9,2.8v-2.6c1-1.8,3.1-2.8,5.9-2.8
										C176.7,156.3,178.9,156.8,180.5,157.5"/>
									<path class="st34" d="M168.8,159.1v2.6c-0.2,0.3-0.3,0.6-0.4,0.9V160C168.6,159.7,168.7,159.4,168.8,159.1"/>
									<path class="st34" d="M168.5,160v2.6c-0.1,0.3-0.2,0.6-0.2,0.9v-2.6C168.3,160.5,168.4,160.3,168.5,160"/>
									<path class="st34" d="M168.3,160.8v2.6c0,0.3-0.1,0.6-0.1,0.8v-2.6C168.2,161.4,168.2,161.1,168.3,160.8"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_401_" class="st0" d="M175.7,163.5c-1.3,0-1.4,0.8-1.4,1v-2.6c0-0.2,0.1-1,1.4-1c1,0,2.8,0.4,4.8,1.2v2.6
										C178.5,163.9,176.7,163.5,175.7,163.5z"/>
								</defs>
								<clipPath id="SVGID_402_">
									<use xlink:href="#SVGID_401_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st227">
									<path class="st34" d="M180.5,162.1v2.6c-1.9-0.9-3.8-1.2-4.8-1.2c-0.8,0-1.1,0.3-1.3,0.5v-2.6c0.2-0.3,0.5-0.5,1.3-0.5
										C176.7,160.8,178.5,161.2,180.5,162.1"/>
									<path class="st34" d="M174.5,161.4v2.6c0,0.1-0.1,0.1-0.1,0.2v-2.6C174.4,161.5,174.4,161.5,174.5,161.4"/>
									<path class="st34" d="M174.4,161.6v2.6c0,0.1,0,0.1,0,0.2v-2.6C174.3,161.7,174.4,161.6,174.4,161.6"/>
									<path class="st34" d="M174.3,161.7v2.6c0,0.1,0,0.1,0,0.1v-2.6C174.3,161.8,174.3,161.8,174.3,161.7"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_403_" class="st0" d="M175.2,163v2.6c-0.6-0.2-0.9-0.6-0.9-1.1l0-2.6C174.3,162.4,174.6,162.7,175.2,163z"
										/>
								</defs>
								<clipPath id="SVGID_404_">
									<use xlink:href="#SVGID_403_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st228">
									<path class="st34" d="M175.2,163v2.6c-0.6-0.2-0.9-0.6-0.9-1.1v-2.6C174.3,162.4,174.6,162.7,175.2,163"/>
								</g>
							</g>
							<g>
								<polygon class="st34" points="181.5,163.7 181.5,166.3 180.7,165.9 180.7,163.3 								"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_405_" class="st0" d="M169.6,167.5c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6c0,1.4,0.4,2.4,1.4,3.2
										C169.6,165.8,169.6,166.7,169.6,167.5c0-0.9,0-1.7,0-2.6c1.4,1.2,3.2,1.6,4.3,1.8c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										C172.8,169.1,171,168.8,169.6,167.5z"/>
								</defs>
								<clipPath id="SVGID_406_">
									<use xlink:href="#SVGID_405_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st229">
									<path class="st34" d="M174.4,166.9v2.6c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6
										c0,1.4,0.4,2.4,1.4,3.2c1.4,1.2,3.2,1.6,4.3,1.8C174.1,166.8,174.3,166.8,174.4,166.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_407_" class="st0" d="M174.4,169.5c-0.1,0-0.3-0.1-0.5-0.1c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0.2,0,0.4,0.1,0.5,0.1C174.4,167.7,174.4,168.6,174.4,169.5z"/>
								</defs>
								<clipPath id="SVGID_408_">
									<use xlink:href="#SVGID_407_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st230">
									<path class="st34" d="M174.4,166.9v2.6c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6
										c0,1.4,0.4,2.4,1.4,3.2c1.4,1.2,3.2,1.6,4.3,1.8C174.1,166.8,174.3,166.8,174.4,166.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_409_" class="st0" d="M173.7,167.7v2.6c-1.1-0.2-3.2-0.6-4.8-2c-1.2-1-1.7-2.3-1.7-4l0-2.6
										c0,1.7,0.6,3,1.7,4C170.5,167.1,172.5,167.5,173.7,167.7z"/>
								</defs>
								<clipPath id="SVGID_410_">
									<use xlink:href="#SVGID_409_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st231">
									<path class="st34" d="M173.7,167.7v2.6c-1.1-0.2-3.2-0.6-4.8-2c-1.2-1-1.7-2.3-1.7-4v-2.6c0,1.7,0.6,3,1.7,4
										C170.5,167.1,172.5,167.5,173.7,167.7"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_411_" class="st0" d="M174.2,167.8v2.6c-0.1,0-0.3-0.1-0.5-0.1v-2.6C173.9,167.8,174,167.8,174.2,167.8z"/>
								</defs>
								<clipPath id="SVGID_412_">
									<use xlink:href="#SVGID_411_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st232">
									<path class="st34" d="M174.2,167.8v2.6c-0.1,0-0.3-0.1-0.5-0.1v-2.6C173.9,167.8,174,167.8,174.2,167.8"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_413_" class="st0" d="M175.3,170.5c0-0.3-0.1-0.9-0.9-1c0-0.8,0-1.5,0-2.3v-0.3c0.8,0.2,0.9,0.7,0.9,1v0.3
										C175.3,168.9,175.3,169.8,175.3,170.5z"/>
								</defs>
								<clipPath id="SVGID_414_">
									<use xlink:href="#SVGID_413_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st233">
									<path class="st34" d="M175.3,167.9v2.6c0-0.3-0.1-0.9-0.9-1v-2.6C175.2,167,175.3,167.6,175.3,167.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_415_" class="st0" d="M181.5,168.1v2.6c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2
										V163c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6C180.9,165.6,181.5,166.7,181.5,168.1z"/>
								</defs>
								<clipPath id="SVGID_416_">
									<use xlink:href="#SVGID_415_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st234">
									<path class="st34" d="M181.5,168.1v2.6c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2V163
										c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6C180.9,165.6,181.5,166.7,181.5,168.1"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_417_" class="st0" d="M175.3,167.9l0,2.6c0,0.3-0.1,1.1-1.4,1.1c-1.2,0-3.2-0.5-5.2-1.7l0-2.6
										c2,1.1,4.1,1.7,5.2,1.7C175.2,169,175.3,168.1,175.3,167.9z"/>
								</defs>
								<clipPath id="SVGID_418_">
									<use xlink:href="#SVGID_417_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st235">
									<path class="st34" d="M175.3,167.9v2.6c0,0,0,0.1,0,0.2V168C175.3,168,175.3,167.9,175.3,167.9"/>
									<path class="st34" d="M175.3,168v2.6c0,0.1,0,0.1,0,0.2v-2.6C175.3,168.2,175.3,168.1,175.3,168"/>
									<path class="st34" d="M175.2,168.2v2.6c0,0.1,0,0.1-0.1,0.2v-2.6C175.2,168.3,175.2,168.3,175.2,168.2"/>
									<path class="st34" d="M175.2,168.4v2.6c-0.2,0.3-0.5,0.6-1.3,0.6c-1.2,0-3.2-0.5-5.2-1.7v-2.6c2,1.1,4.1,1.7,5.2,1.7
										C174.7,169,175,168.7,175.2,168.4"/>
								</g>
							</g>
							<g>
								<polygon class="st34" points="168,172.7 168,175.4 167.7,175.2 167.7,172.6 								"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_419_" class="st0" d="M181.5,170.7c0,3.2-2.6,5.2-6.7,5.2c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c4.2,0,6.7-2,6.7-5.2v0.3C181.5,169.1,181.5,170,181.5,170.7z"/>
								</defs>
								<clipPath id="SVGID_420_">
									<use xlink:href="#SVGID_419_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st236">
									<path class="st34" d="M181.5,168.1v2.6c0,0.3,0,0.5-0.1,0.8v-2.6C181.4,168.6,181.5,168.3,181.5,168.1"/>
									<path class="st34" d="M181.4,168.9v2.6c0,0.3-0.1,0.5-0.2,0.8v-2.6C181.3,169.4,181.4,169.1,181.4,168.9"/>
									<path class="st34" d="M181.2,169.7v2.6c-0.1,0.3-0.2,0.5-0.4,0.8v-2.6C181,170.2,181.1,169.9,181.2,169.7"/>
									<path class="st34" d="M180.9,170.5v2.6c-1,1.7-3.2,2.8-6.1,2.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2
										C177.7,173.2,179.9,172.2,180.9,170.5"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_421_" class="st0" d="M174.7,175.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2c0,0,0,0,0,0
										c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										c0,0.1,0,0.2,0,0.3C174.7,175.6,174.7,175.7,174.7,175.8C174.7,175.8,174.7,175.8,174.7,175.8z"/>
								</defs>
								<clipPath id="SVGID_422_">
									<use xlink:href="#SVGID_421_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st237">
									<path class="st34" d="M180.9,170.5v2.6c-1,1.7-3.2,2.8-6.1,2.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2
										C177.7,173.2,179.9,172.2,180.9,170.5"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M174.8,156.3c1.9,0,4.2,0.5,5.7,1.2v4.6c-1.9-0.9-3.8-1.2-4.8-1.2c-1.3,0-1.4,0.8-1.4,1
									c0,0.5,0.3,0.9,0.9,1.1c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6c1.1,0.9,1.6,2,1.6,3.4c0,3.2-2.6,5.2-6.8,5.2
									c-2.1,0-4.4-0.5-6-1.2v-4.7c2,1.1,4.1,1.7,5.2,1.7c1.3,0,1.4-0.8,1.4-1.1c0-0.3-0.1-0.9-0.9-1c-0.1,0-0.3-0.1-0.5-0.1
									c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2C168.2,158.4,170.8,156.3,174.8,156.3z"/>
							</g>
							<g>
								<path class="st34" d="M180.5,163.9c1.3,1.1,1.9,2.4,1.9,4.1c0,3.7-3,6.2-7.7,6.2c-2.4,0-5-0.6-6.7-1.5l-0.3-0.1v-7.1
									l0.8,0.5c2.2,1.5,4.5,2,5.4,2c0.3,0,0.4-0.1,0.4-0.1c0,0,0,0-0.2-0.1c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-3.2-0.6-4.8-2
									c-1.2-1-1.7-2.3-1.7-4c0-3.9,3-6.4,7.6-6.4c2.2,0,4.8,0.6,6.4,1.4l0.3,0.1v6.8l-0.7-0.4c-2.2-1.1-4.2-1.5-5-1.5
									c-0.3,0-0.4,0.1-0.4,0.1c0,0,0.1,0,0.2,0.1c0.1,0,0.3,0.1,0.5,0.1C177.1,162.4,179.1,162.8,180.5,163.9z M174.7,173.2
									c4.2,0,6.8-2,6.8-5.2c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2c-0.6-0.2-0.9-0.6-0.9-1.1
									c0-0.2,0.1-1,1.4-1c1,0,2.8,0.4,4.8,1.2v-4.6c-1.5-0.7-3.8-1.2-5.7-1.2c-4,0-6.6,2.1-6.6,5.4c0,1.4,0.4,2.4,1.4,3.2
									c1.4,1.2,3.2,1.6,4.3,1.8c0.2,0,0.4,0.1,0.5,0.1c0.8,0.2,0.9,0.7,0.9,1c0,0.3-0.1,1.1-1.4,1.1c-1.2,0-3.2-0.5-5.2-1.7v4.7
									C170.3,172.7,172.6,173.2,174.7,173.2"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_423_" class="st0" d="M182.5,168.1l0,2.6c0,3.7-3,6.2-7.7,6.2c-2.4,0-5-0.6-6.7-1.5v-2.6
										c1.7,0.9,4.4,1.5,6.7,1.5C179.4,174.2,182.5,171.8,182.5,168.1z"/>
								</defs>
								<clipPath id="SVGID_424_">
									<use xlink:href="#SVGID_423_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st238">
									<path class="st34" d="M182.5,168.1v2.6c0,0.3,0,0.6-0.1,1V169C182.4,168.7,182.5,168.4,182.5,168.1"/>
									<path class="st34" d="M182.4,169v2.6c-0.1,0.3-0.1,0.7-0.2,1V170C182.3,169.7,182.3,169.3,182.4,169"/>
									<path class="st34" d="M182.2,170v2.6c-0.1,0.3-0.3,0.7-0.4,1v-2.6C181.9,170.6,182,170.3,182.2,170"/>
									<path class="st34" d="M181.7,170.9v2.6c-1.2,2-3.7,3.3-7,3.3c-2.4,0-5-0.6-6.7-1.5v-2.6c1.7,0.9,4.4,1.5,6.7,1.5
										C178.1,174.2,180.6,173,181.7,170.9"/>
								</g>
							</g>
						</g>
					</g>
					<defs>
						<filter id="Adobe_OpacityMaskFilter_10_" filterUnits="userSpaceOnUse" x="94.1" y="155.3" width="88.4" height="21.5">
							<feFlood  style="flood-color:white;flood-opacity:1" result="back"/>
							<feBlend  in="SourceGraphic" in2="back" mode="normal"/>
						</filter>
					</defs>
					<mask maskUnits="userSpaceOnUse" x="94.1" y="155.3" width="88.4" height="21.5" id="SVGID_386_">
						<g class="st239">
							
								<image style="overflow:visible;" width="93" height="27" xlink:href="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgEASABIAAD/7AARRHVja3kAAQAEAAAAHgAA/+4AIUFkb2JlAGTAAAAAAQMA
EAMCAwYAAAF0AAABfwAAAaD/2wCEABALCwsMCxAMDBAXDw0PFxsUEBAUGx8XFxcXFx8eFxoaGhoX
Hh4jJSclIx4vLzMzLy9AQEBAQEBAQEBAQEBAQEABEQ8PERMRFRISFRQRFBEUGhQWFhQaJhoaHBoa
JjAjHh4eHiMwKy4nJycuKzU1MDA1NUBAP0BAQEBAQEBAQEBAQP/CABEIABsAXQMBIgACEQEDEQH/
xABfAAEBAQAAAAAAAAAAAAAAAAAAAQcBAQAAAAAAAAAAAAAAAAAAAAAQAQEAAAAAAAAAAAAAAAAA
ADBQEQEAAAAAAAAAAAAAAAAAAABQEgEAAAAAAAAAAAAAAAAAAAAw/9oADAMBAAIRAxEAAADPykWA
AABYAAAAAAf/2gAIAQIAAQUAT//aAAgBAwABBQBP/9oACAEBAAEFAKH/2gAIAQICBj8AT//aAAgB
AwIGPwBP/9oACAEBAQY/AEP/2Q==" transform="matrix(1 0 0 1 91.75 152.5)">
							</image>
						</g>
					</mask>
					<g>
						<g class="st0">
							<g>
								<rect x="95.1" y="156.6" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<rect x="106.8" y="156.6" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<polygon class="st34" points="105.8,163.8 105.8,166.4 100.8,159.2 100.8,156.6 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.8,163.8 106.8,166.4 106.1,166.7 106.1,164.1 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.1,164.1 106.1,166.7 105.8,166.4 105.8,163.8 								"/>
							</g>
							<g>
								<polygon class="st34" points="101.2,165.4 101.2,168 100.5,168.4 100.5,165.8 								"/>
							</g>
							<g>
								<polygon class="st34" points="101.5,165.8 101.5,168.4 101.2,168 101.2,165.4 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.5,173 106.5,175.6 101.5,168.4 101.5,165.8 								"/>
							</g>
							<g>
								<rect x="106.5" y="173" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<rect x="95.1" y="173" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M105.8,155.6h7.4V174h-7.3l-4.4-6.4v6.4h-7.4v-18.4h7.3l4.4,6.4V155.6z M112.2,173v-16.4h-5.4v7.2
									l-0.7,0.3l-0.3-0.3l-5-7.2h-5.7V173h5.4v-7.2l0.7-0.3l0.3,0.3l5,7.2H112.2"/>
							</g>
							<g>
								<polygon class="st34" points="112.2,156.6 112.2,173 106.5,173 101.5,165.8 101.2,165.4 100.5,165.8 100.5,173 95.1,173 
									95.1,156.6 100.8,156.6 105.8,163.8 106.1,164.1 106.8,163.8 106.8,156.6 								"/>
							</g>
							<g>
								<polygon class="st34" points="105.9,174 105.9,176.6 101.5,170.2 101.5,167.6 								"/>
							</g>
							<g>
								<rect x="105.9" y="174" class="st34" width="7.3" height="2.6"/>
							</g>
							<g>
								<rect x="94.1" y="174" class="st34" width="7.4" height="2.6"/>
							</g>
						</g>
						<g class="st0">
							<g class="st0">
								<defs>
									<path id="SVGID_425_" class="st0" d="M133.3,164.8l0,2.6c0-4.7-4-8.4-9.2-8.4c-5.2,0-9.2,3.7-9.2,8.4l0-2.6
										c0-4.7,4-8.4,9.2-8.4C129.3,156.3,133.3,160,133.3,164.8z"/>
								</defs>
								<clipPath id="SVGID_426_">
									<use xlink:href="#SVGID_425_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st240">
									<path class="st34" d="M133.3,164.8v2.6c0-4.7-4-8.4-9.2-8.4c-3.6,0-6.6,1.8-8.1,4.5v-2.6c1.5-2.7,4.6-4.5,8.1-4.5
										C129.3,156.3,133.3,160,133.3,164.8"/>
									<path class="st34" d="M116,160.8v2.6c-0.3,0.5-0.5,0.9-0.6,1.4v-2.6C115.5,161.7,115.7,161.3,116,160.8"/>
									<path class="st34" d="M115.3,162.2v2.6c-0.1,0.4-0.3,0.9-0.3,1.3v-2.6C115.1,163.1,115.2,162.7,115.3,162.2"/>
									<path class="st34" d="M115,163.6v2.6c-0.1,0.4-0.1,0.8-0.1,1.2v-2.6C114.9,164.4,115,164,115,163.6"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_427_" class="st0" d="M120.9,167.5c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3v-0.3c0-1.8,1.4-3.2,3.2-3.2
										c1.8,0,3.2,1.4,3.2,3.2l0,2.6c0-1.8-1.4-3.2-3.2-3.2C122.4,164.3,120.9,165.8,120.9,167.5z"/>
								</defs>
								<clipPath id="SVGID_428_">
									<use xlink:href="#SVGID_427_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st241">
									<path class="st34" d="M127.3,164.9v2.6c0-1.8-1.4-3.2-3.2-3.2c-1.2,0-2.2,0.7-2.8,1.6v-2.6c0.5-1,1.6-1.6,2.8-1.6
										C125.9,161.7,127.3,163.1,127.3,164.9"/>
									<path class="st34" d="M121.3,163.3v2.6c-0.1,0.2-0.2,0.4-0.3,0.6v-2.6C121.2,163.7,121.2,163.5,121.3,163.3"/>
									<path class="st34" d="M121.1,163.9v2.6c-0.1,0.2-0.1,0.3-0.1,0.5v-2.6C121,164.2,121,164.1,121.1,163.9"/>
									<path class="st34" d="M121,164.4v2.6c0,0.2,0,0.3,0,0.5v-2.6C120.9,164.7,120.9,164.6,121,164.4"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_429_" class="st0" d="M126.3,164.9l0,2.6c0-1.2-1-2.2-2.2-2.2c-1.2,0-2.2,1-2.2,2.2l0-2.6
										c0-1.2,1-2.2,2.2-2.2C125.3,162.7,126.3,163.7,126.3,164.9z"/>
								</defs>
								<clipPath id="SVGID_430_">
									<use xlink:href="#SVGID_429_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st242">
									<path class="st34" d="M126.3,164.9v2.6c0-1.2-1-2.2-2.2-2.2c-0.8,0-1.5,0.4-1.9,1.1v-2.6c0.4-0.7,1.1-1.1,1.9-1.1
										C125.3,162.7,126.3,163.7,126.3,164.9"/>
									<path class="st34" d="M122.2,163.8v2.6c-0.1,0.1-0.1,0.3-0.2,0.4v-2.6C122.1,164.1,122.1,163.9,122.2,163.8"/>
									<path class="st34" d="M122,164.2v2.6c0,0.1-0.1,0.2-0.1,0.4v-2.6C122,164.4,122,164.3,122,164.2"/>
									<path class="st34" d="M121.9,164.6v2.6c0,0.1,0,0.2,0,0.3v-2.6C121.9,164.8,121.9,164.7,121.9,164.6"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_431_" class="st0" d="M127.3,164.9l0,2.6c0,1.8-1.4,3.2-3.2,3.2c-1.8,0-3.2-1.4-3.2-3.2v-2.6
										c0,1.8,1.4,3.2,3.2,3.2C125.9,168.1,127.3,166.6,127.3,164.9z"/>
								</defs>
								<clipPath id="SVGID_432_">
									<use xlink:href="#SVGID_431_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st243">
									<path class="st34" d="M127.3,164.9v2.6c0,0.2,0,0.3,0,0.5v-2.6C127.3,165.2,127.3,165.1,127.3,164.9"/>
									<path class="st34" d="M127.3,165.4v2.6c0,0.2-0.1,0.4-0.1,0.5v-2.6C127.2,165.7,127.2,165.5,127.3,165.4"/>
									<path class="st34" d="M127.1,165.9v2.6c-0.1,0.2-0.2,0.4-0.3,0.6v-2.6C127,166.3,127.1,166.1,127.1,165.9"/>
									<path class="st34" d="M126.9,166.5v2.6c-0.5,1-1.6,1.6-2.8,1.6c-1.8,0-3.2-1.4-3.2-3.2v-2.6c0,1.8,1.4,3.2,3.2,3.2
										C125.3,168.1,126.3,167.4,126.9,166.5"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M124.1,161.7c1.8,0,3.2,1.4,3.2,3.2c0,1.8-1.4,3.2-3.2,3.2c-1.8,0-3.2-1.4-3.2-3.2
									C120.9,163.1,122.4,161.7,124.1,161.7z M124.1,167.1c1.2,0,2.2-1,2.2-2.2c0-1.2-1-2.2-2.2-2.2c-1.2,0-2.2,1-2.2,2.2
									C121.9,166.1,122.9,167.1,124.1,167.1"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_433_" class="st0" d="M124.1,175.8c-5.2,0-9.2-3.7-9.2-8.4l0-2.6c0,4.7,4,8.4,9.2,8.4
										c5.2,0,9.2-3.7,9.2-8.4l0,0.3v2.3C133.3,172.1,129.3,175.8,124.1,175.8z"/>
								</defs>
								<clipPath id="SVGID_434_">
									<use xlink:href="#SVGID_433_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st244">
									<path class="st34" d="M133.3,164.8v2.6c0,0.4,0,0.8-0.1,1.2V166C133.3,165.6,133.3,165.2,133.3,164.8"/>
									<path class="st34" d="M133.2,166v2.6c-0.1,0.5-0.2,0.9-0.3,1.3v-2.6C133,166.9,133.2,166.4,133.2,166"/>
									<path class="st34" d="M132.9,167.3v2.6c-0.2,0.5-0.4,1-0.6,1.4v-2.6C132.5,168.3,132.7,167.8,132.9,167.3"/>
									<path class="st34" d="M132.3,168.8v2.6c-1.5,2.7-4.6,4.5-8.1,4.5c-5.2,0-9.2-3.7-9.2-8.4v-2.6c0,4.7,4,8.4,9.2,8.4
										C127.7,173.2,130.7,171.4,132.3,168.8"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M124.1,155.3c5.7,0,10.2,4.2,10.2,9.4c0,5.3-4.5,9.4-10.2,9.4c-5.7,0-10.2-4.1-10.2-9.4
									C113.9,159.5,118.4,155.3,124.1,155.3z M124.1,173.2c5.2,0,9.2-3.7,9.2-8.4c0-4.7-4-8.4-9.2-8.4c-5.2,0-9.2,3.7-9.2,8.4
									C114.9,169.5,119,173.2,124.1,173.2"/>
							</g>
							<g>
								<path class="st34" d="M124.1,156.3c5.2,0,9.2,3.7,9.2,8.4c0,4.7-4,8.4-9.2,8.4c-5.2,0-9.2-3.7-9.2-8.4
									C114.9,160,119,156.3,124.1,156.3z M124.1,168.1c1.8,0,3.2-1.4,3.2-3.2c0-1.8-1.4-3.2-3.2-3.2c-1.8,0-3.2,1.4-3.2,3.2
									C120.9,166.6,122.4,168.1,124.1,168.1"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_435_" class="st0" d="M134.3,164.8v2.6c0,5.3-4.5,9.4-10.2,9.4c-5.7,0-10.2-4.1-10.2-9.4l0-2.6
										c0,5.3,4.5,9.4,10.2,9.4C129.8,174.2,134.3,170.1,134.3,164.8z"/>
								</defs>
								<clipPath id="SVGID_436_">
									<use xlink:href="#SVGID_435_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st245">
									<path class="st34" d="M134.3,164.8v2.6c0,0.5,0,0.9-0.1,1.4v-2.6C134.3,165.7,134.3,165.2,134.3,164.8"/>
									<path class="st34" d="M134.2,166.1v2.6c-0.1,0.5-0.2,1-0.4,1.5v-2.6C134,167.2,134.1,166.7,134.2,166.1"/>
									<path class="st34" d="M133.9,167.6v2.6c-0.2,0.6-0.4,1.1-0.7,1.6v-2.6C133.4,168.7,133.7,168.2,133.9,167.6"/>
									<path class="st34" d="M133.1,169.3v2.6c-1.7,3-5.1,5-9,5c-5.7,0-10.2-4.1-10.2-9.4v-2.6c0,5.3,4.5,9.4,10.2,9.4
										C128.1,174.2,131.4,172.2,133.1,169.3"/>
								</g>
							</g>
						</g>
						<g class="st0">
							<g>
								<rect x="135.3" y="156.6" class="st34" width="15.8" height="2.6"/>
							</g>
							<g>
								<rect x="135.3" y="161.5" class="st34" width="4.9" height="2.6"/>
							</g>
							<g>
								<rect x="146.3" y="161.5" class="st34" width="4.8" height="2.6"/>
							</g>
							<g>
								<rect x="147.3" y="162.5" class="st34" width="4.8" height="2.6"/>
							</g>
							<g>
								<rect x="134.3" y="162.5" class="st34" width="4.9" height="2.6"/>
							</g>
							<g>
								<rect x="140.2" y="173" class="st34" width="6.2" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M134.3,155.6h17.8v6.9h-4.8V174h-8.2v-11.5h-4.9V155.6z M151.2,161.5v-4.9h-15.8v4.9h4.9V173h6.2
									v-11.5H151.2"/>
							</g>
							<g>
								<polygon class="st34" points="151.2,156.6 151.2,161.5 146.3,161.5 146.3,173 140.2,173 140.2,161.5 135.3,161.5 
									135.3,156.6 								"/>
							</g>
							<g>
								<rect x="139.2" y="174" class="st34" width="8.2" height="2.6"/>
							</g>
						</g>
						<g class="st0">
							<g>
								<rect x="154" y="156.6" class="st34" width="11.5" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="161.4" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<rect x="165.5" y="162.4" class="st34" width="1" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="163.3" class="st34" width="4.3" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="166.5" class="st34" width="4.3" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="168.4" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<polygon class="st34" points="165.5,156.6 165.5,161.4 160.2,161.4 160.2,163.3 164.5,163.3 164.5,166.5 160.2,166.5 
									160.2,168.4 165.9,168.4 165.9,173 154,173 154,156.6 								"/>
							</g>
							<g>
								<rect x="154" y="173" class="st34" width="11.8" height="2.6"/>
							</g>
							<g>
								<rect x="153" y="174" class="st34" width="13.8" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M165.5,167.4h1.4v6.5H153v-18.4h13.5v6.8h-1V167.4z M165.9,173v-4.5h-5.7v-2h4.3v-3.1h-4.3v-1.9h5.4
									v-4.8H154V173H165.9"/>
							</g>
						</g>
						<g class="st0">
							<g class="st0">
								<defs>
									<path id="SVGID_437_" class="st0" d="M180.5,157.5l0,2.6c-1.5-0.7-3.8-1.2-5.7-1.2c-4,0-6.6,2.1-6.6,5.4l0-2.6
										c0-3.3,2.6-5.4,6.6-5.4C176.7,156.3,178.9,156.8,180.5,157.5z"/>
								</defs>
								<clipPath id="SVGID_438_">
									<use xlink:href="#SVGID_437_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st246">
									<path class="st34" d="M180.5,157.5v2.6c-1.5-0.7-3.8-1.2-5.7-1.2c-2.8,0-4.9,1-5.9,2.8v-2.6c1-1.8,3.1-2.8,5.9-2.8
										C176.7,156.3,178.9,156.8,180.5,157.5"/>
									<path class="st34" d="M168.8,159.1v2.6c-0.2,0.3-0.3,0.6-0.4,0.9V160C168.6,159.7,168.7,159.4,168.8,159.1"/>
									<path class="st34" d="M168.5,160v2.6c-0.1,0.3-0.2,0.6-0.2,0.9v-2.6C168.3,160.5,168.4,160.3,168.5,160"/>
									<path class="st34" d="M168.3,160.8v2.6c0,0.3-0.1,0.6-0.1,0.8v-2.6C168.2,161.4,168.2,161.1,168.3,160.8"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_439_" class="st0" d="M175.7,163.5c-1.3,0-1.4,0.8-1.4,1v-2.6c0-0.2,0.1-1,1.4-1c1,0,2.8,0.4,4.8,1.2v2.6
										C178.5,163.9,176.7,163.5,175.7,163.5z"/>
								</defs>
								<clipPath id="SVGID_440_">
									<use xlink:href="#SVGID_439_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st247">
									<path class="st34" d="M180.5,162.1v2.6c-1.9-0.9-3.8-1.2-4.8-1.2c-0.8,0-1.1,0.3-1.3,0.5v-2.6c0.2-0.3,0.5-0.5,1.3-0.5
										C176.7,160.8,178.5,161.2,180.5,162.1"/>
									<path class="st34" d="M174.5,161.4v2.6c0,0.1-0.1,0.1-0.1,0.2v-2.6C174.4,161.5,174.4,161.5,174.5,161.4"/>
									<path class="st34" d="M174.4,161.6v2.6c0,0.1,0,0.1,0,0.2v-2.6C174.3,161.7,174.4,161.6,174.4,161.6"/>
									<path class="st34" d="M174.3,161.7v2.6c0,0.1,0,0.1,0,0.1v-2.6C174.3,161.8,174.3,161.8,174.3,161.7"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_441_" class="st0" d="M175.2,163v2.6c-0.6-0.2-0.9-0.6-0.9-1.1l0-2.6C174.3,162.4,174.6,162.7,175.2,163z"
										/>
								</defs>
								<clipPath id="SVGID_442_">
									<use xlink:href="#SVGID_441_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st248">
									<path class="st34" d="M175.2,163v2.6c-0.6-0.2-0.9-0.6-0.9-1.1v-2.6C174.3,162.4,174.6,162.7,175.2,163"/>
								</g>
							</g>
							<g>
								<polygon class="st34" points="181.5,163.7 181.5,166.3 180.7,165.9 180.7,163.3 								"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_443_" class="st0" d="M169.6,167.5c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6c0,1.4,0.4,2.4,1.4,3.2
										C169.6,165.8,169.6,166.7,169.6,167.5c0-0.9,0-1.7,0-2.6c1.4,1.2,3.2,1.6,4.3,1.8c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										C172.8,169.1,171,168.8,169.6,167.5z"/>
								</defs>
								<clipPath id="SVGID_444_">
									<use xlink:href="#SVGID_443_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st249">
									<path class="st34" d="M174.4,166.9v2.6c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6
										c0,1.4,0.4,2.4,1.4,3.2c1.4,1.2,3.2,1.6,4.3,1.8C174.1,166.8,174.3,166.8,174.4,166.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_445_" class="st0" d="M174.4,169.5c-0.1,0-0.3-0.1-0.5-0.1c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0.2,0,0.4,0.1,0.5,0.1C174.4,167.7,174.4,168.6,174.4,169.5z"/>
								</defs>
								<clipPath id="SVGID_446_">
									<use xlink:href="#SVGID_445_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st250">
									<path class="st34" d="M174.4,166.9v2.6c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6
										c0,1.4,0.4,2.4,1.4,3.2c1.4,1.2,3.2,1.6,4.3,1.8C174.1,166.8,174.3,166.8,174.4,166.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_447_" class="st0" d="M173.7,167.7v2.6c-1.1-0.2-3.2-0.6-4.8-2c-1.2-1-1.7-2.3-1.7-4l0-2.6
										c0,1.7,0.6,3,1.7,4C170.5,167.1,172.5,167.5,173.7,167.7z"/>
								</defs>
								<clipPath id="SVGID_448_">
									<use xlink:href="#SVGID_447_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st251">
									<path class="st34" d="M173.7,167.7v2.6c-1.1-0.2-3.2-0.6-4.8-2c-1.2-1-1.7-2.3-1.7-4v-2.6c0,1.7,0.6,3,1.7,4
										C170.5,167.1,172.5,167.5,173.7,167.7"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_449_" class="st0" d="M174.2,167.8v2.6c-0.1,0-0.3-0.1-0.5-0.1v-2.6C173.9,167.8,174,167.8,174.2,167.8z"/>
								</defs>
								<clipPath id="SVGID_450_">
									<use xlink:href="#SVGID_449_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st252">
									<path class="st34" d="M174.2,167.8v2.6c-0.1,0-0.3-0.1-0.5-0.1v-2.6C173.9,167.8,174,167.8,174.2,167.8"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_451_" class="st0" d="M175.3,170.5c0-0.3-0.1-0.9-0.9-1c0-0.8,0-1.5,0-2.3v-0.3c0.8,0.2,0.9,0.7,0.9,1v0.3
										C175.3,168.9,175.3,169.8,175.3,170.5z"/>
								</defs>
								<clipPath id="SVGID_452_">
									<use xlink:href="#SVGID_451_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st253">
									<path class="st34" d="M175.3,167.9v2.6c0-0.3-0.1-0.9-0.9-1v-2.6C175.2,167,175.3,167.6,175.3,167.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_453_" class="st0" d="M181.5,168.1v2.6c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2
										V163c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6C180.9,165.6,181.5,166.7,181.5,168.1z"/>
								</defs>
								<clipPath id="SVGID_454_">
									<use xlink:href="#SVGID_453_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st254">
									<path class="st34" d="M181.5,168.1v2.6c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2V163
										c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6C180.9,165.6,181.5,166.7,181.5,168.1"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_455_" class="st0" d="M175.3,167.9l0,2.6c0,0.3-0.1,1.1-1.4,1.1c-1.2,0-3.2-0.5-5.2-1.7l0-2.6
										c2,1.1,4.1,1.7,5.2,1.7C175.2,169,175.3,168.1,175.3,167.9z"/>
								</defs>
								<clipPath id="SVGID_456_">
									<use xlink:href="#SVGID_455_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st255">
									<path class="st34" d="M175.3,167.9v2.6c0,0,0,0.1,0,0.2V168C175.3,168,175.3,167.9,175.3,167.9"/>
									<path class="st34" d="M175.3,168v2.6c0,0.1,0,0.1,0,0.2v-2.6C175.3,168.2,175.3,168.1,175.3,168"/>
									<path class="st34" d="M175.2,168.2v2.6c0,0.1,0,0.1-0.1,0.2v-2.6C175.2,168.3,175.2,168.3,175.2,168.2"/>
									<path class="st34" d="M175.2,168.4v2.6c-0.2,0.3-0.5,0.6-1.3,0.6c-1.2,0-3.2-0.5-5.2-1.7v-2.6c2,1.1,4.1,1.7,5.2,1.7
										C174.7,169,175,168.7,175.2,168.4"/>
								</g>
							</g>
							<g>
								<polygon class="st34" points="168,172.7 168,175.4 167.7,175.2 167.7,172.6 								"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_457_" class="st0" d="M181.5,170.7c0,3.2-2.6,5.2-6.7,5.2c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c4.2,0,6.7-2,6.7-5.2v0.3C181.5,169.1,181.5,170,181.5,170.7z"/>
								</defs>
								<clipPath id="SVGID_458_">
									<use xlink:href="#SVGID_457_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st256">
									<path class="st34" d="M181.5,168.1v2.6c0,0.3,0,0.5-0.1,0.8v-2.6C181.4,168.6,181.5,168.3,181.5,168.1"/>
									<path class="st34" d="M181.4,168.9v2.6c0,0.3-0.1,0.5-0.2,0.8v-2.6C181.3,169.4,181.4,169.1,181.4,168.9"/>
									<path class="st34" d="M181.2,169.7v2.6c-0.1,0.3-0.2,0.5-0.4,0.8v-2.6C181,170.2,181.1,169.9,181.2,169.7"/>
									<path class="st34" d="M180.9,170.5v2.6c-1,1.7-3.2,2.8-6.1,2.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2
										C177.7,173.2,179.9,172.2,180.9,170.5"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_459_" class="st0" d="M174.7,175.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2c0,0,0,0,0,0
										c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										c0,0.1,0,0.2,0,0.3C174.7,175.6,174.7,175.7,174.7,175.8C174.7,175.8,174.7,175.8,174.7,175.8z"/>
								</defs>
								<clipPath id="SVGID_460_">
									<use xlink:href="#SVGID_459_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st257">
									<path class="st34" d="M180.9,170.5v2.6c-1,1.7-3.2,2.8-6.1,2.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2
										C177.7,173.2,179.9,172.2,180.9,170.5"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M174.8,156.3c1.9,0,4.2,0.5,5.7,1.2v4.6c-1.9-0.9-3.8-1.2-4.8-1.2c-1.3,0-1.4,0.8-1.4,1
									c0,0.5,0.3,0.9,0.9,1.1c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6c1.1,0.9,1.6,2,1.6,3.4c0,3.2-2.6,5.2-6.8,5.2
									c-2.1,0-4.4-0.5-6-1.2v-4.7c2,1.1,4.1,1.7,5.2,1.7c1.3,0,1.4-0.8,1.4-1.1c0-0.3-0.1-0.9-0.9-1c-0.1,0-0.3-0.1-0.5-0.1
									c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2C168.2,158.4,170.8,156.3,174.8,156.3z"/>
							</g>
							<g>
								<path class="st34" d="M180.5,163.9c1.3,1.1,1.9,2.4,1.9,4.1c0,3.7-3,6.2-7.7,6.2c-2.4,0-5-0.6-6.7-1.5l-0.3-0.1v-7.1
									l0.8,0.5c2.2,1.5,4.5,2,5.4,2c0.3,0,0.4-0.1,0.4-0.1c0,0,0,0-0.2-0.1c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-3.2-0.6-4.8-2
									c-1.2-1-1.7-2.3-1.7-4c0-3.9,3-6.4,7.6-6.4c2.2,0,4.8,0.6,6.4,1.4l0.3,0.1v6.8l-0.7-0.4c-2.2-1.1-4.2-1.5-5-1.5
									c-0.3,0-0.4,0.1-0.4,0.1c0,0,0.1,0,0.2,0.1c0.1,0,0.3,0.1,0.5,0.1C177.1,162.4,179.1,162.8,180.5,163.9z M174.7,173.2
									c4.2,0,6.8-2,6.8-5.2c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2c-0.6-0.2-0.9-0.6-0.9-1.1
									c0-0.2,0.1-1,1.4-1c1,0,2.8,0.4,4.8,1.2v-4.6c-1.5-0.7-3.8-1.2-5.7-1.2c-4,0-6.6,2.1-6.6,5.4c0,1.4,0.4,2.4,1.4,3.2
									c1.4,1.2,3.2,1.6,4.3,1.8c0.2,0,0.4,0.1,0.5,0.1c0.8,0.2,0.9,0.7,0.9,1c0,0.3-0.1,1.1-1.4,1.1c-1.2,0-3.2-0.5-5.2-1.7v4.7
									C170.3,172.7,172.6,173.2,174.7,173.2"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_461_" class="st0" d="M182.5,168.1l0,2.6c0,3.7-3,6.2-7.7,6.2c-2.4,0-5-0.6-6.7-1.5v-2.6
										c1.7,0.9,4.4,1.5,6.7,1.5C179.4,174.2,182.5,171.8,182.5,168.1z"/>
								</defs>
								<clipPath id="SVGID_462_">
									<use xlink:href="#SVGID_461_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st258">
									<path class="st34" d="M182.5,168.1v2.6c0,0.3,0,0.6-0.1,1V169C182.4,168.7,182.5,168.4,182.5,168.1"/>
									<path class="st34" d="M182.4,169v2.6c-0.1,0.3-0.1,0.7-0.2,1V170C182.3,169.7,182.3,169.3,182.4,169"/>
									<path class="st34" d="M182.2,170v2.6c-0.1,0.3-0.3,0.7-0.4,1v-2.6C181.9,170.6,182,170.3,182.2,170"/>
									<path class="st34" d="M181.7,170.9v2.6c-1.2,2-3.7,3.3-7,3.3c-2.4,0-5-0.6-6.7-1.5v-2.6c1.7,0.9,4.4,1.5,6.7,1.5
										C178.1,174.2,180.6,173,181.7,170.9"/>
								</g>
							</g>
						</g>
					</g>
				</g>
			</g>
			<defs>
				<filter id="Adobe_OpacityMaskFilter_11_" filterUnits="userSpaceOnUse" x="94.1" y="155.3" width="88.4" height="21.5">
					<feFlood  style="flood-color:white;flood-opacity:1" result="back"/>
					<feBlend  in="SourceGraphic" in2="back" mode="normal"/>
				</filter>
			</defs>
			<mask maskUnits="userSpaceOnUse" x="94.1" y="155.3" width="88.4" height="21.5" id="SVGID_463_">
				<g class="st259">
					
						<image style="overflow:visible;" width="93" height="27" xlink:href="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgEASABIAAD/7AARRHVja3kAAQAEAAAAHgAA/+4AIUFkb2JlAGTAAAAAAQMA
EAMCAwYAAAF0AAABfwAAAaD/2wCEABALCwsMCxAMDBAXDw0PFxsUEBAUGx8XFxcXFx8eFxoaGhoX
Hh4jJSclIx4vLzMzLy9AQEBAQEBAQEBAQEBAQEABEQ8PERMRFRISFRQRFBEUGhQWFhQaJhoaHBoa
JjAjHh4eHiMwKy4nJycuKzU1MDA1NUBAP0BAQEBAQEBAQEBAQP/CABEIABsAXQMBIgACEQEDEQH/
xABfAAEBAQAAAAAAAAAAAAAAAAAAAQcBAQAAAAAAAAAAAAAAAAAAAAAQAQEAAAAAAAAAAAAAAAAA
ADBQEQEAAAAAAAAAAAAAAAAAAABQEgEAAAAAAAAAAAAAAAAAAAAw/9oADAMBAAIRAxEAAADPykWA
AABYAAAAAAf/2gAIAQIAAQUAT//aAAgBAwABBQBP/9oACAEBAAEFAKH/2gAIAQICBj8AT//aAAgB
AwIGPwBP/9oACAEBAQY/AEP/2Q==" transform="matrix(1 0 0 1 91.75 152.5)">
					</image>
				</g>
			</mask>
			<g class="st260">
				<g>
					<g>
						<g class="st0">
							<g>
								<rect x="95.1" y="156.6" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<rect x="106.8" y="156.6" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<polygon class="st34" points="105.8,163.8 105.8,166.4 100.8,159.2 100.8,156.6 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.8,163.8 106.8,166.4 106.1,166.7 106.1,164.1 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.1,164.1 106.1,166.7 105.8,166.4 105.8,163.8 								"/>
							</g>
							<g>
								<polygon class="st34" points="101.2,165.4 101.2,168 100.5,168.4 100.5,165.8 								"/>
							</g>
							<g>
								<polygon class="st34" points="101.5,165.8 101.5,168.4 101.2,168 101.2,165.4 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.5,173 106.5,175.6 101.5,168.4 101.5,165.8 								"/>
							</g>
							<g>
								<rect x="106.5" y="173" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<rect x="95.1" y="173" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M105.8,155.6h7.4V174h-7.3l-4.4-6.4v6.4h-7.4v-18.4h7.3l4.4,6.4V155.6z M112.2,173v-16.4h-5.4v7.2
									l-0.7,0.3l-0.3-0.3l-5-7.2h-5.7V173h5.4v-7.2l0.7-0.3l0.3,0.3l5,7.2H112.2"/>
							</g>
							<g>
								<polygon class="st34" points="112.2,156.6 112.2,173 106.5,173 101.5,165.8 101.2,165.4 100.5,165.8 100.5,173 95.1,173 
									95.1,156.6 100.8,156.6 105.8,163.8 106.1,164.1 106.8,163.8 106.8,156.6 								"/>
							</g>
							<g>
								<polygon class="st34" points="105.9,174 105.9,176.6 101.5,170.2 101.5,167.6 								"/>
							</g>
							<g>
								<rect x="105.9" y="174" class="st34" width="7.3" height="2.6"/>
							</g>
							<g>
								<rect x="94.1" y="174" class="st34" width="7.4" height="2.6"/>
							</g>
						</g>
						<g class="st0">
							<g class="st0">
								<defs>
									<path id="SVGID_464_" class="st0" d="M133.3,164.8l0,2.6c0-4.7-4-8.4-9.2-8.4c-5.2,0-9.2,3.7-9.2,8.4l0-2.6
										c0-4.7,4-8.4,9.2-8.4C129.3,156.3,133.3,160,133.3,164.8z"/>
								</defs>
								<clipPath id="SVGID_465_">
									<use xlink:href="#SVGID_464_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st261">
									<path class="st34" d="M133.3,164.8v2.6c0-4.7-4-8.4-9.2-8.4c-3.6,0-6.6,1.8-8.1,4.5v-2.6c1.5-2.7,4.6-4.5,8.1-4.5
										C129.3,156.3,133.3,160,133.3,164.8"/>
									<path class="st34" d="M116,160.8v2.6c-0.3,0.5-0.5,0.9-0.6,1.4v-2.6C115.5,161.7,115.7,161.3,116,160.8"/>
									<path class="st34" d="M115.3,162.2v2.6c-0.1,0.4-0.3,0.9-0.3,1.3v-2.6C115.1,163.1,115.2,162.7,115.3,162.2"/>
									<path class="st34" d="M115,163.6v2.6c-0.1,0.4-0.1,0.8-0.1,1.2v-2.6C114.9,164.4,115,164,115,163.6"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_466_" class="st0" d="M120.9,167.5c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3v-0.3c0-1.8,1.4-3.2,3.2-3.2
										c1.8,0,3.2,1.4,3.2,3.2l0,2.6c0-1.8-1.4-3.2-3.2-3.2C122.4,164.3,120.9,165.8,120.9,167.5z"/>
								</defs>
								<clipPath id="SVGID_467_">
									<use xlink:href="#SVGID_466_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st262">
									<path class="st34" d="M127.3,164.9v2.6c0-1.8-1.4-3.2-3.2-3.2c-1.2,0-2.2,0.7-2.8,1.6v-2.6c0.5-1,1.6-1.6,2.8-1.6
										C125.9,161.7,127.3,163.1,127.3,164.9"/>
									<path class="st34" d="M121.3,163.3v2.6c-0.1,0.2-0.2,0.4-0.3,0.6v-2.6C121.2,163.7,121.2,163.5,121.3,163.3"/>
									<path class="st34" d="M121.1,163.9v2.6c-0.1,0.2-0.1,0.3-0.1,0.5v-2.6C121,164.2,121,164.1,121.1,163.9"/>
									<path class="st34" d="M121,164.4v2.6c0,0.2,0,0.3,0,0.5v-2.6C120.9,164.7,120.9,164.6,121,164.4"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_468_" class="st0" d="M126.3,164.9l0,2.6c0-1.2-1-2.2-2.2-2.2c-1.2,0-2.2,1-2.2,2.2l0-2.6
										c0-1.2,1-2.2,2.2-2.2C125.3,162.7,126.3,163.7,126.3,164.9z"/>
								</defs>
								<clipPath id="SVGID_469_">
									<use xlink:href="#SVGID_468_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st263">
									<path class="st34" d="M126.3,164.9v2.6c0-1.2-1-2.2-2.2-2.2c-0.8,0-1.5,0.4-1.9,1.1v-2.6c0.4-0.7,1.1-1.1,1.9-1.1
										C125.3,162.7,126.3,163.7,126.3,164.9"/>
									<path class="st34" d="M122.2,163.8v2.6c-0.1,0.1-0.1,0.3-0.2,0.4v-2.6C122.1,164.1,122.1,163.9,122.2,163.8"/>
									<path class="st34" d="M122,164.2v2.6c0,0.1-0.1,0.2-0.1,0.4v-2.6C122,164.4,122,164.3,122,164.2"/>
									<path class="st34" d="M121.9,164.6v2.6c0,0.1,0,0.2,0,0.3v-2.6C121.9,164.8,121.9,164.7,121.9,164.6"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_470_" class="st0" d="M127.3,164.9l0,2.6c0,1.8-1.4,3.2-3.2,3.2c-1.8,0-3.2-1.4-3.2-3.2v-2.6
										c0,1.8,1.4,3.2,3.2,3.2C125.9,168.1,127.3,166.6,127.3,164.9z"/>
								</defs>
								<clipPath id="SVGID_471_">
									<use xlink:href="#SVGID_470_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st264">
									<path class="st34" d="M127.3,164.9v2.6c0,0.2,0,0.3,0,0.5v-2.6C127.3,165.2,127.3,165.1,127.3,164.9"/>
									<path class="st34" d="M127.3,165.4v2.6c0,0.2-0.1,0.4-0.1,0.5v-2.6C127.2,165.7,127.2,165.5,127.3,165.4"/>
									<path class="st34" d="M127.1,165.9v2.6c-0.1,0.2-0.2,0.4-0.3,0.6v-2.6C127,166.3,127.1,166.1,127.1,165.9"/>
									<path class="st34" d="M126.9,166.5v2.6c-0.5,1-1.6,1.6-2.8,1.6c-1.8,0-3.2-1.4-3.2-3.2v-2.6c0,1.8,1.4,3.2,3.2,3.2
										C125.3,168.1,126.3,167.4,126.9,166.5"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M124.1,161.7c1.8,0,3.2,1.4,3.2,3.2c0,1.8-1.4,3.2-3.2,3.2c-1.8,0-3.2-1.4-3.2-3.2
									C120.9,163.1,122.4,161.7,124.1,161.7z M124.1,167.1c1.2,0,2.2-1,2.2-2.2c0-1.2-1-2.2-2.2-2.2c-1.2,0-2.2,1-2.2,2.2
									C121.9,166.1,122.9,167.1,124.1,167.1"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_472_" class="st0" d="M124.1,175.8c-5.2,0-9.2-3.7-9.2-8.4l0-2.6c0,4.7,4,8.4,9.2,8.4
										c5.2,0,9.2-3.7,9.2-8.4l0,0.3v2.3C133.3,172.1,129.3,175.8,124.1,175.8z"/>
								</defs>
								<clipPath id="SVGID_473_">
									<use xlink:href="#SVGID_472_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st265">
									<path class="st34" d="M133.3,164.8v2.6c0,0.4,0,0.8-0.1,1.2V166C133.3,165.6,133.3,165.2,133.3,164.8"/>
									<path class="st34" d="M133.2,166v2.6c-0.1,0.5-0.2,0.9-0.3,1.3v-2.6C133,166.9,133.2,166.4,133.2,166"/>
									<path class="st34" d="M132.9,167.3v2.6c-0.2,0.5-0.4,1-0.6,1.4v-2.6C132.5,168.3,132.7,167.8,132.9,167.3"/>
									<path class="st34" d="M132.3,168.8v2.6c-1.5,2.7-4.6,4.5-8.1,4.5c-5.2,0-9.2-3.7-9.2-8.4v-2.6c0,4.7,4,8.4,9.2,8.4
										C127.7,173.2,130.7,171.4,132.3,168.8"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M124.1,155.3c5.7,0,10.2,4.2,10.2,9.4c0,5.3-4.5,9.4-10.2,9.4c-5.7,0-10.2-4.1-10.2-9.4
									C113.9,159.5,118.4,155.3,124.1,155.3z M124.1,173.2c5.2,0,9.2-3.7,9.2-8.4c0-4.7-4-8.4-9.2-8.4c-5.2,0-9.2,3.7-9.2,8.4
									C114.9,169.5,119,173.2,124.1,173.2"/>
							</g>
							<g>
								<path class="st34" d="M124.1,156.3c5.2,0,9.2,3.7,9.2,8.4c0,4.7-4,8.4-9.2,8.4c-5.2,0-9.2-3.7-9.2-8.4
									C114.9,160,119,156.3,124.1,156.3z M124.1,168.1c1.8,0,3.2-1.4,3.2-3.2c0-1.8-1.4-3.2-3.2-3.2c-1.8,0-3.2,1.4-3.2,3.2
									C120.9,166.6,122.4,168.1,124.1,168.1"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_474_" class="st0" d="M134.3,164.8v2.6c0,5.3-4.5,9.4-10.2,9.4c-5.7,0-10.2-4.1-10.2-9.4l0-2.6
										c0,5.3,4.5,9.4,10.2,9.4C129.8,174.2,134.3,170.1,134.3,164.8z"/>
								</defs>
								<clipPath id="SVGID_475_">
									<use xlink:href="#SVGID_474_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st266">
									<path class="st34" d="M134.3,164.8v2.6c0,0.5,0,0.9-0.1,1.4v-2.6C134.3,165.7,134.3,165.2,134.3,164.8"/>
									<path class="st34" d="M134.2,166.1v2.6c-0.1,0.5-0.2,1-0.4,1.5v-2.6C134,167.2,134.1,166.7,134.2,166.1"/>
									<path class="st34" d="M133.9,167.6v2.6c-0.2,0.6-0.4,1.1-0.7,1.6v-2.6C133.4,168.7,133.7,168.2,133.9,167.6"/>
									<path class="st34" d="M133.1,169.3v2.6c-1.7,3-5.1,5-9,5c-5.7,0-10.2-4.1-10.2-9.4v-2.6c0,5.3,4.5,9.4,10.2,9.4
										C128.1,174.2,131.4,172.2,133.1,169.3"/>
								</g>
							</g>
						</g>
						<g class="st0">
							<g>
								<rect x="135.3" y="156.6" class="st34" width="15.8" height="2.6"/>
							</g>
							<g>
								<rect x="135.3" y="161.5" class="st34" width="4.9" height="2.6"/>
							</g>
							<g>
								<rect x="146.3" y="161.5" class="st34" width="4.8" height="2.6"/>
							</g>
							<g>
								<rect x="147.3" y="162.5" class="st34" width="4.8" height="2.6"/>
							</g>
							<g>
								<rect x="134.3" y="162.5" class="st34" width="4.9" height="2.6"/>
							</g>
							<g>
								<rect x="140.2" y="173" class="st34" width="6.2" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M134.3,155.6h17.8v6.9h-4.8V174h-8.2v-11.5h-4.9V155.6z M151.2,161.5v-4.9h-15.8v4.9h4.9V173h6.2
									v-11.5H151.2"/>
							</g>
							<g>
								<polygon class="st34" points="151.2,156.6 151.2,161.5 146.3,161.5 146.3,173 140.2,173 140.2,161.5 135.3,161.5 
									135.3,156.6 								"/>
							</g>
							<g>
								<rect x="139.2" y="174" class="st34" width="8.2" height="2.6"/>
							</g>
						</g>
						<g class="st0">
							<g>
								<rect x="154" y="156.6" class="st34" width="11.5" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="161.4" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<rect x="165.5" y="162.4" class="st34" width="1" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="163.3" class="st34" width="4.3" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="166.5" class="st34" width="4.3" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="168.4" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<polygon class="st34" points="165.5,156.6 165.5,161.4 160.2,161.4 160.2,163.3 164.5,163.3 164.5,166.5 160.2,166.5 
									160.2,168.4 165.9,168.4 165.9,173 154,173 154,156.6 								"/>
							</g>
							<g>
								<rect x="154" y="173" class="st34" width="11.8" height="2.6"/>
							</g>
							<g>
								<rect x="153" y="174" class="st34" width="13.8" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M165.5,167.4h1.4v6.5H153v-18.4h13.5v6.8h-1V167.4z M165.9,173v-4.5h-5.7v-2h4.3v-3.1h-4.3v-1.9h5.4
									v-4.8H154V173H165.9"/>
							</g>
						</g>
						<g class="st0">
							<g class="st0">
								<defs>
									<path id="SVGID_476_" class="st0" d="M180.5,157.5l0,2.6c-1.5-0.7-3.8-1.2-5.7-1.2c-4,0-6.6,2.1-6.6,5.4l0-2.6
										c0-3.3,2.6-5.4,6.6-5.4C176.7,156.3,178.9,156.8,180.5,157.5z"/>
								</defs>
								<clipPath id="SVGID_477_">
									<use xlink:href="#SVGID_476_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st267">
									<path class="st34" d="M180.5,157.5v2.6c-1.5-0.7-3.8-1.2-5.7-1.2c-2.8,0-4.9,1-5.9,2.8v-2.6c1-1.8,3.1-2.8,5.9-2.8
										C176.7,156.3,178.9,156.8,180.5,157.5"/>
									<path class="st34" d="M168.8,159.1v2.6c-0.2,0.3-0.3,0.6-0.4,0.9V160C168.6,159.7,168.7,159.4,168.8,159.1"/>
									<path class="st34" d="M168.5,160v2.6c-0.1,0.3-0.2,0.6-0.2,0.9v-2.6C168.3,160.5,168.4,160.3,168.5,160"/>
									<path class="st34" d="M168.3,160.8v2.6c0,0.3-0.1,0.6-0.1,0.8v-2.6C168.2,161.4,168.2,161.1,168.3,160.8"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_478_" class="st0" d="M175.7,163.5c-1.3,0-1.4,0.8-1.4,1v-2.6c0-0.2,0.1-1,1.4-1c1,0,2.8,0.4,4.8,1.2v2.6
										C178.5,163.9,176.7,163.5,175.7,163.5z"/>
								</defs>
								<clipPath id="SVGID_479_">
									<use xlink:href="#SVGID_478_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st268">
									<path class="st34" d="M180.5,162.1v2.6c-1.9-0.9-3.8-1.2-4.8-1.2c-0.8,0-1.1,0.3-1.3,0.5v-2.6c0.2-0.3,0.5-0.5,1.3-0.5
										C176.7,160.8,178.5,161.2,180.5,162.1"/>
									<path class="st34" d="M174.5,161.4v2.6c0,0.1-0.1,0.1-0.1,0.2v-2.6C174.4,161.5,174.4,161.5,174.5,161.4"/>
									<path class="st34" d="M174.4,161.6v2.6c0,0.1,0,0.1,0,0.2v-2.6C174.3,161.7,174.4,161.6,174.4,161.6"/>
									<path class="st34" d="M174.3,161.7v2.6c0,0.1,0,0.1,0,0.1v-2.6C174.3,161.8,174.3,161.8,174.3,161.7"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_480_" class="st0" d="M175.2,163v2.6c-0.6-0.2-0.9-0.6-0.9-1.1l0-2.6C174.3,162.4,174.6,162.7,175.2,163z"
										/>
								</defs>
								<clipPath id="SVGID_481_">
									<use xlink:href="#SVGID_480_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st269">
									<path class="st34" d="M175.2,163v2.6c-0.6-0.2-0.9-0.6-0.9-1.1v-2.6C174.3,162.4,174.6,162.7,175.2,163"/>
								</g>
							</g>
							<g>
								<polygon class="st34" points="181.5,163.7 181.5,166.3 180.7,165.9 180.7,163.3 								"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_482_" class="st0" d="M169.6,167.5c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6c0,1.4,0.4,2.4,1.4,3.2
										C169.6,165.8,169.6,166.7,169.6,167.5c0-0.9,0-1.7,0-2.6c1.4,1.2,3.2,1.6,4.3,1.8c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										C172.8,169.1,171,168.8,169.6,167.5z"/>
								</defs>
								<clipPath id="SVGID_483_">
									<use xlink:href="#SVGID_482_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st270">
									<path class="st34" d="M174.4,166.9v2.6c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6
										c0,1.4,0.4,2.4,1.4,3.2c1.4,1.2,3.2,1.6,4.3,1.8C174.1,166.8,174.3,166.8,174.4,166.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_484_" class="st0" d="M174.4,169.5c-0.1,0-0.3-0.1-0.5-0.1c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0.2,0,0.4,0.1,0.5,0.1C174.4,167.7,174.4,168.6,174.4,169.5z"/>
								</defs>
								<clipPath id="SVGID_485_">
									<use xlink:href="#SVGID_484_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st271">
									<path class="st34" d="M174.4,166.9v2.6c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6
										c0,1.4,0.4,2.4,1.4,3.2c1.4,1.2,3.2,1.6,4.3,1.8C174.1,166.8,174.3,166.8,174.4,166.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_486_" class="st0" d="M173.7,167.7v2.6c-1.1-0.2-3.2-0.6-4.8-2c-1.2-1-1.7-2.3-1.7-4l0-2.6
										c0,1.7,0.6,3,1.7,4C170.5,167.1,172.5,167.5,173.7,167.7z"/>
								</defs>
								<clipPath id="SVGID_487_">
									<use xlink:href="#SVGID_486_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st272">
									<path class="st34" d="M173.7,167.7v2.6c-1.1-0.2-3.2-0.6-4.8-2c-1.2-1-1.7-2.3-1.7-4v-2.6c0,1.7,0.6,3,1.7,4
										C170.5,167.1,172.5,167.5,173.7,167.7"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_488_" class="st0" d="M174.2,167.8v2.6c-0.1,0-0.3-0.1-0.5-0.1v-2.6C173.9,167.8,174,167.8,174.2,167.8z"/>
								</defs>
								<clipPath id="SVGID_489_">
									<use xlink:href="#SVGID_488_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st273">
									<path class="st34" d="M174.2,167.8v2.6c-0.1,0-0.3-0.1-0.5-0.1v-2.6C173.9,167.8,174,167.8,174.2,167.8"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_490_" class="st0" d="M175.3,170.5c0-0.3-0.1-0.9-0.9-1c0-0.8,0-1.5,0-2.3v-0.3c0.8,0.2,0.9,0.7,0.9,1v0.3
										C175.3,168.9,175.3,169.8,175.3,170.5z"/>
								</defs>
								<clipPath id="SVGID_491_">
									<use xlink:href="#SVGID_490_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st274">
									<path class="st34" d="M175.3,167.9v2.6c0-0.3-0.1-0.9-0.9-1v-2.6C175.2,167,175.3,167.6,175.3,167.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_492_" class="st0" d="M181.5,168.1v2.6c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2
										V163c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6C180.9,165.6,181.5,166.7,181.5,168.1z"/>
								</defs>
								<clipPath id="SVGID_493_">
									<use xlink:href="#SVGID_492_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st275">
									<path class="st34" d="M181.5,168.1v2.6c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2V163
										c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6C180.9,165.6,181.5,166.7,181.5,168.1"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_494_" class="st0" d="M175.3,167.9l0,2.6c0,0.3-0.1,1.1-1.4,1.1c-1.2,0-3.2-0.5-5.2-1.7l0-2.6
										c2,1.1,4.1,1.7,5.2,1.7C175.2,169,175.3,168.1,175.3,167.9z"/>
								</defs>
								<clipPath id="SVGID_495_">
									<use xlink:href="#SVGID_494_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st276">
									<path class="st34" d="M175.3,167.9v2.6c0,0,0,0.1,0,0.2V168C175.3,168,175.3,167.9,175.3,167.9"/>
									<path class="st34" d="M175.3,168v2.6c0,0.1,0,0.1,0,0.2v-2.6C175.3,168.2,175.3,168.1,175.3,168"/>
									<path class="st34" d="M175.2,168.2v2.6c0,0.1,0,0.1-0.1,0.2v-2.6C175.2,168.3,175.2,168.3,175.2,168.2"/>
									<path class="st34" d="M175.2,168.4v2.6c-0.2,0.3-0.5,0.6-1.3,0.6c-1.2,0-3.2-0.5-5.2-1.7v-2.6c2,1.1,4.1,1.7,5.2,1.7
										C174.7,169,175,168.7,175.2,168.4"/>
								</g>
							</g>
							<g>
								<polygon class="st34" points="168,172.7 168,175.4 167.7,175.2 167.7,172.6 								"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_496_" class="st0" d="M181.5,170.7c0,3.2-2.6,5.2-6.7,5.2c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c4.2,0,6.7-2,6.7-5.2v0.3C181.5,169.1,181.5,170,181.5,170.7z"/>
								</defs>
								<clipPath id="SVGID_497_">
									<use xlink:href="#SVGID_496_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st277">
									<path class="st34" d="M181.5,168.1v2.6c0,0.3,0,0.5-0.1,0.8v-2.6C181.4,168.6,181.5,168.3,181.5,168.1"/>
									<path class="st34" d="M181.4,168.9v2.6c0,0.3-0.1,0.5-0.2,0.8v-2.6C181.3,169.4,181.4,169.1,181.4,168.9"/>
									<path class="st34" d="M181.2,169.7v2.6c-0.1,0.3-0.2,0.5-0.4,0.8v-2.6C181,170.2,181.1,169.9,181.2,169.7"/>
									<path class="st34" d="M180.9,170.5v2.6c-1,1.7-3.2,2.8-6.1,2.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2
										C177.7,173.2,179.9,172.2,180.9,170.5"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_498_" class="st0" d="M174.7,175.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2c0,0,0,0,0,0
										c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										c0,0.1,0,0.2,0,0.3C174.7,175.6,174.7,175.7,174.7,175.8C174.7,175.8,174.7,175.8,174.7,175.8z"/>
								</defs>
								<clipPath id="SVGID_499_">
									<use xlink:href="#SVGID_498_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st278">
									<path class="st34" d="M180.9,170.5v2.6c-1,1.7-3.2,2.8-6.1,2.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2
										C177.7,173.2,179.9,172.2,180.9,170.5"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M174.8,156.3c1.9,0,4.2,0.5,5.7,1.2v4.6c-1.9-0.9-3.8-1.2-4.8-1.2c-1.3,0-1.4,0.8-1.4,1
									c0,0.5,0.3,0.9,0.9,1.1c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6c1.1,0.9,1.6,2,1.6,3.4c0,3.2-2.6,5.2-6.8,5.2
									c-2.1,0-4.4-0.5-6-1.2v-4.7c2,1.1,4.1,1.7,5.2,1.7c1.3,0,1.4-0.8,1.4-1.1c0-0.3-0.1-0.9-0.9-1c-0.1,0-0.3-0.1-0.5-0.1
									c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2C168.2,158.4,170.8,156.3,174.8,156.3z"/>
							</g>
							<g>
								<path class="st34" d="M180.5,163.9c1.3,1.1,1.9,2.4,1.9,4.1c0,3.7-3,6.2-7.7,6.2c-2.4,0-5-0.6-6.7-1.5l-0.3-0.1v-7.1
									l0.8,0.5c2.2,1.5,4.5,2,5.4,2c0.3,0,0.4-0.1,0.4-0.1c0,0,0,0-0.2-0.1c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-3.2-0.6-4.8-2
									c-1.2-1-1.7-2.3-1.7-4c0-3.9,3-6.4,7.6-6.4c2.2,0,4.8,0.6,6.4,1.4l0.3,0.1v6.8l-0.7-0.4c-2.2-1.1-4.2-1.5-5-1.5
									c-0.3,0-0.4,0.1-0.4,0.1c0,0,0.1,0,0.2,0.1c0.1,0,0.3,0.1,0.5,0.1C177.1,162.4,179.1,162.8,180.5,163.9z M174.7,173.2
									c4.2,0,6.8-2,6.8-5.2c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2c-0.6-0.2-0.9-0.6-0.9-1.1
									c0-0.2,0.1-1,1.4-1c1,0,2.8,0.4,4.8,1.2v-4.6c-1.5-0.7-3.8-1.2-5.7-1.2c-4,0-6.6,2.1-6.6,5.4c0,1.4,0.4,2.4,1.4,3.2
									c1.4,1.2,3.2,1.6,4.3,1.8c0.2,0,0.4,0.1,0.5,0.1c0.8,0.2,0.9,0.7,0.9,1c0,0.3-0.1,1.1-1.4,1.1c-1.2,0-3.2-0.5-5.2-1.7v4.7
									C170.3,172.7,172.6,173.2,174.7,173.2"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_500_" class="st0" d="M182.5,168.1l0,2.6c0,3.7-3,6.2-7.7,6.2c-2.4,0-5-0.6-6.7-1.5v-2.6
										c1.7,0.9,4.4,1.5,6.7,1.5C179.4,174.2,182.5,171.8,182.5,168.1z"/>
								</defs>
								<clipPath id="SVGID_501_">
									<use xlink:href="#SVGID_500_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st279">
									<path class="st34" d="M182.5,168.1v2.6c0,0.3,0,0.6-0.1,1V169C182.4,168.7,182.5,168.4,182.5,168.1"/>
									<path class="st34" d="M182.4,169v2.6c-0.1,0.3-0.1,0.7-0.2,1V170C182.3,169.7,182.3,169.3,182.4,169"/>
									<path class="st34" d="M182.2,170v2.6c-0.1,0.3-0.3,0.7-0.4,1v-2.6C181.9,170.6,182,170.3,182.2,170"/>
									<path class="st34" d="M181.7,170.9v2.6c-1.2,2-3.7,3.3-7,3.3c-2.4,0-5-0.6-6.7-1.5v-2.6c1.7,0.9,4.4,1.5,6.7,1.5
										C178.1,174.2,180.6,173,181.7,170.9"/>
								</g>
							</g>
						</g>
					</g>
					<defs>
						<filter id="Adobe_OpacityMaskFilter_12_" filterUnits="userSpaceOnUse" x="94.1" y="155.3" width="88.4" height="21.5">
							<feFlood  style="flood-color:white;flood-opacity:1" result="back"/>
							<feBlend  in="SourceGraphic" in2="back" mode="normal"/>
						</filter>
					</defs>
					<mask maskUnits="userSpaceOnUse" x="94.1" y="155.3" width="88.4" height="21.5" id="SVGID_463_">
						<g class="st280">
							
								<image style="overflow:visible;" width="93" height="27" xlink:href="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgEASABIAAD/7AARRHVja3kAAQAEAAAAHgAA/+4AIUFkb2JlAGTAAAAAAQMA
EAMCAwYAAAF0AAABfwAAAaD/2wCEABALCwsMCxAMDBAXDw0PFxsUEBAUGx8XFxcXFx8eFxoaGhoX
Hh4jJSclIx4vLzMzLy9AQEBAQEBAQEBAQEBAQEABEQ8PERMRFRISFRQRFBEUGhQWFhQaJhoaHBoa
JjAjHh4eHiMwKy4nJycuKzU1MDA1NUBAP0BAQEBAQEBAQEBAQP/CABEIABsAXQMBIgACEQEDEQH/
xABfAAEBAQAAAAAAAAAAAAAAAAAAAQcBAQAAAAAAAAAAAAAAAAAAAAAQAQEAAAAAAAAAAAAAAAAA
ADBQEQEAAAAAAAAAAAAAAAAAAABQEgEAAAAAAAAAAAAAAAAAAAAw/9oADAMBAAIRAxEAAADPykWA
AABYAAAAAAf/2gAIAQIAAQUAT//aAAgBAwABBQBP/9oACAEBAAEFAKH/2gAIAQICBj8AT//aAAgB
AwIGPwBP/9oACAEBAQY/AEP/2Q==" transform="matrix(1 0 0 1 91.75 152.5)">
							</image>
						</g>
					</mask>
					<g>
						<g class="st0">
							<g>
								<rect x="95.1" y="156.6" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<rect x="106.8" y="156.6" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<polygon class="st34" points="105.8,163.8 105.8,166.4 100.8,159.2 100.8,156.6 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.8,163.8 106.8,166.4 106.1,166.7 106.1,164.1 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.1,164.1 106.1,166.7 105.8,166.4 105.8,163.8 								"/>
							</g>
							<g>
								<polygon class="st34" points="101.2,165.4 101.2,168 100.5,168.4 100.5,165.8 								"/>
							</g>
							<g>
								<polygon class="st34" points="101.5,165.8 101.5,168.4 101.2,168 101.2,165.4 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.5,173 106.5,175.6 101.5,168.4 101.5,165.8 								"/>
							</g>
							<g>
								<rect x="106.5" y="173" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<rect x="95.1" y="173" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M105.8,155.6h7.4V174h-7.3l-4.4-6.4v6.4h-7.4v-18.4h7.3l4.4,6.4V155.6z M112.2,173v-16.4h-5.4v7.2
									l-0.7,0.3l-0.3-0.3l-5-7.2h-5.7V173h5.4v-7.2l0.7-0.3l0.3,0.3l5,7.2H112.2"/>
							</g>
							<g>
								<polygon class="st34" points="112.2,156.6 112.2,173 106.5,173 101.5,165.8 101.2,165.4 100.5,165.8 100.5,173 95.1,173 
									95.1,156.6 100.8,156.6 105.8,163.8 106.1,164.1 106.8,163.8 106.8,156.6 								"/>
							</g>
							<g>
								<polygon class="st34" points="105.9,174 105.9,176.6 101.5,170.2 101.5,167.6 								"/>
							</g>
							<g>
								<rect x="105.9" y="174" class="st34" width="7.3" height="2.6"/>
							</g>
							<g>
								<rect x="94.1" y="174" class="st34" width="7.4" height="2.6"/>
							</g>
						</g>
						<g class="st0">
							<g class="st0">
								<defs>
									<path id="SVGID_502_" class="st0" d="M133.3,164.8l0,2.6c0-4.7-4-8.4-9.2-8.4c-5.2,0-9.2,3.7-9.2,8.4l0-2.6
										c0-4.7,4-8.4,9.2-8.4C129.3,156.3,133.3,160,133.3,164.8z"/>
								</defs>
								<clipPath id="SVGID_503_">
									<use xlink:href="#SVGID_502_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st281">
									<path class="st34" d="M133.3,164.8v2.6c0-4.7-4-8.4-9.2-8.4c-3.6,0-6.6,1.8-8.1,4.5v-2.6c1.5-2.7,4.6-4.5,8.1-4.5
										C129.3,156.3,133.3,160,133.3,164.8"/>
									<path class="st34" d="M116,160.8v2.6c-0.3,0.5-0.5,0.9-0.6,1.4v-2.6C115.5,161.7,115.7,161.3,116,160.8"/>
									<path class="st34" d="M115.3,162.2v2.6c-0.1,0.4-0.3,0.9-0.3,1.3v-2.6C115.1,163.1,115.2,162.7,115.3,162.2"/>
									<path class="st34" d="M115,163.6v2.6c-0.1,0.4-0.1,0.8-0.1,1.2v-2.6C114.9,164.4,115,164,115,163.6"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_504_" class="st0" d="M120.9,167.5c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3v-0.3c0-1.8,1.4-3.2,3.2-3.2
										c1.8,0,3.2,1.4,3.2,3.2l0,2.6c0-1.8-1.4-3.2-3.2-3.2C122.4,164.3,120.9,165.8,120.9,167.5z"/>
								</defs>
								<clipPath id="SVGID_505_">
									<use xlink:href="#SVGID_504_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st282">
									<path class="st34" d="M127.3,164.9v2.6c0-1.8-1.4-3.2-3.2-3.2c-1.2,0-2.2,0.7-2.8,1.6v-2.6c0.5-1,1.6-1.6,2.8-1.6
										C125.9,161.7,127.3,163.1,127.3,164.9"/>
									<path class="st34" d="M121.3,163.3v2.6c-0.1,0.2-0.2,0.4-0.3,0.6v-2.6C121.2,163.7,121.2,163.5,121.3,163.3"/>
									<path class="st34" d="M121.1,163.9v2.6c-0.1,0.2-0.1,0.3-0.1,0.5v-2.6C121,164.2,121,164.1,121.1,163.9"/>
									<path class="st34" d="M121,164.4v2.6c0,0.2,0,0.3,0,0.5v-2.6C120.9,164.7,120.9,164.6,121,164.4"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_506_" class="st0" d="M126.3,164.9l0,2.6c0-1.2-1-2.2-2.2-2.2c-1.2,0-2.2,1-2.2,2.2l0-2.6
										c0-1.2,1-2.2,2.2-2.2C125.3,162.7,126.3,163.7,126.3,164.9z"/>
								</defs>
								<clipPath id="SVGID_507_">
									<use xlink:href="#SVGID_506_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st283">
									<path class="st34" d="M126.3,164.9v2.6c0-1.2-1-2.2-2.2-2.2c-0.8,0-1.5,0.4-1.9,1.1v-2.6c0.4-0.7,1.1-1.1,1.9-1.1
										C125.3,162.7,126.3,163.7,126.3,164.9"/>
									<path class="st34" d="M122.2,163.8v2.6c-0.1,0.1-0.1,0.3-0.2,0.4v-2.6C122.1,164.1,122.1,163.9,122.2,163.8"/>
									<path class="st34" d="M122,164.2v2.6c0,0.1-0.1,0.2-0.1,0.4v-2.6C122,164.4,122,164.3,122,164.2"/>
									<path class="st34" d="M121.9,164.6v2.6c0,0.1,0,0.2,0,0.3v-2.6C121.9,164.8,121.9,164.7,121.9,164.6"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_508_" class="st0" d="M127.3,164.9l0,2.6c0,1.8-1.4,3.2-3.2,3.2c-1.8,0-3.2-1.4-3.2-3.2v-2.6
										c0,1.8,1.4,3.2,3.2,3.2C125.9,168.1,127.3,166.6,127.3,164.9z"/>
								</defs>
								<clipPath id="SVGID_509_">
									<use xlink:href="#SVGID_508_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st284">
									<path class="st34" d="M127.3,164.9v2.6c0,0.2,0,0.3,0,0.5v-2.6C127.3,165.2,127.3,165.1,127.3,164.9"/>
									<path class="st34" d="M127.3,165.4v2.6c0,0.2-0.1,0.4-0.1,0.5v-2.6C127.2,165.7,127.2,165.5,127.3,165.4"/>
									<path class="st34" d="M127.1,165.9v2.6c-0.1,0.2-0.2,0.4-0.3,0.6v-2.6C127,166.3,127.1,166.1,127.1,165.9"/>
									<path class="st34" d="M126.9,166.5v2.6c-0.5,1-1.6,1.6-2.8,1.6c-1.8,0-3.2-1.4-3.2-3.2v-2.6c0,1.8,1.4,3.2,3.2,3.2
										C125.3,168.1,126.3,167.4,126.9,166.5"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M124.1,161.7c1.8,0,3.2,1.4,3.2,3.2c0,1.8-1.4,3.2-3.2,3.2c-1.8,0-3.2-1.4-3.2-3.2
									C120.9,163.1,122.4,161.7,124.1,161.7z M124.1,167.1c1.2,0,2.2-1,2.2-2.2c0-1.2-1-2.2-2.2-2.2c-1.2,0-2.2,1-2.2,2.2
									C121.9,166.1,122.9,167.1,124.1,167.1"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_510_" class="st0" d="M124.1,175.8c-5.2,0-9.2-3.7-9.2-8.4l0-2.6c0,4.7,4,8.4,9.2,8.4
										c5.2,0,9.2-3.7,9.2-8.4l0,0.3v2.3C133.3,172.1,129.3,175.8,124.1,175.8z"/>
								</defs>
								<clipPath id="SVGID_511_">
									<use xlink:href="#SVGID_510_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st285">
									<path class="st34" d="M133.3,164.8v2.6c0,0.4,0,0.8-0.1,1.2V166C133.3,165.6,133.3,165.2,133.3,164.8"/>
									<path class="st34" d="M133.2,166v2.6c-0.1,0.5-0.2,0.9-0.3,1.3v-2.6C133,166.9,133.2,166.4,133.2,166"/>
									<path class="st34" d="M132.9,167.3v2.6c-0.2,0.5-0.4,1-0.6,1.4v-2.6C132.5,168.3,132.7,167.8,132.9,167.3"/>
									<path class="st34" d="M132.3,168.8v2.6c-1.5,2.7-4.6,4.5-8.1,4.5c-5.2,0-9.2-3.7-9.2-8.4v-2.6c0,4.7,4,8.4,9.2,8.4
										C127.7,173.2,130.7,171.4,132.3,168.8"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M124.1,155.3c5.7,0,10.2,4.2,10.2,9.4c0,5.3-4.5,9.4-10.2,9.4c-5.7,0-10.2-4.1-10.2-9.4
									C113.9,159.5,118.4,155.3,124.1,155.3z M124.1,173.2c5.2,0,9.2-3.7,9.2-8.4c0-4.7-4-8.4-9.2-8.4c-5.2,0-9.2,3.7-9.2,8.4
									C114.9,169.5,119,173.2,124.1,173.2"/>
							</g>
							<g>
								<path class="st34" d="M124.1,156.3c5.2,0,9.2,3.7,9.2,8.4c0,4.7-4,8.4-9.2,8.4c-5.2,0-9.2-3.7-9.2-8.4
									C114.9,160,119,156.3,124.1,156.3z M124.1,168.1c1.8,0,3.2-1.4,3.2-3.2c0-1.8-1.4-3.2-3.2-3.2c-1.8,0-3.2,1.4-3.2,3.2
									C120.9,166.6,122.4,168.1,124.1,168.1"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_512_" class="st0" d="M134.3,164.8v2.6c0,5.3-4.5,9.4-10.2,9.4c-5.7,0-10.2-4.1-10.2-9.4l0-2.6
										c0,5.3,4.5,9.4,10.2,9.4C129.8,174.2,134.3,170.1,134.3,164.8z"/>
								</defs>
								<clipPath id="SVGID_513_">
									<use xlink:href="#SVGID_512_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st286">
									<path class="st34" d="M134.3,164.8v2.6c0,0.5,0,0.9-0.1,1.4v-2.6C134.3,165.7,134.3,165.2,134.3,164.8"/>
									<path class="st34" d="M134.2,166.1v2.6c-0.1,0.5-0.2,1-0.4,1.5v-2.6C134,167.2,134.1,166.7,134.2,166.1"/>
									<path class="st34" d="M133.9,167.6v2.6c-0.2,0.6-0.4,1.1-0.7,1.6v-2.6C133.4,168.7,133.7,168.2,133.9,167.6"/>
									<path class="st34" d="M133.1,169.3v2.6c-1.7,3-5.1,5-9,5c-5.7,0-10.2-4.1-10.2-9.4v-2.6c0,5.3,4.5,9.4,10.2,9.4
										C128.1,174.2,131.4,172.2,133.1,169.3"/>
								</g>
							</g>
						</g>
						<g class="st0">
							<g>
								<rect x="135.3" y="156.6" class="st34" width="15.8" height="2.6"/>
							</g>
							<g>
								<rect x="135.3" y="161.5" class="st34" width="4.9" height="2.6"/>
							</g>
							<g>
								<rect x="146.3" y="161.5" class="st34" width="4.8" height="2.6"/>
							</g>
							<g>
								<rect x="147.3" y="162.5" class="st34" width="4.8" height="2.6"/>
							</g>
							<g>
								<rect x="134.3" y="162.5" class="st34" width="4.9" height="2.6"/>
							</g>
							<g>
								<rect x="140.2" y="173" class="st34" width="6.2" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M134.3,155.6h17.8v6.9h-4.8V174h-8.2v-11.5h-4.9V155.6z M151.2,161.5v-4.9h-15.8v4.9h4.9V173h6.2
									v-11.5H151.2"/>
							</g>
							<g>
								<polygon class="st34" points="151.2,156.6 151.2,161.5 146.3,161.5 146.3,173 140.2,173 140.2,161.5 135.3,161.5 
									135.3,156.6 								"/>
							</g>
							<g>
								<rect x="139.2" y="174" class="st34" width="8.2" height="2.6"/>
							</g>
						</g>
						<g class="st0">
							<g>
								<rect x="154" y="156.6" class="st34" width="11.5" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="161.4" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<rect x="165.5" y="162.4" class="st34" width="1" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="163.3" class="st34" width="4.3" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="166.5" class="st34" width="4.3" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="168.4" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<polygon class="st34" points="165.5,156.6 165.5,161.4 160.2,161.4 160.2,163.3 164.5,163.3 164.5,166.5 160.2,166.5 
									160.2,168.4 165.9,168.4 165.9,173 154,173 154,156.6 								"/>
							</g>
							<g>
								<rect x="154" y="173" class="st34" width="11.8" height="2.6"/>
							</g>
							<g>
								<rect x="153" y="174" class="st34" width="13.8" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M165.5,167.4h1.4v6.5H153v-18.4h13.5v6.8h-1V167.4z M165.9,173v-4.5h-5.7v-2h4.3v-3.1h-4.3v-1.9h5.4
									v-4.8H154V173H165.9"/>
							</g>
						</g>
						<g class="st0">
							<g class="st0">
								<defs>
									<path id="SVGID_514_" class="st0" d="M180.5,157.5l0,2.6c-1.5-0.7-3.8-1.2-5.7-1.2c-4,0-6.6,2.1-6.6,5.4l0-2.6
										c0-3.3,2.6-5.4,6.6-5.4C176.7,156.3,178.9,156.8,180.5,157.5z"/>
								</defs>
								<clipPath id="SVGID_515_">
									<use xlink:href="#SVGID_514_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st287">
									<path class="st34" d="M180.5,157.5v2.6c-1.5-0.7-3.8-1.2-5.7-1.2c-2.8,0-4.9,1-5.9,2.8v-2.6c1-1.8,3.1-2.8,5.9-2.8
										C176.7,156.3,178.9,156.8,180.5,157.5"/>
									<path class="st34" d="M168.8,159.1v2.6c-0.2,0.3-0.3,0.6-0.4,0.9V160C168.6,159.7,168.7,159.4,168.8,159.1"/>
									<path class="st34" d="M168.5,160v2.6c-0.1,0.3-0.2,0.6-0.2,0.9v-2.6C168.3,160.5,168.4,160.3,168.5,160"/>
									<path class="st34" d="M168.3,160.8v2.6c0,0.3-0.1,0.6-0.1,0.8v-2.6C168.2,161.4,168.2,161.1,168.3,160.8"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_516_" class="st0" d="M175.7,163.5c-1.3,0-1.4,0.8-1.4,1v-2.6c0-0.2,0.1-1,1.4-1c1,0,2.8,0.4,4.8,1.2v2.6
										C178.5,163.9,176.7,163.5,175.7,163.5z"/>
								</defs>
								<clipPath id="SVGID_517_">
									<use xlink:href="#SVGID_516_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st288">
									<path class="st34" d="M180.5,162.1v2.6c-1.9-0.9-3.8-1.2-4.8-1.2c-0.8,0-1.1,0.3-1.3,0.5v-2.6c0.2-0.3,0.5-0.5,1.3-0.5
										C176.7,160.8,178.5,161.2,180.5,162.1"/>
									<path class="st34" d="M174.5,161.4v2.6c0,0.1-0.1,0.1-0.1,0.2v-2.6C174.4,161.5,174.4,161.5,174.5,161.4"/>
									<path class="st34" d="M174.4,161.6v2.6c0,0.1,0,0.1,0,0.2v-2.6C174.3,161.7,174.4,161.6,174.4,161.6"/>
									<path class="st34" d="M174.3,161.7v2.6c0,0.1,0,0.1,0,0.1v-2.6C174.3,161.8,174.3,161.8,174.3,161.7"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_518_" class="st0" d="M175.2,163v2.6c-0.6-0.2-0.9-0.6-0.9-1.1l0-2.6C174.3,162.4,174.6,162.7,175.2,163z"
										/>
								</defs>
								<clipPath id="SVGID_519_">
									<use xlink:href="#SVGID_518_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st289">
									<path class="st34" d="M175.2,163v2.6c-0.6-0.2-0.9-0.6-0.9-1.1v-2.6C174.3,162.4,174.6,162.7,175.2,163"/>
								</g>
							</g>
							<g>
								<polygon class="st34" points="181.5,163.7 181.5,166.3 180.7,165.9 180.7,163.3 								"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_520_" class="st0" d="M169.6,167.5c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6c0,1.4,0.4,2.4,1.4,3.2
										C169.6,165.8,169.6,166.7,169.6,167.5c0-0.9,0-1.7,0-2.6c1.4,1.2,3.2,1.6,4.3,1.8c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										C172.8,169.1,171,168.8,169.6,167.5z"/>
								</defs>
								<clipPath id="SVGID_521_">
									<use xlink:href="#SVGID_520_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st290">
									<path class="st34" d="M174.4,166.9v2.6c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6
										c0,1.4,0.4,2.4,1.4,3.2c1.4,1.2,3.2,1.6,4.3,1.8C174.1,166.8,174.3,166.8,174.4,166.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_522_" class="st0" d="M174.4,169.5c-0.1,0-0.3-0.1-0.5-0.1c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0.2,0,0.4,0.1,0.5,0.1C174.4,167.7,174.4,168.6,174.4,169.5z"/>
								</defs>
								<clipPath id="SVGID_523_">
									<use xlink:href="#SVGID_522_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st291">
									<path class="st34" d="M174.4,166.9v2.6c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6
										c0,1.4,0.4,2.4,1.4,3.2c1.4,1.2,3.2,1.6,4.3,1.8C174.1,166.8,174.3,166.8,174.4,166.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_524_" class="st0" d="M173.7,167.7v2.6c-1.1-0.2-3.2-0.6-4.8-2c-1.2-1-1.7-2.3-1.7-4l0-2.6
										c0,1.7,0.6,3,1.7,4C170.5,167.1,172.5,167.5,173.7,167.7z"/>
								</defs>
								<clipPath id="SVGID_525_">
									<use xlink:href="#SVGID_524_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st292">
									<path class="st34" d="M173.7,167.7v2.6c-1.1-0.2-3.2-0.6-4.8-2c-1.2-1-1.7-2.3-1.7-4v-2.6c0,1.7,0.6,3,1.7,4
										C170.5,167.1,172.5,167.5,173.7,167.7"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_526_" class="st0" d="M174.2,167.8v2.6c-0.1,0-0.3-0.1-0.5-0.1v-2.6C173.9,167.8,174,167.8,174.2,167.8z"/>
								</defs>
								<clipPath id="SVGID_527_">
									<use xlink:href="#SVGID_526_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st293">
									<path class="st34" d="M174.2,167.8v2.6c-0.1,0-0.3-0.1-0.5-0.1v-2.6C173.9,167.8,174,167.8,174.2,167.8"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_528_" class="st0" d="M175.3,170.5c0-0.3-0.1-0.9-0.9-1c0-0.8,0-1.5,0-2.3v-0.3c0.8,0.2,0.9,0.7,0.9,1v0.3
										C175.3,168.9,175.3,169.8,175.3,170.5z"/>
								</defs>
								<clipPath id="SVGID_529_">
									<use xlink:href="#SVGID_528_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st294">
									<path class="st34" d="M175.3,167.9v2.6c0-0.3-0.1-0.9-0.9-1v-2.6C175.2,167,175.3,167.6,175.3,167.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_530_" class="st0" d="M181.5,168.1v2.6c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2
										V163c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6C180.9,165.6,181.5,166.7,181.5,168.1z"/>
								</defs>
								<clipPath id="SVGID_531_">
									<use xlink:href="#SVGID_530_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st295">
									<path class="st34" d="M181.5,168.1v2.6c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2V163
										c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6C180.9,165.6,181.5,166.7,181.5,168.1"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_532_" class="st0" d="M175.3,167.9l0,2.6c0,0.3-0.1,1.1-1.4,1.1c-1.2,0-3.2-0.5-5.2-1.7l0-2.6
										c2,1.1,4.1,1.7,5.2,1.7C175.2,169,175.3,168.1,175.3,167.9z"/>
								</defs>
								<clipPath id="SVGID_533_">
									<use xlink:href="#SVGID_532_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st296">
									<path class="st34" d="M175.3,167.9v2.6c0,0,0,0.1,0,0.2V168C175.3,168,175.3,167.9,175.3,167.9"/>
									<path class="st34" d="M175.3,168v2.6c0,0.1,0,0.1,0,0.2v-2.6C175.3,168.2,175.3,168.1,175.3,168"/>
									<path class="st34" d="M175.2,168.2v2.6c0,0.1,0,0.1-0.1,0.2v-2.6C175.2,168.3,175.2,168.3,175.2,168.2"/>
									<path class="st34" d="M175.2,168.4v2.6c-0.2,0.3-0.5,0.6-1.3,0.6c-1.2,0-3.2-0.5-5.2-1.7v-2.6c2,1.1,4.1,1.7,5.2,1.7
										C174.7,169,175,168.7,175.2,168.4"/>
								</g>
							</g>
							<g>
								<polygon class="st34" points="168,172.7 168,175.4 167.7,175.2 167.7,172.6 								"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_534_" class="st0" d="M181.5,170.7c0,3.2-2.6,5.2-6.7,5.2c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c4.2,0,6.7-2,6.7-5.2v0.3C181.5,169.1,181.5,170,181.5,170.7z"/>
								</defs>
								<clipPath id="SVGID_535_">
									<use xlink:href="#SVGID_534_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st297">
									<path class="st34" d="M181.5,168.1v2.6c0,0.3,0,0.5-0.1,0.8v-2.6C181.4,168.6,181.5,168.3,181.5,168.1"/>
									<path class="st34" d="M181.4,168.9v2.6c0,0.3-0.1,0.5-0.2,0.8v-2.6C181.3,169.4,181.4,169.1,181.4,168.9"/>
									<path class="st34" d="M181.2,169.7v2.6c-0.1,0.3-0.2,0.5-0.4,0.8v-2.6C181,170.2,181.1,169.9,181.2,169.7"/>
									<path class="st34" d="M180.9,170.5v2.6c-1,1.7-3.2,2.8-6.1,2.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2
										C177.7,173.2,179.9,172.2,180.9,170.5"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_536_" class="st0" d="M174.7,175.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2c0,0,0,0,0,0
										c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										c0,0.1,0,0.2,0,0.3C174.7,175.6,174.7,175.7,174.7,175.8C174.7,175.8,174.7,175.8,174.7,175.8z"/>
								</defs>
								<clipPath id="SVGID_537_">
									<use xlink:href="#SVGID_536_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st298">
									<path class="st34" d="M180.9,170.5v2.6c-1,1.7-3.2,2.8-6.1,2.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2
										C177.7,173.2,179.9,172.2,180.9,170.5"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M174.8,156.3c1.9,0,4.2,0.5,5.7,1.2v4.6c-1.9-0.9-3.8-1.2-4.8-1.2c-1.3,0-1.4,0.8-1.4,1
									c0,0.5,0.3,0.9,0.9,1.1c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6c1.1,0.9,1.6,2,1.6,3.4c0,3.2-2.6,5.2-6.8,5.2
									c-2.1,0-4.4-0.5-6-1.2v-4.7c2,1.1,4.1,1.7,5.2,1.7c1.3,0,1.4-0.8,1.4-1.1c0-0.3-0.1-0.9-0.9-1c-0.1,0-0.3-0.1-0.5-0.1
									c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2C168.2,158.4,170.8,156.3,174.8,156.3z"/>
							</g>
							<g>
								<path class="st34" d="M180.5,163.9c1.3,1.1,1.9,2.4,1.9,4.1c0,3.7-3,6.2-7.7,6.2c-2.4,0-5-0.6-6.7-1.5l-0.3-0.1v-7.1
									l0.8,0.5c2.2,1.5,4.5,2,5.4,2c0.3,0,0.4-0.1,0.4-0.1c0,0,0,0-0.2-0.1c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-3.2-0.6-4.8-2
									c-1.2-1-1.7-2.3-1.7-4c0-3.9,3-6.4,7.6-6.4c2.2,0,4.8,0.6,6.4,1.4l0.3,0.1v6.8l-0.7-0.4c-2.2-1.1-4.2-1.5-5-1.5
									c-0.3,0-0.4,0.1-0.4,0.1c0,0,0.1,0,0.2,0.1c0.1,0,0.3,0.1,0.5,0.1C177.1,162.4,179.1,162.8,180.5,163.9z M174.7,173.2
									c4.2,0,6.8-2,6.8-5.2c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2c-0.6-0.2-0.9-0.6-0.9-1.1
									c0-0.2,0.1-1,1.4-1c1,0,2.8,0.4,4.8,1.2v-4.6c-1.5-0.7-3.8-1.2-5.7-1.2c-4,0-6.6,2.1-6.6,5.4c0,1.4,0.4,2.4,1.4,3.2
									c1.4,1.2,3.2,1.6,4.3,1.8c0.2,0,0.4,0.1,0.5,0.1c0.8,0.2,0.9,0.7,0.9,1c0,0.3-0.1,1.1-1.4,1.1c-1.2,0-3.2-0.5-5.2-1.7v4.7
									C170.3,172.7,172.6,173.2,174.7,173.2"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_538_" class="st0" d="M182.5,168.1l0,2.6c0,3.7-3,6.2-7.7,6.2c-2.4,0-5-0.6-6.7-1.5v-2.6
										c1.7,0.9,4.4,1.5,6.7,1.5C179.4,174.2,182.5,171.8,182.5,168.1z"/>
								</defs>
								<clipPath id="SVGID_539_">
									<use xlink:href="#SVGID_538_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st299">
									<path class="st34" d="M182.5,168.1v2.6c0,0.3,0,0.6-0.1,1V169C182.4,168.7,182.5,168.4,182.5,168.1"/>
									<path class="st34" d="M182.4,169v2.6c-0.1,0.3-0.1,0.7-0.2,1V170C182.3,169.7,182.3,169.3,182.4,169"/>
									<path class="st34" d="M182.2,170v2.6c-0.1,0.3-0.3,0.7-0.4,1v-2.6C181.9,170.6,182,170.3,182.2,170"/>
									<path class="st34" d="M181.7,170.9v2.6c-1.2,2-3.7,3.3-7,3.3c-2.4,0-5-0.6-6.7-1.5v-2.6c1.7,0.9,4.4,1.5,6.7,1.5
										C178.1,174.2,180.6,173,181.7,170.9"/>
								</g>
							</g>
						</g>
					</g>
				</g>
				<defs>
					<filter id="Adobe_OpacityMaskFilter_13_" filterUnits="userSpaceOnUse" x="94.1" y="155.3" width="88.4" height="21.5">
						<feFlood  style="flood-color:white;flood-opacity:1" result="back"/>
						<feBlend  in="SourceGraphic" in2="back" mode="normal"/>
					</filter>
				</defs>
				<mask maskUnits="userSpaceOnUse" x="94.1" y="155.3" width="88.4" height="21.5" id="SVGID_540_">
					<g class="st300">
						
							<image style="overflow:visible;" width="93" height="27" xlink:href="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgEASABIAAD/7AARRHVja3kAAQAEAAAAHgAA/+4AIUFkb2JlAGTAAAAAAQMA
EAMCAwYAAAF0AAABfwAAAaD/2wCEABALCwsMCxAMDBAXDw0PFxsUEBAUGx8XFxcXFx8eFxoaGhoX
Hh4jJSclIx4vLzMzLy9AQEBAQEBAQEBAQEBAQEABEQ8PERMRFRISFRQRFBEUGhQWFhQaJhoaHBoa
JjAjHh4eHiMwKy4nJycuKzU1MDA1NUBAP0BAQEBAQEBAQEBAQP/CABEIABsAXQMBIgACEQEDEQH/
xABfAAEBAQAAAAAAAAAAAAAAAAAAAQcBAQAAAAAAAAAAAAAAAAAAAAAQAQEAAAAAAAAAAAAAAAAA
ADBQEQEAAAAAAAAAAAAAAAAAAABQEgEAAAAAAAAAAAAAAAAAAAAw/9oADAMBAAIRAxEAAADPykWA
AABYAAAAAAf/2gAIAQIAAQUAT//aAAgBAwABBQBP/9oACAEBAAEFAKH/2gAIAQICBj8AT//aAAgB
AwIGPwBP/9oACAEBAQY/AEP/2Q==" transform="matrix(1 0 0 1 91.75 152.5)">
						</image>
					</g>
				</mask>
				<g class="st301">
					<g>
						<g class="st0">
							<g>
								<rect x="95.1" y="156.6" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<rect x="106.8" y="156.6" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<polygon class="st34" points="105.8,163.8 105.8,166.4 100.8,159.2 100.8,156.6 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.8,163.8 106.8,166.4 106.1,166.7 106.1,164.1 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.1,164.1 106.1,166.7 105.8,166.4 105.8,163.8 								"/>
							</g>
							<g>
								<polygon class="st34" points="101.2,165.4 101.2,168 100.5,168.4 100.5,165.8 								"/>
							</g>
							<g>
								<polygon class="st34" points="101.5,165.8 101.5,168.4 101.2,168 101.2,165.4 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.5,173 106.5,175.6 101.5,168.4 101.5,165.8 								"/>
							</g>
							<g>
								<rect x="106.5" y="173" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<rect x="95.1" y="173" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M105.8,155.6h7.4V174h-7.3l-4.4-6.4v6.4h-7.4v-18.4h7.3l4.4,6.4V155.6z M112.2,173v-16.4h-5.4v7.2
									l-0.7,0.3l-0.3-0.3l-5-7.2h-5.7V173h5.4v-7.2l0.7-0.3l0.3,0.3l5,7.2H112.2"/>
							</g>
							<g>
								<polygon class="st34" points="112.2,156.6 112.2,173 106.5,173 101.5,165.8 101.2,165.4 100.5,165.8 100.5,173 95.1,173 
									95.1,156.6 100.8,156.6 105.8,163.8 106.1,164.1 106.8,163.8 106.8,156.6 								"/>
							</g>
							<g>
								<polygon class="st34" points="105.9,174 105.9,176.6 101.5,170.2 101.5,167.6 								"/>
							</g>
							<g>
								<rect x="105.9" y="174" class="st34" width="7.3" height="2.6"/>
							</g>
							<g>
								<rect x="94.1" y="174" class="st34" width="7.4" height="2.6"/>
							</g>
						</g>
						<g class="st0">
							<g class="st0">
								<defs>
									<path id="SVGID_541_" class="st0" d="M133.3,164.8l0,2.6c0-4.7-4-8.4-9.2-8.4c-5.2,0-9.2,3.7-9.2,8.4l0-2.6
										c0-4.7,4-8.4,9.2-8.4C129.3,156.3,133.3,160,133.3,164.8z"/>
								</defs>
								<clipPath id="SVGID_542_">
									<use xlink:href="#SVGID_541_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st302">
									<path class="st34" d="M133.3,164.8v2.6c0-4.7-4-8.4-9.2-8.4c-3.6,0-6.6,1.8-8.1,4.5v-2.6c1.5-2.7,4.6-4.5,8.1-4.5
										C129.3,156.3,133.3,160,133.3,164.8"/>
									<path class="st34" d="M116,160.8v2.6c-0.3,0.5-0.5,0.9-0.6,1.4v-2.6C115.5,161.7,115.7,161.3,116,160.8"/>
									<path class="st34" d="M115.3,162.2v2.6c-0.1,0.4-0.3,0.9-0.3,1.3v-2.6C115.1,163.1,115.2,162.7,115.3,162.2"/>
									<path class="st34" d="M115,163.6v2.6c-0.1,0.4-0.1,0.8-0.1,1.2v-2.6C114.9,164.4,115,164,115,163.6"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_543_" class="st0" d="M120.9,167.5c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3v-0.3c0-1.8,1.4-3.2,3.2-3.2
										c1.8,0,3.2,1.4,3.2,3.2l0,2.6c0-1.8-1.4-3.2-3.2-3.2C122.4,164.3,120.9,165.8,120.9,167.5z"/>
								</defs>
								<clipPath id="SVGID_544_">
									<use xlink:href="#SVGID_543_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st303">
									<path class="st34" d="M127.3,164.9v2.6c0-1.8-1.4-3.2-3.2-3.2c-1.2,0-2.2,0.7-2.8,1.6v-2.6c0.5-1,1.6-1.6,2.8-1.6
										C125.9,161.7,127.3,163.1,127.3,164.9"/>
									<path class="st34" d="M121.3,163.3v2.6c-0.1,0.2-0.2,0.4-0.3,0.6v-2.6C121.2,163.7,121.2,163.5,121.3,163.3"/>
									<path class="st34" d="M121.1,163.9v2.6c-0.1,0.2-0.1,0.3-0.1,0.5v-2.6C121,164.2,121,164.1,121.1,163.9"/>
									<path class="st34" d="M121,164.4v2.6c0,0.2,0,0.3,0,0.5v-2.6C120.9,164.7,120.9,164.6,121,164.4"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_545_" class="st0" d="M126.3,164.9l0,2.6c0-1.2-1-2.2-2.2-2.2c-1.2,0-2.2,1-2.2,2.2l0-2.6
										c0-1.2,1-2.2,2.2-2.2C125.3,162.7,126.3,163.7,126.3,164.9z"/>
								</defs>
								<clipPath id="SVGID_546_">
									<use xlink:href="#SVGID_545_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st304">
									<path class="st34" d="M126.3,164.9v2.6c0-1.2-1-2.2-2.2-2.2c-0.8,0-1.5,0.4-1.9,1.1v-2.6c0.4-0.7,1.1-1.1,1.9-1.1
										C125.3,162.7,126.3,163.7,126.3,164.9"/>
									<path class="st34" d="M122.2,163.8v2.6c-0.1,0.1-0.1,0.3-0.2,0.4v-2.6C122.1,164.1,122.1,163.9,122.2,163.8"/>
									<path class="st34" d="M122,164.2v2.6c0,0.1-0.1,0.2-0.1,0.4v-2.6C122,164.4,122,164.3,122,164.2"/>
									<path class="st34" d="M121.9,164.6v2.6c0,0.1,0,0.2,0,0.3v-2.6C121.9,164.8,121.9,164.7,121.9,164.6"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_547_" class="st0" d="M127.3,164.9l0,2.6c0,1.8-1.4,3.2-3.2,3.2c-1.8,0-3.2-1.4-3.2-3.2v-2.6
										c0,1.8,1.4,3.2,3.2,3.2C125.9,168.1,127.3,166.6,127.3,164.9z"/>
								</defs>
								<clipPath id="SVGID_548_">
									<use xlink:href="#SVGID_547_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st305">
									<path class="st34" d="M127.3,164.9v2.6c0,0.2,0,0.3,0,0.5v-2.6C127.3,165.2,127.3,165.1,127.3,164.9"/>
									<path class="st34" d="M127.3,165.4v2.6c0,0.2-0.1,0.4-0.1,0.5v-2.6C127.2,165.7,127.2,165.5,127.3,165.4"/>
									<path class="st34" d="M127.1,165.9v2.6c-0.1,0.2-0.2,0.4-0.3,0.6v-2.6C127,166.3,127.1,166.1,127.1,165.9"/>
									<path class="st34" d="M126.9,166.5v2.6c-0.5,1-1.6,1.6-2.8,1.6c-1.8,0-3.2-1.4-3.2-3.2v-2.6c0,1.8,1.4,3.2,3.2,3.2
										C125.3,168.1,126.3,167.4,126.9,166.5"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M124.1,161.7c1.8,0,3.2,1.4,3.2,3.2c0,1.8-1.4,3.2-3.2,3.2c-1.8,0-3.2-1.4-3.2-3.2
									C120.9,163.1,122.4,161.7,124.1,161.7z M124.1,167.1c1.2,0,2.2-1,2.2-2.2c0-1.2-1-2.2-2.2-2.2c-1.2,0-2.2,1-2.2,2.2
									C121.9,166.1,122.9,167.1,124.1,167.1"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_549_" class="st0" d="M124.1,175.8c-5.2,0-9.2-3.7-9.2-8.4l0-2.6c0,4.7,4,8.4,9.2,8.4
										c5.2,0,9.2-3.7,9.2-8.4l0,0.3v2.3C133.3,172.1,129.3,175.8,124.1,175.8z"/>
								</defs>
								<clipPath id="SVGID_550_">
									<use xlink:href="#SVGID_549_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st306">
									<path class="st34" d="M133.3,164.8v2.6c0,0.4,0,0.8-0.1,1.2V166C133.3,165.6,133.3,165.2,133.3,164.8"/>
									<path class="st34" d="M133.2,166v2.6c-0.1,0.5-0.2,0.9-0.3,1.3v-2.6C133,166.9,133.2,166.4,133.2,166"/>
									<path class="st34" d="M132.9,167.3v2.6c-0.2,0.5-0.4,1-0.6,1.4v-2.6C132.5,168.3,132.7,167.8,132.9,167.3"/>
									<path class="st34" d="M132.3,168.8v2.6c-1.5,2.7-4.6,4.5-8.1,4.5c-5.2,0-9.2-3.7-9.2-8.4v-2.6c0,4.7,4,8.4,9.2,8.4
										C127.7,173.2,130.7,171.4,132.3,168.8"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M124.1,155.3c5.7,0,10.2,4.2,10.2,9.4c0,5.3-4.5,9.4-10.2,9.4c-5.7,0-10.2-4.1-10.2-9.4
									C113.9,159.5,118.4,155.3,124.1,155.3z M124.1,173.2c5.2,0,9.2-3.7,9.2-8.4c0-4.7-4-8.4-9.2-8.4c-5.2,0-9.2,3.7-9.2,8.4
									C114.9,169.5,119,173.2,124.1,173.2"/>
							</g>
							<g>
								<path class="st34" d="M124.1,156.3c5.2,0,9.2,3.7,9.2,8.4c0,4.7-4,8.4-9.2,8.4c-5.2,0-9.2-3.7-9.2-8.4
									C114.9,160,119,156.3,124.1,156.3z M124.1,168.1c1.8,0,3.2-1.4,3.2-3.2c0-1.8-1.4-3.2-3.2-3.2c-1.8,0-3.2,1.4-3.2,3.2
									C120.9,166.6,122.4,168.1,124.1,168.1"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_551_" class="st0" d="M134.3,164.8v2.6c0,5.3-4.5,9.4-10.2,9.4c-5.7,0-10.2-4.1-10.2-9.4l0-2.6
										c0,5.3,4.5,9.4,10.2,9.4C129.8,174.2,134.3,170.1,134.3,164.8z"/>
								</defs>
								<clipPath id="SVGID_552_">
									<use xlink:href="#SVGID_551_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st307">
									<path class="st34" d="M134.3,164.8v2.6c0,0.5,0,0.9-0.1,1.4v-2.6C134.3,165.7,134.3,165.2,134.3,164.8"/>
									<path class="st34" d="M134.2,166.1v2.6c-0.1,0.5-0.2,1-0.4,1.5v-2.6C134,167.2,134.1,166.7,134.2,166.1"/>
									<path class="st34" d="M133.9,167.6v2.6c-0.2,0.6-0.4,1.1-0.7,1.6v-2.6C133.4,168.7,133.7,168.2,133.9,167.6"/>
									<path class="st34" d="M133.1,169.3v2.6c-1.7,3-5.1,5-9,5c-5.7,0-10.2-4.1-10.2-9.4v-2.6c0,5.3,4.5,9.4,10.2,9.4
										C128.1,174.2,131.4,172.2,133.1,169.3"/>
								</g>
							</g>
						</g>
						<g class="st0">
							<g>
								<rect x="135.3" y="156.6" class="st34" width="15.8" height="2.6"/>
							</g>
							<g>
								<rect x="135.3" y="161.5" class="st34" width="4.9" height="2.6"/>
							</g>
							<g>
								<rect x="146.3" y="161.5" class="st34" width="4.8" height="2.6"/>
							</g>
							<g>
								<rect x="147.3" y="162.5" class="st34" width="4.8" height="2.6"/>
							</g>
							<g>
								<rect x="134.3" y="162.5" class="st34" width="4.9" height="2.6"/>
							</g>
							<g>
								<rect x="140.2" y="173" class="st34" width="6.2" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M134.3,155.6h17.8v6.9h-4.8V174h-8.2v-11.5h-4.9V155.6z M151.2,161.5v-4.9h-15.8v4.9h4.9V173h6.2
									v-11.5H151.2"/>
							</g>
							<g>
								<polygon class="st34" points="151.2,156.6 151.2,161.5 146.3,161.5 146.3,173 140.2,173 140.2,161.5 135.3,161.5 
									135.3,156.6 								"/>
							</g>
							<g>
								<rect x="139.2" y="174" class="st34" width="8.2" height="2.6"/>
							</g>
						</g>
						<g class="st0">
							<g>
								<rect x="154" y="156.6" class="st34" width="11.5" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="161.4" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<rect x="165.5" y="162.4" class="st34" width="1" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="163.3" class="st34" width="4.3" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="166.5" class="st34" width="4.3" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="168.4" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<polygon class="st34" points="165.5,156.6 165.5,161.4 160.2,161.4 160.2,163.3 164.5,163.3 164.5,166.5 160.2,166.5 
									160.2,168.4 165.9,168.4 165.9,173 154,173 154,156.6 								"/>
							</g>
							<g>
								<rect x="154" y="173" class="st34" width="11.8" height="2.6"/>
							</g>
							<g>
								<rect x="153" y="174" class="st34" width="13.8" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M165.5,167.4h1.4v6.5H153v-18.4h13.5v6.8h-1V167.4z M165.9,173v-4.5h-5.7v-2h4.3v-3.1h-4.3v-1.9h5.4
									v-4.8H154V173H165.9"/>
							</g>
						</g>
						<g class="st0">
							<g class="st0">
								<defs>
									<path id="SVGID_553_" class="st0" d="M180.5,157.5l0,2.6c-1.5-0.7-3.8-1.2-5.7-1.2c-4,0-6.6,2.1-6.6,5.4l0-2.6
										c0-3.3,2.6-5.4,6.6-5.4C176.7,156.3,178.9,156.8,180.5,157.5z"/>
								</defs>
								<clipPath id="SVGID_554_">
									<use xlink:href="#SVGID_553_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st308">
									<path class="st34" d="M180.5,157.5v2.6c-1.5-0.7-3.8-1.2-5.7-1.2c-2.8,0-4.9,1-5.9,2.8v-2.6c1-1.8,3.1-2.8,5.9-2.8
										C176.7,156.3,178.9,156.8,180.5,157.5"/>
									<path class="st34" d="M168.8,159.1v2.6c-0.2,0.3-0.3,0.6-0.4,0.9V160C168.6,159.7,168.7,159.4,168.8,159.1"/>
									<path class="st34" d="M168.5,160v2.6c-0.1,0.3-0.2,0.6-0.2,0.9v-2.6C168.3,160.5,168.4,160.3,168.5,160"/>
									<path class="st34" d="M168.3,160.8v2.6c0,0.3-0.1,0.6-0.1,0.8v-2.6C168.2,161.4,168.2,161.1,168.3,160.8"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_555_" class="st0" d="M175.7,163.5c-1.3,0-1.4,0.8-1.4,1v-2.6c0-0.2,0.1-1,1.4-1c1,0,2.8,0.4,4.8,1.2v2.6
										C178.5,163.9,176.7,163.5,175.7,163.5z"/>
								</defs>
								<clipPath id="SVGID_556_">
									<use xlink:href="#SVGID_555_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st309">
									<path class="st34" d="M180.5,162.1v2.6c-1.9-0.9-3.8-1.2-4.8-1.2c-0.8,0-1.1,0.3-1.3,0.5v-2.6c0.2-0.3,0.5-0.5,1.3-0.5
										C176.7,160.8,178.5,161.2,180.5,162.1"/>
									<path class="st34" d="M174.5,161.4v2.6c0,0.1-0.1,0.1-0.1,0.2v-2.6C174.4,161.5,174.4,161.5,174.5,161.4"/>
									<path class="st34" d="M174.4,161.6v2.6c0,0.1,0,0.1,0,0.2v-2.6C174.3,161.7,174.4,161.6,174.4,161.6"/>
									<path class="st34" d="M174.3,161.7v2.6c0,0.1,0,0.1,0,0.1v-2.6C174.3,161.8,174.3,161.8,174.3,161.7"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_557_" class="st0" d="M175.2,163v2.6c-0.6-0.2-0.9-0.6-0.9-1.1l0-2.6C174.3,162.4,174.6,162.7,175.2,163z"
										/>
								</defs>
								<clipPath id="SVGID_558_">
									<use xlink:href="#SVGID_557_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st310">
									<path class="st34" d="M175.2,163v2.6c-0.6-0.2-0.9-0.6-0.9-1.1v-2.6C174.3,162.4,174.6,162.7,175.2,163"/>
								</g>
							</g>
							<g>
								<polygon class="st34" points="181.5,163.7 181.5,166.3 180.7,165.9 180.7,163.3 								"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_559_" class="st0" d="M169.6,167.5c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6c0,1.4,0.4,2.4,1.4,3.2
										C169.6,165.8,169.6,166.7,169.6,167.5c0-0.9,0-1.7,0-2.6c1.4,1.2,3.2,1.6,4.3,1.8c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										C172.8,169.1,171,168.8,169.6,167.5z"/>
								</defs>
								<clipPath id="SVGID_560_">
									<use xlink:href="#SVGID_559_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st311">
									<path class="st34" d="M174.4,166.9v2.6c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6
										c0,1.4,0.4,2.4,1.4,3.2c1.4,1.2,3.2,1.6,4.3,1.8C174.1,166.8,174.3,166.8,174.4,166.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_561_" class="st0" d="M174.4,169.5c-0.1,0-0.3-0.1-0.5-0.1c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0.2,0,0.4,0.1,0.5,0.1C174.4,167.7,174.4,168.6,174.4,169.5z"/>
								</defs>
								<clipPath id="SVGID_562_">
									<use xlink:href="#SVGID_561_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st312">
									<path class="st34" d="M174.4,166.9v2.6c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6
										c0,1.4,0.4,2.4,1.4,3.2c1.4,1.2,3.2,1.6,4.3,1.8C174.1,166.8,174.3,166.8,174.4,166.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_563_" class="st0" d="M173.7,167.7v2.6c-1.1-0.2-3.2-0.6-4.8-2c-1.2-1-1.7-2.3-1.7-4l0-2.6
										c0,1.7,0.6,3,1.7,4C170.5,167.1,172.5,167.5,173.7,167.7z"/>
								</defs>
								<clipPath id="SVGID_564_">
									<use xlink:href="#SVGID_563_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st313">
									<path class="st34" d="M173.7,167.7v2.6c-1.1-0.2-3.2-0.6-4.8-2c-1.2-1-1.7-2.3-1.7-4v-2.6c0,1.7,0.6,3,1.7,4
										C170.5,167.1,172.5,167.5,173.7,167.7"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_565_" class="st0" d="M174.2,167.8v2.6c-0.1,0-0.3-0.1-0.5-0.1v-2.6C173.9,167.8,174,167.8,174.2,167.8z"/>
								</defs>
								<clipPath id="SVGID_566_">
									<use xlink:href="#SVGID_565_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st314">
									<path class="st34" d="M174.2,167.8v2.6c-0.1,0-0.3-0.1-0.5-0.1v-2.6C173.9,167.8,174,167.8,174.2,167.8"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_567_" class="st0" d="M175.3,170.5c0-0.3-0.1-0.9-0.9-1c0-0.8,0-1.5,0-2.3v-0.3c0.8,0.2,0.9,0.7,0.9,1v0.3
										C175.3,168.9,175.3,169.8,175.3,170.5z"/>
								</defs>
								<clipPath id="SVGID_568_">
									<use xlink:href="#SVGID_567_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st315">
									<path class="st34" d="M175.3,167.9v2.6c0-0.3-0.1-0.9-0.9-1v-2.6C175.2,167,175.3,167.6,175.3,167.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_569_" class="st0" d="M181.5,168.1v2.6c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2
										V163c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6C180.9,165.6,181.5,166.7,181.5,168.1z"/>
								</defs>
								<clipPath id="SVGID_570_">
									<use xlink:href="#SVGID_569_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st316">
									<path class="st34" d="M181.5,168.1v2.6c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2V163
										c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6C180.9,165.6,181.5,166.7,181.5,168.1"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_571_" class="st0" d="M175.3,167.9l0,2.6c0,0.3-0.1,1.1-1.4,1.1c-1.2,0-3.2-0.5-5.2-1.7l0-2.6
										c2,1.1,4.1,1.7,5.2,1.7C175.2,169,175.3,168.1,175.3,167.9z"/>
								</defs>
								<clipPath id="SVGID_572_">
									<use xlink:href="#SVGID_571_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st317">
									<path class="st34" d="M175.3,167.9v2.6c0,0,0,0.1,0,0.2V168C175.3,168,175.3,167.9,175.3,167.9"/>
									<path class="st34" d="M175.3,168v2.6c0,0.1,0,0.1,0,0.2v-2.6C175.3,168.2,175.3,168.1,175.3,168"/>
									<path class="st34" d="M175.2,168.2v2.6c0,0.1,0,0.1-0.1,0.2v-2.6C175.2,168.3,175.2,168.3,175.2,168.2"/>
									<path class="st34" d="M175.2,168.4v2.6c-0.2,0.3-0.5,0.6-1.3,0.6c-1.2,0-3.2-0.5-5.2-1.7v-2.6c2,1.1,4.1,1.7,5.2,1.7
										C174.7,169,175,168.7,175.2,168.4"/>
								</g>
							</g>
							<g>
								<polygon class="st34" points="168,172.7 168,175.4 167.7,175.2 167.7,172.6 								"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_573_" class="st0" d="M181.5,170.7c0,3.2-2.6,5.2-6.7,5.2c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c4.2,0,6.7-2,6.7-5.2v0.3C181.5,169.1,181.5,170,181.5,170.7z"/>
								</defs>
								<clipPath id="SVGID_574_">
									<use xlink:href="#SVGID_573_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st318">
									<path class="st34" d="M181.5,168.1v2.6c0,0.3,0,0.5-0.1,0.8v-2.6C181.4,168.6,181.5,168.3,181.5,168.1"/>
									<path class="st34" d="M181.4,168.9v2.6c0,0.3-0.1,0.5-0.2,0.8v-2.6C181.3,169.4,181.4,169.1,181.4,168.9"/>
									<path class="st34" d="M181.2,169.7v2.6c-0.1,0.3-0.2,0.5-0.4,0.8v-2.6C181,170.2,181.1,169.9,181.2,169.7"/>
									<path class="st34" d="M180.9,170.5v2.6c-1,1.7-3.2,2.8-6.1,2.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2
										C177.7,173.2,179.9,172.2,180.9,170.5"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_575_" class="st0" d="M174.7,175.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2c0,0,0,0,0,0
										c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										c0,0.1,0,0.2,0,0.3C174.7,175.6,174.7,175.7,174.7,175.8C174.7,175.8,174.7,175.8,174.7,175.8z"/>
								</defs>
								<clipPath id="SVGID_576_">
									<use xlink:href="#SVGID_575_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st319">
									<path class="st34" d="M180.9,170.5v2.6c-1,1.7-3.2,2.8-6.1,2.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2
										C177.7,173.2,179.9,172.2,180.9,170.5"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M174.8,156.3c1.9,0,4.2,0.5,5.7,1.2v4.6c-1.9-0.9-3.8-1.2-4.8-1.2c-1.3,0-1.4,0.8-1.4,1
									c0,0.5,0.3,0.9,0.9,1.1c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6c1.1,0.9,1.6,2,1.6,3.4c0,3.2-2.6,5.2-6.8,5.2
									c-2.1,0-4.4-0.5-6-1.2v-4.7c2,1.1,4.1,1.7,5.2,1.7c1.3,0,1.4-0.8,1.4-1.1c0-0.3-0.1-0.9-0.9-1c-0.1,0-0.3-0.1-0.5-0.1
									c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2C168.2,158.4,170.8,156.3,174.8,156.3z"/>
							</g>
							<g>
								<path class="st34" d="M180.5,163.9c1.3,1.1,1.9,2.4,1.9,4.1c0,3.7-3,6.2-7.7,6.2c-2.4,0-5-0.6-6.7-1.5l-0.3-0.1v-7.1
									l0.8,0.5c2.2,1.5,4.5,2,5.4,2c0.3,0,0.4-0.1,0.4-0.1c0,0,0,0-0.2-0.1c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-3.2-0.6-4.8-2
									c-1.2-1-1.7-2.3-1.7-4c0-3.9,3-6.4,7.6-6.4c2.2,0,4.8,0.6,6.4,1.4l0.3,0.1v6.8l-0.7-0.4c-2.2-1.1-4.2-1.5-5-1.5
									c-0.3,0-0.4,0.1-0.4,0.1c0,0,0.1,0,0.2,0.1c0.1,0,0.3,0.1,0.5,0.1C177.1,162.4,179.1,162.8,180.5,163.9z M174.7,173.2
									c4.2,0,6.8-2,6.8-5.2c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2c-0.6-0.2-0.9-0.6-0.9-1.1
									c0-0.2,0.1-1,1.4-1c1,0,2.8,0.4,4.8,1.2v-4.6c-1.5-0.7-3.8-1.2-5.7-1.2c-4,0-6.6,2.1-6.6,5.4c0,1.4,0.4,2.4,1.4,3.2
									c1.4,1.2,3.2,1.6,4.3,1.8c0.2,0,0.4,0.1,0.5,0.1c0.8,0.2,0.9,0.7,0.9,1c0,0.3-0.1,1.1-1.4,1.1c-1.2,0-3.2-0.5-5.2-1.7v4.7
									C170.3,172.7,172.6,173.2,174.7,173.2"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_577_" class="st0" d="M182.5,168.1l0,2.6c0,3.7-3,6.2-7.7,6.2c-2.4,0-5-0.6-6.7-1.5v-2.6
										c1.7,0.9,4.4,1.5,6.7,1.5C179.4,174.2,182.5,171.8,182.5,168.1z"/>
								</defs>
								<clipPath id="SVGID_578_">
									<use xlink:href="#SVGID_577_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st320">
									<path class="st34" d="M182.5,168.1v2.6c0,0.3,0,0.6-0.1,1V169C182.4,168.7,182.5,168.4,182.5,168.1"/>
									<path class="st34" d="M182.4,169v2.6c-0.1,0.3-0.1,0.7-0.2,1V170C182.3,169.7,182.3,169.3,182.4,169"/>
									<path class="st34" d="M182.2,170v2.6c-0.1,0.3-0.3,0.7-0.4,1v-2.6C181.9,170.6,182,170.3,182.2,170"/>
									<path class="st34" d="M181.7,170.9v2.6c-1.2,2-3.7,3.3-7,3.3c-2.4,0-5-0.6-6.7-1.5v-2.6c1.7,0.9,4.4,1.5,6.7,1.5
										C178.1,174.2,180.6,173,181.7,170.9"/>
								</g>
							</g>
						</g>
					</g>
					<defs>
						<filter id="Adobe_OpacityMaskFilter_14_" filterUnits="userSpaceOnUse" x="94.1" y="155.3" width="88.4" height="21.5">
							<feFlood  style="flood-color:white;flood-opacity:1" result="back"/>
							<feBlend  in="SourceGraphic" in2="back" mode="normal"/>
						</filter>
					</defs>
					<mask maskUnits="userSpaceOnUse" x="94.1" y="155.3" width="88.4" height="21.5" id="SVGID_540_">
						<g class="st321">
							
								<image style="overflow:visible;" width="93" height="27" xlink:href="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgEASABIAAD/7AARRHVja3kAAQAEAAAAHgAA/+4AIUFkb2JlAGTAAAAAAQMA
EAMCAwYAAAF0AAABfwAAAaD/2wCEABALCwsMCxAMDBAXDw0PFxsUEBAUGx8XFxcXFx8eFxoaGhoX
Hh4jJSclIx4vLzMzLy9AQEBAQEBAQEBAQEBAQEABEQ8PERMRFRISFRQRFBEUGhQWFhQaJhoaHBoa
JjAjHh4eHiMwKy4nJycuKzU1MDA1NUBAP0BAQEBAQEBAQEBAQP/CABEIABsAXQMBIgACEQEDEQH/
xABfAAEBAQAAAAAAAAAAAAAAAAAAAQcBAQAAAAAAAAAAAAAAAAAAAAAQAQEAAAAAAAAAAAAAAAAA
ADBQEQEAAAAAAAAAAAAAAAAAAABQEgEAAAAAAAAAAAAAAAAAAAAw/9oADAMBAAIRAxEAAADPykWA
AABYAAAAAAf/2gAIAQIAAQUAT//aAAgBAwABBQBP/9oACAEBAAEFAKH/2gAIAQICBj8AT//aAAgB
AwIGPwBP/9oACAEBAQY/AEP/2Q==" transform="matrix(1 0 0 1 91.75 152.5)">
							</image>
						</g>
					</mask>
					<g>
						<g class="st0">
							<g>
								<rect x="95.1" y="156.6" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<rect x="106.8" y="156.6" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<polygon class="st34" points="105.8,163.8 105.8,166.4 100.8,159.2 100.8,156.6 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.8,163.8 106.8,166.4 106.1,166.7 106.1,164.1 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.1,164.1 106.1,166.7 105.8,166.4 105.8,163.8 								"/>
							</g>
							<g>
								<polygon class="st34" points="101.2,165.4 101.2,168 100.5,168.4 100.5,165.8 								"/>
							</g>
							<g>
								<polygon class="st34" points="101.5,165.8 101.5,168.4 101.2,168 101.2,165.4 								"/>
							</g>
							<g>
								<polygon class="st34" points="106.5,173 106.5,175.6 101.5,168.4 101.5,165.8 								"/>
							</g>
							<g>
								<rect x="106.5" y="173" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<rect x="95.1" y="173" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M105.8,155.6h7.4V174h-7.3l-4.4-6.4v6.4h-7.4v-18.4h7.3l4.4,6.4V155.6z M112.2,173v-16.4h-5.4v7.2
									l-0.7,0.3l-0.3-0.3l-5-7.2h-5.7V173h5.4v-7.2l0.7-0.3l0.3,0.3l5,7.2H112.2"/>
							</g>
							<g>
								<polygon class="st34" points="112.2,156.6 112.2,173 106.5,173 101.5,165.8 101.2,165.4 100.5,165.8 100.5,173 95.1,173 
									95.1,156.6 100.8,156.6 105.8,163.8 106.1,164.1 106.8,163.8 106.8,156.6 								"/>
							</g>
							<g>
								<polygon class="st34" points="105.9,174 105.9,176.6 101.5,170.2 101.5,167.6 								"/>
							</g>
							<g>
								<rect x="105.9" y="174" class="st34" width="7.3" height="2.6"/>
							</g>
							<g>
								<rect x="94.1" y="174" class="st34" width="7.4" height="2.6"/>
							</g>
						</g>
						<g class="st0">
							<g class="st0">
								<defs>
									<path id="SVGID_579_" class="st0" d="M133.3,164.8l0,2.6c0-4.7-4-8.4-9.2-8.4c-5.2,0-9.2,3.7-9.2,8.4l0-2.6
										c0-4.7,4-8.4,9.2-8.4C129.3,156.3,133.3,160,133.3,164.8z"/>
								</defs>
								<clipPath id="SVGID_580_">
									<use xlink:href="#SVGID_579_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st322">
									<path class="st34" d="M133.3,164.8v2.6c0-4.7-4-8.4-9.2-8.4c-3.6,0-6.6,1.8-8.1,4.5v-2.6c1.5-2.7,4.6-4.5,8.1-4.5
										C129.3,156.3,133.3,160,133.3,164.8"/>
									<path class="st34" d="M116,160.8v2.6c-0.3,0.5-0.5,0.9-0.6,1.4v-2.6C115.5,161.7,115.7,161.3,116,160.8"/>
									<path class="st34" d="M115.3,162.2v2.6c-0.1,0.4-0.3,0.9-0.3,1.3v-2.6C115.1,163.1,115.2,162.7,115.3,162.2"/>
									<path class="st34" d="M115,163.6v2.6c-0.1,0.4-0.1,0.8-0.1,1.2v-2.6C114.9,164.4,115,164,115,163.6"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_581_" class="st0" d="M120.9,167.5c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3v-0.3c0-1.8,1.4-3.2,3.2-3.2
										c1.8,0,3.2,1.4,3.2,3.2l0,2.6c0-1.8-1.4-3.2-3.2-3.2C122.4,164.3,120.9,165.8,120.9,167.5z"/>
								</defs>
								<clipPath id="SVGID_582_">
									<use xlink:href="#SVGID_581_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st323">
									<path class="st34" d="M127.3,164.9v2.6c0-1.8-1.4-3.2-3.2-3.2c-1.2,0-2.2,0.7-2.8,1.6v-2.6c0.5-1,1.6-1.6,2.8-1.6
										C125.9,161.7,127.3,163.1,127.3,164.9"/>
									<path class="st34" d="M121.3,163.3v2.6c-0.1,0.2-0.2,0.4-0.3,0.6v-2.6C121.2,163.7,121.2,163.5,121.3,163.3"/>
									<path class="st34" d="M121.1,163.9v2.6c-0.1,0.2-0.1,0.3-0.1,0.5v-2.6C121,164.2,121,164.1,121.1,163.9"/>
									<path class="st34" d="M121,164.4v2.6c0,0.2,0,0.3,0,0.5v-2.6C120.9,164.7,120.9,164.6,121,164.4"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_583_" class="st0" d="M126.3,164.9l0,2.6c0-1.2-1-2.2-2.2-2.2c-1.2,0-2.2,1-2.2,2.2l0-2.6
										c0-1.2,1-2.2,2.2-2.2C125.3,162.7,126.3,163.7,126.3,164.9z"/>
								</defs>
								<clipPath id="SVGID_584_">
									<use xlink:href="#SVGID_583_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st324">
									<path class="st34" d="M126.3,164.9v2.6c0-1.2-1-2.2-2.2-2.2c-0.8,0-1.5,0.4-1.9,1.1v-2.6c0.4-0.7,1.1-1.1,1.9-1.1
										C125.3,162.7,126.3,163.7,126.3,164.9"/>
									<path class="st34" d="M122.2,163.8v2.6c-0.1,0.1-0.1,0.3-0.2,0.4v-2.6C122.1,164.1,122.1,163.9,122.2,163.8"/>
									<path class="st34" d="M122,164.2v2.6c0,0.1-0.1,0.2-0.1,0.4v-2.6C122,164.4,122,164.3,122,164.2"/>
									<path class="st34" d="M121.9,164.6v2.6c0,0.1,0,0.2,0,0.3v-2.6C121.9,164.8,121.9,164.7,121.9,164.6"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_585_" class="st0" d="M127.3,164.9l0,2.6c0,1.8-1.4,3.2-3.2,3.2c-1.8,0-3.2-1.4-3.2-3.2v-2.6
										c0,1.8,1.4,3.2,3.2,3.2C125.9,168.1,127.3,166.6,127.3,164.9z"/>
								</defs>
								<clipPath id="SVGID_586_">
									<use xlink:href="#SVGID_585_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st325">
									<path class="st34" d="M127.3,164.9v2.6c0,0.2,0,0.3,0,0.5v-2.6C127.3,165.2,127.3,165.1,127.3,164.9"/>
									<path class="st34" d="M127.3,165.4v2.6c0,0.2-0.1,0.4-0.1,0.5v-2.6C127.2,165.7,127.2,165.5,127.3,165.4"/>
									<path class="st34" d="M127.1,165.9v2.6c-0.1,0.2-0.2,0.4-0.3,0.6v-2.6C127,166.3,127.1,166.1,127.1,165.9"/>
									<path class="st34" d="M126.9,166.5v2.6c-0.5,1-1.6,1.6-2.8,1.6c-1.8,0-3.2-1.4-3.2-3.2v-2.6c0,1.8,1.4,3.2,3.2,3.2
										C125.3,168.1,126.3,167.4,126.9,166.5"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M124.1,161.7c1.8,0,3.2,1.4,3.2,3.2c0,1.8-1.4,3.2-3.2,3.2c-1.8,0-3.2-1.4-3.2-3.2
									C120.9,163.1,122.4,161.7,124.1,161.7z M124.1,167.1c1.2,0,2.2-1,2.2-2.2c0-1.2-1-2.2-2.2-2.2c-1.2,0-2.2,1-2.2,2.2
									C121.9,166.1,122.9,167.1,124.1,167.1"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_587_" class="st0" d="M124.1,175.8c-5.2,0-9.2-3.7-9.2-8.4l0-2.6c0,4.7,4,8.4,9.2,8.4
										c5.2,0,9.2-3.7,9.2-8.4l0,0.3v2.3C133.3,172.1,129.3,175.8,124.1,175.8z"/>
								</defs>
								<clipPath id="SVGID_588_">
									<use xlink:href="#SVGID_587_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st326">
									<path class="st34" d="M133.3,164.8v2.6c0,0.4,0,0.8-0.1,1.2V166C133.3,165.6,133.3,165.2,133.3,164.8"/>
									<path class="st34" d="M133.2,166v2.6c-0.1,0.5-0.2,0.9-0.3,1.3v-2.6C133,166.9,133.2,166.4,133.2,166"/>
									<path class="st34" d="M132.9,167.3v2.6c-0.2,0.5-0.4,1-0.6,1.4v-2.6C132.5,168.3,132.7,167.8,132.9,167.3"/>
									<path class="st34" d="M132.3,168.8v2.6c-1.5,2.7-4.6,4.5-8.1,4.5c-5.2,0-9.2-3.7-9.2-8.4v-2.6c0,4.7,4,8.4,9.2,8.4
										C127.7,173.2,130.7,171.4,132.3,168.8"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M124.1,155.3c5.7,0,10.2,4.2,10.2,9.4c0,5.3-4.5,9.4-10.2,9.4c-5.7,0-10.2-4.1-10.2-9.4
									C113.9,159.5,118.4,155.3,124.1,155.3z M124.1,173.2c5.2,0,9.2-3.7,9.2-8.4c0-4.7-4-8.4-9.2-8.4c-5.2,0-9.2,3.7-9.2,8.4
									C114.9,169.5,119,173.2,124.1,173.2"/>
							</g>
							<g>
								<path class="st34" d="M124.1,156.3c5.2,0,9.2,3.7,9.2,8.4c0,4.7-4,8.4-9.2,8.4c-5.2,0-9.2-3.7-9.2-8.4
									C114.9,160,119,156.3,124.1,156.3z M124.1,168.1c1.8,0,3.2-1.4,3.2-3.2c0-1.8-1.4-3.2-3.2-3.2c-1.8,0-3.2,1.4-3.2,3.2
									C120.9,166.6,122.4,168.1,124.1,168.1"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_589_" class="st0" d="M134.3,164.8v2.6c0,5.3-4.5,9.4-10.2,9.4c-5.7,0-10.2-4.1-10.2-9.4l0-2.6
										c0,5.3,4.5,9.4,10.2,9.4C129.8,174.2,134.3,170.1,134.3,164.8z"/>
								</defs>
								<clipPath id="SVGID_590_">
									<use xlink:href="#SVGID_589_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st327">
									<path class="st34" d="M134.3,164.8v2.6c0,0.5,0,0.9-0.1,1.4v-2.6C134.3,165.7,134.3,165.2,134.3,164.8"/>
									<path class="st34" d="M134.2,166.1v2.6c-0.1,0.5-0.2,1-0.4,1.5v-2.6C134,167.2,134.1,166.7,134.2,166.1"/>
									<path class="st34" d="M133.9,167.6v2.6c-0.2,0.6-0.4,1.1-0.7,1.6v-2.6C133.4,168.7,133.7,168.2,133.9,167.6"/>
									<path class="st34" d="M133.1,169.3v2.6c-1.7,3-5.1,5-9,5c-5.7,0-10.2-4.1-10.2-9.4v-2.6c0,5.3,4.5,9.4,10.2,9.4
										C128.1,174.2,131.4,172.2,133.1,169.3"/>
								</g>
							</g>
						</g>
						<g class="st0">
							<g>
								<rect x="135.3" y="156.6" class="st34" width="15.8" height="2.6"/>
							</g>
							<g>
								<rect x="135.3" y="161.5" class="st34" width="4.9" height="2.6"/>
							</g>
							<g>
								<rect x="146.3" y="161.5" class="st34" width="4.8" height="2.6"/>
							</g>
							<g>
								<rect x="147.3" y="162.5" class="st34" width="4.8" height="2.6"/>
							</g>
							<g>
								<rect x="134.3" y="162.5" class="st34" width="4.9" height="2.6"/>
							</g>
							<g>
								<rect x="140.2" y="173" class="st34" width="6.2" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M134.3,155.6h17.8v6.9h-4.8V174h-8.2v-11.5h-4.9V155.6z M151.2,161.5v-4.9h-15.8v4.9h4.9V173h6.2
									v-11.5H151.2"/>
							</g>
							<g>
								<polygon class="st34" points="151.2,156.6 151.2,161.5 146.3,161.5 146.3,173 140.2,173 140.2,161.5 135.3,161.5 
									135.3,156.6 								"/>
							</g>
							<g>
								<rect x="139.2" y="174" class="st34" width="8.2" height="2.6"/>
							</g>
						</g>
						<g class="st0">
							<g>
								<rect x="154" y="156.6" class="st34" width="11.5" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="161.4" class="st34" width="5.4" height="2.6"/>
							</g>
							<g>
								<rect x="165.5" y="162.4" class="st34" width="1" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="163.3" class="st34" width="4.3" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="166.5" class="st34" width="4.3" height="2.6"/>
							</g>
							<g>
								<rect x="160.2" y="168.4" class="st34" width="5.7" height="2.6"/>
							</g>
							<g>
								<polygon class="st34" points="165.5,156.6 165.5,161.4 160.2,161.4 160.2,163.3 164.5,163.3 164.5,166.5 160.2,166.5 
									160.2,168.4 165.9,168.4 165.9,173 154,173 154,156.6 								"/>
							</g>
							<g>
								<rect x="154" y="173" class="st34" width="11.8" height="2.6"/>
							</g>
							<g>
								<rect x="153" y="174" class="st34" width="13.8" height="2.6"/>
							</g>
							<g>
								<path class="st34" d="M165.5,167.4h1.4v6.5H153v-18.4h13.5v6.8h-1V167.4z M165.9,173v-4.5h-5.7v-2h4.3v-3.1h-4.3v-1.9h5.4
									v-4.8H154V173H165.9"/>
							</g>
						</g>
						<g class="st0">
							<g class="st0">
								<defs>
									<path id="SVGID_591_" class="st0" d="M180.5,157.5l0,2.6c-1.5-0.7-3.8-1.2-5.7-1.2c-4,0-6.6,2.1-6.6,5.4l0-2.6
										c0-3.3,2.6-5.4,6.6-5.4C176.7,156.3,178.9,156.8,180.5,157.5z"/>
								</defs>
								<clipPath id="SVGID_592_">
									<use xlink:href="#SVGID_591_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st328">
									<path class="st34" d="M180.5,157.5v2.6c-1.5-0.7-3.8-1.2-5.7-1.2c-2.8,0-4.9,1-5.9,2.8v-2.6c1-1.8,3.1-2.8,5.9-2.8
										C176.7,156.3,178.9,156.8,180.5,157.5"/>
									<path class="st34" d="M168.8,159.1v2.6c-0.2,0.3-0.3,0.6-0.4,0.9V160C168.6,159.7,168.7,159.4,168.8,159.1"/>
									<path class="st34" d="M168.5,160v2.6c-0.1,0.3-0.2,0.6-0.2,0.9v-2.6C168.3,160.5,168.4,160.3,168.5,160"/>
									<path class="st34" d="M168.3,160.8v2.6c0,0.3-0.1,0.6-0.1,0.8v-2.6C168.2,161.4,168.2,161.1,168.3,160.8"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_593_" class="st0" d="M175.7,163.5c-1.3,0-1.4,0.8-1.4,1v-2.6c0-0.2,0.1-1,1.4-1c1,0,2.8,0.4,4.8,1.2v2.6
										C178.5,163.9,176.7,163.5,175.7,163.5z"/>
								</defs>
								<clipPath id="SVGID_594_">
									<use xlink:href="#SVGID_593_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st329">
									<path class="st34" d="M180.5,162.1v2.6c-1.9-0.9-3.8-1.2-4.8-1.2c-0.8,0-1.1,0.3-1.3,0.5v-2.6c0.2-0.3,0.5-0.5,1.3-0.5
										C176.7,160.8,178.5,161.2,180.5,162.1"/>
									<path class="st34" d="M174.5,161.4v2.6c0,0.1-0.1,0.1-0.1,0.2v-2.6C174.4,161.5,174.4,161.5,174.5,161.4"/>
									<path class="st34" d="M174.4,161.6v2.6c0,0.1,0,0.1,0,0.2v-2.6C174.3,161.7,174.4,161.6,174.4,161.6"/>
									<path class="st34" d="M174.3,161.7v2.6c0,0.1,0,0.1,0,0.1v-2.6C174.3,161.8,174.3,161.8,174.3,161.7"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_595_" class="st0" d="M175.2,163v2.6c-0.6-0.2-0.9-0.6-0.9-1.1l0-2.6C174.3,162.4,174.6,162.7,175.2,163z"
										/>
								</defs>
								<clipPath id="SVGID_596_">
									<use xlink:href="#SVGID_595_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st330">
									<path class="st34" d="M175.2,163v2.6c-0.6-0.2-0.9-0.6-0.9-1.1v-2.6C174.3,162.4,174.6,162.7,175.2,163"/>
								</g>
							</g>
							<g>
								<polygon class="st34" points="181.5,163.7 181.5,166.3 180.7,165.9 180.7,163.3 								"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_597_" class="st0" d="M169.6,167.5c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6c0,1.4,0.4,2.4,1.4,3.2
										C169.6,165.8,169.6,166.7,169.6,167.5c0-0.9,0-1.7,0-2.6c1.4,1.2,3.2,1.6,4.3,1.8c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										C172.8,169.1,171,168.8,169.6,167.5z"/>
								</defs>
								<clipPath id="SVGID_598_">
									<use xlink:href="#SVGID_597_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st331">
									<path class="st34" d="M174.4,166.9v2.6c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6
										c0,1.4,0.4,2.4,1.4,3.2c1.4,1.2,3.2,1.6,4.3,1.8C174.1,166.8,174.3,166.8,174.4,166.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_599_" class="st0" d="M174.4,169.5c-0.1,0-0.3-0.1-0.5-0.1c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0.2,0,0.4,0.1,0.5,0.1C174.4,167.7,174.4,168.6,174.4,169.5z"/>
								</defs>
								<clipPath id="SVGID_600_">
									<use xlink:href="#SVGID_599_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st332">
									<path class="st34" d="M174.4,166.9v2.6c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2v-2.6
										c0,1.4,0.4,2.4,1.4,3.2c1.4,1.2,3.2,1.6,4.3,1.8C174.1,166.8,174.3,166.8,174.4,166.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_601_" class="st0" d="M173.7,167.7v2.6c-1.1-0.2-3.2-0.6-4.8-2c-1.2-1-1.7-2.3-1.7-4l0-2.6
										c0,1.7,0.6,3,1.7,4C170.5,167.1,172.5,167.5,173.7,167.7z"/>
								</defs>
								<clipPath id="SVGID_602_">
									<use xlink:href="#SVGID_601_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st333">
									<path class="st34" d="M173.7,167.7v2.6c-1.1-0.2-3.2-0.6-4.8-2c-1.2-1-1.7-2.3-1.7-4v-2.6c0,1.7,0.6,3,1.7,4
										C170.5,167.1,172.5,167.5,173.7,167.7"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_603_" class="st0" d="M174.2,167.8v2.6c-0.1,0-0.3-0.1-0.5-0.1v-2.6C173.9,167.8,174,167.8,174.2,167.8z"/>
								</defs>
								<clipPath id="SVGID_604_">
									<use xlink:href="#SVGID_603_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st334">
									<path class="st34" d="M174.2,167.8v2.6c-0.1,0-0.3-0.1-0.5-0.1v-2.6C173.9,167.8,174,167.8,174.2,167.8"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_605_" class="st0" d="M175.3,170.5c0-0.3-0.1-0.9-0.9-1c0-0.8,0-1.5,0-2.3v-0.3c0.8,0.2,0.9,0.7,0.9,1v0.3
										C175.3,168.9,175.3,169.8,175.3,170.5z"/>
								</defs>
								<clipPath id="SVGID_606_">
									<use xlink:href="#SVGID_605_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st335">
									<path class="st34" d="M175.3,167.9v2.6c0-0.3-0.1-0.9-0.9-1v-2.6C175.2,167,175.3,167.6,175.3,167.9"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_607_" class="st0" d="M181.5,168.1v2.6c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2
										V163c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6C180.9,165.6,181.5,166.7,181.5,168.1z"/>
								</defs>
								<clipPath id="SVGID_608_">
									<use xlink:href="#SVGID_607_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st336">
									<path class="st34" d="M181.5,168.1v2.6c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2V163
										c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6C180.9,165.6,181.5,166.7,181.5,168.1"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_609_" class="st0" d="M175.3,167.9l0,2.6c0,0.3-0.1,1.1-1.4,1.1c-1.2,0-3.2-0.5-5.2-1.7l0-2.6
										c2,1.1,4.1,1.7,5.2,1.7C175.2,169,175.3,168.1,175.3,167.9z"/>
								</defs>
								<clipPath id="SVGID_610_">
									<use xlink:href="#SVGID_609_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st337">
									<path class="st34" d="M175.3,167.9v2.6c0,0,0,0.1,0,0.2V168C175.3,168,175.3,167.9,175.3,167.9"/>
									<path class="st34" d="M175.3,168v2.6c0,0.1,0,0.1,0,0.2v-2.6C175.3,168.2,175.3,168.1,175.3,168"/>
									<path class="st34" d="M175.2,168.2v2.6c0,0.1,0,0.1-0.1,0.2v-2.6C175.2,168.3,175.2,168.3,175.2,168.2"/>
									<path class="st34" d="M175.2,168.4v2.6c-0.2,0.3-0.5,0.6-1.3,0.6c-1.2,0-3.2-0.5-5.2-1.7v-2.6c2,1.1,4.1,1.7,5.2,1.7
										C174.7,169,175,168.7,175.2,168.4"/>
								</g>
							</g>
							<g>
								<polygon class="st34" points="168,172.7 168,175.4 167.7,175.2 167.7,172.6 								"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_611_" class="st0" d="M181.5,170.7c0,3.2-2.6,5.2-6.7,5.2c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
										c4.2,0,6.7-2,6.7-5.2v0.3C181.5,169.1,181.5,170,181.5,170.7z"/>
								</defs>
								<clipPath id="SVGID_612_">
									<use xlink:href="#SVGID_611_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st338">
									<path class="st34" d="M181.5,168.1v2.6c0,0.3,0,0.5-0.1,0.8v-2.6C181.4,168.6,181.5,168.3,181.5,168.1"/>
									<path class="st34" d="M181.4,168.9v2.6c0,0.3-0.1,0.5-0.2,0.8v-2.6C181.3,169.4,181.4,169.1,181.4,168.9"/>
									<path class="st34" d="M181.2,169.7v2.6c-0.1,0.3-0.2,0.5-0.4,0.8v-2.6C181,170.2,181.1,169.9,181.2,169.7"/>
									<path class="st34" d="M180.9,170.5v2.6c-1,1.7-3.2,2.8-6.1,2.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2
										C177.7,173.2,179.9,172.2,180.9,170.5"/>
								</g>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_613_" class="st0" d="M174.7,175.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2c0,0,0,0,0,0
										c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
										c0,0.1,0,0.2,0,0.3C174.7,175.6,174.7,175.7,174.7,175.8C174.7,175.8,174.7,175.8,174.7,175.8z"/>
								</defs>
								<clipPath id="SVGID_614_">
									<use xlink:href="#SVGID_613_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st339">
									<path class="st34" d="M180.9,170.5v2.6c-1,1.7-3.2,2.8-6.1,2.8c-2.1,0-4.4-0.5-6-1.2V172c1.6,0.7,3.9,1.2,6,1.2
										C177.7,173.2,179.9,172.2,180.9,170.5"/>
								</g>
							</g>
							<g>
								<path class="st34" d="M174.8,156.3c1.9,0,4.2,0.5,5.7,1.2v4.6c-1.9-0.9-3.8-1.2-4.8-1.2c-1.3,0-1.4,0.8-1.4,1
									c0,0.5,0.3,0.9,0.9,1.1c0.2,0,0.4,0.1,0.6,0.2c1,0.2,2.7,0.6,4,1.6c1.1,0.9,1.6,2,1.6,3.4c0,3.2-2.6,5.2-6.8,5.2
									c-2.1,0-4.4-0.5-6-1.2v-4.7c2,1.1,4.1,1.7,5.2,1.7c1.3,0,1.4-0.8,1.4-1.1c0-0.3-0.1-0.9-0.9-1c-0.1,0-0.3-0.1-0.5-0.1
									c-1.1-0.2-2.9-0.6-4.3-1.8c-0.9-0.8-1.4-1.9-1.4-3.2C168.2,158.4,170.8,156.3,174.8,156.3z"/>
							</g>
							<g>
								<path class="st34" d="M180.5,163.9c1.3,1.1,1.9,2.4,1.9,4.1c0,3.7-3,6.2-7.7,6.2c-2.4,0-5-0.6-6.7-1.5l-0.3-0.1v-7.1
									l0.8,0.5c2.2,1.5,4.5,2,5.4,2c0.3,0,0.4-0.1,0.4-0.1c0,0,0,0-0.2-0.1c-0.1,0-0.3-0.1-0.5-0.1c-1.1-0.2-3.2-0.6-4.8-2
									c-1.2-1-1.7-2.3-1.7-4c0-3.9,3-6.4,7.6-6.4c2.2,0,4.8,0.6,6.4,1.4l0.3,0.1v6.8l-0.7-0.4c-2.2-1.1-4.2-1.5-5-1.5
									c-0.3,0-0.4,0.1-0.4,0.1c0,0,0.1,0,0.2,0.1c0.1,0,0.3,0.1,0.5,0.1C177.1,162.4,179.1,162.8,180.5,163.9z M174.7,173.2
									c4.2,0,6.8-2,6.8-5.2c0-1.4-0.5-2.5-1.6-3.4c-1.3-1-3-1.4-4-1.6c-0.3-0.1-0.5-0.1-0.6-0.2c-0.6-0.2-0.9-0.6-0.9-1.1
									c0-0.2,0.1-1,1.4-1c1,0,2.8,0.4,4.8,1.2v-4.6c-1.5-0.7-3.8-1.2-5.7-1.2c-4,0-6.6,2.1-6.6,5.4c0,1.4,0.4,2.4,1.4,3.2
									c1.4,1.2,3.2,1.6,4.3,1.8c0.2,0,0.4,0.1,0.5,0.1c0.8,0.2,0.9,0.7,0.9,1c0,0.3-0.1,1.1-1.4,1.1c-1.2,0-3.2-0.5-5.2-1.7v4.7
									C170.3,172.7,172.6,173.2,174.7,173.2"/>
							</g>
							<g class="st0">
								<defs>
									<path id="SVGID_615_" class="st0" d="M182.5,168.1l0,2.6c0,3.7-3,6.2-7.7,6.2c-2.4,0-5-0.6-6.7-1.5v-2.6
										c1.7,0.9,4.4,1.5,6.7,1.5C179.4,174.2,182.5,171.8,182.5,168.1z"/>
								</defs>
								<clipPath id="SVGID_616_">
									<use xlink:href="#SVGID_615_"  style="overflow:visible;"/>
								</clipPath>
								<g class="st340">
									<path class="st34" d="M182.5,168.1v2.6c0,0.3,0,0.6-0.1,1V169C182.4,168.7,182.5,168.4,182.5,168.1"/>
									<path class="st34" d="M182.4,169v2.6c-0.1,0.3-0.1,0.7-0.2,1V170C182.3,169.7,182.3,169.3,182.4,169"/>
									<path class="st34" d="M182.2,170v2.6c-0.1,0.3-0.3,0.7-0.4,1v-2.6C181.9,170.6,182,170.3,182.2,170"/>
									<path class="st34" d="M181.7,170.9v2.6c-1.2,2-3.7,3.3-7,3.3c-2.4,0-5-0.6-6.7-1.5v-2.6c1.7,0.9,4.4,1.5,6.7,1.5
										C178.1,174.2,180.6,173,181.7,170.9"/>
								</g>
							</g>
						</g>
					</g>
				</g>
			</g>
		</g>
	</g>
</g>
<g id="Lines" class="st341">
	<linearGradient id="SVGID_617_" gradientUnits="userSpaceOnUse" x1="305.5338" y1="148.4157" x2="73.6061" y2="148.4157">
		<stop  offset="0" style="stop-color:#333399"/>
		<stop  offset="1" style="stop-color:#FF00CC"/>
	</linearGradient>
	<linearGradient id="SVGID_618_" gradientUnits="userSpaceOnUse" x1="73.6061" y1="148.4157" x2="305.5338" y2="148.4157">
		<stop  offset="0" style="stop-color:#333399"/>
		<stop  offset="1" style="stop-color:#FF00CC"/>
	</linearGradient>
	<line class="st342" x1="73.6" y1="148.4" x2="305.5" y2="148.4"/>
	<linearGradient id="SVGID_619_" gradientUnits="userSpaceOnUse" x1="102.5089" y1="178.9878" x2="142.8223" y2="178.9878">
		<stop  offset="0" style="stop-color:#333399"/>
		<stop  offset="1" style="stop-color:#FF00CC"/>
	</linearGradient>
	<line class="st343" x1="102.5" y1="179" x2="142.8" y2="179"/>
	<linearGradient id="SVGID_620_" gradientUnits="userSpaceOnUse" x1="247.2536" y1="178.9878" x2="287.567" y2="178.9878">
		<stop  offset="0" style="stop-color:#333399"/>
		<stop  offset="1" style="stop-color:#FF00CC"/>
	</linearGradient>
	<line class="st344" x1="247.3" y1="179" x2="287.6" y2="179"/>
</g>
<g id="Layer_1">
	<g>
		<g>
			<linearGradient id="SVGID_621_" gradientUnits="userSpaceOnUse" x1="109.8584" y1="0" x2="109.8584" y2="271.5">
				<stop  offset="0" style="stop-color:#333399"/>
				<stop  offset="1" style="stop-color:#FF00CC"/>
			</linearGradient>
			<path class="st345" d="M180.4,249.4c-62.8,0-113.7-50.9-113.7-113.7S117.6,22.1,180.4,22.1c13.8,0,27.1,2.5,39.4,7
				C196.6,10.9,167.5,0,135.8,0C60.8,0,0,60.8,0,135.8s60.8,135.8,135.8,135.8c31.7,0,60.9-10.9,84-29.1
				C207.5,246.9,194.2,249.4,180.4,249.4z"/>
			<path class="st346" d="M135.8,272C60.6,272-0.5,210.9-0.5,135.8C-0.5,60.6,60.6-0.5,135.8-0.5c30.9,0,60.1,10.1,84.3,29.2l2.5,2
				l-3-1.1c-12.5-4.6-25.7-7-39.2-7C118,22.6,67.2,73.3,67.2,135.8c0,62.4,50.8,113.2,113.2,113.2c13.5,0,26.7-2.3,39.2-7l3-1.1
				l-2.5,2C195.8,261.9,166.7,272,135.8,272z M135.8,0.5C61.2,0.5,0.5,61.2,0.5,135.8S61.2,271,135.8,271c29.6,0,57.6-9.3,81.2-27.1
				c-11.8,4-24,6-36.6,6c-63,0-114.2-51.2-114.2-114.2S117.4,21.6,180.4,21.6c12.5,0,24.8,2,36.6,6C193.4,9.8,165.4,0.5,135.8,0.5z"
				/>
		</g>
		<defs>
			<filter id="Adobe_OpacityMaskFilter_15_" filterUnits="userSpaceOnUse" x="-0.5" y="-0.5" width="223" height="272.5">
				<feFlood  style="flood-color:white;flood-opacity:1" result="back"/>
				<feBlend  in="SourceGraphic" in2="back" mode="normal"/>
			</filter>
		</defs>
		<mask maskUnits="userSpaceOnUse" x="-0.5" y="-0.5" width="223" height="272.5" id="SVGID_622_">
			<g class="st347">
				
					<image style="overflow:visible;" width="228" height="277" xlink:href="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgEASABIAAD/7AARRHVja3kAAQAEAAAAHgAA/+4AIUFkb2JlAGTAAAAAAQMA
EAMCAwYAAAMWAAAExQAABOr/2wCEABALCwsMCxAMDBAXDw0PFxsUEBAUGx8XFxcXFx8eFxoaGhoX
Hh4jJSclIx4vLzMzLy9AQEBAQEBAQEBAQEBAQEABEQ8PERMRFRISFRQRFBEUGhQWFhQaJhoaHBoa
JjAjHh4eHiMwKy4nJycuKzU1MDA1NUBAP0BAQEBAQEBAQEBAQP/CABEIARUA5AMBIgACEQEDEQH/
xABuAAEAAwEBAQAAAAAAAAAAAAAAAgMEAQUHAQEAAAAAAAAAAAAAAAAAAAAAEAADAQEBAAMBAAMA
AAAAAAAAAQIDEUAQMRIhMFCAEQEAAAAAAAAAAAAAAAAAAACQEgEAAAAAAAAAAAAAAAAAAACA/9oA
DAMBAAIRAxEAAAD5+AAAAAdOJdIdmIJ8IJCLo4AAAAAAAdOdnYVzt6VrOFfJQEUTvOcOuAAAAAAA
SFsriFk+kOTgRh2kQ5E7wAAAAAAAB07fzSLJTIJQIVSzkanAAAAAAAAABbDUT0RvOdlwhTZnKs06
gAAAAAAAAAdLdlOost5M5Cykqx6MRXwAAAAAAAAAFld5p1U6SyQQovzGbHoygAAAAAAAAAHdObWa
9FOgm7wqy6chjotqAAAAAAAAAAJbMew2aM+gnzvCjJryGCE4AAAAAAAAAAEtePWbtGXSWcCnHsyn
n13UgAAAAAAAAADTmuPQ049Jf2PSvNqznn59mQ4AAAAAAAAABOHTbr8/Wa502Cm6ox4/QxlAAAAA
AAAAAALNeG89C3JeXQ7wpy7c5gjfScAAAAAAAAAAnAbNGDQbJUWEqrIGbNtoMqcAAAAAAAAAADtl
XTZdhuNamR2uyJnp11GZZA4AAAAAAAAABOA0W5JmrtEicQrhbEp5ZEglw4AAAAAAAAACUqxd2kXK
hPkR3nAOAAAAAAAAAAAAAAAAAAAH/9oACAECAAEFAP8Ag7//2gAIAQMAAQUA/wCDv//aAAgBAQAB
BQD/ADcOHDhzzcFJ+T8n5OHDhzxqRQKBQfk4NDH8d8KXSYJzFB+TgxjY2NnfCp6TmTApODQymVQ6
O+KZ6RmTApPyMplUVQ68cz0zzJgUnBlMui7G++NLpnBEEyJDKZdGljffJnHTOCJEjhRbNLKrvkld
eUESTJwZbNKNK8uUmckSJfFGjNaKfX45XXlJnJKEhls1o1r++TNf3KSESvijRm1Fvr8mKMkQhIZZ
qzZj8i+8UZohCGWas2f98k/eKM0SIZobM1f98kfeKMyfijQ2NPvyR94mZPxRobGn35J+8WZk/FGh
sar++SfvFmbIYhlmqNl/fIvvFmTIYmMs1RsvNkzJkMT+KNEbIf35IfHlRFEs6UWjWTRcfkT/ALlR
nRNCYy0aI1nzZ1x50RRLOlFo1ktcfkT48rM6JoTGUjSTSBrnliuGdkUKjoy0aSXPmi+EWTYqOjKR
clT5U+EXwiybFR0ZSLgqfKnwi+E6Cs/R0ZSKkqRrydJsnQVio6MaKkcjRzyKuE2KxWfo6MY0NHDn
j6KhWKz9n6Ojfw/P0/R+j9H6OnTp3/Tf/9oACAECAgY/AAd//9oACAEDAgY/AAd//9oACAEBAQY/
AH3/AP/Z" transform="matrix(1 0 0 1 -3.25 -2.5)">
				</image>
			</g>
		</mask>
		<g class="st348">
			<path class="st34" d="M180.4,249.4c-62.8,0-113.7-50.9-113.7-113.7S117.6,22.1,180.4,22.1c13.8,0,27.1,2.5,39.4,7
				C196.6,10.9,167.5,0,135.8,0C60.8,0,0,60.8,0,135.8s60.8,135.8,135.8,135.8c31.7,0,60.9-10.9,84-29.1
				C207.5,246.9,194.2,249.4,180.4,249.4z"/>
			<path class="st34" d="M135.8,272C60.6,272-0.5,210.9-0.5,135.8C-0.5,60.6,60.6-0.5,135.8-0.5c30.9,0,60.1,10.1,84.3,29.2l2.5,2
				l-3-1.1c-12.5-4.6-25.7-7-39.2-7C118,22.6,67.2,73.3,67.2,135.8c0,62.4,50.8,113.2,113.2,113.2c13.5,0,26.7-2.3,39.2-7l3-1.1
				l-2.5,2C195.8,261.9,166.7,272,135.8,272z M135.8,0.5C61.2,0.5,0.5,61.2,0.5,135.8S61.2,271,135.8,271c29.6,0,57.6-9.3,81.2-27.1
				c-11.8,4-24,6-36.6,6c-63,0-114.2-51.2-114.2-114.2S117.4,21.6,180.4,21.6c12.5,0,24.8,2,36.6,6C193.4,9.8,165.4,0.5,135.8,0.5z"
				/>
		</g>
	</g>
</g>
<g id="TopO">
	<g>
		<g>
			<g class="st0">
				<g class="st0">
					<defs>
						<path id="SVGID_623_" class="st0" d="M107.2,92.4c-6.1,0-11,9.9-11,22l0-2.6c0-12.1,4.9-22,11-22c6,0,11,9.9,11,22l0,2.6
							C118.2,102.3,113.3,92.4,107.2,92.4z"/>
					</defs>
					<clipPath id="SVGID_624_">
						<use xlink:href="#SVGID_623_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st349">
						<path class="st11" d="M118.2,111.8v2.6c0-12.1-4.9-22-11-22c-2.8,0-5.3,2.1-7.2,5.5v-2.6c1.9-3.4,4.5-5.5,7.2-5.5
							C113.3,89.8,118.2,99.7,118.2,111.8"/>
						<path class="st12" d="M100,95.2v2.6c-0.7,1.3-1.4,2.8-1.9,4.4v-2.6C98.6,98,99.2,96.5,100,95.2"/>
						<path class="st13" d="M98.1,99.6v2.6c-0.6,1.8-1.1,3.7-1.4,5.8v-2.6C97,103.4,97.5,101.4,98.1,99.6"/>
						<path class="st14" d="M96.7,105.5v2.6c-0.3,2-0.5,4.1-0.5,6.3v-2.6C96.2,109.6,96.4,107.5,96.7,105.5"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_625_" class="st0" d="M117.2,111.8l0,2.6c0-11.6-4.5-21-10-21c-5.5,0-10,9.4-10,21l0-2.6c0-11.6,4.5-21,10-21
							C112.7,90.8,117.2,100.2,117.2,111.8z"/>
					</defs>
					<clipPath id="SVGID_626_">
						<use xlink:href="#SVGID_625_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st350">
						<path class="st6" d="M117.2,111.8v2.6c0-11.6-4.5-21-10-21c-2.4,0-4.7,1.8-6.4,4.9v-2.6c1.7-3,4-4.9,6.4-4.9
							C112.7,90.8,117.2,100.2,117.2,111.8"/>
						<path class="st7" d="M100.8,95.7v2.6c-0.7,1.2-1.3,2.6-1.8,4.1v-2.6C99.5,98.2,100.1,96.9,100.8,95.7"/>
						<path class="st8" d="M99,99.8v2.6c-0.6,1.7-1,3.6-1.3,5.7v-2.6C98,103.4,98.4,101.5,99,99.8"/>
						<path class="st9" d="M97.7,105.5v2.6c-0.3,2-0.5,4.1-0.5,6.3v-2.6C97.2,109.6,97.4,107.5,97.7,105.5"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_627_" class="st0" d="M135.6,113.3l0,2.6c0-14.8-12.6-26.3-28.6-26.3c-16,0-28.6,11.6-28.6,26.3l0-2.6
							C78.4,98.6,91,87,107,87C123,87,135.6,98.6,135.6,113.3z"/>
					</defs>
					<clipPath id="SVGID_628_">
						<use xlink:href="#SVGID_627_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st351">
						<path class="st6" d="M135.6,113.3v2.6c0-14.8-12.6-26.3-28.6-26.3c-11.1,0-20.6,5.6-25.3,13.9v-2.6C86.5,92.6,95.9,87,107,87
							C123,87,135.6,98.6,135.6,113.3"/>
						<path class="st7" d="M81.7,100.9v2.6c-0.8,1.4-1.5,2.9-2,4.5v-2.6C80.2,103.8,80.9,102.3,81.7,100.9"/>
						<path class="st8" d="M79.7,105.4v2.6c-0.4,1.3-0.8,2.7-1,4.2v-2.6C78.9,108.1,79.3,106.7,79.7,105.4"/>
						<path class="st9" d="M78.7,109.6v2.6c-0.2,1.2-0.3,2.5-0.3,3.8v-2.6C78.4,112,78.5,110.8,78.7,109.6"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_629_" class="st0" d="M118.2,111.8l0,2.6c0,12.1-4.9,22-11,22c-6.1,0-11-9.9-11-22l0-2.6c0,12.1,4.9,22,11,22
							C113.3,133.8,118.2,123.9,118.2,111.8z"/>
					</defs>
					<clipPath id="SVGID_630_">
						<use xlink:href="#SVGID_629_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st352">
						<path class="st9" d="M118.2,111.8v2.6c0,2.2-0.2,4.3-0.5,6.3v-2.6C118,116.1,118.2,114,118.2,111.8"/>
						<path class="st8" d="M117.7,118.1v2.6c-0.3,2.1-0.8,4.1-1.4,5.8v-2.6C116.9,122.2,117.4,120.2,117.7,118.1"/>
						<path class="st7" d="M116.3,123.9v2.6c-0.5,1.6-1.2,3.1-1.9,4.4v-2.6C115.2,127,115.8,125.6,116.3,123.9"/>
						<path class="st6" d="M114.4,128.3v2.6c-1.9,3.4-4.5,5.5-7.2,5.5c-6.1,0-11-9.9-11-22v-2.6c0,12.1,4.9,22,11,22
							C110,133.8,112.5,131.7,114.4,128.3"/>
					</g>
				</g>
				<g>
					<path class="st3" d="M107.2,89.8c6,0,11,9.9,11,22c0,12.1-4.9,22-11,22c-6.1,0-11-9.9-11-22C96.2,99.7,101.2,89.8,107.2,89.8z
						 M107.2,132.8c5.5,0,10-9.4,10-21c0-11.6-4.5-21-10-21c-5.5,0-10,9.4-10,21C97.2,123.4,101.7,132.8,107.2,132.8"/>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_631_" class="st0" d="M107,142.3c-16,0-28.6-11.6-28.6-26.3l0-2.6c0,14.8,12.6,26.3,28.6,26.3
							c16,0,28.6-11.6,28.6-26.3l0,2.6C135.6,130.7,123,142.3,107,142.3z"/>
					</defs>
					<clipPath id="SVGID_632_">
						<use xlink:href="#SVGID_631_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st353">
						<path class="st14" d="M135.6,113.3v2.6c0,1.3-0.1,2.5-0.3,3.8v-2.6C135.5,115.9,135.6,114.6,135.6,113.3"/>
						<path class="st13" d="M135.3,117.1v2.6c-0.2,1.4-0.5,2.8-1,4.2v-2.6C134.8,119.9,135.1,118.5,135.3,117.1"/>
						<path class="st12" d="M134.3,121.3v2.6c-0.5,1.6-1.2,3.1-2,4.5v-2.6C133.1,124.3,133.8,122.8,134.3,121.3"/>
						<path class="st11" d="M132.3,125.8v2.6c-4.7,8.3-14.2,13.9-25.3,13.9c-16,0-28.6-11.6-28.6-26.3v-2.6
							c0,14.8,12.6,26.3,28.6,26.3C118.1,139.6,127.6,134.1,132.3,125.8"/>
					</g>
				</g>
				<g>
					<path class="st3" d="M107,86c16.6,0,29.6,12,29.6,27.3c0,15.3-13,27.3-29.6,27.3c-16.6,0-29.6-12-29.6-27.3
						C77.4,98,90.4,86,107,86z M107,139.6c16,0,28.6-11.6,28.6-26.3C135.6,98.6,123,87,107,87c-16,0-28.6,11.6-28.6,26.3
						C78.4,128.1,91,139.6,107,139.6"/>
				</g>
				<g>
					<path class="st4" d="M107,87c16,0,28.6,11.6,28.6,26.3c0,14.8-12.6,26.3-28.6,26.3c-16,0-28.6-11.6-28.6-26.3
						C78.4,98.6,91,87,107,87z M107.2,133.8c6,0,11-9.9,11-22c0-12.1-4.9-22-11-22c-6.1,0-11,9.9-11,22
						C96.2,123.9,101.2,133.8,107.2,133.8"/>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_633_" class="st0" d="M136.6,113.3v2.6c0,15.3-13,27.3-29.6,27.3c-16.6,0-29.6-12-29.6-27.3l0-2.6
							c0,15.3,13,27.3,29.6,27.3C123.6,140.6,136.6,128.6,136.6,113.3z"/>
					</defs>
					<clipPath id="SVGID_634_">
						<use xlink:href="#SVGID_633_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st354">
						<path class="st9" d="M136.6,113.3v2.6c0,1.3-0.1,2.6-0.3,3.9v-2.6C136.5,116,136.6,114.7,136.6,113.3"/>
						<path class="st8" d="M136.3,117.3v2.6c-0.2,1.5-0.6,2.9-1,4.3v-2.6C135.7,120.2,136.1,118.7,136.3,117.3"/>
						<path class="st7" d="M135.3,121.6v2.6c-0.5,1.6-1.2,3.2-2.1,4.7v-2.6C134,124.8,134.7,123.2,135.3,121.6"/>
						<path class="st6" d="M133.2,126.3v2.6c-4.9,8.6-14.7,14.4-26.2,14.4c-16.6,0-29.6-12-29.6-27.3v-2.6c0,15.3,13,27.3,29.6,27.3
							C118.5,140.6,128.3,134.9,133.2,126.3"/>
					</g>
				</g>
			</g>
		</g>
		<defs>
			<filter id="Adobe_OpacityMaskFilter_16_" filterUnits="userSpaceOnUse" x="77.4" y="86" width="59.2" height="57.3">
				<feFlood  style="flood-color:white;flood-opacity:1" result="back"/>
				<feBlend  in="SourceGraphic" in2="back" mode="normal"/>
			</filter>
		</defs>
		<mask maskUnits="userSpaceOnUse" x="77.4" y="86" width="59.2" height="57.3" id="SVGID_635_">
			<g class="st355">
				
					<image style="overflow:visible;" width="64" height="62" xlink:href="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgEASABIAAD/7AARRHVja3kAAQAEAAAAHgAA/+4AIUFkb2JlAGTAAAAAAQMA
EAMCAwYAAAGDAAABlwAAAbj/2wCEABALCwsMCxAMDBAXDw0PFxsUEBAUGx8XFxcXFx8eFxoaGhoX
Hh4jJSclIx4vLzMzLy9AQEBAQEBAQEBAQEBAQEABEQ8PERMRFRISFRQRFBEUGhQWFhQaJhoaHBoa
JjAjHh4eHiMwKy4nJycuKzU1MDA1NUBAP0BAQEBAQEBAQEBAQP/CABEIAD4AQAMBIgACEQEDEQH/
xABiAAEBAQEAAAAAAAAAAAAAAAAAAQIHAQEAAAAAAAAAAAAAAAAAAAAAEAEBAQEAAAAAAAAAAAAA
AAABEEAAEQEAAAAAAAAAAAAAAAAAAABgEgEAAAAAAAAAAAAAAAAAAABA/9oADAMBAAIRAxEAAADn
4CwFIBZoksFlE1kazRLBZS5sALAWAD//2gAIAQIAAQUAwf/aAAgBAwABBQDB/9oACAEBAAEFAMTS
kaR4jSMOaY//2gAIAQICBj8AB//aAAgBAwIGPwAH/9oACAEBAQY/AAH/2Q==" transform="matrix(1 0 0 1 74.75 83.5)">
				</image>
			</g>
		</mask>
		<g class="st356">
			<g class="st0">
				<g class="st0">
					<defs>
						<path id="SVGID_636_" class="st0" d="M107.2,92.4c-6.1,0-11,9.9-11,22l0-2.6c0-12.1,4.9-22,11-22c6,0,11,9.9,11,22l0,2.6
							C118.2,102.3,113.3,92.4,107.2,92.4z"/>
					</defs>
					<clipPath id="SVGID_637_">
						<use xlink:href="#SVGID_636_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st357">
						<path class="st34" d="M118.2,111.8v2.6c0-12.1-4.9-22-11-22c-2.8,0-5.3,2.1-7.2,5.5v-2.6c1.9-3.4,4.5-5.5,7.2-5.5
							C113.3,89.8,118.2,99.7,118.2,111.8"/>
						<path class="st34" d="M100,95.2v2.6c-0.7,1.3-1.4,2.8-1.9,4.4v-2.6C98.6,98,99.2,96.5,100,95.2"/>
						<path class="st34" d="M98.1,99.6v2.6c-0.6,1.8-1.1,3.7-1.4,5.8v-2.6C97,103.4,97.5,101.4,98.1,99.6"/>
						<path class="st34" d="M96.7,105.5v2.6c-0.3,2-0.5,4.1-0.5,6.3v-2.6C96.2,109.6,96.4,107.5,96.7,105.5"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_638_" class="st0" d="M117.2,111.8l0,2.6c0-11.6-4.5-21-10-21c-5.5,0-10,9.4-10,21l0-2.6c0-11.6,4.5-21,10-21
							C112.7,90.8,117.2,100.2,117.2,111.8z"/>
					</defs>
					<clipPath id="SVGID_639_">
						<use xlink:href="#SVGID_638_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st358">
						<path class="st34" d="M117.2,111.8v2.6c0-11.6-4.5-21-10-21c-2.4,0-4.7,1.8-6.4,4.9v-2.6c1.7-3,4-4.9,6.4-4.9
							C112.7,90.8,117.2,100.2,117.2,111.8"/>
						<path class="st34" d="M100.8,95.7v2.6c-0.7,1.2-1.3,2.6-1.8,4.1v-2.6C99.5,98.2,100.1,96.9,100.8,95.7"/>
						<path class="st34" d="M99,99.8v2.6c-0.6,1.7-1,3.6-1.3,5.7v-2.6C98,103.4,98.4,101.5,99,99.8"/>
						<path class="st34" d="M97.7,105.5v2.6c-0.3,2-0.5,4.1-0.5,6.3v-2.6C97.2,109.6,97.4,107.5,97.7,105.5"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_640_" class="st0" d="M135.6,113.3l0,2.6c0-14.8-12.6-26.3-28.6-26.3c-16,0-28.6,11.6-28.6,26.3l0-2.6
							C78.4,98.6,91,87,107,87C123,87,135.6,98.6,135.6,113.3z"/>
					</defs>
					<clipPath id="SVGID_641_">
						<use xlink:href="#SVGID_640_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st359">
						<path class="st34" d="M135.6,113.3v2.6c0-14.8-12.6-26.3-28.6-26.3c-11.1,0-20.6,5.6-25.3,13.9v-2.6C86.5,92.6,95.9,87,107,87
							C123,87,135.6,98.6,135.6,113.3"/>
						<path class="st34" d="M81.7,100.9v2.6c-0.8,1.4-1.5,2.9-2,4.5v-2.6C80.2,103.8,80.9,102.3,81.7,100.9"/>
						<path class="st34" d="M79.7,105.4v2.6c-0.4,1.3-0.8,2.7-1,4.2v-2.6C78.9,108.1,79.3,106.7,79.7,105.4"/>
						<path class="st34" d="M78.7,109.6v2.6c-0.2,1.2-0.3,2.5-0.3,3.8v-2.6C78.4,112,78.5,110.8,78.7,109.6"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_642_" class="st0" d="M118.2,111.8l0,2.6c0,12.1-4.9,22-11,22c-6.1,0-11-9.9-11-22l0-2.6c0,12.1,4.9,22,11,22
							C113.3,133.8,118.2,123.9,118.2,111.8z"/>
					</defs>
					<clipPath id="SVGID_643_">
						<use xlink:href="#SVGID_642_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st360">
						<path class="st34" d="M118.2,111.8v2.6c0,2.2-0.2,4.3-0.5,6.3v-2.6C118,116.1,118.2,114,118.2,111.8"/>
						<path class="st34" d="M117.7,118.1v2.6c-0.3,2.1-0.8,4.1-1.4,5.8v-2.6C116.9,122.2,117.4,120.2,117.7,118.1"/>
						<path class="st34" d="M116.3,123.9v2.6c-0.5,1.6-1.2,3.1-1.9,4.4v-2.6C115.2,127,115.8,125.6,116.3,123.9"/>
						<path class="st34" d="M114.4,128.3v2.6c-1.9,3.4-4.5,5.5-7.2,5.5c-6.1,0-11-9.9-11-22v-2.6c0,12.1,4.9,22,11,22
							C110,133.8,112.5,131.7,114.4,128.3"/>
					</g>
				</g>
				<g>
					<path class="st34" d="M107.2,89.8c6,0,11,9.9,11,22c0,12.1-4.9,22-11,22c-6.1,0-11-9.9-11-22C96.2,99.7,101.2,89.8,107.2,89.8z
						 M107.2,132.8c5.5,0,10-9.4,10-21c0-11.6-4.5-21-10-21c-5.5,0-10,9.4-10,21C97.2,123.4,101.7,132.8,107.2,132.8"/>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_644_" class="st0" d="M107,142.3c-16,0-28.6-11.6-28.6-26.3l0-2.6c0,14.8,12.6,26.3,28.6,26.3
							c16,0,28.6-11.6,28.6-26.3l0,2.6C135.6,130.7,123,142.3,107,142.3z"/>
					</defs>
					<clipPath id="SVGID_645_">
						<use xlink:href="#SVGID_644_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st361">
						<path class="st34" d="M135.6,113.3v2.6c0,1.3-0.1,2.5-0.3,3.8v-2.6C135.5,115.9,135.6,114.6,135.6,113.3"/>
						<path class="st34" d="M135.3,117.1v2.6c-0.2,1.4-0.5,2.8-1,4.2v-2.6C134.8,119.9,135.1,118.5,135.3,117.1"/>
						<path class="st34" d="M134.3,121.3v2.6c-0.5,1.6-1.2,3.1-2,4.5v-2.6C133.1,124.3,133.8,122.8,134.3,121.3"/>
						<path class="st34" d="M132.3,125.8v2.6c-4.7,8.3-14.2,13.9-25.3,13.9c-16,0-28.6-11.6-28.6-26.3v-2.6
							c0,14.8,12.6,26.3,28.6,26.3C118.1,139.6,127.6,134.1,132.3,125.8"/>
					</g>
				</g>
				<g>
					<path class="st34" d="M107,86c16.6,0,29.6,12,29.6,27.3c0,15.3-13,27.3-29.6,27.3c-16.6,0-29.6-12-29.6-27.3
						C77.4,98,90.4,86,107,86z M107,139.6c16,0,28.6-11.6,28.6-26.3C135.6,98.6,123,87,107,87c-16,0-28.6,11.6-28.6,26.3
						C78.4,128.1,91,139.6,107,139.6"/>
				</g>
				<g>
					<path class="st34" d="M107,87c16,0,28.6,11.6,28.6,26.3c0,14.8-12.6,26.3-28.6,26.3c-16,0-28.6-11.6-28.6-26.3
						C78.4,98.6,91,87,107,87z M107.2,133.8c6,0,11-9.9,11-22c0-12.1-4.9-22-11-22c-6.1,0-11,9.9-11,22
						C96.2,123.9,101.2,133.8,107.2,133.8"/>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_646_" class="st0" d="M136.6,113.3v2.6c0,15.3-13,27.3-29.6,27.3c-16.6,0-29.6-12-29.6-27.3l0-2.6
							c0,15.3,13,27.3,29.6,27.3C123.6,140.6,136.6,128.6,136.6,113.3z"/>
					</defs>
					<clipPath id="SVGID_647_">
						<use xlink:href="#SVGID_646_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st362">
						<path class="st34" d="M136.6,113.3v2.6c0,1.3-0.1,2.6-0.3,3.9v-2.6C136.5,116,136.6,114.7,136.6,113.3"/>
						<path class="st34" d="M136.3,117.3v2.6c-0.2,1.5-0.6,2.9-1,4.3v-2.6C135.7,120.2,136.1,118.7,136.3,117.3"/>
						<path class="st34" d="M135.3,121.6v2.6c-0.5,1.6-1.2,3.2-2.1,4.7v-2.6C134,124.8,134.7,123.2,135.3,121.6"/>
						<path class="st34" d="M133.2,126.3v2.6c-4.9,8.6-14.7,14.4-26.2,14.4c-16.6,0-29.6-12-29.6-27.3v-2.6
							c0,15.3,13,27.3,29.6,27.3C118.5,140.6,128.3,134.9,133.2,126.3"/>
					</g>
				</g>
			</g>
		</g>
	</g>
</g>
<g id="s">
	<g>
	</g>
	<g>
		<g>
			<g class="st0">
				<g class="st0">
					<defs>
						<path id="SVGID_648_" class="st0" d="M177.2,89l0,2.6c-4.7-2.3-11.9-3.9-18.1-3.9c-12.6,0-20.7,6.7-20.7,17v-2.6
							c0-10.4,8.1-17,20.7-17C165.3,85.1,172.5,86.6,177.2,89z"/>
					</defs>
					<clipPath id="SVGID_649_">
						<use xlink:href="#SVGID_648_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st363">
						<path class="st6" d="M177.2,89v2.6c-4.7-2.3-11.9-3.9-18.1-3.9c-8.9,0-15.5,3.3-18.7,8.9V94c3.2-5.6,9.8-8.9,18.7-8.9
							C165.3,85.1,172.5,86.6,177.2,89"/>
						<path class="st7" d="M140.5,94v2.6c-0.5,0.9-0.9,1.8-1.2,2.7v-2.6C139.6,95.7,140,94.8,140.5,94"/>
						<path class="st8" d="M139.2,96.7v2.6c-0.3,0.9-0.5,1.8-0.6,2.7v-2.6C138.7,98.5,139,97.5,139.2,96.7"/>
						<path class="st9" d="M138.6,99.4v2.6c-0.1,0.9-0.2,1.8-0.2,2.7v-2.6C138.4,101.2,138.5,100.3,138.6,99.4"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_650_" class="st0" d="M162,103.2c-2.9,0-3.2,1.5-3.2,2.1v-2.6c0-0.6,0.3-2.1,3.2-2.1c2.6,0,8.7,1.2,15.3,4.3
							v2.6C170.7,104.4,164.5,103.2,162,103.2z"/>
					</defs>
					<clipPath id="SVGID_651_">
						<use xlink:href="#SVGID_650_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st364">
						<path class="st11" d="M177.2,104.9v2.6c-6.6-3.1-12.7-4.3-15.3-4.3c-1.8,0-2.6,0.6-2.9,1.2v-2.6c0.3-0.6,1.1-1.2,2.9-1.2
							C164.5,100.6,170.7,101.8,177.2,104.9"/>
						<path class="st12" d="M159,101.8v2.6c-0.1,0.1-0.1,0.2-0.1,0.3v-2.6C158.9,102,159,101.9,159,101.8"/>
						<path class="st13" d="M158.9,102.1v2.6c0,0.1-0.1,0.2-0.1,0.3v-2.6C158.8,102.3,158.8,102.2,158.9,102.1"/>
						<path class="st14" d="M158.8,102.4v2.6c0,0.1,0,0.2,0,0.3v-2.6C158.8,102.6,158.8,102.5,158.8,102.4"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_652_" class="st0" d="M177.5,106.2v2.6c-6.7-3.3-13-4.5-15.5-4.5c-1,0-2.2,0.2-2.2,1.1v-2.6
							c0-0.9,1.2-1.1,2.2-1.1C164.5,101.6,170.8,102.8,177.5,106.2z"/>
					</defs>
					<clipPath id="SVGID_653_">
						<use xlink:href="#SVGID_652_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st365">
						<path class="st6" d="M177.5,106.2v2.6c-6.7-3.3-13-4.5-15.5-4.5c-0.8,0-1.7,0.1-2.1,0.7v-2.6c0.3-0.5,1.2-0.7,2.1-0.7
							C164.5,101.6,170.8,102.8,177.5,106.2"/>
						<path class="st7" d="M159.9,102.3v2.6c0,0,0,0.1-0.1,0.1v-2.6C159.9,102.4,159.9,102.3,159.9,102.3"/>
						<path class="st8" d="M159.8,102.4v2.6c0,0,0,0.1,0,0.1v-2.6C159.8,102.5,159.8,102.5,159.8,102.4"/>
						<path class="st9" d="M159.8,102.6v2.6c0,0,0,0.1,0,0.1v-2.6C159.8,102.6,159.8,102.6,159.8,102.6"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_654_" class="st0" d="M160.8,105v2.6c-0.6-0.2-2-0.7-2-2.3l0-2.6C158.8,104.3,160.2,104.8,160.8,105z"/>
					</defs>
					<clipPath id="SVGID_655_">
						<use xlink:href="#SVGID_654_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st366">
						<path class="st6" d="M160.8,105v2.6c-0.6-0.2-2-0.7-2-2.3v-2.6C158.8,104.3,160.2,104.8,160.8,105"/>
					</g>
				</g>
				<g>
					<polygon class="st1" points="178.2,106.5 178.2,109.1 177.5,108.8 177.5,106.2 					"/>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_656_" class="st0" d="M175.1,110.4v2.6c-4-3.2-9.3-4.3-12.5-5c-0.8-0.2-1.4-0.3-1.8-0.4V105
							c0.4,0.1,1.1,0.3,1.8,0.4C165.8,106.1,171.1,107.3,175.1,110.4z"/>
					</defs>
					<clipPath id="SVGID_657_">
						<use xlink:href="#SVGID_656_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st367">
						<path class="st6" d="M175.1,110.4v2.6c-4-3.2-9.3-4.3-12.5-5c-0.8-0.2-1.4-0.3-1.8-0.4V105c0.4,0.1,1.1,0.3,1.8,0.4
							C165.8,106.1,171.1,107.3,175.1,110.4"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_658_" class="st0" d="M157.8,118.6v2.6c-0.4-0.1-1-0.2-1.6-0.4c-3.1-0.6-9-1.8-13.3-5.7
							c-3-2.7-4.4-6.1-4.4-10.5v-2.6c0,4.4,1.5,7.8,4.4,10.5c4.4,3.8,10.2,5,13.3,5.7C156.8,118.4,157.3,118.5,157.8,118.6z"/>
					</defs>
					<clipPath id="SVGID_659_">
						<use xlink:href="#SVGID_658_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st368">
						<path class="st11" d="M157.8,118.6v2.6c-0.4-0.1-1-0.2-1.6-0.4c-3.1-0.6-9-1.8-13.3-5.7c-3-2.7-4.4-6.1-4.4-10.5v-2.6
							c0,4.4,1.5,7.8,4.4,10.5c4.4,3.8,10.2,5,13.3,5.7C156.8,118.4,157.3,118.5,157.8,118.6"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_660_" class="st0" d="M157.5,119.6v2.6c-0.4-0.1-0.9-0.2-1.5-0.3c-3.2-0.7-9.2-1.9-13.8-5.9
							c-3.2-2.9-4.8-6.5-4.8-11.2l0-2.6c0,4.7,1.6,8.4,4.8,11.2c4.6,4,10.6,5.2,13.8,5.9C156.6,119.4,157.1,119.5,157.5,119.6z"/>
					</defs>
					<clipPath id="SVGID_661_">
						<use xlink:href="#SVGID_660_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st369">
						<path class="st6" d="M157.5,119.6v2.6c-0.4-0.1-0.9-0.2-1.5-0.3c-3.2-0.7-9.2-1.9-13.8-5.9c-3.2-2.9-4.8-6.5-4.8-11.2v-2.6
							c0,4.7,1.6,8.4,4.8,11.2c4.6,4,10.6,5.2,13.8,5.9C156.6,119.4,157.1,119.5,157.5,119.6"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_662_" class="st0" d="M159.7,123.4c0-1.7-1.5-2-2-2.1v-2.6c0.5,0.1,2,0.4,2,2.1v0.3V123.4
							C159.7,123.4,159.7,123.4,159.7,123.4C159.7,123.4,159.7,123.4,159.7,123.4z"/>
					</defs>
					<clipPath id="SVGID_663_">
						<use xlink:href="#SVGID_662_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st370">
						<path class="st11" d="M159.7,120.8v2.6c0-1.7-1.5-2-2-2.1v-2.6C158.2,118.7,159.7,119.1,159.7,120.8"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_664_" class="st0" d="M158.7,120.8l0,2.6c0-0.5-0.2-0.9-1.2-1.2v-2.6C158.5,119.8,158.7,120.2,158.7,120.8z"/>
					</defs>
					<clipPath id="SVGID_665_">
						<use xlink:href="#SVGID_664_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st371">
						<path class="st6" d="M158.7,120.8v2.6c0-0.5-0.2-0.9-1.2-1.2v-2.6C158.5,119.8,158.7,120.2,158.7,120.8"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_666_" class="st0" d="M180.2,121.3v2.6c0-4.5-1.7-8.1-5.1-10.8v-2.6C178.5,113.2,180.2,116.7,180.2,121.3z"/>
					</defs>
					<clipPath id="SVGID_667_">
						<use xlink:href="#SVGID_666_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st372">
						<path class="st6" d="M180.2,121.3v2.6c0-4.5-1.7-8.1-5.1-10.8v-2.6C178.5,113.2,180.2,116.7,180.2,121.3"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_668_" class="st0" d="M159.7,120.8l0,2.6c0,0.8-0.4,2.2-3.2,2.2c-3,0-9.8-1.5-16.6-5.8l0-2.6
							c6.8,4.2,13.6,5.8,16.6,5.8C159.3,123,159.7,121.6,159.7,120.8z"/>
					</defs>
					<clipPath id="SVGID_669_">
						<use xlink:href="#SVGID_668_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st373">
						<path class="st9" d="M159.7,120.8v2.6c0,0.1,0,0.2,0,0.4v-2.6C159.7,121,159.7,120.9,159.7,120.8"/>
						<path class="st8" d="M159.7,121.1v2.6c0,0.1,0,0.2-0.1,0.4v-2.6C159.6,121.3,159.7,121.2,159.7,121.1"/>
						<path class="st7" d="M159.6,121.5v2.6c0,0.1-0.1,0.2-0.2,0.3v-2.6C159.5,121.7,159.6,121.6,159.6,121.5"/>
						<path class="st6" d="M159.5,121.8v2.6c-0.4,0.6-1.2,1.2-2.9,1.2c-3,0-9.8-1.5-16.6-5.8v-2.6c6.8,4.2,13.6,5.8,16.6,5.8
							C158.3,123,159.1,122.4,159.5,121.8"/>
					</g>
				</g>
				<g>
					<polygon class="st1" points="139.2,134.4 139.2,137 138.9,136.9 138.9,134.3 					"/>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_670_" class="st0" d="M180.2,123.9c0,10-8.3,16.5-21.2,16.5c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
							c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
							c12.9,0,21.2-6.5,21.2-16.5C180.2,122,180.2,123.1,180.2,123.9z"/>
					</defs>
					<clipPath id="SVGID_671_">
						<use xlink:href="#SVGID_670_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st374">
						<path class="st14" d="M180.2,121.3v2.6c0,0.9-0.1,1.7-0.2,2.5v-2.6C180.1,122.9,180.2,122.1,180.2,121.3"/>
						<path class="st13" d="M180,123.7v2.6c-0.1,0.9-0.3,1.7-0.6,2.5v-2.6C179.7,125.5,179.9,124.6,180,123.7"/>
						<path class="st12" d="M179.4,126.3v2.6c-0.3,0.9-0.7,1.8-1.1,2.6v-2.6C178.7,128,179.1,127.2,179.4,126.3"/>
						<path class="st11" d="M178.3,128.9v2.6c-3.2,5.6-10.1,8.9-19.3,8.9c-6.6,0-14.2-1.6-19-4v-2.6c4.8,2.4,12.4,4,19,4
							C168.2,137.7,175.1,134.4,178.3,128.9"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_672_" class="st0" d="M158.9,140.3c-6.6,0-14.2-1.6-19-4c0-0.8,0-1.5,0-2.3v-0.3c4.8,2.4,12.4,4,19,4
							c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
							c0,0.1,0,0.2,0,0.3C158.9,140.1,158.9,140.2,158.9,140.3z"/>
					</defs>
					<clipPath id="SVGID_673_">
						<use xlink:href="#SVGID_672_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st375">
						<path class="st11" d="M178.3,128.9v2.6c-3.2,5.6-10.1,8.9-19.3,8.9c-6.6,0-14.2-1.6-19-4v-2.6c4.8,2.4,12.4,4,19,4
							C168.2,137.7,175.1,134.4,178.3,128.9"/>
					</g>
				</g>
				<g>
					<path class="st4" d="M159.1,85.1c6.2,0,13.4,1.6,18.1,3.9v15.9c-6.6-3.1-12.7-4.3-15.3-4.3c-2.9,0-3.2,1.5-3.2,2.1
						c0,1.6,1.5,2.1,2,2.3c0.4,0.1,1.1,0.3,1.8,0.4c3.2,0.7,8.5,1.8,12.5,5c3.4,2.8,5.1,6.3,5.1,10.8c0,10-8.3,16.5-21.2,16.5
						c-6.6,0-14.2-1.6-19-4v-16.5c6.8,4.2,13.6,5.8,16.6,5.8c2.8,0,3.2-1.4,3.2-2.2c0-1.7-1.5-2-2-2.1c-0.4-0.1-1-0.2-1.6-0.4
						c-3.1-0.6-9-1.8-13.3-5.7c-3-2.7-4.4-6.1-4.4-10.5C138.4,91.8,146.5,85.1,159.1,85.1z"/>
				</g>
				<g>
					<path class="st3" d="M175.7,109.6c3.7,3,5.5,6.8,5.5,11.6c0,10.6-8.7,17.5-22.2,17.5c-7,0-14.7-1.7-19.7-4.3l-0.3-0.1v-18.9
						l0.8,0.5c6.9,4.5,13.9,6.1,16.9,6.1c2.2,0,2.2-0.9,2.2-1.2c0-0.5-0.2-0.9-1.2-1.2c-0.4-0.1-0.9-0.2-1.5-0.3
						c-3.2-0.7-9.2-1.9-13.8-5.9c-3.2-2.9-4.8-6.5-4.8-11.2c0-11,8.5-18,21.7-18c6.4,0,14,1.7,18.8,4.2l0.3,0.1v18.2l-0.7-0.4
						c-6.7-3.3-13-4.5-15.5-4.5c-1,0-2.2,0.2-2.2,1.1c0,0.6,0.4,1,1.3,1.3c0.4,0.1,1,0.2,1.8,0.4
						C166.1,105.2,171.5,106.3,175.7,109.6z M158.9,137.7c12.9,0,21.2-6.5,21.2-16.5c0-4.5-1.7-8.1-5.1-10.8c-4-3.2-9.3-4.3-12.5-5
						c-0.8-0.2-1.4-0.3-1.8-0.4c-0.6-0.2-2-0.7-2-2.3c0-0.6,0.3-2.1,3.2-2.1c2.6,0,8.7,1.2,15.3,4.3V89c-4.7-2.3-11.9-3.9-18.1-3.9
						c-12.6,0-20.7,6.7-20.7,17c0,4.4,1.5,7.8,4.4,10.5c4.4,3.8,10.2,5,13.3,5.7c0.6,0.1,1.2,0.2,1.6,0.4c0.5,0.1,2,0.4,2,2.1
						c0,0.8-0.4,2.2-3.2,2.2c-3,0-9.8-1.5-16.6-5.8v16.5C144.7,136.1,152.3,137.7,158.9,137.7"/>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_674_" class="st0" d="M181.2,121.3l0,2.6c0,10.6-8.7,17.5-22.2,17.5c-7,0-14.7-1.7-19.7-4.3v-2.6
							c5,2.6,12.8,4.3,19.7,4.3C172.4,138.7,181.2,131.9,181.2,121.3z"/>
					</defs>
					<clipPath id="SVGID_675_">
						<use xlink:href="#SVGID_674_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st376">
						<path class="st9" d="M181.2,121.3v2.6c0,0.9-0.1,1.8-0.2,2.7v-2.6C181.1,123.1,181.2,122.2,181.2,121.3"/>
						<path class="st8" d="M181,123.9v2.6c-0.1,0.9-0.4,1.8-0.6,2.7v-2.6C180.6,125.8,180.8,124.9,181,123.9"/>
						<path class="st7" d="M180.3,126.6v2.6c-0.3,1-0.7,1.9-1.2,2.7v-2.6C179.6,128.5,180,127.6,180.3,126.6"/>
						<path class="st6" d="M179.1,129.4v2.6c-3.3,5.9-10.6,9.3-20.2,9.3c-7,0-14.7-1.7-19.7-4.3v-2.6c5,2.6,12.8,4.3,19.7,4.3
							C168.6,138.7,175.8,135.2,179.1,129.4"/>
					</g>
				</g>
			</g>
		</g>
		<defs>
			<filter id="Adobe_OpacityMaskFilter_17_" filterUnits="userSpaceOnUse" x="137.4" y="84.1" width="43.8" height="57.3">
				<feFlood  style="flood-color:white;flood-opacity:1" result="back"/>
				<feBlend  in="SourceGraphic" in2="back" mode="normal"/>
			</filter>
		</defs>
		<mask maskUnits="userSpaceOnUse" x="137.4" y="84.1" width="43.8" height="57.3" id="SVGID_676_">
			<g class="st377">
				
					<image style="overflow:visible;" width="49" height="62" xlink:href="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgEASABIAAD/7AARRHVja3kAAQAEAAAAHgAA/+4AIUFkb2JlAGTAAAAAAQMA
EAMCAwYAAAGYAAAB1QAAAfb/2wCEABALCwsMCxAMDBAXDw0PFxsUEBAUGx8XFxcXFx8eFxoaGhoX
Hh4jJSclIx4vLzMzLy9AQEBAQEBAQEBAQEBAQEABEQ8PERMRFRISFRQRFBEUGhQWFhQaJhoaHBoa
JjAjHh4eHiMwKy4nJycuKzU1MDA1NUBAP0BAQEBAQEBAQEBAQP/CABEIAD4AMQMBIgACEQEDEQH/
xABrAAEBAQEBAQAAAAAAAAAAAAAAAwQCAQcBAQAAAAAAAAAAAAAAAAAAAAAQAAICAgIDAAAAAAAA
AAAAAAABAgMQMSARQCESEQEAAAAAAAAAAAAAAAAAAABQEgEAAAAAAAAAAAAAAAAAAABA/9oADAMB
AAIRAxEAAAD5+UOFxnUmAe3hpKOhmhozgHujPY0uRLPeB4B3wLojrgAP/9oACAECAAEFAPA//9oA
CAEDAAEFAPA//9oACAEBAAEFAMfLPlnXCMexVjgSj0PEd1oURosQ9kd1iJFo8LdQhlhLYt1sUhyJ
yJZjLoVg7CUuPs98P//aAAgBAgIGPwAH/9oACAEDAgY/AAf/2gAIAQEBBj8AY//Z" transform="matrix(1 0 0 1 134.75 81.5)">
				</image>
			</g>
		</mask>
		<g class="st378">
			<g class="st0">
				<g class="st0">
					<defs>
						<path id="SVGID_677_" class="st0" d="M177.2,89l0,2.6c-4.7-2.3-11.9-3.9-18.1-3.9c-12.6,0-20.7,6.7-20.7,17v-2.6
							c0-10.4,8.1-17,20.7-17C165.3,85.1,172.5,86.6,177.2,89z"/>
					</defs>
					<clipPath id="SVGID_678_">
						<use xlink:href="#SVGID_677_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st379">
						<path class="st34" d="M177.2,89v2.6c-4.7-2.3-11.9-3.9-18.1-3.9c-8.9,0-15.5,3.3-18.7,8.9V94c3.2-5.6,9.8-8.9,18.7-8.9
							C165.3,85.1,172.5,86.6,177.2,89"/>
						<path class="st34" d="M140.5,94v2.6c-0.5,0.9-0.9,1.8-1.2,2.7v-2.6C139.6,95.7,140,94.8,140.5,94"/>
						<path class="st34" d="M139.2,96.7v2.6c-0.3,0.9-0.5,1.8-0.6,2.7v-2.6C138.7,98.5,139,97.5,139.2,96.7"/>
						<path class="st34" d="M138.6,99.4v2.6c-0.1,0.9-0.2,1.8-0.2,2.7v-2.6C138.4,101.2,138.5,100.3,138.6,99.4"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_679_" class="st0" d="M162,103.2c-2.9,0-3.2,1.5-3.2,2.1v-2.6c0-0.6,0.3-2.1,3.2-2.1c2.6,0,8.7,1.2,15.3,4.3
							v2.6C170.7,104.4,164.5,103.2,162,103.2z"/>
					</defs>
					<clipPath id="SVGID_680_">
						<use xlink:href="#SVGID_679_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st380">
						<path class="st34" d="M177.2,104.9v2.6c-6.6-3.1-12.7-4.3-15.3-4.3c-1.8,0-2.6,0.6-2.9,1.2v-2.6c0.3-0.6,1.1-1.2,2.9-1.2
							C164.5,100.6,170.7,101.8,177.2,104.9"/>
						<path class="st34" d="M159,101.8v2.6c-0.1,0.1-0.1,0.2-0.1,0.3v-2.6C158.9,102,159,101.9,159,101.8"/>
						<path class="st34" d="M158.9,102.1v2.6c0,0.1-0.1,0.2-0.1,0.3v-2.6C158.8,102.3,158.8,102.2,158.9,102.1"/>
						<path class="st34" d="M158.8,102.4v2.6c0,0.1,0,0.2,0,0.3v-2.6C158.8,102.6,158.8,102.5,158.8,102.4"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_681_" class="st0" d="M177.5,106.2v2.6c-6.7-3.3-13-4.5-15.5-4.5c-1,0-2.2,0.2-2.2,1.1v-2.6
							c0-0.9,1.2-1.1,2.2-1.1C164.5,101.6,170.8,102.8,177.5,106.2z"/>
					</defs>
					<clipPath id="SVGID_682_">
						<use xlink:href="#SVGID_681_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st381">
						<path class="st34" d="M177.5,106.2v2.6c-6.7-3.3-13-4.5-15.5-4.5c-0.8,0-1.7,0.1-2.1,0.7v-2.6c0.3-0.5,1.2-0.7,2.1-0.7
							C164.5,101.6,170.8,102.8,177.5,106.2"/>
						<path class="st34" d="M159.9,102.3v2.6c0,0,0,0.1-0.1,0.1v-2.6C159.9,102.4,159.9,102.3,159.9,102.3"/>
						<path class="st34" d="M159.8,102.4v2.6c0,0,0,0.1,0,0.1v-2.6C159.8,102.5,159.8,102.5,159.8,102.4"/>
						<path class="st34" d="M159.8,102.6v2.6c0,0,0,0.1,0,0.1v-2.6C159.8,102.6,159.8,102.6,159.8,102.6"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_683_" class="st0" d="M160.8,105v2.6c-0.6-0.2-2-0.7-2-2.3l0-2.6C158.8,104.3,160.2,104.8,160.8,105z"/>
					</defs>
					<clipPath id="SVGID_684_">
						<use xlink:href="#SVGID_683_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st382">
						<path class="st34" d="M160.8,105v2.6c-0.6-0.2-2-0.7-2-2.3v-2.6C158.8,104.3,160.2,104.8,160.8,105"/>
					</g>
				</g>
				<g>
					<polygon class="st34" points="178.2,106.5 178.2,109.1 177.5,108.8 177.5,106.2 					"/>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_685_" class="st0" d="M175.1,110.4v2.6c-4-3.2-9.3-4.3-12.5-5c-0.8-0.2-1.4-0.3-1.8-0.4V105
							c0.4,0.1,1.1,0.3,1.8,0.4C165.8,106.1,171.1,107.3,175.1,110.4z"/>
					</defs>
					<clipPath id="SVGID_686_">
						<use xlink:href="#SVGID_685_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st383">
						<path class="st34" d="M175.1,110.4v2.6c-4-3.2-9.3-4.3-12.5-5c-0.8-0.2-1.4-0.3-1.8-0.4V105c0.4,0.1,1.1,0.3,1.8,0.4
							C165.8,106.1,171.1,107.3,175.1,110.4"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_687_" class="st0" d="M157.8,118.6v2.6c-0.4-0.1-1-0.2-1.6-0.4c-3.1-0.6-9-1.8-13.3-5.7
							c-3-2.7-4.4-6.1-4.4-10.5v-2.6c0,4.4,1.5,7.8,4.4,10.5c4.4,3.8,10.2,5,13.3,5.7C156.8,118.4,157.3,118.5,157.8,118.6z"/>
					</defs>
					<clipPath id="SVGID_688_">
						<use xlink:href="#SVGID_687_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st384">
						<path class="st34" d="M157.8,118.6v2.6c-0.4-0.1-1-0.2-1.6-0.4c-3.1-0.6-9-1.8-13.3-5.7c-3-2.7-4.4-6.1-4.4-10.5v-2.6
							c0,4.4,1.5,7.8,4.4,10.5c4.4,3.8,10.2,5,13.3,5.7C156.8,118.4,157.3,118.5,157.8,118.6"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_689_" class="st0" d="M157.5,119.6v2.6c-0.4-0.1-0.9-0.2-1.5-0.3c-3.2-0.7-9.2-1.9-13.8-5.9
							c-3.2-2.9-4.8-6.5-4.8-11.2l0-2.6c0,4.7,1.6,8.4,4.8,11.2c4.6,4,10.6,5.2,13.8,5.9C156.6,119.4,157.1,119.5,157.5,119.6z"/>
					</defs>
					<clipPath id="SVGID_690_">
						<use xlink:href="#SVGID_689_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st385">
						<path class="st34" d="M157.5,119.6v2.6c-0.4-0.1-0.9-0.2-1.5-0.3c-3.2-0.7-9.2-1.9-13.8-5.9c-3.2-2.9-4.8-6.5-4.8-11.2v-2.6
							c0,4.7,1.6,8.4,4.8,11.2c4.6,4,10.6,5.2,13.8,5.9C156.6,119.4,157.1,119.5,157.5,119.6"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_691_" class="st0" d="M159.7,123.4c0-1.7-1.5-2-2-2.1v-2.6c0.5,0.1,2,0.4,2,2.1v0.3V123.4
							C159.7,123.4,159.7,123.4,159.7,123.4C159.7,123.4,159.7,123.4,159.7,123.4z"/>
					</defs>
					<clipPath id="SVGID_692_">
						<use xlink:href="#SVGID_691_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st386">
						<path class="st34" d="M159.7,120.8v2.6c0-1.7-1.5-2-2-2.1v-2.6C158.2,118.7,159.7,119.1,159.7,120.8"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_693_" class="st0" d="M158.7,120.8l0,2.6c0-0.5-0.2-0.9-1.2-1.2v-2.6C158.5,119.8,158.7,120.2,158.7,120.8z"/>
					</defs>
					<clipPath id="SVGID_694_">
						<use xlink:href="#SVGID_693_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st387">
						<path class="st34" d="M158.7,120.8v2.6c0-0.5-0.2-0.9-1.2-1.2v-2.6C158.5,119.8,158.7,120.2,158.7,120.8"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_695_" class="st0" d="M180.2,121.3v2.6c0-4.5-1.7-8.1-5.1-10.8v-2.6C178.5,113.2,180.2,116.7,180.2,121.3z"/>
					</defs>
					<clipPath id="SVGID_696_">
						<use xlink:href="#SVGID_695_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st388">
						<path class="st34" d="M180.2,121.3v2.6c0-4.5-1.7-8.1-5.1-10.8v-2.6C178.5,113.2,180.2,116.7,180.2,121.3"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_697_" class="st0" d="M159.7,120.8l0,2.6c0,0.8-0.4,2.2-3.2,2.2c-3,0-9.8-1.5-16.6-5.8l0-2.6
							c6.8,4.2,13.6,5.8,16.6,5.8C159.3,123,159.7,121.6,159.7,120.8z"/>
					</defs>
					<clipPath id="SVGID_698_">
						<use xlink:href="#SVGID_697_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st389">
						<path class="st34" d="M159.7,120.8v2.6c0,0.1,0,0.2,0,0.4v-2.6C159.7,121,159.7,120.9,159.7,120.8"/>
						<path class="st34" d="M159.7,121.1v2.6c0,0.1,0,0.2-0.1,0.4v-2.6C159.6,121.3,159.7,121.2,159.7,121.1"/>
						<path class="st34" d="M159.6,121.5v2.6c0,0.1-0.1,0.2-0.2,0.3v-2.6C159.5,121.7,159.6,121.6,159.6,121.5"/>
						<path class="st34" d="M159.5,121.8v2.6c-0.4,0.6-1.2,1.2-2.9,1.2c-3,0-9.8-1.5-16.6-5.8v-2.6c6.8,4.2,13.6,5.8,16.6,5.8
							C158.3,123,159.1,122.4,159.5,121.8"/>
					</g>
				</g>
				<g>
					<polygon class="st34" points="139.2,134.4 139.2,137 138.9,136.9 138.9,134.3 					"/>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_699_" class="st0" d="M180.2,123.9c0,10-8.3,16.5-21.2,16.5c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
							c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
							c12.9,0,21.2-6.5,21.2-16.5C180.2,122,180.2,123.1,180.2,123.9z"/>
					</defs>
					<clipPath id="SVGID_700_">
						<use xlink:href="#SVGID_699_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st390">
						<path class="st34" d="M180.2,121.3v2.6c0,0.9-0.1,1.7-0.2,2.5v-2.6C180.1,122.9,180.2,122.1,180.2,121.3"/>
						<path class="st34" d="M180,123.7v2.6c-0.1,0.9-0.3,1.7-0.6,2.5v-2.6C179.7,125.5,179.9,124.6,180,123.7"/>
						<path class="st34" d="M179.4,126.3v2.6c-0.3,0.9-0.7,1.8-1.1,2.6v-2.6C178.7,128,179.1,127.2,179.4,126.3"/>
						<path class="st34" d="M178.3,128.9v2.6c-3.2,5.6-10.1,8.9-19.3,8.9c-6.6,0-14.2-1.6-19-4v-2.6c4.8,2.4,12.4,4,19,4
							C168.2,137.7,175.1,134.4,178.3,128.9"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_701_" class="st0" d="M158.9,140.3c-6.6,0-14.2-1.6-19-4c0-0.8,0-1.5,0-2.3v-0.3c4.8,2.4,12.4,4,19,4
							c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
							c0,0.1,0,0.2,0,0.3C158.9,140.1,158.9,140.2,158.9,140.3z"/>
					</defs>
					<clipPath id="SVGID_702_">
						<use xlink:href="#SVGID_701_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st391">
						<path class="st34" d="M178.3,128.9v2.6c-3.2,5.6-10.1,8.9-19.3,8.9c-6.6,0-14.2-1.6-19-4v-2.6c4.8,2.4,12.4,4,19,4
							C168.2,137.7,175.1,134.4,178.3,128.9"/>
					</g>
				</g>
				<g>
					<path class="st34" d="M159.1,85.1c6.2,0,13.4,1.6,18.1,3.9v15.9c-6.6-3.1-12.7-4.3-15.3-4.3c-2.9,0-3.2,1.5-3.2,2.1
						c0,1.6,1.5,2.1,2,2.3c0.4,0.1,1.1,0.3,1.8,0.4c3.2,0.7,8.5,1.8,12.5,5c3.4,2.8,5.1,6.3,5.1,10.8c0,10-8.3,16.5-21.2,16.5
						c-6.6,0-14.2-1.6-19-4v-16.5c6.8,4.2,13.6,5.8,16.6,5.8c2.8,0,3.2-1.4,3.2-2.2c0-1.7-1.5-2-2-2.1c-0.4-0.1-1-0.2-1.6-0.4
						c-3.1-0.6-9-1.8-13.3-5.7c-3-2.7-4.4-6.1-4.4-10.5C138.4,91.8,146.5,85.1,159.1,85.1z"/>
				</g>
				<g>
					<path class="st34" d="M175.7,109.6c3.7,3,5.5,6.8,5.5,11.6c0,10.6-8.7,17.5-22.2,17.5c-7,0-14.7-1.7-19.7-4.3l-0.3-0.1v-18.9
						l0.8,0.5c6.9,4.5,13.9,6.1,16.9,6.1c2.2,0,2.2-0.9,2.2-1.2c0-0.5-0.2-0.9-1.2-1.2c-0.4-0.1-0.9-0.2-1.5-0.3
						c-3.2-0.7-9.2-1.9-13.8-5.9c-3.2-2.9-4.8-6.5-4.8-11.2c0-11,8.5-18,21.7-18c6.4,0,14,1.7,18.8,4.2l0.3,0.1v18.2l-0.7-0.4
						c-6.7-3.3-13-4.5-15.5-4.5c-1,0-2.2,0.2-2.2,1.1c0,0.6,0.4,1,1.3,1.3c0.4,0.1,1,0.2,1.8,0.4
						C166.1,105.2,171.5,106.3,175.7,109.6z M158.9,137.7c12.9,0,21.2-6.5,21.2-16.5c0-4.5-1.7-8.1-5.1-10.8c-4-3.2-9.3-4.3-12.5-5
						c-0.8-0.2-1.4-0.3-1.8-0.4c-0.6-0.2-2-0.7-2-2.3c0-0.6,0.3-2.1,3.2-2.1c2.6,0,8.7,1.2,15.3,4.3V89c-4.7-2.3-11.9-3.9-18.1-3.9
						c-12.6,0-20.7,6.7-20.7,17c0,4.4,1.5,7.8,4.4,10.5c4.4,3.8,10.2,5,13.3,5.7c0.6,0.1,1.2,0.2,1.6,0.4c0.5,0.1,2,0.4,2,2.1
						c0,0.8-0.4,2.2-3.2,2.2c-3,0-9.8-1.5-16.6-5.8v16.5C144.7,136.1,152.3,137.7,158.9,137.7"/>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_703_" class="st0" d="M181.2,121.3l0,2.6c0,10.6-8.7,17.5-22.2,17.5c-7,0-14.7-1.7-19.7-4.3v-2.6
							c5,2.6,12.8,4.3,19.7,4.3C172.4,138.7,181.2,131.9,181.2,121.3z"/>
					</defs>
					<clipPath id="SVGID_704_">
						<use xlink:href="#SVGID_703_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st392">
						<path class="st34" d="M181.2,121.3v2.6c0,0.9-0.1,1.8-0.2,2.7v-2.6C181.1,123.1,181.2,122.2,181.2,121.3"/>
						<path class="st34" d="M181,123.9v2.6c-0.1,0.9-0.4,1.8-0.6,2.7v-2.6C180.6,125.8,180.8,124.9,181,123.9"/>
						<path class="st34" d="M180.3,126.6v2.6c-0.3,1-0.7,1.9-1.2,2.7v-2.6C179.6,128.5,180,127.6,180.3,126.6"/>
						<path class="st34" d="M179.1,129.4v2.6c-3.3,5.9-10.6,9.3-20.2,9.3c-7,0-14.7-1.7-19.7-4.3v-2.6c5,2.6,12.8,4.3,19.7,4.3
							C168.6,138.7,175.8,135.2,179.1,129.4"/>
					</g>
				</g>
			</g>
		</g>
	</g>
</g>
<g id="circle">
	<g>
		<g>
			<g class="st0">
				<g>
					<polygon class="st1" points="182.8,83.4 182.8,86 182.8,85.4 182.8,82.8 					"/>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_705_" class="st0" d="M235.8,83.7v2.6c-4,0-8,0-12,0v-2.6C227.8,83.7,231.8,83.7,235.8,83.7z"/>
					</defs>
					<clipPath id="SVGID_706_">
						<use xlink:href="#SVGID_705_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st393">
						<path class="st6" d="M235.8,83.5v2.9c-4,0-8,0-12,0v-2.6c4,0,8,0,12,0"/>
					</g>
				</g>
				<g>
					<rect x="235.8" y="83.5" class="st1" width="5" height="3"/>
				</g>
				<g class="st0">
					<defs>
						<polygon id="SVGID_707_" class="st0" points="240.8,83.8 240.8,86.4 240.8,86.4 240.8,83.7 						"/>
					</defs>
					<clipPath id="SVGID_708_">
						<use xlink:href="#SVGID_707_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st394">
						<polyline class="st6" points="240.8,83.5 240.8,86.4 240.8,86.4 240.8,83.7 240.8,83.5 						"/>
					</g>
				</g>
				<g>
					<rect x="183.8" y="83.5" class="st1" width="1" height="3"/>
				</g>
				<g>
					<polygon class="st395" points="242.8,83.3 242.8,85.9 241.8,86.5 241.8,83.9 					"/>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_709_" class="st0" d="M205.8,89.1v2.6c0-0.5-0.5-1-0.9-1.5c-0.8-1.1-1.6-2.3-2.4-3.4c-0.2-0.3-0.5-0.5-0.7-0.5
							c-3.1,0-6.3,0-9.4,0c-2.5,0-4.6,0-7.6,0v-2.6c3,0,5.2,0,7.6,0c3.1,0,6.2,0,9.3,0c0.2,0,0.6,0.2,0.8,0.5
							c0.9,1.1,1.5,2.2,2.4,3.4C205.2,88.1,205.8,88.6,205.8,89.1z"/>
					</defs>
					<clipPath id="SVGID_710_">
						<use xlink:href="#SVGID_709_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st396">
						<path class="st6" d="M205.8,89.5v2.2c0-0.5-0.5-1-0.9-1.5c-0.8-1.1-1.6-2.3-2.4-3.4c-0.2-0.3-0.5-0.5-0.7-0.5
							c-3.1,0-6.3,0-9.4,0c-2.5,0-4.6,0-7.6,0v-2.6c3,0,5.2,0,7.6,0c3.1,0,6.2,0,9.3,0c0.2,0,0.6,0.2,0.8,0.5
							c0.9,1.1,1.5,2.2,2.4,3.4c0.4,0.5,0.9,1,0.9,1.5"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_711_" class="st0" d="M223.8,83.7v2.6c-1,0-0.8,0.1-1.2,0.6c-2.8,3.9-5.9,7.8-8.7,11.8
							c-0.2,0.3-0.1,0.7-1.1,1.1v-2.6c1-0.4,0.8-0.7,1-1.1c2.8-3.9,5.8-7.8,8.7-11.8C222.8,83.9,222.8,83.8,223.8,83.7z"/>
					</defs>
					<clipPath id="SVGID_712_">
						<use xlink:href="#SVGID_711_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st397">
						<path class="st6" d="M223.8,83.5v2.8c-1,0-0.8,0.1-1.2,0.6c-2.8,3.9-5.9,7.8-8.7,11.8c-0.2,0.3-0.1,0.7-1.1,1.1v-2.6
							c1-0.4,0.8-0.7,1-1.1c2.8-3.9,5.8-7.8,8.7-11.8c0.4-0.5,0.3-0.6,1.3-0.6"/>
					</g>
				</g>
				<g>
					<polygon class="st1" points="212.8,97.8 212.8,100.4 205.8,91.7 205.8,89.1 					"/>
				</g>
				<g>
					<polygon class="st398" points="212.8,97.2 212.8,99.8 212.8,100.4 212.8,97.8 					"/>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_713_" class="st0" d="M184.8,109.8v2.6c0-9.6-1-18.8-2-26.5v-2.6C183.8,91,184.8,100.2,184.8,109.8z"/>
					</defs>
					<clipPath id="SVGID_714_">
						<use xlink:href="#SVGID_713_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st399">
						<path class="st6" d="M184.8,109.5v3c0-9.6-1-18.8-2-26.5v-2.6c1,7.7,2,16.9,2,26.5"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_715_" class="st0" d="M185.8,112.5c0-9.5-1-18.5-2-26.1v-2.6c1,7.6,2.1,16.7,2.1,25.7h-0.1v1.7
							C185.8,111.6,185.8,112.1,185.8,112.5z"/>
					</defs>
					<clipPath id="SVGID_716_">
						<use xlink:href="#SVGID_715_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st400">
						<path class="st11" d="M185.8,109.5v3c0-9.5-1-18.5-2-26.1v-2.6c1,7.6,2,16.6,2,26.1"/>
					</g>
				</g>
				<g>
					<polygon class="st401" points="220.8,109.7 220.8,112.3 220.8,113.7 220.8,111 					"/>
				</g>
				<g>
					<polygon class="st2" points="201.8,111 201.8,113.6 200.8,114.1 200.8,111.5 					"/>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_717_" class="st0" d="M241.8,83.9v2.6c-1,5.1-4,18.6-4,35.1v-2.6C237.8,102.5,240.8,89,241.8,83.9z"/>
					</defs>
					<clipPath id="SVGID_718_">
						<use xlink:href="#SVGID_717_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st402">
						<path class="st8" d="M241.8,83.5v3c0,2.4-1,6.8-2,12.5v-2.6c1-5.7,2-10.1,2-12.5"/>
						<path class="st9" d="M239.8,96.5v2.6c-1,6.2-2,14-2,22.6v-2.6C237.8,110.5,238.8,102.7,239.8,96.5"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_719_" class="st0" d="M240.8,83.8v2.6c-1,5.1-4,18.8-4,35.3v-2.6C236.8,102.5,239.8,88.9,240.8,83.8z"/>
					</defs>
					<clipPath id="SVGID_720_">
						<use xlink:href="#SVGID_719_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st403">
						<path class="st13" d="M240.8,83.5v2.9c0,2.5-1,6.8-2,12.5v-2.6c1-5.7,2-10.1,2-12.5"/>
						<path class="st14" d="M238.8,96.5v2.4c-1,6.3-2,14.1-2,22.8v-2.6c0-8.7,1-16.5,2-22.8"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_721_" class="st0" d="M220.8,111v2.6c-2,3-4.2,6-6.3,8.8c-0.9,1.2-2.7,2.4-2.7,3.7v-2.6c0-1.2,1.8-2.4,2.7-3.7
							C216.6,117,218.8,114.1,220.8,111z"/>
					</defs>
					<clipPath id="SVGID_722_">
						<use xlink:href="#SVGID_721_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st404">
						<path class="st11" d="M220.8,111.5v2.2c-2,3-4.2,6-6.3,8.8c-0.9,1.2-2.7,2.4-2.7,3.7v-2.6c0-1.2,1.8-2.4,2.7-3.7
							c2.1-2.8,4.3-5.8,6.3-8.8"/>
					</g>
				</g>
				<g>
					<polygon class="st2" points="211.8,123.5 211.8,126.1 201.8,113.6 201.8,111 					"/>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_723_" class="st0" d="M219.8,113v2.6c-1,2.6-3.4,5.1-5.2,7.5c-1,1.4-1.8,2.8-2.8,4.2v-2.6
							c1-1.4,1.8-2.8,2.8-4.2C216.4,118,218.8,115.5,219.8,113z"/>
					</defs>
					<clipPath id="SVGID_724_">
						<use xlink:href="#SVGID_723_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st405">
						<path class="st6" d="M219.8,112.5v3.1c-1,2.6-3.4,5.1-5.2,7.5c-1,1.4-1.8,2.8-2.8,4.2v-2.6c1-1.4,1.8-2.8,2.8-4.2
							c1.8-2.4,4.2-4.9,5.2-7.5"/>
					</g>
				</g>
				<g>
					<polygon class="st406" points="211.8,124.7 211.8,127.3 211.8,127.8 211.8,125.2 					"/>
				</g>
				<g>
					<polygon class="st1" points="211.8,125.2 211.8,127.8 201.8,115.5 201.8,112.9 					"/>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_725_" class="st0" d="M220.8,126.9v2.6c0-2.5,0-4.9-1-7.4v-2.6C220.8,121.9,220.8,124.4,220.8,126.9z"/>
					</defs>
					<clipPath id="SVGID_726_">
						<use xlink:href="#SVGID_725_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st407">
						<path class="st6" d="M220.8,126.5v3c0-2.5,0-4.9-1-7.4v-2.6c1,2.5,1,4.9,1,7.4"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_727_" class="st0" d="M221.8,129.5c0-2.5,0-4.9-1-7.4v-2.6c1,2.5,1,4.9,1,7.4
							C221.8,127.6,221.8,128.7,221.8,129.5z"/>
					</defs>
					<clipPath id="SVGID_728_">
						<use xlink:href="#SVGID_727_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st408">
						<path class="st11" d="M221.8,126.5v3c0-2.5,0-4.9-1-7.4v-2.6c1,2.5,1,4.9,1,7.4"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_729_" class="st0" d="M221.8,126.9v2.6c0,3.5-1,7-1,10.4v-2.6C220.8,133.8,221.8,130.3,221.8,126.9z"/>
					</defs>
					<clipPath id="SVGID_730_">
						<use xlink:href="#SVGID_729_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st409">
						<path class="st9" d="M221.8,126.5v3c0,3.5-1,7-1,10.4v-2.6c0-3.5,1-7,1-10.4"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_731_" class="st0" d="M185.8,109.8v2.6c0,10.1-1,19.4-3,27.8v-2.6C184.8,129.3,185.8,119.9,185.8,109.8z"/>
					</defs>
					<clipPath id="SVGID_732_">
						<use xlink:href="#SVGID_731_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st410">
						<path class="st9" d="M185.8,109.5v3c0,8.1-1,15.7-2,22.7v-2.6c1-7,2-14.6,2-22.7"/>
						<path class="st8" d="M183.8,132.5v2.6c0,1.8,0,3.5-1,5.2v-2.6C183.8,136,183.8,134.3,183.8,132.5"/>
					</g>
				</g>
				<g>
					<rect x="182.8" y="137.5" class="st2" width="18" height="3"/>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_733_" class="st0" d="M220.8,140.7c0-0.3,0-0.5,0-0.8v-2.6c0,0.3,0,0.5,0,0.8c0,0.1,0,0.2,0,0.3
							c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3s0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
							c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0C220.8,140.5,220.8,140.6,220.8,140.7
							C220.8,140.6,220.8,140.6,220.8,140.7C220.8,140.6,220.8,140.6,220.8,140.7z"/>
					</defs>
					<clipPath id="SVGID_734_">
						<use xlink:href="#SVGID_733_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st411">
						<path class="st11" d="M244.8,161.5v2.6c-5-0.4-10.2-2.1-14.3-5.2c-6.4-4.7-9.7-11.1-9.7-19v-2.6c0,7.8,3.3,14.2,9.7,19
							C234.6,159.3,239.8,161.1,244.8,161.5"/>
					</g>
				</g>
				<g>
					<rect x="181.8" y="138.5" class="st1" width="20" height="3"/>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_735_" class="st0" d="M221,140.7c0-0.9-0.1-1.7-0.1-2.6c0.2,7.3,3.9,13.3,8.9,17.8c0,0.1,0,0.2,0,0.3
							c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3s0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
							c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0s0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0
							C224.8,153.9,221.2,147.9,221,140.7z"/>
					</defs>
					<clipPath id="SVGID_736_">
						<use xlink:href="#SVGID_735_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st412">
						<path class="st11" d="M244.8,161.5v2.6c-5-0.4-10.2-2.1-14.3-5.2c-6.4-4.7-9.7-11.1-9.7-19v-2.6c0,7.8,3.3,14.2,9.7,19
							C234.6,159.3,239.8,161.1,244.8,161.5"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_737_" class="st0" d="M244.8,161.5v2.6c-4-6-8-23.1-8-42.4v-2.6C236.8,138.4,240.8,155.5,244.8,161.5z"/>
					</defs>
					<clipPath id="SVGID_738_">
						<use xlink:href="#SVGID_737_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st413">
						<path class="st6" d="M244.8,161.5v2.6c-4-6-8-23.1-8-42.4v-2.6C236.8,138.4,240.8,155.5,244.8,161.5"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_739_" class="st0" d="M229.8,158.5C229.8,158.4,229.8,158.4,229.8,158.5C229.8,158.4,229.8,158.4,229.8,158.5
							c0-0.1,0-0.1,0-0.1c0,0,0,0,0,0s0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
							c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3s0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
							c1,0.1,0.5,0.3,0.7,0.4c4.2,3.1,9.3,4.8,14.3,5.2v2.6c-5-0.4-10.2-2.1-14.3-5.2C230.2,158.7,230.8,158.6,229.8,158.5z"/>
					</defs>
					<clipPath id="SVGID_740_">
						<use xlink:href="#SVGID_739_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st414">
						<path class="st11" d="M244.8,161.5v2.6c-5-0.4-10.2-2.1-14.3-5.2c-6.4-4.7-9.7-11.1-9.7-19v-2.6c0,7.8,3.3,14.2,9.7,19
							C234.6,159.3,239.8,161.1,244.8,161.5"/>
					</g>
				</g>
				<g>
					<path class="st3" d="M246.6,161.8l0.5,0.8l-1,0c-6.1-0.2-11.5-2.1-16-5.5c-6.6-4.9-10.2-11.6-10.2-19.8
						c0-5.9,0.8-11.9-0.2-17.8V113c-1,2.6-3.4,5.1-5.2,7.5c-1,1.4-1.9,2.8-3,4.2l-0.6,0.6l-9.3-12.4v25.6H182l0.1-0.5
						c1.7-8.5,2.6-17.9,2.6-28.2c0-9.6-0.8-18.8-2.2-26.5l-0.1-0.5l0.5-0.2l2.3-0.1c1.8,0,3.7,0,5.5,0c3.7,0,7.4,0.1,11.1,0.2
						c0.6,0,1.2,0.5,1.5,0.9c0.9,1.1,1.7,2.3,2.6,3.4c0.4,0.5,0.7,1,1.1,1.5l5.7,7.7c0.2-0.2,0.3-0.4,0.5-0.6
						c2.8-3.9,5.7-7.8,8.5-11.8c0.5-0.8,1.1-1.2,1.9-1.2c4.1,0,8.2-0.1,12.3-0.1h4.9c0.8,0,1.4,0.1,1.4,0.7l0,0.7
						c-1.3,5.1-4.2,18.6-4.2,35.2c0,19.6,3.5,37.4,8.3,42.5C246.4,161.6,246.6,161.6,246.6,161.8z M230.8,156.2
						c4.2,3.1,8.8,4.8,14.2,5.2c-4.7-6-7.9-23.1-7.9-42.4c0-16.6,2.9-30.3,4.2-35.4c-0.1,0-0.3-0.1-0.4-0.1H236
						c-4.1,0-8.2,0.1-12.3,0.1c-0.5,0-0.8,0.2-1.1,0.7c-2.8,3.9-5.6,7.9-8.5,11.8c-0.2,0.3-0.5,0.7-0.8,1.1l-0.5,0.6l-6.5-8.7
						c-0.4-0.5-0.7-1-1.1-1.5c-0.8-1.1-1.7-2.3-2.6-3.4c-0.2-0.3-0.6-0.5-0.8-0.5c-3.1,0-6.1-0.2-9.2-0.2c-2.5,0-4.9-0.1-7.4-0.1
						h-1.7c1.4,8,2.1,16.7,2.1,26.1c0,10.1-0.8,19.9-2.5,27.9h17.6v-26l1.1-0.5l9.4,12.5c0.9-1.2,1.8-2.4,2.8-3.7
						c2.1-2.8,4.1-5.8,6.1-8.8l0.7-1.4v9.8c1,5.9,0.3,11.9,0.2,17.8C221,145.1,224.4,151.5,230.8,156.2"/>
				</g>
				<g>
					<path class="st4" d="M223.6,83.6c4.1,0,8.2-0.1,12.3-0.1h4.9c0.1,0,0.2,0.1,0.4,0.1c-1.3,5.1-4.2,18.8-4.2,35.4
						c0,19.4,3.2,36.4,7.9,42.4c-5.4-0.4-10-2.1-14.2-5.2c-6.4-4.7-9.8-11.1-9.8-18.9c0-5.9,0.8-11.9-0.2-17.8v-9.8l-0.7,1.4
						c-2,3-4.1,6-6.2,8.8c-0.9,1.2-1.7,2.4-2.6,3.7l-9.4-12.5l-1.1,0.5v26h-17.6c1.6-8,2.5-17.8,2.5-27.9c0-9.5-0.7-18.1-2.1-26.1
						h1.7c2.5,0,4.9,0.1,7.4,0.1c3.1,0,6.1,0.1,9.2,0.1c0.2,0,0.5,0.2,0.8,0.5c0.9,1.1,1.7,2.3,2.6,3.4c0.4,0.5,0.7,1,1.1,1.5
						l6.5,8.7l0.5-0.6c0.3-0.4,0.6-0.7,0.8-1.1c2.8-3.9,5.7-7.9,8.5-11.8C222.8,83.8,223.1,83.6,223.6,83.6z"/>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_741_" class="st0" d="M245.8,162.5v2.6c-6-0.2-11.5-2.1-16.1-5.5c-6.6-4.9-9.9-11.6-9.9-19.8v-2.6
							c0,8.2,3.3,14.8,9.9,19.8C234.3,160.5,239.8,162.3,245.8,162.5z"/>
					</defs>
					<clipPath id="SVGID_742_">
						<use xlink:href="#SVGID_741_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st415">
						<path class="st6" d="M245.8,162.5v2.7c-6-0.2-11.5-2.1-16.1-5.5c-6.6-4.9-9.9-11.6-9.9-19.8v-2.6c0,8.2,3.3,14.8,9.9,19.8
							C234.3,160.5,239.8,162.3,245.8,162.5"/>
					</g>
				</g>
				<g>
					<polygon class="st1" points="246.8,162.6 246.8,165.2 245.8,165.2 245.8,162.5 					"/>
				</g>
			</g>
		</g>
		<defs>
			<filter id="Adobe_OpacityMaskFilter_18_" filterUnits="userSpaceOnUse" x="181.8" y="82.5" width="65.4" height="82.7">
				<feFlood  style="flood-color:white;flood-opacity:1" result="back"/>
				<feBlend  in="SourceGraphic" in2="back" mode="normal"/>
			</filter>
		</defs>
		<mask maskUnits="userSpaceOnUse" x="181.8" y="82.5" width="65.4" height="82.7" id="SVGID_743_">
			<g class="st416">
				
					<image style="overflow:visible;" width="70" height="87" xlink:href="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgEASABIAAD/7AARRHVja3kAAQAEAAAAHgAA/+4AIUFkb2JlAGTAAAAAAQMA
EAMCAwYAAAGkAAAB3QAAAf7/2wCEABALCwsMCxAMDBAXDw0PFxsUEBAUGx8XFxcXFx8eFxoaGhoX
Hh4jJSclIx4vLzMzLy9AQEBAQEBAQEBAQEBAQEABEQ8PERMRFRISFRQRFBEUGhQWFhQaJhoaHBoa
JjAjHh4eHiMwKy4nJycuKzU1MDA1NUBAP0BAQEBAQEBAQEBAQP/CABEIAFcARgMBIgACEQEDEQH/
xABpAAEBAQEBAAAAAAAAAAAAAAAAAgEDBwEBAAAAAAAAAAAAAAAAAAAAABAAAgMAAgMAAAAAAAAA
AAAAAAEQEQIgQDAxEhEBAAAAAAAAAAAAAAAAAAAAYBIBAAAAAAAAAAAAAAAAAAAAQP/aAAwDAQAC
EQMRAAAA8/3LJzpBjaJy4AHXl1LzRmhHLryAHTnRWThe86NipAG4AG5ps1IAAABUgAAAAAB//9oA
CAECAAEFAO9//9oACAEDAAEFAO9//9oACAEBAAEFACipoqUJDyNCQsjQ4XvJQ8iyUaHC95LLLLND
hCY9UfZ9i1Zp8XKHwscofgXhUPqf/9oACAECAgY/AHf/2gAIAQMCBj8Ad//aAAgBAQEGPwBH/9k=" transform="matrix(1 0 0 1 179.75 80.5)">
				</image>
			</g>
		</mask>
		<g class="st417">
			<g class="st0">
				<g>
					<polygon class="st34" points="182.8,83.4 182.8,86 182.8,85.4 182.8,82.8 					"/>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_744_" class="st0" d="M235.8,83.7v2.6c-4,0-8,0-12,0v-2.6C227.8,83.7,231.8,83.7,235.8,83.7z"/>
					</defs>
					<clipPath id="SVGID_745_">
						<use xlink:href="#SVGID_744_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st418">
						<path class="st34" d="M235.8,83.5v2.9c-4,0-8,0-12,0v-2.6c4,0,8,0,12,0"/>
					</g>
				</g>
				<g>
					<rect x="235.8" y="83.5" class="st34" width="5" height="3"/>
				</g>
				<g class="st0">
					<defs>
						<polygon id="SVGID_746_" class="st0" points="240.8,83.8 240.8,86.4 240.8,86.4 240.8,83.7 						"/>
					</defs>
					<clipPath id="SVGID_747_">
						<use xlink:href="#SVGID_746_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st419">
						<polyline class="st34" points="240.8,83.5 240.8,86.4 240.8,86.4 240.8,83.7 240.8,83.5 						"/>
					</g>
				</g>
				<g>
					<rect x="183.8" y="83.5" class="st34" width="1" height="3"/>
				</g>
				<g>
					<polygon class="st34" points="242.8,83.3 242.8,85.9 241.8,86.5 241.8,83.9 					"/>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_748_" class="st0" d="M205.8,89.1v2.6c0-0.5-0.5-1-0.9-1.5c-0.8-1.1-1.6-2.3-2.4-3.4c-0.2-0.3-0.5-0.5-0.7-0.5
							c-3.1,0-6.3,0-9.4,0c-2.5,0-4.6,0-7.6,0v-2.6c3,0,5.2,0,7.6,0c3.1,0,6.2,0,9.3,0c0.2,0,0.6,0.2,0.8,0.5
							c0.9,1.1,1.5,2.2,2.4,3.4C205.2,88.1,205.8,88.6,205.8,89.1z"/>
					</defs>
					<clipPath id="SVGID_749_">
						<use xlink:href="#SVGID_748_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st420">
						<path class="st34" d="M205.8,89.5v2.2c0-0.5-0.5-1-0.9-1.5c-0.8-1.1-1.6-2.3-2.4-3.4c-0.2-0.3-0.5-0.5-0.7-0.5
							c-3.1,0-6.3,0-9.4,0c-2.5,0-4.6,0-7.6,0v-2.6c3,0,5.2,0,7.6,0c3.1,0,6.2,0,9.3,0c0.2,0,0.6,0.2,0.8,0.5
							c0.9,1.1,1.5,2.2,2.4,3.4c0.4,0.5,0.9,1,0.9,1.5"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_750_" class="st0" d="M223.8,83.7v2.6c-1,0-0.8,0.1-1.2,0.6c-2.8,3.9-5.9,7.8-8.7,11.8
							c-0.2,0.3-0.1,0.7-1.1,1.1v-2.6c1-0.4,0.8-0.7,1-1.1c2.8-3.9,5.8-7.8,8.7-11.8C222.8,83.9,222.8,83.8,223.8,83.7z"/>
					</defs>
					<clipPath id="SVGID_751_">
						<use xlink:href="#SVGID_750_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st421">
						<path class="st34" d="M223.8,83.5v2.8c-1,0-0.8,0.1-1.2,0.6c-2.8,3.9-5.9,7.8-8.7,11.8c-0.2,0.3-0.1,0.7-1.1,1.1v-2.6
							c1-0.4,0.8-0.7,1-1.1c2.8-3.9,5.8-7.8,8.7-11.8c0.4-0.5,0.3-0.6,1.3-0.6"/>
					</g>
				</g>
				<g>
					<polygon class="st34" points="212.8,97.8 212.8,100.4 205.8,91.7 205.8,89.1 					"/>
				</g>
				<g>
					<polygon class="st34" points="212.8,97.2 212.8,99.8 212.8,100.4 212.8,97.8 					"/>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_752_" class="st0" d="M184.8,109.8v2.6c0-9.6-1-18.8-2-26.5v-2.6C183.8,91,184.8,100.2,184.8,109.8z"/>
					</defs>
					<clipPath id="SVGID_753_">
						<use xlink:href="#SVGID_752_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st422">
						<path class="st34" d="M184.8,109.5v3c0-9.6-1-18.8-2-26.5v-2.6c1,7.7,2,16.9,2,26.5"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_754_" class="st0" d="M185.8,112.5c0-9.5-1-18.5-2-26.1v-2.6c1,7.6,2.1,16.7,2.1,25.7h-0.1v1.7
							C185.8,111.6,185.8,112.1,185.8,112.5z"/>
					</defs>
					<clipPath id="SVGID_755_">
						<use xlink:href="#SVGID_754_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st423">
						<path class="st34" d="M185.8,109.5v3c0-9.5-1-18.5-2-26.1v-2.6c1,7.6,2,16.6,2,26.1"/>
					</g>
				</g>
				<g>
					<polygon class="st34" points="220.8,109.7 220.8,112.3 220.8,113.7 220.8,111 					"/>
				</g>
				<g>
					<polygon class="st34" points="201.8,111 201.8,113.6 200.8,114.1 200.8,111.5 					"/>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_756_" class="st0" d="M241.8,83.9v2.6c-1,5.1-4,18.6-4,35.1v-2.6C237.8,102.5,240.8,89,241.8,83.9z"/>
					</defs>
					<clipPath id="SVGID_757_">
						<use xlink:href="#SVGID_756_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st424">
						<path class="st34" d="M241.8,83.5v3c0,2.4-1,6.8-2,12.5v-2.6c1-5.7,2-10.1,2-12.5"/>
						<path class="st34" d="M239.8,96.5v2.6c-1,6.2-2,14-2,22.6v-2.6C237.8,110.5,238.8,102.7,239.8,96.5"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_758_" class="st0" d="M240.8,83.8v2.6c-1,5.1-4,18.8-4,35.3v-2.6C236.8,102.5,239.8,88.9,240.8,83.8z"/>
					</defs>
					<clipPath id="SVGID_759_">
						<use xlink:href="#SVGID_758_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st425">
						<path class="st34" d="M240.8,83.5v2.9c0,2.5-1,6.8-2,12.5v-2.6c1-5.7,2-10.1,2-12.5"/>
						<path class="st34" d="M238.8,96.5v2.4c-1,6.3-2,14.1-2,22.8v-2.6c0-8.7,1-16.5,2-22.8"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_760_" class="st0" d="M220.8,111v2.6c-2,3-4.2,6-6.3,8.8c-0.9,1.2-2.7,2.4-2.7,3.7v-2.6c0-1.2,1.8-2.4,2.7-3.7
							C216.6,117,218.8,114.1,220.8,111z"/>
					</defs>
					<clipPath id="SVGID_761_">
						<use xlink:href="#SVGID_760_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st426">
						<path class="st34" d="M220.8,111.5v2.2c-2,3-4.2,6-6.3,8.8c-0.9,1.2-2.7,2.4-2.7,3.7v-2.6c0-1.2,1.8-2.4,2.7-3.7
							c2.1-2.8,4.3-5.8,6.3-8.8"/>
					</g>
				</g>
				<g>
					<polygon class="st34" points="211.8,123.5 211.8,126.1 201.8,113.6 201.8,111 					"/>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_762_" class="st0" d="M219.8,113v2.6c-1,2.6-3.4,5.1-5.2,7.5c-1,1.4-1.8,2.8-2.8,4.2v-2.6
							c1-1.4,1.8-2.8,2.8-4.2C216.4,118,218.8,115.5,219.8,113z"/>
					</defs>
					<clipPath id="SVGID_763_">
						<use xlink:href="#SVGID_762_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st427">
						<path class="st34" d="M219.8,112.5v3.1c-1,2.6-3.4,5.1-5.2,7.5c-1,1.4-1.8,2.8-2.8,4.2v-2.6c1-1.4,1.8-2.8,2.8-4.2
							c1.8-2.4,4.2-4.9,5.2-7.5"/>
					</g>
				</g>
				<g>
					<polygon class="st34" points="211.8,124.7 211.8,127.3 211.8,127.8 211.8,125.2 					"/>
				</g>
				<g>
					<polygon class="st34" points="211.8,125.2 211.8,127.8 201.8,115.5 201.8,112.9 					"/>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_764_" class="st0" d="M220.8,126.9v2.6c0-2.5,0-4.9-1-7.4v-2.6C220.8,121.9,220.8,124.4,220.8,126.9z"/>
					</defs>
					<clipPath id="SVGID_765_">
						<use xlink:href="#SVGID_764_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st428">
						<path class="st34" d="M220.8,126.5v3c0-2.5,0-4.9-1-7.4v-2.6c1,2.5,1,4.9,1,7.4"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_766_" class="st0" d="M221.8,129.5c0-2.5,0-4.9-1-7.4v-2.6c1,2.5,1,4.9,1,7.4
							C221.8,127.6,221.8,128.7,221.8,129.5z"/>
					</defs>
					<clipPath id="SVGID_767_">
						<use xlink:href="#SVGID_766_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st429">
						<path class="st34" d="M221.8,126.5v3c0-2.5,0-4.9-1-7.4v-2.6c1,2.5,1,4.9,1,7.4"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_768_" class="st0" d="M221.8,126.9v2.6c0,3.5-1,7-1,10.4v-2.6C220.8,133.8,221.8,130.3,221.8,126.9z"/>
					</defs>
					<clipPath id="SVGID_769_">
						<use xlink:href="#SVGID_768_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st430">
						<path class="st34" d="M221.8,126.5v3c0,3.5-1,7-1,10.4v-2.6c0-3.5,1-7,1-10.4"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_770_" class="st0" d="M185.8,109.8v2.6c0,10.1-1,19.4-3,27.8v-2.6C184.8,129.3,185.8,119.9,185.8,109.8z"/>
					</defs>
					<clipPath id="SVGID_771_">
						<use xlink:href="#SVGID_770_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st431">
						<path class="st34" d="M185.8,109.5v3c0,8.1-1,15.7-2,22.7v-2.6c1-7,2-14.6,2-22.7"/>
						<path class="st34" d="M183.8,132.5v2.6c0,1.8,0,3.5-1,5.2v-2.6C183.8,136,183.8,134.3,183.8,132.5"/>
					</g>
				</g>
				<g>
					<rect x="182.8" y="137.5" class="st34" width="18" height="3"/>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_772_" class="st0" d="M220.8,140.7c0-0.3,0-0.5,0-0.8v-2.6c0,0.3,0,0.5,0,0.8c0,0.1,0,0.2,0,0.3
							c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3s0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
							c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0C220.8,140.5,220.8,140.6,220.8,140.7
							C220.8,140.6,220.8,140.6,220.8,140.7C220.8,140.6,220.8,140.6,220.8,140.7z"/>
					</defs>
					<clipPath id="SVGID_773_">
						<use xlink:href="#SVGID_772_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st432">
						<path class="st34" d="M244.8,161.5v2.6c-5-0.4-10.2-2.1-14.3-5.2c-6.4-4.7-9.7-11.1-9.7-19v-2.6c0,7.8,3.3,14.2,9.7,19
							C234.6,159.3,239.8,161.1,244.8,161.5"/>
					</g>
				</g>
				<g>
					<rect x="181.8" y="138.5" class="st34" width="20" height="3"/>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_774_" class="st0" d="M221,140.7c0-0.9-0.1-1.7-0.1-2.6c0.2,7.3,3.9,13.3,8.9,17.8c0,0.1,0,0.2,0,0.3
							c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3s0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3
							c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0s0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0
							C224.8,153.9,221.2,147.9,221,140.7z"/>
					</defs>
					<clipPath id="SVGID_775_">
						<use xlink:href="#SVGID_774_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st433">
						<path class="st34" d="M244.8,161.5v2.6c-5-0.4-10.2-2.1-14.3-5.2c-6.4-4.7-9.7-11.1-9.7-19v-2.6c0,7.8,3.3,14.2,9.7,19
							C234.6,159.3,239.8,161.1,244.8,161.5"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_776_" class="st0" d="M244.8,161.5v2.6c-4-6-8-23.1-8-42.4v-2.6C236.8,138.4,240.8,155.5,244.8,161.5z"/>
					</defs>
					<clipPath id="SVGID_777_">
						<use xlink:href="#SVGID_776_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st434">
						<path class="st34" d="M244.8,161.5v2.6c-4-6-8-23.1-8-42.4v-2.6C236.8,138.4,240.8,155.5,244.8,161.5"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_778_" class="st0" d="M229.8,158.5C229.8,158.4,229.8,158.4,229.8,158.5C229.8,158.4,229.8,158.4,229.8,158.5
							c0-0.1,0-0.1,0-0.1c0,0,0,0,0,0s0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
							c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3s0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.3
							c1,0.1,0.5,0.3,0.7,0.4c4.2,3.1,9.3,4.8,14.3,5.2v2.6c-5-0.4-10.2-2.1-14.3-5.2C230.2,158.7,230.8,158.6,229.8,158.5z"/>
					</defs>
					<clipPath id="SVGID_779_">
						<use xlink:href="#SVGID_778_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st435">
						<path class="st34" d="M244.8,161.5v2.6c-5-0.4-10.2-2.1-14.3-5.2c-6.4-4.7-9.7-11.1-9.7-19v-2.6c0,7.8,3.3,14.2,9.7,19
							C234.6,159.3,239.8,161.1,244.8,161.5"/>
					</g>
				</g>
				<g>
					<path class="st34" d="M246.6,161.8l0.5,0.8l-1,0c-6.1-0.2-11.5-2.1-16-5.5c-6.6-4.9-10.2-11.6-10.2-19.8
						c0-5.9,0.8-11.9-0.2-17.8V113c-1,2.6-3.4,5.1-5.2,7.5c-1,1.4-1.9,2.8-3,4.2l-0.6,0.6l-9.3-12.4v25.6H182l0.1-0.5
						c1.7-8.5,2.6-17.9,2.6-28.2c0-9.6-0.8-18.8-2.2-26.5l-0.1-0.5l0.5-0.2l2.3-0.1c1.8,0,3.7,0,5.5,0c3.7,0,7.4,0.1,11.1,0.2
						c0.6,0,1.2,0.5,1.5,0.9c0.9,1.1,1.7,2.3,2.6,3.4c0.4,0.5,0.7,1,1.1,1.5l5.7,7.7c0.2-0.2,0.3-0.4,0.5-0.6
						c2.8-3.9,5.7-7.8,8.5-11.8c0.5-0.8,1.1-1.2,1.9-1.2c4.1,0,8.2-0.1,12.3-0.1h4.9c0.8,0,1.4,0.1,1.4,0.7l0,0.7
						c-1.3,5.1-4.2,18.6-4.2,35.2c0,19.6,3.5,37.4,8.3,42.5C246.4,161.6,246.6,161.6,246.6,161.8z M230.8,156.2
						c4.2,3.1,8.8,4.8,14.2,5.2c-4.7-6-7.9-23.1-7.9-42.4c0-16.6,2.9-30.3,4.2-35.4c-0.1,0-0.3-0.1-0.4-0.1H236
						c-4.1,0-8.2,0.1-12.3,0.1c-0.5,0-0.8,0.2-1.1,0.7c-2.8,3.9-5.6,7.9-8.5,11.8c-0.2,0.3-0.5,0.7-0.8,1.1l-0.5,0.6l-6.5-8.7
						c-0.4-0.5-0.7-1-1.1-1.5c-0.8-1.1-1.7-2.3-2.6-3.4c-0.2-0.3-0.6-0.5-0.8-0.5c-3.1,0-6.1-0.2-9.2-0.2c-2.5,0-4.9-0.1-7.4-0.1
						h-1.7c1.4,8,2.1,16.7,2.1,26.1c0,10.1-0.8,19.9-2.5,27.9h17.6v-26l1.1-0.5l9.4,12.5c0.9-1.2,1.8-2.4,2.8-3.7
						c2.1-2.8,4.1-5.8,6.1-8.8l0.7-1.4v9.8c1,5.9,0.3,11.9,0.2,17.8C221,145.1,224.4,151.5,230.8,156.2"/>
				</g>
				<g>
					<path class="st34" d="M223.6,83.6c4.1,0,8.2-0.1,12.3-0.1h4.9c0.1,0,0.2,0.1,0.4,0.1c-1.3,5.1-4.2,18.8-4.2,35.4
						c0,19.4,3.2,36.4,7.9,42.4c-5.4-0.4-10-2.1-14.2-5.2c-6.4-4.7-9.8-11.1-9.8-18.9c0-5.9,0.8-11.9-0.2-17.8v-9.8l-0.7,1.4
						c-2,3-4.1,6-6.2,8.8c-0.9,1.2-1.7,2.4-2.6,3.7l-9.4-12.5l-1.1,0.5v26h-17.6c1.6-8,2.5-17.8,2.5-27.9c0-9.5-0.7-18.1-2.1-26.1
						h1.7c2.5,0,4.9,0.1,7.4,0.1c3.1,0,6.1,0.1,9.2,0.1c0.2,0,0.5,0.2,0.8,0.5c0.9,1.1,1.7,2.3,2.6,3.4c0.4,0.5,0.7,1,1.1,1.5
						l6.5,8.7l0.5-0.6c0.3-0.4,0.6-0.7,0.8-1.1c2.8-3.9,5.7-7.9,8.5-11.8C222.8,83.8,223.1,83.6,223.6,83.6z"/>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_780_" class="st0" d="M245.8,162.5v2.6c-6-0.2-11.5-2.1-16.1-5.5c-6.6-4.9-9.9-11.6-9.9-19.8v-2.6
							c0,8.2,3.3,14.8,9.9,19.8C234.3,160.5,239.8,162.3,245.8,162.5z"/>
					</defs>
					<clipPath id="SVGID_781_">
						<use xlink:href="#SVGID_780_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st436">
						<path class="st34" d="M245.8,162.5v2.7c-6-0.2-11.5-2.1-16.1-5.5c-6.6-4.9-9.9-11.6-9.9-19.8v-2.6c0,8.2,3.3,14.8,9.9,19.8
							C234.3,160.5,239.8,162.3,245.8,162.5"/>
					</g>
				</g>
				<g>
					<polygon class="st34" points="246.8,162.6 246.8,165.2 245.8,165.2 245.8,162.5 					"/>
				</g>
			</g>
		</g>
	</g>
</g>
<g id="i">
	<g>
		<g>
			<g class="st0">
				<g>
					<rect x="244.1" y="88.6" class="st1" width="20.5" height="2.6"/>
				</g>
				<g>
					<rect x="244.1" y="139.6" class="st2" width="20.5" height="2.6"/>
				</g>
				<g>
					<rect x="244.1" y="88.6" class="st4" width="20.5" height="51.1"/>
				</g>
				<g>
					<rect x="243.1" y="140.6" class="st1" width="22.5" height="2.6"/>
				</g>
				<g>
					<path class="st3" d="M243.1,87.6h22.5v53.1h-22.5V87.6z M264.5,139.6V88.6h-20.5v51.1H264.5"/>
				</g>
			</g>
		</g>
		<defs>
			<filter id="Adobe_OpacityMaskFilter_19_" filterUnits="userSpaceOnUse" x="243.1" y="87.6" width="22.5" height="55.7">
				<feFlood  style="flood-color:white;flood-opacity:1" result="back"/>
				<feBlend  in="SourceGraphic" in2="back" mode="normal"/>
			</filter>
		</defs>
		<mask maskUnits="userSpaceOnUse" x="243.1" y="87.6" width="22.5" height="55.7" id="SVGID_782_">
			<g class="st437">
				
					<image style="overflow:visible;" width="27" height="60" xlink:href="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgEASABIAAD/7AARRHVja3kAAQAEAAAAHgAA/+4AIUFkb2JlAGTAAAAAAQMA
EAMCAwYAAAF9AAABlQAAAbb/2wCEABALCwsMCxAMDBAXDw0PFxsUEBAUGx8XFxcXFx8eFxoaGhoX
Hh4jJSclIx4vLzMzLy9AQEBAQEBAQEBAQEBAQEABEQ8PERMRFRISFRQRFBEUGhQWFhQaJhoaHBoa
JjAjHh4eHiMwKy4nJycuKzU1MDA1NUBAP0BAQEBAQEBAQEBAQP/CABEIADwAGwMBIgACEQEDEQH/
xABlAAEBAQEAAAAAAAAAAAAAAAACAQAHAQEAAAAAAAAAAAAAAAAAAAAAEAADAAMBAAAAAAAAAAAA
AAAAEAEwEQIgEQEAAAAAAAAAAAAAAAAAAABQEgEAAAAAAAAAAAAAAAAAAAAw/9oADAMBAAIRAxEA
AADn+tDrCosJRKwiFE1mLNj/2gAIAQIAAQUAwf/aAAgBAwABBQDB/9oACAEBAAEFADThoqhCqLpR
dKGyvZfX/9oACAECAgY/AA//2gAIAQMCBj8AD//aAAgBAQEGPwAD/9k=" transform="matrix(1 0 0 1 240.75 85.5)">
				</image>
			</g>
		</mask>
		<g class="st438">
			<g class="st0">
				<g>
					<rect x="244.1" y="88.6" class="st34" width="20.5" height="2.6"/>
				</g>
				<g>
					<rect x="244.1" y="139.6" class="st34" width="20.5" height="2.6"/>
				</g>
				<g>
					<rect x="244.1" y="88.6" class="st34" width="20.5" height="51.1"/>
				</g>
				<g>
					<rect x="243.1" y="140.6" class="st34" width="22.5" height="2.6"/>
				</g>
				<g>
					<path class="st34" d="M243.1,87.6h22.5v53.1h-22.5V87.6z M264.5,139.6V88.6h-20.5v51.1H264.5"/>
				</g>
			</g>
		</g>
	</g>
</g>
<g id="c">
	<g>
		<g>
			<g class="st0">
				<g>
					<polygon class="st1" points="313.6,110.3 313.6,112.9 312.9,112.6 312.9,110 					"/>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_783_" class="st0" d="M312.6,90.6v2.6c-3.4-1.7-9.4-2.8-14.8-2.8c-17.3,0-29.9,11.1-29.9,26.3v-2.6
							c0-15.3,12.6-26.3,29.9-26.3C303.3,87.8,309.2,88.9,312.6,90.6z"/>
					</defs>
					<clipPath id="SVGID_784_">
						<use xlink:href="#SVGID_783_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st439">
						<path class="st6" d="M312.6,90.6v2.6c-3.4-1.7-9.4-2.8-14.8-2.8c-12.1,0-21.8,5.4-26.6,13.8v-2.6c4.8-8.4,14.6-13.8,26.6-13.8
							C303.3,87.8,309.2,88.9,312.6,90.6"/>
						<path class="st7" d="M271.1,101.6v2.6c-0.8,1.4-1.4,2.8-1.9,4.4V106C269.7,104.5,270.3,103,271.1,101.6"/>
						<path class="st8" d="M269.2,106v2.6c-0.4,1.3-0.8,2.7-1,4.2v-2.6C268.4,108.7,268.7,107.3,269.2,106"/>
						<path class="st9" d="M268.2,110.2v2.6c-0.2,1.3-0.3,2.6-0.3,3.9v-2.6C267.9,112.8,268,111.5,268.2,110.2"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_785_" class="st0" d="M300.1,108.3c-7.2,0-11.8,3.5-11.8,8.8v-2.6c0-5.4,4.6-8.8,11.8-8.8
							c4.9,0,9.4,1.6,12.5,3v2.6C309.5,109.9,305.1,108.3,300.1,108.3z"/>
					</defs>
					<clipPath id="SVGID_786_">
						<use xlink:href="#SVGID_785_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st440">
						<path class="st11" d="M312.6,108.7v2.6c-3.1-1.4-7.5-3-12.5-3c-5.2,0-9,1.8-10.8,4.9v-2.6c1.7-3,5.6-4.9,10.8-4.9
							C305.1,105.7,309.5,107.3,312.6,108.7"/>
						<path class="st12" d="M289.4,110.6v2.6c-0.2,0.4-0.4,0.9-0.6,1.4v-2.6C288.9,111.5,289.1,111,289.4,110.6"/>
						<path class="st13" d="M288.8,111.9v2.6c-0.1,0.4-0.2,0.9-0.3,1.3v-2.6C288.5,112.8,288.6,112.4,288.8,111.9"/>
						<path class="st14" d="M288.4,113.3v2.6c-0.1,0.4-0.1,0.9-0.1,1.3v-2.6C288.3,114.1,288.4,113.7,288.4,113.3"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_787_" class="st0" d="M312.9,110v2.6c-3-1.5-7.6-3.3-12.7-3.3c-6.7,0-10.8,3-10.8,7.8v-2.6
							c0-4.8,4.1-7.8,10.8-7.8C305.3,106.7,309.9,108.5,312.9,110z"/>
					</defs>
					<clipPath id="SVGID_788_">
						<use xlink:href="#SVGID_787_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st441">
						<path class="st6" d="M312.9,110v2.6c-3-1.5-7.6-3.3-12.7-3.3c-4.8,0-8.3,1.6-9.9,4.3V111c1.6-2.7,5.1-4.3,9.9-4.3
							C305.3,106.7,309.9,108.5,312.9,110"/>
						<path class="st7" d="M290.2,111v2.6c-0.2,0.4-0.4,0.8-0.5,1.2v-2.6C289.8,111.8,290,111.4,290.2,111"/>
						<path class="st8" d="M289.7,112.2v2.6c-0.1,0.4-0.2,0.8-0.3,1.2v-2.6C289.5,113,289.6,112.6,289.7,112.2"/>
						<path class="st9" d="M289.4,113.4v2.6c-0.1,0.4-0.1,0.8-0.1,1.2v-2.6C289.3,114.2,289.4,113.8,289.4,113.4"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_789_" class="st0" d="M312.9,120.2l0,2.6c-3.9,2-8.5,3.1-12.8,3.1c-7.3,0-11.8-3.4-11.8-8.8l0-2.6
							c0,5.4,4.5,8.8,11.8,8.8C304.4,123.3,309,122.2,312.9,120.2z"/>
					</defs>
					<clipPath id="SVGID_790_">
						<use xlink:href="#SVGID_789_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st442">
						<path class="st6" d="M312.9,120.2v2.6c-3.9,2-8.5,3.1-12.8,3.1c-7.3,0-11.8-3.4-11.8-8.8v-2.6c0,5.4,4.5,8.8,11.8,8.8
							C304.4,123.3,309,122.2,312.9,120.2"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_791_" class="st0" d="M271.4,129.7c-2.2-3.8-3.5-8.2-3.5-13v-2.6c0,4.8,1.2,9.2,3.5,13c0,0,0,0.1,0,0.2
							c0,0.1,0,0.1,0,0.2c0,0.1,0,0.1,0,0.2c0,0.1,0,0.1,0,0.2s0,0.1,0,0.2c0,0.1,0,0.1,0,0.2c0,0.1,0,0.1,0,0.2c0,0,0,0.1,0,0.2
							c0,0,0,0.1,0,0.1c0,0,0,0.1,0,0.2c0,0,0,0.1,0,0.2c0,0,0,0.1,0,0.2s0,0.1,0,0.2c0,0,0,0.1,0,0.2c0,0,0,0.1,0,0.2
							C271.4,129.6,271.4,129.7,271.4,129.7z"/>
					</defs>
					<clipPath id="SVGID_792_">
						<use xlink:href="#SVGID_791_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st443">
						<path class="st11" d="M312.9,137.4v2.6c-3.6,1.8-9.7,3-15.3,3c-17.2,0-29.7-11.1-29.7-26.3v-2.6c0,15.3,12.5,26.3,29.7,26.3
							C303.2,140.4,309.3,139.2,312.9,137.4"/>
					</g>
				</g>
				<g>
					<polygon class="st1" points="313.9,138 313.9,140.6 313.6,140.8 313.6,138.1 					"/>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_793_" class="st0" d="M271.4,129.7c0,0,0-0.1,0-0.1c0,0,0-0.1,0-0.2c0,0,0-0.1,0-0.2c0,0,0-0.1,0-0.2
							s0-0.1,0-0.2c0,0,0-0.1,0-0.2c0,0,0-0.1,0-0.2c0,0,0-0.1,0-0.1c0,0,0-0.1,0-0.2c0-0.1,0-0.1,0-0.2c0-0.1,0-0.1,0-0.2
							c0-0.1,0-0.1,0-0.2s0-0.1,0-0.2c0-0.1,0-0.1,0-0.2c0-0.1,0-0.1,0-0.2c0,0,0-0.1,0-0.2c4.9,8.2,14.4,13.4,26.2,13.4
							c5.6,0,11.7-1.2,15.3-3v2.6c-3.6,1.8-9.7,3-15.3,3C285.8,143.1,276.2,137.9,271.4,129.7z"/>
					</defs>
					<clipPath id="SVGID_794_">
						<use xlink:href="#SVGID_793_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st444">
						<path class="st11" d="M312.9,137.4v2.6c-3.6,1.8-9.7,3-15.3,3c-17.2,0-29.7-11.1-29.7-26.3v-2.6c0,15.3,12.5,26.3,29.7,26.3
							C303.2,140.4,309.3,139.2,312.9,137.4"/>
					</g>
				</g>
				<g>
					<path class="st4" d="M297.8,87.8c5.5,0,11.4,1.1,14.8,2.8v18.1c-3.1-1.4-7.5-3-12.5-3c-7.2,0-11.8,3.5-11.8,8.8
						c0,5.4,4.5,8.8,11.8,8.8c4.3,0,8.9-1.1,12.8-3.1v17.2c-3.6,1.8-9.7,3-15.3,3c-17.2,0-29.7-11.1-29.7-26.3
						C267.9,98.9,280.4,87.8,297.8,87.8z"/>
				</g>
				<g>
					<path class="st3" d="M313.1,118.9l0.7-0.4V138l-0.3,0.1c-3.7,2-10.2,3.3-16.1,3.3c-17.8,0-30.7-11.5-30.7-27.3
						c0-15.8,13-27.3,30.9-27.3c5.8,0,12,1.2,15.6,3.1l0.3,0.1v20.3l-0.7-0.4c-3-1.5-7.6-3.3-12.7-3.3c-6.7,0-10.8,3-10.8,7.8
						c0,4.8,4.1,7.8,10.8,7.8C304.6,122.3,309.3,121.1,313.1,118.9z M312.9,137.4v-17.2c-3.9,2-8.5,3.1-12.8,3.1
						c-7.3,0-11.8-3.4-11.8-8.8c0-5.4,4.6-8.8,11.8-8.8c4.9,0,9.4,1.6,12.5,3V90.6c-3.4-1.7-9.4-2.8-14.8-2.8
						c-17.3,0-29.9,11.1-29.9,26.3c0,15.3,12.5,26.3,29.7,26.3C303.2,140.4,309.3,139.2,312.9,137.4"/>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_795_" class="st0" d="M313.6,138.1v2.6c-3.7,2-10.2,3.3-16.1,3.3c-17.8,0-30.7-11.5-30.7-27.3v-2.6
							c0,15.8,12.9,27.3,30.7,27.3C303.5,141.4,309.9,140.1,313.6,138.1z"/>
					</defs>
					<clipPath id="SVGID_796_">
						<use xlink:href="#SVGID_795_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st445">
						<path class="st6" d="M313.6,138.1v2.6c-3.7,2-10.2,3.3-16.1,3.3c-17.8,0-30.7-11.5-30.7-27.3v-2.6c0,15.8,12.9,27.3,30.7,27.3
							C303.5,141.4,309.9,140.1,313.6,138.1"/>
					</g>
				</g>
			</g>
		</g>
		<defs>
			<filter id="Adobe_OpacityMaskFilter_20_" filterUnits="userSpaceOnUse" x="266.9" y="86.8" width="47" height="57.3">
				<feFlood  style="flood-color:white;flood-opacity:1" result="back"/>
				<feBlend  in="SourceGraphic" in2="back" mode="normal"/>
			</filter>
		</defs>
		<mask maskUnits="userSpaceOnUse" x="266.9" y="86.8" width="47" height="57.3" id="SVGID_797_">
			<g class="st446">
				
					<image style="overflow:visible;" width="52" height="62" xlink:href="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgEASABIAAD/7AARRHVja3kAAQAEAAAAHgAA/+4AIUFkb2JlAGTAAAAAAQMA
EAMCAwYAAAGPAAABswAAAdT/2wCEABALCwsMCxAMDBAXDw0PFxsUEBAUGx8XFxcXFx8eFxoaGhoX
Hh4jJSclIx4vLzMzLy9AQEBAQEBAQEBAQEBAQEABEQ8PERMRFRISFRQRFBEUGhQWFhQaJhoaHBoa
JjAjHh4eHiMwKy4nJycuKzU1MDA1NUBAP0BAQEBAQEBAQEBAQP/CABEIAD4ANAMBIgACEQEDEQH/
xABnAAEBAQEBAAAAAAAAAAAAAAAAAQIDBwEBAAAAAAAAAAAAAAAAAAAAABABAQEBAQEBAQAAAAAA
AAAAAQARECBAMAIRAQAAAAAAAAAAAAAAAAAAAFASAQAAAAAAAAAAAAAAAAAAAED/2gAMAwEAAhED
EQAAAPPykuqYm8kA3nZc3makAF3z2XG4YahAAauBqQAf/9oACAECAAEFAPg//9oACAEDAAEFAPg/
/9oACAEBAAEFAO5Z4CCySeEHFtnhF/TL4ImTyPEsnuw22z+P/9oACAECAgY/AAf/2gAIAQMCBj8A
B//aAAgBAQEGPwBj/9k=" transform="matrix(1 0 0 1 264.75 84.5)">
				</image>
			</g>
		</mask>
		<g class="st447">
			<g class="st0">
				<g>
					<polygon class="st34" points="313.6,110.3 313.6,112.9 312.9,112.6 312.9,110 					"/>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_798_" class="st0" d="M312.6,90.6v2.6c-3.4-1.7-9.4-2.8-14.8-2.8c-17.3,0-29.9,11.1-29.9,26.3v-2.6
							c0-15.3,12.6-26.3,29.9-26.3C303.3,87.8,309.2,88.9,312.6,90.6z"/>
					</defs>
					<clipPath id="SVGID_799_">
						<use xlink:href="#SVGID_798_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st448">
						<path class="st34" d="M312.6,90.6v2.6c-3.4-1.7-9.4-2.8-14.8-2.8c-12.1,0-21.8,5.4-26.6,13.8v-2.6
							c4.8-8.4,14.6-13.8,26.6-13.8C303.3,87.8,309.2,88.9,312.6,90.6"/>
						<path class="st34" d="M271.1,101.6v2.6c-0.8,1.4-1.4,2.8-1.9,4.4V106C269.7,104.5,270.3,103,271.1,101.6"/>
						<path class="st34" d="M269.2,106v2.6c-0.4,1.3-0.8,2.7-1,4.2v-2.6C268.4,108.7,268.7,107.3,269.2,106"/>
						<path class="st34" d="M268.2,110.2v2.6c-0.2,1.3-0.3,2.6-0.3,3.9v-2.6C267.9,112.8,268,111.5,268.2,110.2"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_800_" class="st0" d="M300.1,108.3c-7.2,0-11.8,3.5-11.8,8.8v-2.6c0-5.4,4.6-8.8,11.8-8.8
							c4.9,0,9.4,1.6,12.5,3v2.6C309.5,109.9,305.1,108.3,300.1,108.3z"/>
					</defs>
					<clipPath id="SVGID_801_">
						<use xlink:href="#SVGID_800_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st449">
						<path class="st34" d="M312.6,108.7v2.6c-3.1-1.4-7.5-3-12.5-3c-5.2,0-9,1.8-10.8,4.9v-2.6c1.7-3,5.6-4.9,10.8-4.9
							C305.1,105.7,309.5,107.3,312.6,108.7"/>
						<path class="st34" d="M289.4,110.6v2.6c-0.2,0.4-0.4,0.9-0.6,1.4v-2.6C288.9,111.5,289.1,111,289.4,110.6"/>
						<path class="st34" d="M288.8,111.9v2.6c-0.1,0.4-0.2,0.9-0.3,1.3v-2.6C288.5,112.8,288.6,112.4,288.8,111.9"/>
						<path class="st34" d="M288.4,113.3v2.6c-0.1,0.4-0.1,0.9-0.1,1.3v-2.6C288.3,114.1,288.4,113.7,288.4,113.3"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_802_" class="st0" d="M312.9,110v2.6c-3-1.5-7.6-3.3-12.7-3.3c-6.7,0-10.8,3-10.8,7.8v-2.6
							c0-4.8,4.1-7.8,10.8-7.8C305.3,106.7,309.9,108.5,312.9,110z"/>
					</defs>
					<clipPath id="SVGID_803_">
						<use xlink:href="#SVGID_802_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st450">
						<path class="st34" d="M312.9,110v2.6c-3-1.5-7.6-3.3-12.7-3.3c-4.8,0-8.3,1.6-9.9,4.3V111c1.6-2.7,5.1-4.3,9.9-4.3
							C305.3,106.7,309.9,108.5,312.9,110"/>
						<path class="st34" d="M290.2,111v2.6c-0.2,0.4-0.4,0.8-0.5,1.2v-2.6C289.8,111.8,290,111.4,290.2,111"/>
						<path class="st34" d="M289.7,112.2v2.6c-0.1,0.4-0.2,0.8-0.3,1.2v-2.6C289.5,113,289.6,112.6,289.7,112.2"/>
						<path class="st34" d="M289.4,113.4v2.6c-0.1,0.4-0.1,0.8-0.1,1.2v-2.6C289.3,114.2,289.4,113.8,289.4,113.4"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_804_" class="st0" d="M312.9,120.2l0,2.6c-3.9,2-8.5,3.1-12.8,3.1c-7.3,0-11.8-3.4-11.8-8.8l0-2.6
							c0,5.4,4.5,8.8,11.8,8.8C304.4,123.3,309,122.2,312.9,120.2z"/>
					</defs>
					<clipPath id="SVGID_805_">
						<use xlink:href="#SVGID_804_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st451">
						<path class="st34" d="M312.9,120.2v2.6c-3.9,2-8.5,3.1-12.8,3.1c-7.3,0-11.8-3.4-11.8-8.8v-2.6c0,5.4,4.5,8.8,11.8,8.8
							C304.4,123.3,309,122.2,312.9,120.2"/>
					</g>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_806_" class="st0" d="M271.4,129.7c-2.2-3.8-3.5-8.2-3.5-13v-2.6c0,4.8,1.2,9.2,3.5,13c0,0,0,0.1,0,0.2
							c0,0.1,0,0.1,0,0.2c0,0.1,0,0.1,0,0.2c0,0.1,0,0.1,0,0.2s0,0.1,0,0.2c0,0.1,0,0.1,0,0.2c0,0.1,0,0.1,0,0.2c0,0,0,0.1,0,0.2
							c0,0,0,0.1,0,0.1c0,0,0,0.1,0,0.2c0,0,0,0.1,0,0.2c0,0,0,0.1,0,0.2s0,0.1,0,0.2c0,0,0,0.1,0,0.2c0,0,0,0.1,0,0.2
							C271.4,129.6,271.4,129.7,271.4,129.7z"/>
					</defs>
					<clipPath id="SVGID_807_">
						<use xlink:href="#SVGID_806_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st452">
						<path class="st34" d="M312.9,137.4v2.6c-3.6,1.8-9.7,3-15.3,3c-17.2,0-29.7-11.1-29.7-26.3v-2.6c0,15.3,12.5,26.3,29.7,26.3
							C303.2,140.4,309.3,139.2,312.9,137.4"/>
					</g>
				</g>
				<g>
					<polygon class="st34" points="313.9,138 313.9,140.6 313.6,140.8 313.6,138.1 					"/>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_808_" class="st0" d="M271.4,129.7c0,0,0-0.1,0-0.1c0,0,0-0.1,0-0.2c0,0,0-0.1,0-0.2c0,0,0-0.1,0-0.2
							s0-0.1,0-0.2c0,0,0-0.1,0-0.2c0,0,0-0.1,0-0.2c0,0,0-0.1,0-0.1c0,0,0-0.1,0-0.2c0-0.1,0-0.1,0-0.2c0-0.1,0-0.1,0-0.2
							c0-0.1,0-0.1,0-0.2s0-0.1,0-0.2c0-0.1,0-0.1,0-0.2c0-0.1,0-0.1,0-0.2c0,0,0-0.1,0-0.2c4.9,8.2,14.4,13.4,26.2,13.4
							c5.6,0,11.7-1.2,15.3-3v2.6c-3.6,1.8-9.7,3-15.3,3C285.8,143.1,276.2,137.9,271.4,129.7z"/>
					</defs>
					<clipPath id="SVGID_809_">
						<use xlink:href="#SVGID_808_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st453">
						<path class="st34" d="M312.9,137.4v2.6c-3.6,1.8-9.7,3-15.3,3c-17.2,0-29.7-11.1-29.7-26.3v-2.6c0,15.3,12.5,26.3,29.7,26.3
							C303.2,140.4,309.3,139.2,312.9,137.4"/>
					</g>
				</g>
				<g>
					<path class="st34" d="M297.8,87.8c5.5,0,11.4,1.1,14.8,2.8v18.1c-3.1-1.4-7.5-3-12.5-3c-7.2,0-11.8,3.5-11.8,8.8
						c0,5.4,4.5,8.8,11.8,8.8c4.3,0,8.9-1.1,12.8-3.1v17.2c-3.6,1.8-9.7,3-15.3,3c-17.2,0-29.7-11.1-29.7-26.3
						C267.9,98.9,280.4,87.8,297.8,87.8z"/>
				</g>
				<g>
					<path class="st34" d="M313.1,118.9l0.7-0.4V138l-0.3,0.1c-3.7,2-10.2,3.3-16.1,3.3c-17.8,0-30.7-11.5-30.7-27.3
						c0-15.8,13-27.3,30.9-27.3c5.8,0,12,1.2,15.6,3.1l0.3,0.1v20.3l-0.7-0.4c-3-1.5-7.6-3.3-12.7-3.3c-6.7,0-10.8,3-10.8,7.8
						c0,4.8,4.1,7.8,10.8,7.8C304.6,122.3,309.3,121.1,313.1,118.9z M312.9,137.4v-17.2c-3.9,2-8.5,3.1-12.8,3.1
						c-7.3,0-11.8-3.4-11.8-8.8c0-5.4,4.6-8.8,11.8-8.8c4.9,0,9.4,1.6,12.5,3V90.6c-3.4-1.7-9.4-2.8-14.8-2.8
						c-17.3,0-29.9,11.1-29.9,26.3c0,15.3,12.5,26.3,29.7,26.3C303.2,140.4,309.3,139.2,312.9,137.4"/>
				</g>
				<g class="st0">
					<defs>
						<path id="SVGID_810_" class="st0" d="M313.6,138.1v2.6c-3.7,2-10.2,3.3-16.1,3.3c-17.8,0-30.7-11.5-30.7-27.3v-2.6
							c0,15.8,12.9,27.3,30.7,27.3C303.5,141.4,309.9,140.1,313.6,138.1z"/>
					</defs>
					<clipPath id="SVGID_811_">
						<use xlink:href="#SVGID_810_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st454">
						<path class="st34" d="M313.6,138.1v2.6c-3.7,2-10.2,3.3-16.1,3.3c-17.8,0-30.7-11.5-30.7-27.3v-2.6
							c0,15.8,12.9,27.3,30.7,27.3C303.5,141.4,309.9,140.1,313.6,138.1"/>
					</g>
				</g>
			</g>
		</g>
	</g>
	<g>
	</g>
</g>
<g id="Star">
	<g>
		<g>
			<g class="st0">
				<g>
					<polygon class="st1" points="262.8,54.7 262.8,57.3 253.8,61.5 253.8,58.9 					"/>
				</g>
				<g>
					<polygon class="st1" points="253.8,58.9 253.8,61.5 245.8,56.2 245.8,53.6 					"/>
				</g>
				<g>
					<polygon class="st1" points="245.8,63.2 245.8,65.9 243.8,54.3 243.8,51.7 					"/>
				</g>
				<g>
					<polygon class="st2" points="246.8,63.7 246.8,66.3 245.8,56.2 245.8,53.6 					"/>
				</g>
				<g>
					<polygon class="st455" points="264.8,53 264.8,55.6 261.8,66.9 261.8,64.3 					"/>
				</g>
				<g>
					<polygon class="st456" points="262.8,54.7 262.8,57.3 260.8,67.2 260.8,64.5 					"/>
				</g>
				<g>
					<polygon class="st457" points="246.8,63.7 246.8,66.3 238.8,72.9 238.8,70.2 					"/>
				</g>
				<g>
					<polygon class="st1" points="267.8,72 267.8,74.6 260.8,67.2 260.8,64.5 					"/>
				</g>
				<g>
					<polygon class="st2" points="248.8,72.3 248.8,74.9 238.8,72.9 238.8,70.2 					"/>
				</g>
				<g>
					<polygon class="st2" points="267.8,72 267.8,74.6 257.8,75.5 257.8,72.8 					"/>
				</g>
				<g>
					<polygon class="st1" points="247.8,73.2 247.8,75.8 236.8,73.4 236.8,70.8 					"/>
				</g>
				<g>
					<polygon class="st1" points="269.8,72.8 269.8,75.5 257.8,76.4 257.8,73.8 					"/>
				</g>
				<g>
					<polygon class="st11" points="257.8,72.8 257.8,75.5 252.8,84.2 252.8,81.6 					"/>
				</g>
				<g>
					<polygon class="st2" points="252.8,81.6 252.8,84.2 248.8,74.9 248.8,72.3 					"/>
				</g>
				<g>
					<path class="st3" d="M261.9,64.3l7.8,8.6l-11.6,1l-5.8,10.1l-4.5-10.7l-11.4-2.4l8.8-7.6l-1.3-11.5l9.9,6l10.6-4.8L261.9,64.3z
						 M257.5,72.8l10.1-0.8l-6.8-7.5l2.3-9.8l-9.2,4.1l-8.6-5.2l1.1,10l-7.6,6.6l9.9,2.1l3.9,9.3L257.5,72.8"/>
				</g>
				<g>
					<polygon class="st4" points="245.3,53.6 253.9,58.9 263.1,54.7 260.8,64.5 267.6,72 257.5,72.8 252.5,81.6 248.6,72.3 
						238.7,70.2 246.4,63.7 					"/>
				</g>
				<g>
					<polygon class="st6" points="257.8,73.8 257.8,76.4 252.8,86.5 252.8,83.9 					"/>
				</g>
				<g>
					<polygon class="st1" points="252.8,83.9 252.8,86.5 247.8,75.8 247.8,73.2 					"/>
				</g>
			</g>
		</g>
		<defs>
			<filter id="Adobe_OpacityMaskFilter_21_" filterUnits="userSpaceOnUse" x="236.5" y="51.7" width="33.2" height="34.7">
				<feFlood  style="flood-color:white;flood-opacity:1" result="back"/>
				<feBlend  in="SourceGraphic" in2="back" mode="normal"/>
			</filter>
		</defs>
		<mask maskUnits="userSpaceOnUse" x="236.5" y="51.7" width="33.2" height="34.7" id="SVGID_812_">
			<g class="st458">
				
					<image style="overflow:visible;" width="38" height="39" xlink:href="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgEASABIAAD/7AARRHVja3kAAQAEAAAAHgAA/+4AIUFkb2JlAGTAAAAAAQMA
EAMCAwYAAAFxAAABfAAAAZ3/2wCEABALCwsMCxAMDBAXDw0PFxsUEBAUGx8XFxcXFx8eFxoaGhoX
Hh4jJSclIx4vLzMzLy9AQEBAQEBAQEBAQEBAQEABEQ8PERMRFRISFRQRFBEUGhQWFhQaJhoaHBoa
JjAjHh4eHiMwKy4nJycuKzU1MDA1NUBAP0BAQEBAQEBAQEBAQP/CABEIACcAJgMBIgACEQEDEQH/
xABfAAEBAQAAAAAAAAAAAAAAAAAAAQcBAQAAAAAAAAAAAAAAAAAAAAAQAQEAAAAAAAAAAAAAAAAA
ADBAEQEAAAAAAAAAAAAAAAAAAABAEgEAAAAAAAAAAAAAAAAAAAAw/9oADAMBAAIRAxEAAADPwFgA
BYAAAAH/2gAIAQIAAQUAH//aAAgBAwABBQAf/9oACAEBAAEFAKf/2gAIAQICBj8AH//aAAgBAwIG
PwAf/9oACAEBAQY/AE//2Q==" transform="matrix(1 0 0 1 233.75 49.5)">
				</image>
			</g>
		</mask>
		<g class="st459">
			<g class="st0">
				<g>
					<polygon class="st34" points="262.8,54.7 262.8,57.3 253.8,61.5 253.8,58.9 					"/>
				</g>
				<g>
					<polygon class="st34" points="253.8,58.9 253.8,61.5 245.8,56.2 245.8,53.6 					"/>
				</g>
				<g>
					<polygon class="st34" points="245.8,63.2 245.8,65.9 243.8,54.3 243.8,51.7 					"/>
				</g>
				<g>
					<polygon class="st34" points="246.8,63.7 246.8,66.3 245.8,56.2 245.8,53.6 					"/>
				</g>
				<g>
					<polygon class="st34" points="264.8,53 264.8,55.6 261.8,66.9 261.8,64.3 					"/>
				</g>
				<g>
					<polygon class="st34" points="262.8,54.7 262.8,57.3 260.8,67.2 260.8,64.5 					"/>
				</g>
				<g>
					<polygon class="st34" points="246.8,63.7 246.8,66.3 238.8,72.9 238.8,70.2 					"/>
				</g>
				<g>
					<polygon class="st34" points="267.8,72 267.8,74.6 260.8,67.2 260.8,64.5 					"/>
				</g>
				<g>
					<polygon class="st34" points="248.8,72.3 248.8,74.9 238.8,72.9 238.8,70.2 					"/>
				</g>
				<g>
					<polygon class="st34" points="267.8,72 267.8,74.6 257.8,75.5 257.8,72.8 					"/>
				</g>
				<g>
					<polygon class="st34" points="247.8,73.2 247.8,75.8 236.8,73.4 236.8,70.8 					"/>
				</g>
				<g>
					<polygon class="st34" points="269.8,72.8 269.8,75.5 257.8,76.4 257.8,73.8 					"/>
				</g>
				<g>
					<polygon class="st34" points="257.8,72.8 257.8,75.5 252.8,84.2 252.8,81.6 					"/>
				</g>
				<g>
					<polygon class="st34" points="252.8,81.6 252.8,84.2 248.8,74.9 248.8,72.3 					"/>
				</g>
				<g>
					<path class="st34" d="M261.9,64.3l7.8,8.6l-11.6,1l-5.8,10.1l-4.5-10.7l-11.4-2.4l8.8-7.6l-1.3-11.5l9.9,6l10.6-4.8L261.9,64.3
						z M257.5,72.8l10.1-0.8l-6.8-7.5l2.3-9.8l-9.2,4.1l-8.6-5.2l1.1,10l-7.6,6.6l9.9,2.1l3.9,9.3L257.5,72.8"/>
				</g>
				<g>
					<polygon class="st34" points="245.3,53.6 253.9,58.9 263.1,54.7 260.8,64.5 267.6,72 257.5,72.8 252.5,81.6 248.6,72.3 
						238.7,70.2 246.4,63.7 					"/>
				</g>
				<g>
					<polygon class="st34" points="257.8,73.8 257.8,76.4 252.8,86.5 252.8,83.9 					"/>
				</g>
				<g>
					<polygon class="st34" points="252.8,83.9 252.8,86.5 247.8,75.8 247.8,73.2 					"/>
				</g>
			</g>
		</g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
</g>
</svg>
            </SvgIcon>
);

export default CosmicIcon;

