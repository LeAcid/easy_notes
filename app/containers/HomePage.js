import React, { Component } from 'react';
import Home from '../components/Home';
import AppBar from '../components/AppBar';
import SideMenu from '../components/SideMenu';
import Note from '../components/Note';
import { EditorState, ContentState, convertFromRaw } from 'draft-js';
import Dexie from 'dexie';
import { rawEditorState } from './../utils/EditorInstructions';

export default class HomePage extends Component {

    constructor(){
        super();

        const content = convertFromRaw( rawEditorState );
        const openingScreen = EditorState.createWithContent( content );

        this.state = {
            msg: '',
            notebooks: [],
            pages: [],
            content: openingScreen,
            page: []
        };
        console.log(rawEditorState);

        this.database = this.database.bind(this);
        this.resetEditor = this.resetEditor.bind(this);
    }

    /**
     * Open Database Create Schema
     * Initialize NoteBook and Pages
     */
    componentDidMount(){

        this.database('SYNC_NOTEBOOKS');
        //this.database('ADD_NOTEBOOK', null, 'NEW NOTEBOOK');
        //this.database('ADD_PAGE', 1, 'Page ONE', 'Here is the notes for the page');
        this.database('GET_PAGES', 1);
    }

    /**
     * Build Out Database and Accept Command Arguments
     * As Strings
     */
    database( action = '', id = null, name = null, note = null){
        var self = this;
        var db = new Dexie('EasyNote');

        db.version(1).stores({
            notebooks: '++id, name',
            pages: '++id, notebook_id, title, note'
        });

        db.open().catch(function(error){
            console.log('Error: ' + error);
        });

        /**
         * Check Action and
         * Perform Desired DB Operation
         */
        switch( action ){
            case 'ADD_NOTEBOOK':
                db.transaction('rw', db.notebooks, db.pages, function*(){
                    var notebook_one = yield db.notebooks.add({
                        name: name
                    });
                }).catch(function(err){
                    console.log(err);
                });
                    self.database('SYNC_NOTEBOOKS'); //Sync pages after add
                break;
            case 'ADD_PAGE':
                db.transaction('rw', db.notebooks, db.pages, function*(){
                    var page = yield db.pages.add({
                        notebook_id: id,
                        title: name,
                        note: note
                    });
                }).catch(function(err){
                    console.log(err);
                });
                    self.database('GET_PAGES', id); //Sync pages after add
                break;
            case 'SYNC_NOTEBOOKS':
                db.transaction('rw', db.notebooks, db.pages, function*(){
                    var notebooks = yield db.notebooks.toArray();
                    self.setState({ notebooks: notebooks });
                }).catch(function(err){
                    console.log(err);
                });
            case 'SAVE':
                db.transaction('rw', db.notebooks, db.pages, function*(){
                    yield db.pages.update( id,{ title: name, note: note})
                        .then(function(update){
                            update ? console.log('success') : console.log('failed');
                        });
                }).catch(function(err){
                    console.log(err);
                });
                break;
            case 'GET_PAGES':
                db.transaction('rw', db.notebooks, db.pages, function*(){
                    var pages = yield db.pages.where('notebook_id').equals(id).toArray();
                    self.setState({ pages: pages });
                }).catch(function(err){
                    console.log(err);
                });
                break;
            case 'GET_PAGE':
                db.transaction('rw', db.notebooks, db.pages, function*(){
                    var page = yield db.pages.where('id').equals(id).toArray();
                    console.log(page[0].note);
                    self.setState({
                        page: page,
                        content: page[0].note
                    });

                }).catch(function(err){
                    console.log(err);
                });
                break;
            case 'DELETE_PAGE':
                db.transaction('rw', db.notebooks, db.pages, function*(){
                    var page = yield db.pages.delete(id);
                }).catch(function(err){
                    console.log(err);
                });
                    self.resetEditor();
                    self.database('GET_PAGES', name); //Sync pages after delete
                break;
            case 'DELETE_NOTEBOOK':
                db.transaction('rw', db.notebooks, db.pages, function*(){
                    let notebook_id =  yield db.notebooks.where('name').equals(name).first();
                    let notebook = db.notebooks.where('name').equals(name).delete().then(function(count){
                        console.log('notebooks deleted: ' + count);
                    });
                    let pages = db.pages.where('notebook_id').equals(notebook_id.id).delete().then(function(count){
                        console.log('pages deleted: ' + count);
                    });
                }).catch(function(err){
                    console.log(err);
                });
                    self.resetEditor();
                    self.database('GET_PAGES', name); //Sync pages after delete
                    self.database('SYNC_NOTEBOOKS'); //Sync Notebooks after delete
                break;
            default: console.log('Sorry No Valid Action Detected!');
                break;
        }
    }

    /**
     * Reset the editor after a file delete
     */
    resetEditor(){
        let content = rawEditorState;

        this.setState({
            content: content,
            title: 'Page Deleted Succesfully',
            page: new Array()
        });
    }

    /**
     * Layout and Render Entire UI
     */
    render() {
        return (
        <div>
            <SideMenu db={ this.database } notebooks={ this.state.notebooks } pages={ this.state.pages } />
            <Note resetEditor={ this.resetEditor }
                  db={ this.database }
                  page={ this.state.page }
                  content={ this.state.content }
                  title={ this.state.title }
            />
            <AppBar db={this.database} />
        </div>
        );
    }
}
